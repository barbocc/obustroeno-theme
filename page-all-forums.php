<?php get_header(); ?>
<div id="full_content">
	<div id="main_content" class="post_page single_posts single_all-forums">
		<div id="main">
			<div id="article">
				<?php /*<h1><?=the_title()?></h1> */?>
				<div class="questionsFront__title">Все вопросы — ответы</div>
				<div class="front_forum_container">
					<div class="front_forum_menu__tabs page_forum_menu__tabs">
						<ul>
							<li class="active">Последние вопросы</li><li>Последние ответы</li><li>Вопросы без ответов</li><li>Самые обсуждаемые</li><?php if(current_user_can('administrator') || current_user_can('expert')) { ?><li>Требуется ответ</li><?php } ?>
						</ul>
					</div>
				</div>
				<div class="questionsFront">
					<div class="questionsFront__tab questionsPage__tab visible">
						<ul class="questionsFront__list">
							<?php forum_last_new_reply_str(); //ПОСЛЕДНИЕ ВОПРОСЫ?>
						</ul>
					</div>
					<div class="questionsFront__tab questionsPage__tab">
						<ul class="questionsFront__list">
							<?php forum_last_reply_str(); //ПОСЛЕДНИЕ ОТВЕТЫ?>
						</ul>
					</div>
					<div class="questionsFront__tab questionsPage__tab">
						<ul class="questionsFront__list">
							<?=test_view_forum_str(); //ВОПРОСЫ БЕЗ ОТВЕТОВ?>
						</ul>
					</div>
					<div class="questionsFront__tab questionsPage__tab">
						<ul class="questionsFront__list">
							<?php forum_count_reply_str(); //САМЫЕ ОБСУЖДАЕМЫЕ?>
						</ul>
					</div>
					<div class="questionsFront__tab questionsPage__tab">
						<ul class="questionsFront__list">
							<?php forum_required_reply_str(); //Требуется ответ?>
						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<?php get_footer(); ?>