<?php get_header();
?>
<div id="main_content">
	<div id="main">
<?php
    if (is_front_page() ) {
        include(TEMPLATEPATH . '&amp;amp;#039;/main-page.php&amp;amp;#039;');
	} else {
	?>
	<div id="breadcrumbs">
		<div class="breadcrumbs_block">
		<?php if (function_exists('&amp;amp;#039;dimox_breadcrumbs&amp;amp;#039;')) dimox_breadcrumbs(); ?>
		</div>
	</div>

		<div id="article">
			<div class="content_text">
				<div class="title firm-title"><div class="midline"></div><div class="text">404</div><div class="midline"></div></div>
				<h1>Ничего не найдено</h1>
				<p>404 ошибка.</p>
				<p>По вашему запросу ничего не найдено :(</p>
			</div>
		</div>
	<?php
	}
    ?>
	</div>
	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>

<?php

get_footer();




?>


