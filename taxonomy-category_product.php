<?php get_header(); ?>

<div id="full_content">
<div id="main_content">
	<div id="main">
		<div id="breadcrumbs">
			<div class="breadcrumbs_block">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			</div>
		</div>
	
		<div id="article" class="question_page">
			<div class="content_text question_page">
				<div class="title firm-title"><div class="midline"></div><div class="text">Категория <?php 
				if (stripos($_SERVER['REQUEST_URI'], 'goods') != false) {
					print 'товаров';
				} else {
					print 'услуг';
				}
				?></div><div class="midline"></div></div>
				<?php echo do_shortcode( '[category_product_list]' ); ?>
				
			</div>
		</div>
	</div>

	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>
