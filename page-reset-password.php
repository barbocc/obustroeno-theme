<?php get_header(); 
?>
<div id="full_content">
<?php if (is_front_page() || is_page('firms') || is_page('foto')){
	print '<div id="top_fon"></div>';
}
?>
<div id="main_content">
	<div id="main">
<?php if (!isset($_GET['key']) || !isset($_GET['login']) || is_wp_error(check_password_reset_key($_GET['key'], $_GET['login']))) { // если параметры не передали или ф-я проверки вернула ошибку
	echo '<p>Ключ и (или) логин ни были переданы, либо не верны.</p>';
	//resetpass
} else { // если все ок показываем форму ?>
	<h1 class="resetpass_title">Переустановите пароль</h1>
	<form name="resetpassform" id="resetpassform" action="" method="post" class="userform">
		<input type="password" name="pass1" id="pass1" placeholder="Новый пароль">
		<input type="password" name="pass2" id="pass2" placeholder="Повторите новый пароль">
		
		<input type="hidden" name="key" value="<?php echo esc_attr($_GET['key']); ?>"><!-- переданные параметры сунем в скрытые поля -->
		<input type="hidden" name="login" value="<?php echo esc_attr($_GET['login']); ?>">
		<input type="submit" value="Изменить пароль">
	    <input type="hidden" name="redirect_to" value="<?php echo isset($_GET['redirect_to']) ? $_GET['redirect_to'] : '/'; ?>">
	    <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('reset_password'); ?>">
	    <input type="hidden" name="action" value="reset_password_front">
	    <div class="response"></div>
	</form>
<?php } ?>
	<div class="clear"></div>
</div>	
</div>
<?php get_footer(); ?>