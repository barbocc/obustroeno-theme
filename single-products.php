<?php get_header(); $tssuat_settings = unserialize(get_option('tssuat_settings')); ?>

<div id="full_content">
<div id="main_content">
	<div id="main">
		<div id="breadcrumbs">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
		</div>

		<div id="article" class="question_page product_page">
			<div class="content_text question_page">
				<div class="title product-title"><div class="text"><?=the_title()?></div></div>
				<?php echo do_shortcode( '[service_page]' ); ?>
			</div>
			<?php if($tssuat_settings['products']){
				comments_template();
			} ?>
		</div>
	</div>

	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>
