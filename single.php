<?php get_header(); $tssuat_settings = unserialize(get_option('tssuat_settings')); global $post; post_count_views( $post->ID, true );
$author_link = $post->post_author;
$author_data = get_userdata($author_link);
$author_link = $author_data->user_nicename;
$author_name = $author_data->display_name;
?>
<?php if(get_post_type() != 'forum' && get_post_type() != 'topic' && get_post_type() != 'reply'){ ?>
<meta name="mediator_author" content="<?= $author_name ?>"/>
<?php } ?>
<div id="full_content">
<div id="main_content" class="post_page single_posts">
	<div id="main">
<?php if(get_post_type() != 'forum' && get_post_type() != 'topic'){ ?><!-- исключаем форум -->
		<div id="breadcrumbs">
			<div class="breadcrumbs_block">
			<?php if (function_exists('dimox_breadcrumbs')) {dimox_breadcrumbs();} ?>
			</div>
		</div>
<?php } ?><!-- исключаем форум, закрывающий тег -->
<?php if(get_post_type() != 'forum' && get_post_type() != 'topic' && get_post_type() != 'reply'){ ?>
		<?php forum_last_news_sidebar(); ?>
		<div id="article">
<?php } else { ?>
		<div id="article" style="width:100%;">
<?php } ?>
			<div class="content_text">
<index>
<!-- google_ad_section_start -->
<?php if(get_post_type() != 'forum' && get_post_type() != 'topic'){ ?><!-- исключаем форум -->
				<?php $author_ava = $post->post_author; print get_avatar($author_ava, '32'); ?>
				<span class="author_name"><a href="/author/<?=$author_link?>"><?=$author_name?></a></span>
				<div class="post_name post_name_single">
					<span class="views"><?=post_count_views($post->ID)?></span>
					<span class="like"><?=post_likes_count_view($post->ID)?></span>
					<span class="comm"><?php
					print $post->comment_count;
					?></span>
				</div>
				<div class="clear"></div>


				<div class="dh"><h1><?=the_title()?></h1></div>
				<?php } ?><!-- исключаем форум, закрывающий тег -->



				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php if(get_post_type() != 'forum' && get_post_type() != 'topic' && get_post_type() != 'reply'){ ?>
<div class="js-mediator-article">
<?php } ?>
                <?php the_content(__('')); ?>
                
<?php if(get_post_type() != 'forum' && get_post_type() != 'topic' && get_post_type() != 'reply'){ ?>
</div> <!-- "js-mediator-article"  -->
<?php } ?>
                <?php if(get_post_type() != 'forum' && get_post_type() != 'topic' && get_post_type() != 'reply'){ ?><!-- исключаем форум -->
                <span class="post_dates"><?php echo get_the_date('j F Y', $post->ID); ?>г.</span>
				<?php
					print '<div class="tags">';
					//$posttags = get_the_tags($post->ID);
					$posttags = get_the_category();
					$posttags_x = array();
					if ($posttags) {
					  foreach($posttags as $tag) {
					  	array_push($posttags_x, '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>');
					  }
					}
					$posttags = implode(",<span style=\"padding:1px;\"></span>", $posttags_x);
					print $posttags;
					print '</div>';
				?>
                <div class="thanks">
                	<span>Если вы хотите выразить благодарность, добавить уточнение или возражение, что-то спросить у автора - добавьте комментарий или скажите <span>спасибо!</span></span>
                </div>
                <?php } ?><!-- исключаем форум, закрывающий тег -->



<!-- google_ad_section_end -->
</index>
                <?php endwhile; else: ?>
                    <p>Нет новостей!</p>
                <?php endif; ?>
                <?php if(get_post_type() != 'forum' && get_post_type() != 'topic' && get_post_type() != 'reply'){ ?>
				<?php wp_pagenavi(); ?>
				<?php } ?>
				
			</div>
		</div>
<!--noindex-->		
		<?php if(get_post_type() != 'forum' && get_post_type() != 'topic' && get_post_type() != 'reply'){ ?><!-- исключаем форум -->
		<?php print do_shortcode('[forum_last_news]'); ?>
		<?php bottom_post_block(); ?>
		<?php if(is_user_logged_in()){ ?>
			<?=post_favorites_view()?>
		<?php } ?>
<?php if(current_user_can('manage_options')){ ?>
<form name="subscribe" class="subscribe_form">
<div class="sub_body sub_body_left">
	<div class="title">Вас заинтересовала статья?</div>
	<ul>
		<li>
			<input type="radio" id="sub_answer_1" name="sub_radio">
			<label for="sub_answer_1">досуг и отдых</label>
		</li>
		<li>
			<input type="radio" id="sub_answer_2" name="sub_radio">
			<label for="sub_answer_2">летняя кухня</label>
		</li>
		<li>
			<input type="radio" id="sub_answer_3" name="sub_radio">
			<label for="sub_answer_3">кухни</label>
		</li>
		<li>
			<input type="radio" id="sub_answer_4" name="sub_radio">
			<label for="sub_answer_4">постройки и оборудование</label>
		</li>
		<li>
			<input type="radio" id="sub_answer_5" name="sub_radio">
			<label for="sub_answer_5">идеи</label>
		</li>
	</ul>
</div>
<div class="sub_body sub_body_right">
	<div class="title">Подпишись на обновления чтобы всегда быть в курсе:</div>
	<ul>
		<li>
			<input type="checkbox" id="sub_answer_6" name="sub_radio">
			<label for="sub_answer_6">Новые комментарии на этой странице</label>
		</li>
		<li>
			<input type="checkbox" id="sub_answer_7" name="sub_radio">
			<label for="sub_answer_7">Одну лучшую статью каждый день</label>
		</li>
		<li>
			<input type="checkbox" id="sub_answer_8" name="sub_radio">
			<label for="sub_answer_8">Еженедельный календарь садовода</label>
		</li>
	</ul>
	<input type="text" name="sub_email" placeholder="Ваш e-mail"></input>
	<input type="submit" name="sub_button" value="подписаться">
</div>


<div class="clear"></div>
</form>
<!--/noindex-->
<?php } ?>
<?php if (function_exists('vote_poll') && !in_pollarchive()): ?>
	<?php endif; ?>
<?=comments_template()?>
<?php// if ( function_exists('smform_view_func')) smform_view_func(); ?>
		<?php //if($tssuat_settings['articles']){ comments_template(); }?>
		
<!--noindex-->		
		<div class="title title_single">Последние ответы на форуме</div>
		<?php forum_last_reply(); ?>
		<div class="title title_single">Возможно вас заинтресуют</div>
				<?php
					$posts = query_posts(array('post' => $post, 'posts_per_page'=>4, 'post_status' => 'publish',  'orderby'=>'rand'));
					$post_img = catch_that_image();
					echo '<div class="popular_posts">';
					foreach($posts as $post) {
						echo '<div class="popular_post"><a href="'.get_permalink($post->ID).'">';
						echo '<div class="post_image" style="background: url('.catch_that_image_by_id($post->ID).') no-repeat;background-size: cover;"></div>';
						echo '<div class="post_link">'.$post->post_title.'</div></a>';
						echo '</div>';
					}
					echo '</div>';
					wp_reset_query();
				?>
				<?php if(current_user_can('manage_options')){ ?>
			<div class="all_blogs"><a href="/articles">Все блоги</a></div>
				<?php } ?>
			<?php } ?><!-- исключаем форум, закрывающий тег -->
	</div>
	<div class="clear"></div>
	<!--/noindex-->	



</div>
</div>
<?php if(get_post_type() != 'forum' && get_post_type() != 'topic' && get_post_type() != 'reply'){ ?>
<script id="js-mpf-mediator-init" data-counter="2819874" data-adaptive="true">!function(e){function t(t,n){if(!(n in e)){for(var r,a=e.document,i=a.scripts,o=i.length;o--;)if(-1!==i[o].src.indexOf(t)){r=i[o];break}if(!r){r=a.createElement("script"),r.type="text/javascript",r.async=!0,r.defer=!0,r.src=t,r.charset="UTF-8";;var d=function(){var e=a.getElementsByTagName("script")[0];e.parentNode.insertBefore(r,e)};"[object Opera]"==e.opera?a.addEventListener?a.addEventListener("DOMContentLoaded",d,!1):e.attachEvent("onload",d):d()}}}t("//top-fwz1.mail.ru/js/code.js","_tmr"),t("//mediator.imgsmail.ru/2/mpf-mediator.min.js","_mediator")}(window);</script>
<?php } ?>
<?php get_footer(); ?>