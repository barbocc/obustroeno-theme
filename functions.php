<?php

/*if(current_user_can('manage_options')) {
	ini_set('display_errors',1);
	error_reporting(E_ALL ^E_NOTICE);
	error_reporting(E_ALL);
}*/

//Email оповещения пользователя, о новом ответе в теме, в которой пользователь принимал участие
function bbp_relpy_take_part($reply_id, $topic_id) {
	global $wpdb;

	$topic = get_post($topic_id);
	$title = $topic->post_title;
	$user_author_topic = $topic->post_author;

	$cur_user = get_userdata(get_post($reply_id)->post_author)->display_name;

	//Все, кто отвечал в теме
	$users = $wpdb->get_col("SELECT `post_author` FROM $wpdb->posts WHERE `post_type` = 'reply' AND `post_parent` = $topic_id AND `post_status` = ('publish' OR 'closed') GROUP BY `post_author`");

	foreach ($users as $user) {

		if($user_author_topic == $user)
			continue;

		$user_data = get_userdata($user);
		$user_name = $user_data->display_name;
		$user_email = $user_data->user_email;

		if($cur_user == $user_name) continue;

		//$action = $user_name.', темы с вашим участием';
		//$action = 'В теме с вашим участием новый ответ';

		$feedUsers = $wpdb->prefix.feedUsers;
		$var = $wpdb->get_var("SELECT `permission` FROM $feedUsers WHERE `user_id` = $user;");
		$perm = unserialize($var);

		$post = get_post($reply_id);
		$reply_author = get_userdata($post->post_author)->display_name;
		$reply_content = $post->post_content;
		$reply_content = apply_filters('the_content', $reply_content);
		$reply_content = str_replace(']]>', ']]>', $reply_content);
		$reply_content = strip_tags($reply_content);
		if (iconv_strlen($reply_content,'UTF-8') > 80) {
			$reply_content = mb_substr($reply_content, 0, 80);
			$reply_content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $reply_content); //убираем последнее слово
		}
		$reply_content .= '...<a href="'.wp_get_shortlink($topic_id).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=relpy_participated&utm_content='.$user_email.'">(Читать далее)</a>';

		$reply_content_full = '<div style="background:#f0d500;width:100%;padding:5px;">';
		$reply_content_full .= $reply_content;
		$reply_content_full .= '</div>';

		$post_topic = get_post($topic_id);$action = 'В теме "'.$post_topic->post_title.'" с вашим участием новый ответ';
		$post_topic = $post_topic->post_title;
		$post_topic = strip_tags($post_topic);
		if (iconv_strlen($post_topic,'UTF-8') > 80) {
			$post_topic = mb_substr($post_topic, 0, 80);
			$post_topic = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $post_topic).'...'; //убираем последнее слово
		}

		$post_topic_full = '<div style="background:#ededed;width:100%;padding:5px;">';
		$post_topic_full .= $post_topic;
		$post_topic_full .= '</div>';

		$txt = '<p>Здравствуйте '. $user_name .', в теме с вашим участием <span><a class="ref" href="'.wp_get_shortlink($topic_id).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=relpy_participated&utm_content='.$user_email.'">&laquo;'.$title.'&raquo;</a></span> новый ответ пользователя <span style="font-weight:bold;">'.$reply_author.'</span></p>';

		$txt .= $reply_content_full;

		$txt .= '<br>Для просмотра полного ответа перейдите, пожалуйста, по ссылке: <a href="'.wp_get_shortlink($topic_id).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=relpy_participated&utm_content='.$user_email.'">'.home_url().'/topic/'.$topic_id.'</a>';

		$txt .= '<br><br>Ранее вы писали в этой теме:<br><br>';

		$txt .= $post_topic_full;

		$txt .= '</div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
        <a href="'.wp_get_shortlink($topic_id).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=relpy_participated&utm_content='.$user_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
            Перейти к теме
        </a><br>
        <span style="text-align:center;display:block;">
        С уважением, сообщество профессионалов <a href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=relpy_participated&utm_content='.$user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
    </footer>';

		if($perm['MyReplySub'] == 'on') {
			if(function_exists('ob_send_mail')){
				ob_send_mail($user_email, $txt, $action);
			} else {
				wp_mail($user_email, $action, $txt);
			}
		}
	}
}
add_action('bbp_new_reply', 'bbp_relpy_take_part', 10, 2);

//Email оповещения пользователя, о новом ответе в ваших темах
function the_replies_in_your_topics($reply_id, $topic_id) {
	global $wpdb;

	$topic = get_post($topic_id);
	$title = $topic->post_title; //заголовок вопроса
	$user_author_topic = $topic->post_author; //автор вопроса

	$cur_user = get_userdata(get_post($reply_id)->post_author)->display_name;

	if($user_author_topic == get_post($reply_id)->post_author)
		continue;

	$user_data = get_userdata($user_author_topic);
	$user_name = $user_data->display_name;
	$user_email = $user_data->user_email;

	$feedUsers = $wpdb->prefix.feedUsers;
	$var = $wpdb->get_var("SELECT `permission` FROM $feedUsers WHERE `user_id` = $user_author_topic;");
	$perm = unserialize($var);

	$post = get_post($reply_id);
	$reply_author = get_userdata($post->post_author)->display_name;
	$reply_content = $post->post_content;
	$reply_content = apply_filters('the_content', $reply_content);
	$reply_content = str_replace(']]>', ']]>', $reply_content);
	$reply_content = strip_tags($reply_content);
	if (iconv_strlen($reply_content,'UTF-8') > 80) {
		$reply_content = mb_substr($reply_content, 0, 80);
		$reply_content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $reply_content); //убираем последнее слово
	}
	$reply_content .= '...<a href="'.wp_get_shortlink($topic_id).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=the_replies_in_your_topics&utm_content='.$user_email.'">(Читать далее)</a>';

	$reply_content_full = '<div style="background:#f0d500;width:100%;padding:5px;">';
	$reply_content_full .= $reply_content;
	$reply_content_full .= '</div>';

	$post_topic = get_post($topic_id);
	$post_topic = $post_topic->post_title;
	$post_topic = strip_tags($post_topic);
	if (iconv_strlen($post_topic,'UTF-8') > 80) {
		$post_topic = mb_substr($post_topic, 0, 80);
		$post_topic = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $post_topic).'...'; //убираем последнее слово
	}
	$action = 'В вашей теме "'.$post_topic.'" новый ответ';
	$post_topic_full = '<div style="background:#ededed;width:100%;padding:5px;">';
	$post_topic_full .= $post_topic;
	$post_topic_full .= '</div>';

	$txt = '<p>Здравствуйте '. $user_name .', в теме с вашим участием <span><a class="ref" href="'.wp_get_shortlink($topic_id).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=the_replies_in_your_topics&utm_content='.$user_email.'">&laquo;'.$title.'&raquo;</a></span> новый ответ пользователя <span style="font-weight:bold;">'.$reply_author.'</span></p>';

	$txt .= $reply_content_full;

	$txt .= '<br>Для просмотра полного ответа перейдите, пожалуйста, по ссылке: <a href="'.wp_get_shortlink($topic_id).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=the_replies_in_your_topics&utm_content='.$user_email.'">'.home_url().'/topic/'.$topic_id.'</a>';

	$txt .= '<br><br>Ранее вы писали в этой теме:<br><br>';

	$txt .= $post_topic_full;

	$txt .= '</div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
    <a href="'.wp_get_shortlink($topic_id).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=the_replies_in_your_topics&utm_content='.$user_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
        Перейти к теме
    </a><br>
    <span style="text-align:center;display:block;">
    С уважением, сообщество профессионалов <a href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=the_replies_in_your_topics&utm_content='.$user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
</footer>';

	if($perm['heRepliesInYourTopicsSub'] == 'on') {
		if(function_exists('ob_send_mail')){
			ob_send_mail($user_email, $txt, $action);
		} else {
			wp_mail($user_email, $action, $txt);
		}
	}
}
add_action('bbp_new_reply', 'the_replies_in_your_topics', 10, 2);

//Email оповещения пользователя, что к его записи добавлен комментарий
function author_new_comment_notify( $comment_ID ){
	global $wpdb;

	$comment = get_comment( $comment_ID );
	$post = get_post( $comment->comment_post_ID );
	$user = get_userdata( $post->post_author );
	$user_com = $comment->comment_author;

	$who = $comment->comment_author;

	if( empty( $user->user_email ) )
		return;

	if($user->display_name == $who)
		return;

	//$action = $user->display_name.', комментарий';
	$action = 'К вашей статье "'.$post->post_title.'" добавил новый комментарий '.$user_com.'';

	$feedUsers = $wpdb->prefix.feedUsers;
	$var = $wpdb->get_var("SELECT `permission` FROM $feedUsers WHERE `user_id` = $post->post_author;");
	$perm = unserialize($var);

	$comment_content = $comment->comment_content;
	$comment_content = apply_filters('the_content', $comment_content);
	$comment_content = str_replace(']]>', ']]>', $comment_content);
	$comment_content = strip_tags($comment_content);
	if (iconv_strlen($comment_content,'UTF-8') > 80) {
		$comment_content = mb_substr($comment_content, 0, 80);
		$comment_content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $comment_content).'...'; //убираем последнее слово
	}
	$comment_content = trim($comment_content).'...';
	$comment_content .= ' <a href="'.wp_get_shortlink($comment->comment_post_ID).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $comment_ID).'&utm_campaign=new_comment_blog&utm_content='.$user->user_email.'">(Читать далее)</a>';

	$comment_content_full = '<div style="background:#f0d500;width:100%;padding:5px;">';
	$comment_content_full .= $comment_content;
	$comment_content_full .= '</div>';

	$txt = '<p>Здравствуйте, '. $user->display_name .', к вашей статье <span><a class="ref" href="'.wp_get_shortlink($comment->comment_post_ID).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $comment_ID).'&utm_campaign=new_comment_blog&utm_content='.$user->user_email.'">&laquo;'.$post->post_title.'&raquo;</a></span> новый комментарий пользователя <span style="font-weight:bold;">'.$user_com.'</span></p>';

	$txt .= $comment_content_full;

	$txt .= '<br>Для просмотра полного текста комментария перейдите, пожалуйста, по ссылке: <a href="'.wp_get_shortlink($comment->comment_post_ID).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $comment_ID).'&utm_campaign=new_comment_blog&utm_content='.$user->user_email.'">'.get_permalink($comment->comment_post_ID).'</a>';

	$txt .= '</div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
        <a href="'.wp_get_shortlink($comment->comment_post_ID).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $comment_ID).'&utm_campaign=new_comment_blog&utm_content='.$user->user_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
            Перейти к комментарию
        </a><br>
        <span style="text-align:center;display:block;">
        С уважением, сообщество профессионалов <a href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $comment_ID).'&utm_campaign=new_comment_blog&utm_content='.$user->user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
    </footer>';

	if($perm['MyBlogComSub'] == 'on') {
		if(function_exists('ob_send_mail')){
			ob_send_mail($user->user_email, $txt, $action);
		} else {
			wp_mail($user->user_email, $action, $txt);
		}
	}
}
add_action( 'comment_post', 'author_new_comment_notify', 10 );

//Email оповещения пользователя, что к записи добавлен комментарий в комментариях с его участием
function comments_to_blogs_from_my_participation( $comment_ID ){
	global $wpdb;

	$comment = get_comment( $comment_ID );
	$post = get_post( $comment->comment_post_ID );
	$user = get_userdata( $post->post_author );
	$user_com = $comment->comment_author;
	$who = $comment->comment_author;

	//Все, кто оставлял коментарии
	$users = $wpdb->get_col("SELECT `user_id` FROM $wpdb->comments WHERE `comment_post_ID` = $comment->comment_post_ID AND `comment_approved` = '1' GROUP BY `comment_author`");

	if($users) {
		foreach ($users as $user_to) {
			$data_to = get_userdata($user_to);

			if($data_to->display_name == $who)
				continue;

			$pttl = $post->post_title;
			if(iconv_strlen($pttl,'UTF-8') > 40) {
				$pttl = mb_substr($pttl, 0, 40);
				$pttl = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $pttl).'...'; //убираем последнее слово
			}
			$action = 'Добавлен новый комментарий к статье, которую вы комментировали "'.$pttl.'"';
			//$action = 'К статье, которую вы комментировали, '.$user_com.' добавил новый комментарий';
			$feedUsers = $wpdb->prefix.feedUsers;
			$var = $wpdb->get_var("SELECT `permission` FROM $feedUsers WHERE `user_id` = $user_to;");
			$perm = unserialize($var);

			$comment_content = $comment->comment_content;
			$comment_content = apply_filters('the_content', $comment_content);
			$comment_content = str_replace(']]>', ']]>', $comment_content);
			$comment_content = strip_tags($comment_content);
			if (iconv_strlen($comment_content,'UTF-8') > 80) {
				$comment_content = mb_substr($comment_content, 0, 80);
				$comment_content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $comment_content).'...'; //убираем последнее слово
			}
			$comment_content = trim($comment_content).'...';
			$comment_content .= ' <a href="'.wp_get_shortlink($comment->comment_post_ID).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $comment_ID).'&utm_campaign=comments_to_blogs_from_my_participation&utm_content='.$data_to->user_email.'">(Читать далее)</a>';

			$comment_content_full = '<div style="background:#f0d500;width:100%;padding:5px;">';
			$comment_content_full .= $comment_content;
			$comment_content_full .= '</div>';

			$txt = '<p>Здравствуйте, '. $data_to->display_name .', к статье, к которой вы оставляли комментарий <span><a class="ref" href="'.wp_get_shortlink($comment->comment_post_ID).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $comment_ID).'&utm_campaign=comments_to_blogs_from_my_participation&utm_content='.$data_to->user_email.'">&laquo;'.$post->post_title.'&raquo;</a></span> новый комментарий пользователя <span style="font-weight:bold;">'.$user_com.'</span></p>';

			$txt .= $comment_content_full;

			$txt .= '<br>Для просмотра полного текста комментария перейдите, пожалуйста, по ссылке: <a href="'.wp_get_shortlink($comment->comment_post_ID).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $comment_ID).'&utm_campaign=comments_to_blogs_from_my_participation&utm_content='.$data_to->user_email.'">'.get_permalink($comment->comment_post_ID).'</a>';

			$txt .= '</div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
		        <a href="'.wp_get_shortlink($comment->comment_post_ID).'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $comment_ID).'&utm_campaign=comments_to_blogs_from_my_participation&utm_content='.$data_to->user_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
		            Перейти к комментарию
		        </a><br>
		        <span style="text-align:center;display:block;">
		        С уважением, сообщество профессионалов <a href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $comment_ID).'&utm_campaign=comments_to_blogs_from_my_participation&utm_content='.$data_to->user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
		    </footer>';

			if($perm['MyCommentMyActionSub'] == 'on') {
				if(function_exists('ob_send_mail')){
					ob_send_mail($data_to->user_email, $txt, $action);
				} else {
					wp_mail($data_to->user_email, $action, $txt);
				}
			}
		}
	}
}
add_action( 'comment_post', 'comments_to_blogs_from_my_participation', 10 );

function ob_send_mail($email, $txt, $action){
    $headers[] = 'From: obustroeno.com <no-reply@obustroeno.com>';
    $headers[] = 'Content-type: text/html';
    $header = '<header style="height: 100px; border-bottom: solid 2px #83bc37; padding-left: 25px; padding-right: 25px;">
        <img src="http://obustroeno.com/wp-content/themes/1brus_mag/img/logo.png" alt="Логотип" style="width: 192px; margin-top: 20px;"/>
        <span style="    text-transform: uppercase;font: 14px Calibri;font-weight: 500;vertical-align: top;display: inline-block;margin-top: 35px;padding-left: 10px;">Сообщество мастеров</span>
        <img src="http://obustroeno.com/wp-content/themes/1brus_mag/img/letter.png" alt="Логотип" style="margin-top: 25px; float: right;"/>
    	</header>
    	<div style="padding-bottom: 40px;padding-left: 25px;padding-right: 25px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;">';

    return wp_mail($email, $action, $header . $txt . $footer, $headers);
}

add_filter( 'wp_mail_content_type', 'set_html_content_type' );

function feedViewDay() {
	if($_POST['countDay']) {
		update_option('postTime', $_POST['countDay']);
	}
}
add_action('init', 'feedViewDay');

//view

//Cookie

function bbp_get_same_topics($topic_id){
	global $wpdb;
	$topic = get_post($topic_id);

	$posts = $wpdb->prefix.posts;
	//var_dump($posts);

	//Запрашиваем данные о добавлении в избранное по id поста
	$sql = 
	"SELECT * FROM ".$posts." WHERE `ID` <> '".$topic_id."' 
	AND `post_status` LIKE 'publish' 
	AND `post_parent` = '". $topic->post_parent ."'
	AND `post_type`='topic' 
	AND `post_title` NOT LIKE '...' 
	LIMIT 3";
	//var_dump($sql);
	$topics = $wpdb->get_results($sql);
	//var_dump($topics);
	return $topics;
}

function register_cookie() {
	wp_register_script('cookie', 'http://yandex.st/jquery/cookie/1.0/jquery.cookie.min.js', false, null, false);
	wp_enqueue_script('cookie');
}
add_action('wp_enqueue_scripts', 'register_cookie');

//Выкидываем из индекса страницы авторов, не являющихся експертами
add_filter('get_header', 'noindexAuthor');

//Роли текущего залогиненого пользователя
function get_current_user_role() {
	global $wp_roles;
	$current_user = wp_get_current_user();
	$roles = $current_user->roles;
	$role = array_shift($roles);
	return $wp_roles->role_names[$role];
}

//Ссылка в админ-баре на посты, которые должны пройти модерацию
function my_admin_bar_render() {
	global $wp_admin_bar;
	$count = wp_count_posts()->pending;
	$wp_admin_bar->add_menu( array(
		'id' => 'new_records',
		'title' => 'На утверждении ('.$count.')', //заголовок ссылки
		'href' => admin_url( 'edit.php?post_status=pending&post_type=post'), //имя файла
		'meta'  => array(
            'title' => 'Посты, из фронтенда, которые должны пройти модерацию.'
        )
	));
}
add_action( 'wp_before_admin_bar_render', 'my_admin_bar_render' );


function custom_bbp_get_reply_author_link( $args = '' ) {
		// Parse arguments against default values

		$r = bbp_parse_args( $args, array(
			'post_id'       => 0,
			'link_title'    => '',
			'type'          => 'both',
			'size'          => 142,
			'sep'           => '&nbsp;',
			'show_role'     => true,
			'show_messages' => true,
			'show_date'     => true,
			'show_place'    => true,
			'show_achives'  => true
		), 'get_reply_author_link' );

		// Used as reply_id
		if ( is_numeric( $args ) ) {
			$reply_id = bbp_get_reply_id( $args );
		} else {
			$reply_id = bbp_get_reply_id( $r['post_id'] );
		}

		// Reply ID is good
		if ( !empty( $reply_id ) ) {

			// Get some useful reply information
			$author_url = bbp_get_reply_author_url( $reply_id );
			$anonymous  = bbp_is_reply_anonymous( $reply_id );

			// Tweak link title if empty
			if ( empty( $r['link_title'] ) ) {
				$link_title = sprintf( empty( $anonymous ) ? __( 'View %s\'s profile', 'bbpress' ) : __( 'Visit %s\'s website', 'bbpress' ), bbp_get_reply_author_display_name( $reply_id ) );

			// Use what was passed if not
			} else {
				$link_title = $r['link_title'];
			}

			// Setup title and author_links array
			$link_title   = !empty( $link_title ) ? ' title="' . esc_attr( $link_title ) . '"' : '';
			$author_links = array();

			// Get display name
			if ( 'name' === $r['type']   || 'both' === $r['type'] ) {
				$author_links['name'] = bbp_get_reply_author_display_name( $reply_id );
			}

			if ( true === $r['show_role'] ) {
				$author_links['role'] = bbp_get_reply_author_role( array( 'reply_id' => $reply_id ) );
			}
			// Get avatar
			if ( 'avatar' === $r['type'] || 'both' === $r['type'] ) {
				$author_links['avatar'] = bbp_get_reply_author_avatar( $reply_id, $r['size'] );
			}
			if (!$anonymous){
				$author_id = bbp_get_reply_author_id( $reply_id );

				//Считаем сообщения на форуме
				$author_count_query = get_posts(
					array( 'post_type'           => bbp_get_topic_post_type(), 
						   'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ), 
						   'posts_per_page'      => -1, 
						   'author'              => $author_id )
					);
	            $post_author_count = count($author_count_query);
	            $reply_count_query = get_posts(
	            	array('post_type'=> bbp_get_reply_post_type(),
						  'posts_per_page' => -1,
						  'post_status'=> array( bbp_get_public_status_id(),
						   bbp_get_closed_status_id() ),
						  'author' => $author_id ));
				$post_author_reply_count = count($reply_count_query);
				$messages_count = (int)$post_author_reply_count + (int)$post_author_count;
				$author_links['messages'] = '<div  class="forum_user_info"><span>Сообщений:</span> ' . $messages_count.'</div>';

				//Дата регистрации
				
				$date =  date("d.m.y", strtotime(get_userdata($author_id)->user_registered));

				$author_links['date_reg'] = '<div class="forum_user_info"><span>Регистрация:</span> ' . $date.'</div>';

				//Город пользователя
				$city = get_usermeta($author_id, 'sity');
				if ($city !== null && $city !== '') {
					$author_links['city'] = '<div class="forum_user_info"><span>Город:</span> ' . $city.'</div>';
					}


				$award_authorship = get_award_of_user('authorship', $author_id)[0];
				if ($award_authorship){
					$author_links['award_authorship'] = '<div class="honors_one forum_award"><a href="/spisok-nagrad">
							<img src="/wp-content/themes/1brus_mag/img/honors/'.$award_authorship['images_honor'].'" data-pagespeed-url-hash="1940506104" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></a>
								<span>'.$award_authorship['degree'].'</span>
							</div>';
							
				}

				$award_knowledge = get_award_of_user('knowledge', $author_id)[0];
				if ($award_knowledge){
					$author_links['award_knowledge'] = '<div class="honors_one  forum_award"><a href="/spisok-nagrad">
							<img src="/wp-content/themes/1brus_mag/img/honors/'.$award_knowledge['images_honor'].'" data-pagespeed-url-hash="1940506104" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></a>
								<span>'.$award_knowledge['degree'].'</span>
							</div>';
							
				}

				$award_comment = get_award_of_user('comment', $author_id)[0];
				if ($award_comment){
					$author_links['award_comment'] = '<div class="honors_one  forum_award"><a href="/spisok-nagrad">
							<img style="width: 30px; float: left;" src="/wp-content/themes/1brus_mag/img/honors/'.$award_comment['images_honor'].'" data-pagespeed-url-hash="1940506104" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></a>
								<span>'.$award_comment['degree'].'</span>
							</div>';
							
				}
				
				$count_list_value = count(get_award_of_user('all', $author_id));
				if($count_list_value > 0) {			
					$author_links['all_awards'] = '<div style="clear: both;"></div><form id="honors_form" class="all_awards_forum" method="get" target="_blank" action="/honor">
						<input type="submit" class="all_honors" style="padding-left: 0px; padding-top: 0px;" value="Все награды">
						<input type="hidden" name="page_namber" value="'.$author_id.'">
						</form>';
				}
			}

			// Link class
			$link_class = ' class="bbp-author-' . esc_attr( $r['type'] ) . '"';

			// Add links if not anonymous and existing user
			if ( empty( $anonymous ) && bbp_user_has_profile( bbp_get_reply_author_id( $reply_id ) ) ) {

				// Assemble the links
				foreach ( $author_links as $link => $link_text ) {
					$link_class = ' class="bbp-author-' . $link . '"';
					if ($link==='name'|| $link==='avatar'){
						$author_link[] = sprintf( '<a href="%1$s"%2$s%3$s>%4$s</a>', esc_url( $author_url ), $link_title, $link_class, $link_text );
					}
					else{
						$author_link[] = $link_text;
					}


				}

				$author_link = implode( $r['sep'], $author_link );

			// No links if anonymous
			} else {
				$author_link = implode( $r['sep'], $author_links );
			}

		// No replies so link is empty
		} else {
			$author_link = '';
		}



		return apply_filters( 'bbp_get_reply_author_link', $author_link, $r );
	}


add_filter( 'auth_cookie_expiration', 'keep_me_logged_in_for_1_year' );
function keep_me_logged_in_for_1_year( $expirein ) {
    return 31556926; // 1 year in seconds
}

//editor styling
add_filter('the_editor_content', "custom_tinymce_style");
function custom_tinymce_style($content) {
    
    // This is for back-end tinymce customization
    add_editor_style(get_stylesheet_directory_uri() . '/css/custom-editor-style.css');
    
    // This is for front-end tinymce customization
    if ( ! is_admin() ) {
        global $editor_styles;
        $editor_styles = (array) $editor_styles;
        $stylesheet    = (array) $stylesheet;
        
        $stylesheet[] = get_stylesheet_directory_uri() . '/css/custom-editor-style.css';
        
        $editor_styles = array_merge( $editor_styles, $stylesheet );
    }
    return $content;
}

//Функция выводит хлебные крошки от bbpress с микроразметкой
function bbp_print_rfd_breadcrumb(){
    	$args = array(
    		'before'          => '<div id="breadcrumbs" class="bbp-breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#"><p>',
    		'after'           => '</p></div>',
    		'sep'             => '<img src="/wp-content/themes/1brus_mag/img/delimiter.png">'
    	    );
    	    
    	$breadcrumb = bbp_get_breadcrumb($args);
    	$needle = '<a href="';
    	$replace = '<span typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="';
    	$breadcrumb = str_replace($needle, $replace, $breadcrumb);
    	
    	$needle = '</a>';
    	$replace = '</a></span>';
    	$breadcrumb = str_replace($needle, $replace, $breadcrumb);
    	$icons = '<div style="float: right !important; padding-bottom: 10px" id="uLogin00228661" class="ulogin_panel" data-ulogin="display=small;providers=vkontakte,odnoklassniki,facebook;fields=first_name,last_name,email,photo,photo_big;optional=phone;redirect_uri=http%3A%2F%2Fobustroeno.com%2F%3Fulogin%3Dtoken%26backurl%3Dhttp%253A%252F%252Fobustroeno.com%252Fforums%252F;force_fields=nickname" data-ulogin-inited="1473900097865"><div class="ulogin-buttons-container" style="margin: 0px; padding: 0px; outline: none; border: none; border-radius: 0px; cursor: default; float: none; position: relative; display: inline-block; width: 25px; height: auto; left: 0px; top: 0px; box-sizing: content-box; max-width: 100%; vertical-align: top; line-height: 0;"><div class="ulogin-button-vkontakte vk-forum" data-uloginbutton="vkontakte" role="button" title="VK" style="margin: 0px 5px 5px 0px; padding: 0px; outline: none; border: none; border-radius: 0px; cursor: pointer; float: left; position: relative; display: inherit; width: 16px; height: 16px; left: 0px; top: 0px; box-sizing: content-box; background: url(&quot;https://ulogin.ru/version/2.0/img/providers-16-classic.png?version=img.2.0.0&quot;) 0px -18px / 16px no-repeat;"></div><div class="ulogin-button-odnoklassniki ok-forum" data-uloginbutton="odnoklassniki" role="button" title="Odnoklassniki" style="margin: 0px 5px 5px 0px; padding: 0px; outline: none; border: none; border-radius: 0px; cursor: pointer; float: left; position: relative; display: inherit; width: 16px; height: 16px; left: 0px; top: 0px; box-sizing: content-box; background: url(&quot;https://ulogin.ru/version/2.0/img/providers-16-classic.png?version=img.2.0.0&quot;) 0px -35px / 16px no-repeat;"></div><div class="ulogin-button-facebook facebook-forum" data-uloginbutton="facebook" role="button" title="Facebook" style="margin: 0px 5px 5px 0px; padding: 0px; outline: none; border: none; border-radius: 0px; cursor: pointer; float: left; position: relative; display: inherit; width: 16px; height: 16px; left: 0px; top: 0px; box-sizing: content-box; background: url(&quot;https://ulogin.ru/version/2.0/img/providers-16-classic.png?version=img.2.0.0&quot;) 0px -69px / 16px no-repeat;"></div></div></div><div style="float: right !important;margin-right: 30px; padding-top: 5px; font-size: 16px;">войти через аккаунт соц сети</div><div style="clear: both;"></div>';
/*
global $wp;

if($wp->request==='forums'){
$links = '
<div class="front_forum_container">
	<div class="front_forum_menu__tabs page_forum_menu__tabs">
						<ul>
							<li class=""><a href="#">Последние вопросы</a></li>
							<li class=""><a href="#">Последние ответы</a></li>
							<li class=""><a href="#">Вопросы без ответов</a></li>
							<li class=""><a href="#">Самые обсуждаемые</a></li>
							<li class="active"><a href="#">Требуется ответ</a></li>
						</ul>
	</div>
</div>
';
}
else{*/
	$links = '';
//}
    	if (!is_user_logged_in()) echo $breadcrumb . $links . '<h1 style="font: 24px Verdana; text-transform: capitalize; font-weght: normal;">Форум</h1>' . $icons;
    	else echo $breadcrumb . $links .  '<h1 style="font: 24px Verdana; text-transform: capitalize; font-weght: normal; padding-bottom: 10px !important;">Форум</h1>';
}

function comment_theme($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	$comment->comment_date = rdate('d M Y', strtotime($comment->comment_date));
	if ($author->avatar == null) $avatar = "/wp-content/themes/1brus_mag/img/comment_user.png";

	$comment_author_id = $comment->user_id;//id автора комментария
	$comment_author = get_userdata($comment_author_id)->user_nicename;

	$ava = get_avatar($comment_author_id, 62);

	$comment_author_sity = get_usermeta($comment_author_id, 'sity');
	?>
	<div class="comment" id="comment-<?=$comment->comment_ID?>">
			<div class="comment_body">
				<a href="/author/<?=$comment_author?>"><div class="comment_photo"><?=$ava?></div></a>
				<div class="comment_body__info">
					<div class="comment_author"><a href="/author/<?=$comment_author?>"><?php comment_author(); ?></a></div>
					<div class="comment_location"><?=$comment_author_sity?></div>
					<div class="comment_date"><?=$comment->comment_date?></div>
				</div>
				<div class="comment_rating"></div>
				<div class="comment_text">
				<?php
					ob_start();
    				comment_text();
    				$comment_text = ob_get_clean();

    				$comment_text = apply_filters('the_content', $comment_text);
					$comment_text = str_replace(']]>', ']]>', $comment_text);

					preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $comment_text, $found);
					$comment_text = str_replace($found[0], '', $comment_text);
    			?>
				<?php
					print $comment_text;

				?>
				<?php comment_reply_link(array_merge( $args, array('respond_id' => 'respond', 'add_below' => 'comment','depth' => $depth, 'max_depth' => $args['max_depth'])), $comment->comment_ID) ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<?php
}

function get_ulogin_user_accounts_panel_front($user_id = 0) {
	global $wpdb, $current_user;
	uLoginPluginSettings::register_database_table();
	wp_enqueue_style('ulogin-prov-style');
	wp_enqueue_style('ulogin-style');
	$user_id = empty($user_id) ? $current_user->ID : $user_id;
	if(empty($user_id))
		return '';
	$unet = $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->ulogin where userid = %d", $user_id), 0);
	$output = '';
	if($unet) {
		$output .= '<div id="ulogin_accounts">';
		foreach($unet as $val) {
			$url = urldecode($val->identity);
			$output .= "<a href=".$url."><div class='ulogin_network big_provider {$val->network}_big'></div></a>";
		}
		$output .= '</div>';
	}

	return $output;
}

require_once dirname(__FILE__) . '/for_users/route.php'; // подключаем распределитель дел

function pr($var) {
	static $int=0;
	print '<hr style="border-top:2px solid red;">';
	echo '<pre><b style="background: red;padding: 1px 5px;">'.$int.'</b> ';
	print_r($var);
	echo '</pre>';
	print '<hr style="border-top:2px solid red;">';
	$int++;
}

function prt($var) {
	static $int=0;
	print '<hr style="border-top:2px solid #479d11;">';
	echo '<pre><b style="background: #479d11;padding: 1px 5px;">'.$int.'</b> ';
	print_r($var);
	echo '</pre>';
	print '<hr style="border-top:2px solid #479d11;">';
	$int++;
}

function prr($var) {
	static $int=0;
	print '<hr style="border-top:2px solid blue;">';
	echo '<pre><b style="background: blue;padding: 1px 5px;">'.$int.'</b> ';
	print_r($var);
	echo '</pre>';
	print '<hr style="border-top:2px solid blue;">';
	$int++;
}

function getPhrase( $number, $titles ) {
    $cases = array( 2, 0, 1, 1, 1, 2 );

    return $titles[ ( $number % 100 > 4 && $number % 100 < 20 ) ? 2 : $cases[ min( $number % 10, 5 ) ] ];
}

function registration_data ($data1, $data2) {
	$datetime1 = date_create($data1);
	$datetime2 = date_create($data2);
	$interval = date_diff($datetime1, $datetime2);
	return $interval->format('%a');
}

function plural_form($number, $after) {
  $cases = array (2, 0, 1, 1, 1, 2);
  return $number.' '.$after[ ($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)] ];
}

function library_list(){
	$inc_lib = get_template_directory().'/inc/library.php';
	include $inc_lib;}
add_shortcode('library_list', 'library_list');

function theme_register_nav_menu() {
	register_nav_menu( 'primary', 'Primary Menu' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu' );

$queried_object = get_queried_object();

print_r($queried_object);

function popular_posts($cat, $count = 4) {
	$tssuat_settings = unserialize(get_option('tssuat_settings'));

	$posts = query_posts(array('cat' => $cat, 'posts_per_page'=>$count, 'post_status' => 'publish',  'orderby'=>'rand'));

	if(!empty($cat) && count($posts) >3 ){
	echo '<div id="popular_posts">';
	echo '<h2>Популярные статьи</h2>';
	echo '<ul class="posts">';
	foreach($posts as $post) {
		$post->post_date = rdate('d M Y', strtotime($post->post_date));

		echo '<li class="popular_post"><a href="'.get_permalink($post->ID).'">';
		if($tssuat_settings['articles']){
		echo comment_percent($post->ID);
		}
		echo '<div class="post_image" style="background-image: url('.catch_that_image_by_id($post->ID).');"></div>';
		echo '<div class="post_link">'.$post->post_title.'</div></a>';
		?>
		<div class="post_date"><a href="<?=get_permalink($post->ID)?>"><?=$post->post_date?></a></div>
		<?php if($tssuat_settings['articles']){ ?>
		<div class="new_posts_comments_count"><a href="<?=get_permalink($post->ID)?>">комментарии</a> (<?= get_comments_number( $post->ID ); ?>)</div>
		<?php } ?>
		<?php
	}
	echo '</ul></div>';
	wp_reset_query();

	}
}

//Необходимого количество симоволов в заголовках
function title_preview( $title="", $needle=75 ){

	if (iconv_strlen($title,'UTF-8') > $needle) {
		$title = mb_substr($title, 0, $needle).'...';
	}

	return $title;
}

//Необходимого количество симоволов в контентах
function content_preview( $content="", $needle=100 ){

	$content = preg_replace(
		array("/\n+/","/\t/"),
		array(" ",""),
		wp_kses( stripslashes( $content ), 'strip' )
	);
	if (iconv_strlen($content,'UTF-8') > $needle) {
		$content = mb_substr($content, 0, $needle).'...';
	}

	return $content;
}

//Подсчет и вывод просмотров
function post_count_views( $post_id, $fix = false ){

	$views = get_post_meta($post_id, 'post_views');

	$views = $views ? $views[0] : 0;

	if( $fix ){
		$views++;
		update_post_meta( $post_id, 'post_views', $views );
	}

	return $views;
}

//Подсчет и вывод лайков
function post_count_likes( $post_id ){

	$rating = get_post_meta($post_id, 'ratings_users', true);
	return $rating ? $rating : 0;
}
//Подсчет и вывод лайков
function post_ratings_score( $post_id ){

	$rating_score = get_post_meta($post_id, 'ratings_score', true);
	return $rating_score ? $rating_score : 0;
}

function last_all_posts ($authordata){
	global $authordata, $post;

	extract( shortcode_atts( array(
			  'amount' => 1
		 ), $attr ) );
	$amount = !is_int($amount) ? (int) $amount : $amount;

	$offset = 0;

	//Получем id автора
	$authordatas = $authordata->ID;

	if ($authordatas == '') {
		return;
	}
	//Если главная страница, то выводить всех авторов и вывод статей начиная с 4той
	if(is_front_page()){
		$authordatas = '';
		$offset = 3;
	}

	wp_reset_query();
	$posts = get_posts( array(
		'numberposts'     => $amount, // тоже самое что posts_per_page
		'orderby'         => 'post_date',
		'order'           => 'asc',
		'post_type'       => 'post',
		'post_status'     => 'publish',
		'author'		  => $authordatas,
		'offset' 		  => $offset

	) );

	print '<div class="frontPosts__list"><ul class="frontPosts__list">';

	foreach ($posts as $k => $post) {

		$GLOBALS['adds_posts'][] = $post->ID;

		//Получаем все изображения поста
		$post_img = catch_that_image_all( $post->post_content );
		$count_img = count($post_img [1]);
		if ($count_img > 4) {
			$count_img -= 4;
		} else {
			$count_img = -1;
		}

		//Получаем имя автора поста
		$author = $post->post_author;
		$author_name = get_userdata($author)->display_name;
		$author_link = get_userdata($author)->user_nicename;

		//Получаем заголовок поста
		$title_amount = !$k ? 120 : 80;
		$post_title = title_preview( $post->post_title, $title_amount );

		/*$post_meta = '
			<span class="post_views">'.post_count_views($post->ID).'</span>
			
			<span class="post_comm">'.$post->comment_count.'</span>';*/

		//Получаем ссылку на пост
		$post_link = get_permalink( $post->ID );

		//Получаем контент поста
		$content = $post->post_content;

		//Преобразуем контент для вывода
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);

		preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
		$content = str_replace($found[0], '', $content);

		$allowed_html = array(
			'br' => array(),
			'em' => array(),
			'strong' => array(),
			'p' => array()
		);

		$content = wp_kses($content, $allowed_html);
		if (iconv_strlen($content,'UTF-8') > 200) {
			$content = mb_substr($content, 0, 200).'...';
			$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово, ибо оно в 99% случаев неполное
		}

		//Получаем теги поста
		$posttags = get_the_tags($post->ID);
		//Инициализируем пустой массив
		$posttags_x = array();
		if ($posttags) {
		  foreach($posttags as $tag) {
		  	//Записываем значени в массив
		  	array_push($posttags_x, '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>');
		  }
		}
		//Объединяем элементы массива в строку, через разделитель - "запятую"
		$posttags = implode(", ", $posttags_x);

		//Получаем количество комментариев
		$count_comments = $post->comment_count;

		//Получаем аватар
		$ava = get_avatar(get_userdata($post->post_author), 30);

		$like = post_likes_count_view($post->ID);

		//Выводим на экран форму
		print '
			<li>
				<div class="postMain">
					<div class="postMain__main">
						<a href="'.$post_link.'"><img src="'.$post_img [1] [0].'"></a>
						<div class="postMain__author">
							<div class="postMain__author__img">
								'.$ava.'
								<a href="/author/'.$author_link.'">'.$author_name.'</a>
							</div>
						</div>
						
						<div class="postMain__title">
							<div class="postMain__title__text">
								<div class="postMain__date">'.rdate('d. m. y', strtotime($post->post_date)).'г.</div>
								<a href="'.$post_link.'">'.$post_title.'</a>
							</div>
							<div class="postMain__additional">
								<div class="postMain__likes">'.$like.'</div>
								<div class="postMain__comment">'.$count_comments.'</div>
							</div>
						</div>
					</div>
					<div class="postMain__images">';
					for ($i = 1; $i <= 3; $i++) {
						if ($post_img [1] [$i] != ''){
					    	print '<div class="postMain__image"><a href="'.$post_link.'"><img src="'.$post_img [1] [$i].'"></a></div>';
					    }
					}
					if ($count_img >= 0) {
						print '<div class="postMain__images__link"><a href="'.$post_link.'"><p>Ещё</p><span>'.$count_img.'</span><span>фото</span></a></div>';
					}
					print '</div>
				</div>
				<div class="postFooter">
					<div class="postFooter__descr">
						<div class="postFooter__tags">
							теги:  '.$posttags.'
						</div>
						<div class="postFooter__text">'.$content.'</div>
					</div>
					<div class="postFooter__link">
						<a href="'.$post_link.'">Читать полностью</a>
					</div>
				</div>
			</li>';
	}
	print '</ul></div>';
	wp_reset_postdata();
}
add_shortcode( 'last_all_posts', 'last_all_posts' );

function last_posts( $attr ) {

	extract( shortcode_atts( array(
			  'amount' => 3
		 ), $attr ) );
	$amount = !is_int($amount) ? (int) $amount : $amount;

	wp_reset_query();
	$posts = get_posts( array(
		'numberposts'     => $amount, // тоже самое что posts_per_page
		'orderby'         => 'post_date',
		'order'           => 'DESC',
		'post_type'       => 'post',
		'post_status'     => 'publish'
	) );


	$html = '';
global $exception_id;
$exception_id = array();
	foreach ($posts as $k => $post) {
		//Записываем id в массив, для сравнения с id вывода на главной
		array_push($exception_id, $post->ID);

		$GLOBALS['adds_posts'][] = $post->ID;

		$post_img = catch_that_image( $post->post_content );

		$author = $post->post_author;
		$author_name = get_userdata($author)->display_name;
		$author_nick = get_userdata($author)->user_nicename;

		$title_amount = !$k ? 80 : 80;
		$post_title = title_preview( $post->post_title, $title_amount );

		//Получаем количество комментариев
		$count_comments = $post->comment_count;

		$post_meta = '
			<span class="post_views">'.post_count_views($post->ID).'</span>
			
			<span class="post_comm">'.$post->comment_count.'</span>';

		$post_link = get_permalink( $post->ID );

		ob_start();
		post_likes_count_view($post->ID);
		$like = ob_get_clean();

		if(!$k){

			$first_post = '
				<img class="img" src="'.$post_img.'">
				<!--<div class="meta">'.$post_meta.'</div>-->
				<div class="title">
				<div class="date">'.rdate('d M Y', strtotime($post->post_date)).'г.</div>
				<a href="'.$post_link.'"><span>'.wordwrap($post_title, 35, "<span class=\"text_clear\"> </span><br><span class=\"text_clear\"> </span>").'</span></a>
				<div class="post_name">Автор: '.$author_name.'
					<span class="like">'.$like.'</span>
					<span class="comm">'.$count_comments.'</span>
				</div>
				</div>';
			continue;
		}
		$html .= '
			<li>
				<a href="'.$post_link.'">
					<img class="img" src="'.$post_img.'">
				</a>
			<div class="title">
				<div class="date">'.rdate('d M Y', strtotime($post->post_date)).'г.</div>
				<a href="'.$post_link.'"><span>'.wordwrap($post_title, 42, "<span class=\"text_clear_2\"> </span><br><span class=\"text_clear_2\"> </span>").'</span></a>
				<div class="post_name_right">Автор: '.$author_name.'
					<span class="like">'.$like.'</span>
					<span class="comm">'.$count_comments.'</span>
				</div>
			</div>
			</li>';
	}

	$html = '<div class="first_post">'.$first_post.'</div><ul>'.$html.'</ul>';

	echo $html;
}
add_shortcode( 'last_posts', 'last_posts' );

function categories_posts( $attr ) {

	extract( shortcode_atts( array(
			  'categories' => ''
		 ), $attr ) );
	$categories = explode(",",$categories );

	foreach ($categories as $category) {
		$posts = get_posts( array(
			'numberposts'		=> 4, // тоже самое что posts_per_page
			'orderby'    		=> 'post_date',
			'order'      		=> 'DESC',
			'post_type'  		=> 'post',
			'post_status'		=> 'publish',
			'category_name'		=> $category,
			'exclude'	 		=> implode(",", $GLOBALS['adds_posts'])
		) );

		if( !empty($posts) ){
			$category = get_category_by_slug( $category );
			$link = get_category_link( $category );
			?>
			<div class="front_block">
				<div class="front_block-title">
					<?= $category->name; ?>
				</div>
				<div class="front_block-content category_posts">

					<?php
					foreach ($posts as $k => $post) {

						$GLOBALS['adds_posts'][] = $post->ID;

						$post_meta = '
							<span class="post_views dark">'.post_count_views($post->ID).'</span>
							<!--span class="post_score dark">'.post_ratings_score($post->ID).'</span-->
							<span class="post_comm dark">'.$post->comment_count.'</span>';

						$post_link = get_permalink( $post->ID );

						if( $k==0 ){

							$post_img = catch_that_image( $post->post_content );
							$post_title = title_preview( $post->post_title, 120 );
							$post_content = content_preview( $post->post_content, 300 );

							echo '
								<div class="first_post">
									<a href="'.$post_link.'"><div class="img" style="background-image:url(\''.$post_img.'\')"></div></a>
									<div class="title"><a href="'.$post_link.'">'.$post_title.'</a></div>
									<div class="content">'.$post_content.'</div>
									<div class="meta">'.$post_meta.'</div>
								</div>';
						}else{
							$post_title = title_preview( $post->post_title, 50 );
							$post_content = content_preview( $post->post_content, 100 );

							echo $k==1 ? '<div class="other_posts"><div class="category_link"><a href="'.$link.'">Все статьи раздела</a></div>' : '';

							echo '
							<div class="other_post">
								<div class="title"><a href="'.$post_link.'">'.$post_title.'</a></div>
								<div class="content">'.$post_content.'</div>
								<div class="meta">'.$post_meta.'</div>
							</div>';

							echo $k==3 ? '</div>' : '';
						}
					}
					?>
				</div>
			</div>
			<?php

		}
	}
}
add_shortcode( 'categories_posts', 'categories_posts' );

function bottom_post_block() {
	$tssuat_settings = unserialize(get_option('tssuat_settings'));
	global $post;
	$id = $post->ID;
	$author = $post->post_author;
	$author_name = get_userdata($author)->display_name;

	if ($meta_values == '') {
		$meta_values = 'http://1.gravatar.com/avatar/3424d775cd3c89e2d4ee823b1438d856?s=84&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D84&amp;r=G';
	}

	?><div id="post_bottom_block">
	<div class="inf-z"><span class="tt">Поделиться  </span><div class="soc2"><script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
	<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="facebook,twitter,vkontakte,odnoklassniki" data-yashareTheme="counter">
		
	</div>
	<?php post_likes_view(); ?>
	</div>
	<?php
	print '</div></div>';
}
add_shortcode( 'bottom_post_block', 'bottom_post_block' );

function sidebar_reviews2($count) {
	$comments = get_comments(array('status' => 'approve', 'type' => 'review', 'orderby' => 'comment_ID', 'number' => $count));

	if($comments) {
		foreach($comments as $comment){
			if(stripos($comment->comment_author_url, 'firms') != false || stripos($comment->comment_author_url, 'services') != false || stripos($comment->comment_author_url, 'goods') != false || stripos($comment->comment_author_url, 'books') != false || stripos($comment->comment_author_url, 'brands') != false || stripos($comment->comment_author_url, 'author') != false) {
				$view = '';
				$comment->comment_date = rdate('rM d, Y', strtotime($comment->comment_date));

				if (stripos($comment->comment_author_url, 'firms') != false) {
					$cat = (get_term($comment->comment_post_ID, 'firms'));
					$url_text = $cat->name;
				} elseif (stripos($comment->comment_author_url, 'services') != false || stripos($comment->comment_author_url, 'goods') != false) {
					$post = get_post($comment->comment_post_ID, 'products', OBJECT);
					$url_text = $post->post_title;
				} elseif (stripos($comment->comment_author_url, 'books') != false) {
					$post = get_post($comment->comment_post_ID, 'books', OBJECT);
					$url_text = $post->post_title;
				} elseif (stripos($comment->comment_author_url, 'brands') != false) {
					$post = get_post($comment->comment_post_ID, 'brands', OBJECT);
					$url_text = $post->post_title;
				}

				$grava = '<img src="/img/comment_user.png">';

				$title = $comment->comment_content;

				if (iconv_strlen($comment->comment_content,'UTF-8') > 200) {
					$comment->comment_content = mb_substr($comment->comment_content, 0, 200).'...';
				}
				$comment_rating = comment_rating($comment->comment_ID);

				print get_term_by( 'slug', $comment->comment_author_url, 'firms' );


				if(stristr($comment->comment_author_url, 'firms') === FALSE) {
					$post_title = get_post($comment->comment_post_ID, OBJECT)->post_title;
				} else {
					$post_title = get_term($comment->comment_post_ID, 'firms')->name;
				}


				$comment_rating = get_comment_meta( $comment->comment_ID, 'rating', false );
				$avg_rating = $comment_rating[0];
				$percent_rating = 100 / 5 * $avg_rating;
				$percent_rating = round($percent_rating, 1);
				$avg_rating = round($avg_rating, 1);
				?>
				<div class="mention">
					<div class="mention_date">
						<a href="<?=$comment->comment_author_url?>#comment-<?=$comment->comment_ID?>"><?=$comment->comment_date?></a>
					</div>
					<div class="mention_title">
						<a href="<?=$comment->comment_author_url?>#comment-<?=$comment->comment_ID?>"><?=$post_title?></a>
					</div>
					<div class="mention_body">
						<?=$comment->comment_content?>
					</div>
					<div class="firm_rating">
						<div class="rating_empty" title="Рейтинг <?=$avg_rating?>/5">
							<div class="rating_full" style="width: <?=$percent_rating?>%;"></div>
						</div>
					</div>
				</div>
				<?php
			}
		}
	} else {
		echo '<div class="comment"><p>Нет доступных отзывов</p></div>';
	}
}


function front_expert_profile($id) {
	$user = get_userdata($id);

	if(get_avatar( get_userdata($user->ID)->user_email, 165) != null) $meta_values = get_avatar( get_userdata($user->ID)->user_email); else $meta_values = '<img src="/wp-content/plugins/firms_by_tss/img/img_not_found_mini.png" width="139" height="165" />';
	$avatar = explode('"', $meta_values);
	$avatar = trim($avatar[1]);

	$name = $user->display_name;
	$slug = $user->user_nicename;

	$user_name = explode('"', $user->display_name);
?>
	<div class="expert_img">
		<a title="<?=$user->display_name?>" href="/author/<?=$slug?>" style="background-image: url(<?=$avatar?>);"></a>

	</div>
	<div class="expert_name">
		<p><?=$user_name[0]?></p>
		<?=$user_name[1]?><?=$user_name[2]?>
	</div>
	<div class="expert_info">
		<?=get_user_meta($id, 'adding1', true)?> <br>
		<?=get_user_meta($id, 'adding2', true)?> 
	</div>
	<div class="expert_links">
		<a href="/author/<?=$slug?>" class="expert_link">Подробнее об эксперте</a>
		<a href="/author/<?=$slug?>" class="expert_ask">Задать вопрос</a>
	</div>
<?php
}

add_shortcode( 'category_list', 'category_list' );
function category_list() {
	?>
	<div id="non_front_sections">
	<div id="sections">
		<ul>
		<?php
			$args = array(
				'orderby'       => 'name',
				'hide_empty'    => true,
				'parent'         => 0
			);
			$parents = get_terms(array('category'), $args);

			foreach($parents as $cat_id) {
				$cat_id = $cat_id->term_id;
				$cat_name = get_cat_name($cat_id);
				$slug = get_category_link( $cat_id );

				$cat_posts = get_posts( array(
					'numberposts'     => 1, // тоже самое что posts_per_page
					'offset'          => 0,
					'category'        => $cat_id,
					'orderby'         => 'post_date',
					'order'           => 'DESC',
					'post_type'       => 'post',
					'post_status'     => 'publish'
				));
				$cat_post = $cat_posts[0];

				$args = array(
					'orderby'       => 'name',
					'hide_empty'    => true,
					'parent'         => $cat_id
				);
				$childs = get_terms(array('category'), $args);



				if($cat_post->ID) {
					$cat_img = catch_that_image_by_id($cat_post->ID);

					if($cat_img == '/images/default.jpg') {
						$cat_img = '/wp-content/plugins/firms_by_tss/img/img_not_found_mini.png';
					}
					?>
					<li class="section">
						<div class="section_block">
						<div class="section_img"><a href="<?=$slug?>">
						<div class="category_img" style="background-image: url(<?=$cat_img?>);"></div>
						</a></div>
						<div class="section_link">
							<a href="<?=$slug?>"><?=$cat_name?></a>
							<ul class="child_cats">
								<?php
									foreach($childs as $child) {
										$child_slug = get_category_link( $child->term_id );
										?>
											<li><a href="<?=$child_slug?>"><?=$child->name?></a></li>
										<?php
									}
								?>
							</ul>
						</div>
						<?php if (count($childs) > 3) { ?>
						<div class="section_shadow"></div>
						<?php } ?>
						</div>
					<?php
				}
				wp_reset_postdata();
			}
		?>
		</ul>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	</div>
	<?php
}

function get_author_posts($offset, $author) {
	global $author_id_au_page;

	$author = $_POST['author'];
	$author = ($author) ? $author : $author_id_au_page;

	$offset = $_POST['offset'];

	if(!$offset) { $offset = 0; }

	if($author == get_current_user_id()){
		$post_status = 'publish, pending';
	} else {
		$post_status = 'publish';
	}
	if(get_current_user_role() == 'Administrator'){
		$post_status = 'publish, pending';
	}

	$query = array(
		'numberposts' => 5,
	    'post_type'  =>  'post',
	    'offset'          => $offset,
	    'meta_query' => array(
	    	array(
	    		'key' => 'private'
	    	)
	    ),
	    'orderby'=>'meta_value_num post_date',
	    'order'=>'DESC',
	    'post_status'     => $post_status,
	    'author'     => $author
	);
	$posts = get_posts($query);

	$count = count($posts);

	if(!$posts) {
		print '<div style="font:0px Verdana;" class="noposts noposts_author">Нет записей</div>';
	}

	author_news_list($posts);
	print '<input class="more_author_posts" type="hidden" data-offset="'.($offset+5).'">';
	print '<input class="more__posts_author" type="hidden" data-author="'.$author.'">';
	print '<input id="authorEntry" type="hidden" data-entry="0">';
}
add_action('wp_ajax_get_author_posts', 'get_author_posts');
add_action('wp_ajax_nopriv_get_author_posts', 'get_author_posts');

function add_comment_rating( $comment_ID, $comment_approved ) {
	add_metadata( 'comment', $comment_ID, 'rating', $_POST['rating_review'], true );
}
add_action( 'comment_post', 'add_comment_rating' );


function similar_posts() {
?>
	<div id="similar_posts">
		<h2>Статьи по теме</h2>
		<ul>
			<?php
				global $post;
				$categories = get_the_category($post->ID);
				$news = get_posts( array(
						'numberposts'     => 4,
						'offset'          => 0,
						'category'        => $categories[0]->term_id,
						'orderby'         => 'post_date',
						'order'           => 'DESC',
						'post_type'       => 'post',
						'post_status'     => 'publish'
				));

				foreach ($news as $new) {
					setup_postdata( $new );
					$img = catch_that_image_by_id($new->ID);

					if ($img == '/images/default.jpg') {
						$img = '/wp-content/plugins/firms_by_tss/img/img_not_found_mini.png';
					}


					$slug = get_permalink( $new->ID );
					$allowed_html = array(
						'br' => array(),
						'em' => array(),
						'strong' => array(),
						'p' => array()
					);
					$content = get_the_content();
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]>', $content);

					preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
					$content = str_replace($found[0], '', $content);

					$content = wp_kses($content, $allowed_html);
					if (iconv_strlen($content,'UTF-8') > 300) {
						$content = mb_substr($content, 0, 300).'...';
					}

					$new->post_date = rdate('d M Y', strtotime($new->post_date));
					?>
						<li data-id="<?=$new->ID?>">
							<div class="front_img">
								<a href="<?=$slug?>"><div class="front_img_inside" style="background-image: url(<?=$img?>);"></div></a>
							</div>

							<div class="post_rating"><?php echo comment_percent($new->ID, 1); ?></div>

							<div class="front_posts_title"><a href="<?=$slug?>"><?=$new->post_title?></a></div>
							<div class="front_posts_text">
								<?=$content?>
							</div>
					<?php
				}
				wp_reset_postdata();
			?>
		</ul>
	</div>
	<?php
}


function post_bottom() {
	global $post;

	$meta_values = 'http://1.gravatar.com/avatar/3424d775cd3c89e2d4ee823b1438d856?s=84&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D84&amp;r=G';

	?>
		<div class="article-bottom">
			<div class="bottom_post_info">
				<div class="author-name">
					<div class="author-name-title">Автор статьи:</div>
					<?php $author = get_userdata( $post->post_author ); $author_name = explode(' ', $author->display_name); ?>
					<a href="/author/<?=$author->user_nicename?>"><?=$author->display_name;?></a>
				</div>
				<div class="post_date">
					Опубликована: <?=rdate('d.m.Y', strtotime($post->post_date))?>
				</div>
			</div>

			<div class="article-bottom-right">
				<div id="article_social">
					<script type="text/javascript">(function() {
					  if (window.pluso)if (typeof window.pluso.start == "function") return;
					  if (window.ifpluso==undefined) { window.ifpluso = 1;
						var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
						s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
						s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
						var h=d[g]('body')[0];
						h.appendChild(s);
					  }})();</script>
					<div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
				</div>


				<div class="post_rating"><?php echo comment_percent($post->ID, 1); ?><div class="post_rating_info">Рейтинг<br>статьи</div></div>
				<a href="#" class="addFavorite">Добавить<br>в избранное</a>
				<a href="#" class="print" onclick="print()">Версия<br>для печати</a>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	<?php
}





function sidebar_test($id, $link) {
	global $wpdb;
	$wp_pro_quiz_master = $wpdb->prefix.wp_pro_quiz_master;
	$sql = 'SELECT name FROM '.$wp_pro_quiz_master.' WHERE id = '.$id.'';
	$result = $wpdb->get_var($sql);
	echo '<div class="title-side">Тест на знания</div>';
	echo '<div class="sidebar_test"><div class="name">'.$result.'</div><a href="'.$link.'"><input type="button" class="pass" value="Пройти тест"></a>
	<div class="all_test_link"><a href="/tests">Все тесты</a></div>
	</div>';
}

// ===============================================
function get_category_parents_ID( $_, $separator = '/', $visited = array() ) {
    $chain = '';
    $parent = &get_category( $_ );
    if ( is_wp_error( $parent ) )
    {
        foreach((get_the_category()) as $category)
        {
            $chain .= get_category_parents_ID( $category, '/', $visited );
        }
    }
    $category = $parent->term_id;
    if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
        $visited[] = $parent->parent;
        $chain .= get_category_parents_ID( $parent->parent, '/', $visited );
    }
     $chain .= $category.$separator;
    return $chain;
}
// ===============================================
remove_filter( 'comment_flood_filter', 'wp_throttle_comment_flood', 10, 3);

function rdate($param, $time=0) {
	if(intval($time)==0)$time=time();
	$MonthNames=array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");

	$MonthNames2=array("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");
	if(strpos($param,'rM')===false) {

	} else {
		return date(str_replace('rM',$MonthNames2[date('n',$time)-1],$param), $time);
	}

	if(strpos($param,'M')===false) return date($param, $time);
		else return date(str_replace('M',$MonthNames[date('n',$time)-1],$param), $time);
}


function dimox_breadcrumbs() {

  //$delimiter = ' / '; // разделить между ссылками
  $delimiter = '<img src="/wp-content/themes/1brus_mag/img/delimiter.png">'; // разделить между ссылками
  $home = 'Главная'; // текст ссылка "Главная"


  $before = '<span typeof="v:Breadcrumb" property="v:title"  rel="v:url" class="current">';
  $after = '</span>';
 $showCurrent = 0; // 1 - показывать название текущей статьи/страницы, 0 - не показывать
 $nofollow = 0;

  $sRel = '';

	if ($nofollow === 1)
	{
		$sRel = ' rel="nofollow" ';
	}
	else
	{
		$sRel = ' rel="v:url" ';
	}

  if ( !is_home() && !is_front_page() || is_paged() ) {

    echo '<div id="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">';

    global $post;
    $homeLink = get_bloginfo('url');
    echo '<span typeof="v:Breadcrumb"> <a property="v:title" ' . $sRel . ' href="' . $homeLink . '">' . $home . '</a></span> ' . $delimiter . ' ';

    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);

      if ($thisCat->parent != 0)
	{
		$sParents = get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' ');

		$sParents = str_replace('<a', '<span typeof="v:Breadcrumb"> <a property="v:title" '. $sRel . '', $sParents);
		$sParents = str_replace('</a>', '</a> </span> ', $sParents);

		echo( $sParents );
	}

 	$sParents  = single_cat_title('', false);
	$sParents = str_replace('<a', '<a property="v:title" rel="v:url"', $sParents);

      echo $before . '' . $sParents . '' . $after;

    } if ( is_tax() ) {
      global $wp_query;

      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->taxonomy;
	  $thisCat_name = $cat_obj->name;
      $thisCat = get_taxonomy($thisCat);
      $parentCat = get_taxonomy($thisCat->parent);

      if ($thisCat->parent != 0)
	{
		$sParents = get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' ');

		$sParents = str_replace('<a', '<span typeof="v:Breadcrumb"> <a property="v:title" '. $sRel . '', $sParents);
		$sParents = str_replace('</a>', '</a> </span> ', $sParents);

		echo( $sParents );
	}
	if ($thisCat->name == 'firms') {
		$sParents  = ('<a href="/firms">'.$thisCat->label.'</a> / <span class="current">'.$thisCat_name.'</span>');
		$sParents = str_replace('<a', '<span typeof="v:Breadcrumb"> <a property="v:title" '. $sRel . '', $sParents);
		$sParents = str_replace('</a>', '</a> </span> ', $sParents);

	} else if ($thisCat->name == 'category_brands') {
		$sParents  = ('<a href="/brands">Бренды</a> / <span class="current">'.$thisCat_name.'</span>');
		$sParents = str_replace('<a', '<span typeof="v:Breadcrumb"> <a property="v:title" '. $sRel . '', $sParents);
		$sParents = str_replace('</a>', '</a> </span> ', $sParents);

	}


	else if ($thisCat->name == 'publishers_books') {
		$sParents  = ('<a>Издатели книг</a> / <span class="current">'.$thisCat_name.'</span>');
		$sParents = str_replace('<a', '<span typeof="v:Breadcrumb"> <a property="v:title" '. $sRel . '', $sParents);
		$sParents = str_replace('</a>', '</a> </span> ', $sParents);

	}else if ($thisCat->name == 'authors_books') {
		$sParents  = ('<a>Авторы книг</a> / <span class="current">'.$thisCat_name.'</span>');
		$sParents = str_replace('<a', '<span typeof="v:Breadcrumb"> <a property="v:title" '. $sRel . '', $sParents);
		$sParents = str_replace('</a>', '</a> </span> ', $sParents);

	}


	else if ($thisCat->name == 'countries') {
		$sParents  = ('<a>Регионы</a> / <span class="current">'.$thisCat_name.'</span>');
		$sParents = str_replace('<a', '<span typeof="v:Breadcrumb"> <a property="v:title" '. $sRel . '', $sParents);
		$sParents = str_replace('</a>', '</a> </span> ', $sParents);

	} else if ($thisCat->name == 'category_product') {
		if (stripos($_SERVER['REDIRECT_URL'], 'goods') != false) {
			$url = 'goods';
			$url_text = 'Товары';
		} else if(stripos($_SERVER['REDIRECT_URL'], 'services') != false) {
			$url = 'services';
			$url_text = 'Услуги';
		} else {
			$url = 'category_product';
			$url_text = 'Категории';
		}
		$sParents  = ('<a href="/'.$url.'">'.$url_text.'</a> / <span class="current">'.$thisCat_name.'</span>');
		$sParents = str_replace('<a', '<span typeof="v:Breadcrumb"> <a property="v:title" '. $sRel . '', $sParents);
		$sParents = str_replace('</a>', '</a> </span> ', $sParents);

	} else {
		$sParents  = single_cat_title($thisCat->label.'', false);
	}
	$sParents = str_replace('<a', '<a property="v:title" rel="v:url"', $sParents);

      echo $before . '' . $sParents . '' . $after;

    } elseif ( is_day() ) {
      echo '<a property="v:title" ' . $sRel . ' href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a property="v:title" ' . $sRel . ' href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      echo '<a property="v:title" ' . $sRel . ' href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;


    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() == 'products' ) {
		$thisPost = get_post();
		$cat_1 = wp_get_object_terms($thisPost->ID, 'goods_services');
		$cat_2 = wp_get_object_terms($thisPost->ID, 'category_product');
		$cats = '<a href="/goods">Товары</a> / <a href="/goods/'.$cat_2[0]->slug.'">'.$cat_2[0]->name.'</a> / '.$thisPost->post_title;

		$cats = str_replace('<a', $linkBefore . '<span typeof="v:Breadcrumb"><a property="v:title" '  . $sRel . $linkAttr, $cats);
        $cats = str_replace('</a>', '</a></span>' . $linkAfter, $cats);

		echo $cats;
      } else if ( get_post_type() == 'services' ) {



		$thisPost = get_post();
		$cat_1 = wp_get_object_terms($thisPost->ID, 'goods_services');
		$cat_2 = wp_get_object_terms($thisPost->ID, 'category_product');
		$cats = '<a href="/services">Услуги</a> / <a href="/services/'.$cat_2[0]->slug.'">'.$cat_2[0]->name.'</a> / '.$thisPost->post_title;

		$cats = str_replace('<a', $linkBefore . '<span typeof="v:Breadcrumb"><a property="v:title" '  . $sRel . $linkAttr, $cats);
        $cats = str_replace('</a>', '</a></span>' . $linkAfter, $cats);

		echo $cats;
      } else if ( get_post_type() == 'brands' ) {
		$thisPost = get_post();
		$cat_1 = wp_get_object_terms($thisPost->ID, 'category_brands');
		$cat_1[0]->name = '<p class="for_replace">'.$cat_1[0]->name.'</p>';
		$cat_1[0]->name = str_replace('<p class="for_replace">', '<span property="v:title">', $cat_1[0]->name);
		$cat_1[0]->name = str_replace('</p>', '</span>', $cat_1[0]->name);
		$cats = '<a href="/brands">Бренды</a> / <a href="/brands/'.$cat_1[0]->slug.'">'.$cat_1[0]->name.'</a> / '.$thisPost->post_title;
		$cats = str_replace('<a', $linkBefore . '<span typeof="v:Breadcrumb"><a property="v:title" '  . $sRel . $linkAttr, $cats);
        $cats = str_replace('</a>', '</a></span>' . $linkAfter, $cats);
		echo $cats;

      } else if ( get_post_type() == 'remonts' ) {
		$thisPost = get_post();
		$cat_1 = wp_get_object_terms($thisPost->ID, 'firms');
		$cat_1[0]->name = '<p class="for_replace">'.$cat_1[0]->name.'</p>';
		$cat_1[0]->name = str_replace('<p class="for_replace">', '<span property="v:title">', $cat_1[0]->name);
		$cat_1[0]->name = str_replace('</p>', '</span>', $cat_1[0]->name);
		$cats = '<a href="/firms">Фирмы</a> / <a href="/firms/'.$cat_1[0]->slug.'">'.$cat_1[0]->name.'</a> / '.$thisPost->post_title;
		$cats = str_replace('<a', $linkBefore . '<span typeof="v:Breadcrumb"><a property="v:title" '  . $sRel . $linkAttr, $cats);
        $cats = str_replace('</a>', '</a></span>' . $linkAfter, $cats);
		echo $cats;

      }else if ( get_post_type() == 'books' ) {
		$thisPost = get_post();
		$cat_1 = wp_get_object_terms($thisPost->ID, 'publishers_books');
		$cats = '<a href="/books">Книги</a> / <a href="/books/'.$cat_1[0]->slug.'">'.$cat_1[0]->name.'</a>'.$delimiter.$thisPost->post_title;
		echo $cats;
      } else if ( get_post_type() == 'questions' ) {
		$thisPost = get_post();
		$cats = '<a href="/questions">Вопросы эксперта</a> / '.$thisPost->post_title;
		echo $cats;
      } else if ( get_post_type() == 'news' ) {

      	echo '<span typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="'.home_url().'/news">Новости</a></span>';


		if ($showCurrent == 0) echo $delimiter . $before . get_the_title() . $after;

      } else if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
        if ($showCurrent == 0) echo $delimiter . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, $delimiter);
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
        $cats = str_replace('<a', $linkBefore . '<span typeof="v:Breadcrumb"><a property="v:title" '  . $sRel . $linkAttr, $cats);
        $cats = str_replace('</a>', '</a></span>' . $linkAfter, $cats);
        echo $cats;
        //if ($showCurrent == 0) echo $delimiter . get_the_title() . $after;
      }


    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;

    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a property="v:title" ' . $sRel . ' href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;

    } elseif ( is_page() && !$post->post_parent ) {
		
      echo $before . get_the_title() . $after;

    } elseif ( is_page() && $post->post_parent ) {
		

      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<span typeof="v:Breadcrumb"><a property="v:title" ' . $sRel . ' href="' . get_permalink($page->ID) . '">' .''. get_the_title($page->ID) .''. '</a></span>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $before . '<span class="current">'.get_the_title() .'</span>'. $after;

	  
	  
	  
	  
    }
	
	elseif ( is_search() ) {
      echo $before . 'Результаты поиска по запросу "' . get_search_query() . '"' . $after;

    } elseif ( is_tag() ) {
      echo $before . 'Записи с тегом "' . single_tag_title('', false) . '"' . $after;

    } elseif ( is_author() ) {
    	global $author;
    	$userdata = get_userdata($author->ID);

    	$author  = ('<a>Авторы</a> / <span class="current">'.$userdata->display_name.'</span>');
    	$author  = str_replace('<a', '<span typeof="v:Breadcrumb"> <a property="v:title" '. $sRel . '', $author);
    	$author = str_replace('</a>', '</a> </span> ', $author);

    	echo $before . $author . $after;

    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }

    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Страница') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }

    echo '</div>';

  }
} // end dimox_breadcrumbs()


function new_excerpt_length($length) {
    return 26;
    }
    add_filter('excerpt_length', 'new_excerpt_length');

	    function new_excerpt_more($excerpt) {
    return str_replace('[...]', '...', $excerpt); }
    add_filter('wp_trim_excerpt', 'new_excerpt_more');

if (function_exists('add_theme_support')) {
 add_theme_support('menus');
}

function catch_that_image( $content = '' ) {
  global $post, $posts;
  $first_img = '';

  $content = empty($content) ? $post->post_content : $content;

  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
    $first_img = get_template_directory_uri() . "/img/no-img.png";
  }
  return $first_img;
}

function catch_that_image_by_id($id) {
	$post = get_post($id);
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	$first_img = $matches [1] [0];

	if(empty($first_img)){ //Defines a default image
		$first_img = "/images/default.jpg";
	}
	return $first_img;
}

function catch_that_image_all( $content = '' ) {
  global $post, $posts;
  $first_img = '';

  $content = empty($content) ? $post->post_content : $content;

  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].+>/i', $content, $matches);
  $first_img = $matches;

  if(empty($first_img)){ //Defines a default image
    $first_img = get_template_directory_uri() . "/img/no-img.png";
  }
  return $first_img;
}

function kama_excerpt($args=''){
    global $post;
        parse_str($args, $i);
        $maxchar     = isset($i['maxchar']) ?  (int)trim($i['maxchar'])     : 150;
        $text        = isset($i['text']) ?          trim($i['text'])        : '';
        $save_format = isset($i['save_format']) ?   trim($i['save_format'])         : false;
        $echo        = isset($i['echo']) ?          false                   : true;

    if (!$text){
        $out = $post->post_excerpt ? $post->post_excerpt : $post->post_content;
        $out = preg_replace ("!\[/?.*\]!U", '', $out ); //убираем шоткоды, например:[singlepic id=3]
        // для тега <!--more-->
        if( !$post->post_excerpt && strpos($post->post_content, '<!--more-->') ){
            preg_match ('/(.*)<!--more-->/s', $out, $match);
            $out = str_replace("\r", '', trim($match[1], "\n"));
            $out = preg_replace( "!\n\n+!s", "</p><p>", $out );
            $out = "<p>". str_replace( "\n", "<br />", $out ) ."</p>";
            if ($echo)
                return print $out;
            return $out;
        }
    }

    $out = $text.$out;
    if (!$post->post_excerpt)
        $out = strip_tags($out, $save_format);

    if ( iconv_strlen($out, 'utf-8') > 150 ){
        $out = iconv_substr( $out, 0, 150, 'utf-8' );
        $out = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $out); //убираем последнее слово, ибо оно в 99% случаев неполное
    }

    if($save_format){
        $out = str_replace( "\r", '', $out );
        $out = preg_replace( "!\n\n+!", "</p><p>", $out );
        $out = "<p>". str_replace ( "\n", "<br />", trim($out) ) ."</p>";
    }

    if($echo) return print $out;
    return $out;
}



/* Функция для вывода последних комментариев в WordPress. Параметры:
$limit - сколко комментов выводить. По дефолту - 10
$ex - обрезка текста комментария до n символов. По дефолту - 45
$cat - Включить(5,12,35) или исключить(-5,-12,-35) категории, указываются id категорий через запятую. По дефолту - пусто - из всех категорий.
$echo - выводить на экран (1) или возвращать (0). По дефолту - 1
$gravatar - показывать иконку gravatar, указывается размер иконки, например, 20 - выведет иконку шириной и высотой в 20px
===================================================================================== */
function kama_recent_comments($limit=10, $ex=45, $cat=0, $echo=1, $gravatar=''){
	$tssuat_settings = unserialize(get_option('tssuat_settings'));

	$not_in = "";
foreach( $tssuat_settings as $post_type=>$status){

	if( !$status ){

		$post_type = $post_type == 'articles' ? 'post' : $post_type;

		$not_in .= "'".$post_type."'".",";
	}
}
	$not_in = $not_in != "" ? "AND p.post_type NOT IN(".substr($not_in, 0, -1).")" : $not_in;



    global $wpdb;
    $and = "";
    if($cat){
        $IN = (strpos($cat,'-')===false)?"IN ($cat)":"NOT IN (".str_replace('-','',$cat).")";
        $join = "LEFT JOIN $wpdb->term_relationships rel ON (p.ID = rel.object_id)
		LEFT JOIN $wpdb->term_taxonomy tax ON (rel.term_taxonomy_id = tax.term_taxonomy_id)";
        $and = "AND tax.taxonomy = 'category'
		AND tax.term_id $IN";
    }



    $sql = "SELECT comment_ID, comment_post_ID, comment_content, post_title, guid, comment_author, comment_author_email, comment_date
	FROM $wpdb->comments com
		LEFT JOIN $wpdb->posts p ON (com.comment_post_ID = p.ID) {$join}
    WHERE comment_approved = '1'
		AND comment_type = '' $not_in {$and}
	ORDER BY comment_date DESC
    LIMIT $limit";



    $results = $wpdb->get_results($sql);

    $out = '';
    foreach ($results as $comment){
        $grava = '<div class="comment_user_face" alt=""></div>';
        $comtext = strip_tags($comment->comment_content);
        $leight = (int) iconv_strlen( $comtext, 'utf-8' );
        if($leight > $ex) $comtext =  iconv_substr($comtext,0,$ex, 'UTF-8').' …'; /**/
        $out .= "\n
      <li>
      	<div class=\"comm_meta\">
			<div class='date'>".rdate('rM d, Y', strtotime($comment->comment_date)). "</div>
			<div class='author'>(".strip_tags($comment->comment_author). ")</div>
		</div>

		<a href='". get_comment_link($comment->comment_ID) ."' title='к записи: {$comment->post_title}'>
			<div class=\"comment_to\">".title_preview( $comment->post_title, 45 )."</div>
			<div class='comment_text'>".$comtext."</div>
		</a>
      </li>";
    }

    if ($echo) echo $out;
    else return $out;
}

function bbp_enable_visual_editor( $args = array() ) {
    $args['tinymce'] = true;
    return $args;
}
add_filter( 'bbp_after_get_the_content_parse_args', 'bbp_enable_visual_editor' );


// ==============================================
// Функция добавляет количество просмотров тем bbpress, см. http://www.magpress.com/blog/adding-topics-view-counter-in-bbpress

if( !function_exists('get_wpbbp_post_view') ) :
////////////////////////////////////////////////////////////////////////////////
// get bbpress topic view counter
////////////////////////////////////////////////////////////////////////////////
function get_wpbbp_post_view($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}
function set_wpbbp_post_view($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if( $count == '' ){
        add_post_meta($postID, $count_key, '1');
    } else {
        $new_count = $count + 1;
        update_post_meta($postID, $count_key, $new_count);
    }
}
endif;

function row_editor_comments(){ 
	//Устанавливаем кнопки для визуального редактора tinymce
	#bold - жирный
	#italic - курсив
	#bullist - маркированный список
	#numlist - нумерованный список
	#image - вставка изображений
	#media - вставка видео
	/*$set_of_buttons = add_filter('mce_buttons', create_function('', 
		"return array(
			'bold',
			'italic',
			'bullist', 
			'numlist',
			'image',
			'media',
		);" 
	));*/
	$settings = array(
	  'media_buttons'=>0,//Показывать медиа кнопку (вставка/загрузка)
	  'textarea_name'=>'comment',//значение атрибута name у поля textarea
	  'quicktags' => 0,//Загружать HTML редактор или нет
	  'teeny'=>0,//Скрывать или нет кнопку, расширяющую возможности визуального редактора
	  'tinymce' => array(
		'toolbar1'=> 'bold,italic,bullist,numlist,media',
		'toolbar2'=> ''
	  ),//$set_of_buttons,//Загружать визуальный редактор TinyMCE или нет. Можно указать параметры редактора напрямую в массиве array().
	  'textarea_rows'=>2,//Количество строк у поля формы. Высота поля ввода текста. По-умолчанию берется значение их настроек админ-панели. По умолчанию: get_option('default_post_edit_rows', 10)
	  'drag_drop_upload'=>1,//Включает поддержку Drag-and-Drop при загрузке файлов
	);
	ob_start();
	wp_editor('', 'comment', $settings);//1)Заранее установленный текст в поле формы. 2)Идентификатор для полей textarea и TinyMCE 3)Массив аргументов
	$aut = ob_get_clean();
	$args = array(
			'comment_field' => $aut,//Код поле ввода комментария. По умолчанию:'<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>'
			//'comment_field' => wp_editor('', 'comment', $settings),
			'id_form'              => 'commentform',
			'id_submit'            => 'submit',
			'class_form'           => 'comment-form',
			'class_submit'         => ''.get_current_user_id().'',
			'name_submit'          => 'submit',
			'title_reply' => '',
			'title_reply_before' => '',
			'title_reply_after' => '',
			'title_reply_to' => '',
			'label_submit' => 'Опубликовать'//Текст кнопки отправки комментария. По умолчанию: "Оставить комментарий".
		);
	return comment_form($args);//Выводит на экран готовый код формы комментирования, который можно использовать в шаблонах.
}

if (!current_user_can('administrator') && !current_user_can('expert') && !current_user_can('editor')):
  show_admin_bar(false);
  function remove_admin_bar_style_frontend() { // css override for the frontend  
      echo '<style type="text/css" media="screen"> 
      html { margin-top: 0px !important; } 
      * html body { margin-top: 0px !important; } 
      </style>';  
    }  
    add_filter('wp_head','remove_admin_bar_style_frontend', 99);
endif;


remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

/* Блок добавления статьи в избранное */
function post_favorites_install(){
	global $wpdb;
	$post_favorites = $wpdb->prefix.post_favorites;
	
	//Создаем таблицу в базе данных, если не существует
	$sql = 
	"
		CREATE TABLE IF NOT EXISTS ".$post_favorites." (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`post_id` int(11) NOT NULL,
			`date` datetime NOT NULL,
			PRIMARY KEY (`id`)
		)	ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	$wpdb->query($sql);
}
add_action('init', 'post_favorites_install');//Хук на активацию функции

function post_favorites_user() {
	global $wpdb;
	$post_favorites = $wpdb->prefix.post_favorites;

	//Принимаем данные, посланные ajax запросом (id пользователя и id поста, где произошло событие)
	$user_id = $_POST['user_id'];
	$post_id = $_POST['post_id'];
	$user_nick = get_userdata($user_id)->user_nicename;

	//Получаем аватар пользователя
	$ava = get_avatar($user_id, 30);

	$sql = 
	"
		SELECT `id`, `user_id`, `post_id` FROM ".$post_favorites." WHERE `user_id`='".$user_id."' and `post_id`='".$post_id."'  ;
	";
	$record = $wpdb->get_results($sql);

	$user_data = get_userdata($user_id);
	$user_name = $user_data->display_name;
	$user_email = $user_data->user_email;

	$post = get_post($post_id);
	$author = $post->post_author;
	$title = $post->post_title;
	$author_data = get_userdata($author);
	$author_email = $author_data->user_email;
	$author_name = $author_data->display_name;

	$action = 'Вашу статью "'.$title.'" добавил в избранное '.$user_name.'';
	$post_link = wp_get_shortlink($post_id);

	$feedUsers = $wpdb->prefix.feedUsers;
	$var = $wpdb->get_var("SELECT `permission` FROM $feedUsers WHERE `user_id` = $author;");
	$perm = unserialize($var);

	//Если пользователь не добавлял статью в избранное
	if(!$record){
		//Добавляем данные в базу данных о добавлении поста в избранное
		$sql = 
		"
			INSERT INTO ".$post_favorites."  (`id`, `user_id`, `post_id`, `date`) VALUES (NULL, ".$user_id.", ".$post_id.", '".current_time('mysql')."');
		";
		$wpdb->query($sql);

		$post_content = $post->post_content;
		$post_content = apply_filters('the_content', $post_content);
		$post_content = str_replace(']]>', ']]>', $post_content);
		$post_content = strip_tags($post_content);
		if (iconv_strlen($post_content,'UTF-8') > 80) {
			$post_content = mb_substr($post_content, 0, 80);
			$post_content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $post_content); //убираем последнее слово
		}
		$post_content = trim($post_content).'...';
		$post_content .= ' <a href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_favorites&utm_content='.$author_email.'">(Читать далее)</a>';

		$post_content_full = '<div style="background:#f0d500;width:100%;padding:5px;">';
		$post_content_full .= $post_content;
		$post_content_full .= '</div>';

		$txt = '<p>Здравствуйте, '. $author_name .', вашу статью <span><a class="ref" href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_favorites&utm_content='.$author_email.'">&laquo;'.$title.'&raquo;</a></span> добавил в избранное <span style="font-weight:bold;">'.$user_name.'</span></p>';

		$txt .= $post_content_full;

		$txt .= '<br>Для просмотра статьи перейдите, пожалуйста, по ссылке: <a href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_favorites&utm_content='.$author_email.'">'.get_permalink($post_id).'</a>';

		$txt .= '</div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
	        <a href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_favorites&utm_content='.$author_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
	            Перейти к статье
	        </a><br>
	        <span style="text-align:center;display:block;">
	        С уважением, сообщество профессионалов <a href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_favorites&utm_content='.$author_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
	    </footer>';

		if($perm['YourFavoriteMyPostSub'] == 'on') {
			if(function_exists('ob_send_mail')){
				ob_send_mail($author_email, $txt, $action);
			} else {
				wp_mail($author_email, $action, $txt);
			}
		}

		//Выводим через ajax аватар пользователя к добавившим в избранное
		print '<li class="new_favorite"><a href="/'.$user_nick.'">'.$ava.'<span class="tooltip_favorite">'.get_userdata($user_id)->display_name.'</span></a></li>';
	} else { 
		//print '<span class="nofavorite">Вы уже добавляли эту статью в избранное.</span>'; 
		foreach ($record as $rec) {
			$sql = 
			"
				DELETE FROM ".$post_favorites."  WHERE `id`='".$rec->id."';
			";
			$wpdb->query($sql);

			/*$txt = '<p>Здравствуйте '. $author_name .', вашу статью удалил из избранного '.$user_name.': <span style="color:blue;"><a class="ref" href="'.$post_link.'">'.$title.'</a></span></p>';
			if($perm['YourFavoriteMyPostSub'] == 'on') {
				if(function_exists('ob_send_mail')){
					ob_send_mail($author_email, $txt, $action);
				} else {
					wp_mail($author_email, $action, $txt);
				}
			}*/

			$action = 'Вашу статью "'.$title.'" удалил из избранного '.$user_name.'';

			$post_content = $post->post_content;
			$post_content = apply_filters('the_content', $post_content);
			$post_content = str_replace(']]>', ']]>', $post_content);
			$post_content = strip_tags($post_content);
			if (iconv_strlen($post_content,'UTF-8') > 80) {
				$post_content = mb_substr($post_content, 0, 80);
				$post_content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $post_content); //убираем последнее слово
			}
			$post_content = trim($post_content).'...';
			$post_content .= ' <a href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_favorites&utm_content='.$author_email.'">(Читать далее)</a>';

			$post_content_full = '<div style="background:#f0d500;width:100%;padding:5px;">';
			$post_content_full .= $post_content;
			$post_content_full .= '</div>';

			$txt = '<p>Здравствуйте, '. $author_name .', вашу статью <span><a class="ref" href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_favorites&utm_content='.$author_email.'">&laquo;'.$title.'&raquo;</a></span> удалил из избранного <span style="font-weight:bold;">'.$user_name.'</span></p>';

			$txt .= $post_content_full;

			$txt .= '<br>Для просмотра статьи перейдите, пожалуйста, по ссылке: <a href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_favorites&utm_content='.$author_email.'">'.get_permalink($post_id).'</a>';

			$txt .= '</div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
		        <a href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_favorites&utm_content='.$author_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
		            Перейти к статье
		        </a><br>
		        <span style="text-align:center;display:block;">
		        С уважением, сообщество профессионалов <a href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_favorites&utm_content='.$author_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
		    </footer>';

			if($perm['YourFavoriteMyPostSub'] == 'on') {
				if(function_exists('ob_send_mail')){
					ob_send_mail($author_email, $txt, $action);
				} else {
					wp_mail($author_email, $action, $txt);
				}
			}
		}
	}
}
add_action('wp_ajax_post_favorites_user', 'post_favorites_user');//Вешаем хук на ajax для авторизированного пользователя

function post_favorites_view() {
	global $wpdb, $post;
	$post_favorites = $wpdb->prefix.post_favorites;

	//Запрашиваем данные о добавлении в избранное по id поста
	$sql = 
	"
		SELECT `user_id` FROM ".$post_favorites." WHERE `post_id`='".$post->ID."' ;
	";
	$users = $wpdb->get_results($sql);

	$users = array_reverse($users, true);

	$cur_user = get_current_user_id();


	$uz = '';
	foreach ($users as $user) {
		if($cur_user == $user->user_id) {
			$uz = 'Удалить из избранного';
		}
	}
	if(!$uz) {
		$uz = 'Добавить в избранное';
	}
	//Выводим форму добавления в избранное
	?>
		<div class="bottom_post_buttons">
			<div class="userFavorite">Добавили в избранное</div>
			<ul>
				<?php if($users) {
						//Если есть записи в базе данных, то выводим список пользователей, добавлявших пост в избранное
						foreach ($users as $user) {
							$user_nick = get_userdata($user->user_id)->user_nicename;
							$class_user = ($cur_user == $user->user_id) ? 'new_favorite' : 'noava';
							$ava = get_avatar($user->user_id, 30);
							?>
								<li class="<?=$class_user?>"><a href="/<?=$user_nick?>"><?=$ava?><span class="tooltip_favorite"><?php print get_userdata($user->user_id)->display_name; ?></span></li>
							<?php	
						}
				} else { print '<span class="nofavorite">Статью еще не добавляли в избранное.</span>'; }?>
			</ul>
			<form method="POST" id="post_favorites" name="post_favorites" onsubmit="return false;">
				<input type="hidden" name="user_id" value="<?php print get_current_user_id(); ?>">
				<input type="hidden" name="post_id" value="<?php print $post->ID; ?>">
				<a class="addFavorite"><input type="submit" value="<?=$uz?>"></a>
			</form>
		</div>
	<?php
}

function count_favorites_posts_menu(){
	global $wpdb, $curauth;
	$post_favorites = $wpdb->prefix.post_favorites;

	$user = get_current_user_id();

	$sql = 
	"
		SELECT `post_id` FROM ".$post_favorites." WHERE `user_id`='".$curauth."' ;
	";
	$all_posts = $wpdb->get_results($sql);

	$count_favorites_posts_menu = count($all_posts);

	if($count_favorites_posts_menu) {
		print $count_favorites_posts_menu;
	} else { print '0'; }
}

function post_favorites_author() {
	global $wpdb, $curauth;
	$post_favorites = $wpdb->prefix.post_favorites;

	//Запрашиваем id записи по id пользователя
	$sql = 
	"
		SELECT `post_id` FROM ".$post_favorites." WHERE `user_id`='".$curauth."' ;
	";
	$result = $wpdb->get_results($sql);

	if($result){
		$post_id = array();
		foreach ($result as $item => $id) {
			array_push($post_id, $id->post_id);
		}
		$post_list = implode(', ', $post_id);

		$posts = get_posts(
			array(
				'post_type' => 'post',
				'include' => $post_list,
			)
		);
		foreach ($posts as $post) {
			$image = catch_that_image_by_id($post->ID);
			?>
				<div class="ideas_block">
					<div class="ideas_block__image">
						<img src="<?=$image?>">
					</div>
					<div class="ideas_block__title">
						<a href="<?=get_permalink($post->ID)?>"><?=$post->post_title?></a>
					</div>
					<?php if(get_current_user_id() == $curauth) { ?>
						<input type="button" class="delete_favorites_profile" name="delete_favorites_profile" data-user_id="<?=$curauth?>" data-post_id="<?=$post->ID?>" value="X">
					<?php } ?>
				</div>
			<?php
		}
		print '<div class="clear"></div>';
	} else { print '<span class="noinfo">У пользователя нет сохраненных идей.</span>'; }
}
/* END Блок добавления статьи в избранное */


/* Сказать спасибо */
function post_likes_install(){
	global $wpdb;
	$post_likes = $wpdb->prefix.post_likes;
	
	$sql = 
	"
		CREATE TABLE IF NOT EXISTS ".$post_likes." (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`post_id` int(11) NOT NULL,
			`date` datetime NOT NULL,
			PRIMARY KEY (`id`)
		)	ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	$wpdb->query($sql);
}
add_action('init', 'post_likes_install');

function post_likes_user() {
	global $wpdb;
	$post_likes = $wpdb->prefix.post_likes;

	$user_id = $_POST['user_id'];
	$post_id = $_POST['post_id'];

	if(is_user_logged_in()) {

		$sql = 
		"
			SELECT `user_id`, `post_id` FROM ".$post_likes." WHERE `user_id`='".$user_id."' and `post_id`='".$post_id."'  ;
		";
		$record = $wpdb->get_results($sql);

		$sql = 
		"
			SELECT `id`, `post_id` FROM ".$post_likes." WHERE `post_id`='".$post_id."' ;
		";
		$all_posts = $wpdb->get_results($sql);
		$count_likes_posts = count($all_posts);
		$count_likes_posts += 1;

		$user_name = get_userdata($user_id)->display_name;

		$post = get_post($post_id);
		$author = $post->post_author;
		$title = $post->post_title;
		$author_data = get_userdata($author);
		$author_email = $author_data->user_email;
		$author_name = $author_data->display_name;

		$action = 'Вам сказал спасибо за статью '.$user_name;
		$post_link = wp_get_shortlink($post_id);

		$feedUsers = $wpdb->prefix.feedUsers;
		$var = $wpdb->get_var("SELECT `permission` FROM $feedUsers WHERE `user_id` = $author;");
		$perm = unserialize($var);

		if(!$record){
			$sql = 
			"
				INSERT INTO ".$post_likes."  (`id`, `user_id`, `post_id`, `date`) VALUES (NULL, ".$user_id.", ".$post_id.", '".current_time('mysql')."');
			";
			$wpdb->query($sql);
			print $count_likes_posts;

			$post_content = $post->post_content;
			$post_content = apply_filters('the_content', $post_content);
			$post_content = str_replace(']]>', ']]>', $post_content);
			$post_content = strip_tags($post_content);
			if (iconv_strlen($post_content,'UTF-8') > 80) {
				$post_content = mb_substr($post_content, 0, 80);
				$post_content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $post_content); //убираем последнее слово
			}
			$post_content .= '...<a href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_thanks&utm_content='.$author_email.'">(Читать далее)</a>';

			$post_content_full = '<div style="background:#f0d500;width:100%;padding:5px;">';
			$post_content_full .= $post_content;
			$post_content_full .= '</div>';

			$txt = '<p>Здравствуйте, '. $author_name .', за вашу статью <span><a class="ref" href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_thanks&utm_content='.$author_email.'">&laquo;'.$title.'&raquo;</a></span> вам сказал <span style="font-weight:bold;">"Спасибо" '.$user_name.'</span></p>';

			$txt .= $post_content_full;

			$txt .= '<br>Для просмотра статьи перейдите, пожалуйста, по ссылке: <a href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_thanks&utm_content='.$author_email.'">'.get_permalink($post_id).'</a>';

			$txt .= '</div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
		        <a href="'.$post_link.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_thanks&utm_content='.$author_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
		            Перейти к статье
		        </a><br>
		        <span style="text-align:center;display:block;">
		        С уважением, сообщество профессионалов <a href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $post_id).'&utm_campaign=added_to_thanks&utm_content='.$author_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
		    </footer>';

			if($perm['AddYourMyPostSub'] == 'on') {
				if(function_exists('ob_send_mail')){
					ob_send_mail($author_email, $txt, $action);
				} else {
					wp_mail($author_email, $action, $txt);
				}
			}
		} else { //Если пользователь уже говорил спасибо к данному посту, то убираем лайк и удаляем запись с базы данных
			//print '<span class="nolikes">Вы уже сказали спасибо.</span>'; 
			$count_likes_posts -= 2;
			foreach ($all_posts as $post) {
				$sql = 
				"
					DELETE FROM ".$post_likes."  WHERE `id`='".$post->id."';
				";
				$wpdb->query($sql);
			}
			print $count_likes_posts;

			/*$txt = '<p>Здравствуйте '. $author_name .', '.$user_name.' удалил спасибо за статью: <span style="color:blue;"><a class="ref" href="'.$post_link.'">'.$title.'</a></span></p>';
			if($perm['AddYourMyPostSub'] == 'on') {
				if(function_exists('ob_send_mail')){
					ob_send_mail($author_email, $txt, $action);
				} else {
					wp_mail($author_email, $action, $txt);
				}
			}*/
		}
	} else { print '<span class="nlo">#</span>'; return false; }
}
add_action('wp_ajax_post_likes_user', 'post_likes_user');
add_action('wp_ajax_nopriv_post_likes_user', 'post_likes_user');

function post_likes_view() {
	global $wpdb, $post;
	$post_likes = $wpdb->prefix.post_likes;

	$user = get_current_user_id();

	$sql = 
	"
		SELECT `post_id` FROM ".$post_likes." WHERE `post_id`='".$post->ID."' ;
	";
	$all_posts = $wpdb->get_results($sql);

	$count_likes_posts = count($all_posts);
	?>
		<form name="like_post" id="like_post" onsubmit="return false;">
			<a><input type="submit" value="Сказать спасибо"></a><span class="like_post_count"><?php if($count_likes_posts) {
					print $count_likes_posts;
				} else { print '0'; }?></span>
			<input type="hidden" name="user_id" value="<?=$user?>">
			<input type="hidden" name="post_id" value="<?=$post->ID?>">
		</form>
	<?php
}

function post_likes_count_view($post) {
	global $wpdb;
	$post_likes = $wpdb->prefix.post_likes;

	$user = get_current_user_id();

	$sql = 
	"
		SELECT `post_id` FROM ".$post_likes." WHERE `post_id`='".$post."' ;
	";
	$all_posts = $wpdb->get_results($sql);

	$count_likes_posts = count($all_posts);
	?>
		<?php if($count_likes_posts) {
			print $count_likes_posts;
		} else { print '0'; }?>
	<?php
}

function count_likes_posts_menu(){
	global $wpdb, $curauth;
	$post_likes = $wpdb->prefix.post_likes;

	$col = $wpdb->get_results("SELECT lk.id, lk.post_id AS lID, ps.ID AS pID FROM $post_likes lk LEFT JOIN $wpdb->posts ps ON lk.post_id = ps.ID 	WHERE lk.user_id = $curauth");
	if($col) {
		foreach ($col as $row) {
			if(!isset($row->pID)) {
				$wpdb->query("DELETE FROM $post_likes WHERE `id` = $row->id");
			}
		}
	}

	$count_likes_posts_menu = $wpdb->get_var("SELECT count(`id`) FROM $post_likes WHERE `user_id`= $curauth");

	if($count_likes_posts_menu) {
		print $count_likes_posts_menu;
	} else { print '0'; }
}

function post_likes_author() {
	global $wpdb, $curauth;
	$post_likes = $wpdb->prefix.post_likes;

	//Запрашиваем id записи по id пользователя
	$sql = 
	"
		SELECT `post_id` FROM ".$post_likes." WHERE `user_id`='".$curauth."' ;
	";
	$result = $wpdb->get_results($sql);

	$count_likes_posts = count($result);

	if($result){
		$post_id = array();
		foreach ($result as $item => $id) {
			array_push($post_id, $id->post_id);
		}
		$post_list = implode(', ', $post_id);

		$posts = get_posts(
			array(
				'post_type' => 'post',
				'include' => $post_list,
			)
		);
		?>
		<div class="a-list">
					<ul>
		<?php
		foreach ($posts as $post) {
			//$image = catch_that_image_by_id($post->ID);
			//Получаем имя автора поста
			$author_name = get_userdata($post->post_author)->display_name;
			$author_link = get_userdata($post->post_author)->user_nicename;

			//Получаем все изображения поста
			$post_img = catch_that_image_all( $post->post_content );
			$count_img = count($post_img [1]);
			if ($count_img > 4) {
				$count_img -= 4;
			} else {
				$count_img = -1;
			}

			//Получаем категории поста
			$posttags = get_the_category($post->ID);
			//Инициализируем пустой массив
			$posttags_x = array();
			if ($posttags) {
			  foreach($posttags as $tag) {
			  	//Записываем значени в массив
			  	array_push($posttags_x, '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>');
			  }
			}
			//Объединяем элементы массива в строку, через разделитель - "запятую"
			$posttags = implode(", ", $posttags_x);

			//Получаем количество комментариев
			//$count_comments = $posts->comment_count;
			$count_comments = get_comments_number( $post->ID );

			//Получаем аватар
			$avatar = get_avatar($post->post_author, 30);

			//Получаем контент поста
			$content = $post->post_content;

			//Преобразуем контент для вывода
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]>', $content);

			preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
			$content = str_replace($found[0], '', $content);

			$allowed_html = array(
				'br' => array(),
				'em' => array(),
				//'strong' => array(),
				'p' => array()
			);

			$content = wp_kses($content, $allowed_html);
			if (iconv_strlen($content,'UTF-8') > 200) {
				$content = mb_substr($content, 0, 200).'...';
				$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово, ибо оно в 99% случаев неполное
			}
			$content = $content.'...';
			?>
				<li class="vis" style="margin-bottom:20px;list-style: none;">
					<div class="postMain">
						<div class="postMain__main">
							<a href="<?=the_permalink($post->ID)?>"><img src="<?=$post_img [1] [0]?>"></a>
							<div class="postMain__author">
								<div class="postMain__author__img">
									<?=$avatar?>
									<a href="/author/<?=$author_link?>"><?=$author_name?></a>
								</div>
							</div>
							<div class="postMain__title">
								<div class="postMain__title__text">
									<div class="postMain__date"><?=rdate('d M Y', strtotime($post->post_date))?>г.</div>
									<a href="<?=the_permalink($post->ID)?>"><?=$post->post_title?></a>
								</div>
								<div class="postMain__additional">
									<div class="postMain__likes"><?=post_likes_count_view($post->ID)?></div>
									<div class="postMain__comment"><?=$count_comments?></div>
								</div>
							</div>
						</div>
						<div class="postMain__images">
							<?php
							for ($i = 1; $i <= 3; $i++) {
								if ($post_img [1] [$i] != ''){
							    	?>
							    		<div class="postMain__image"><a href="<?=the_permalink($post->ID)?>"><img src="<?=$post_img [1] [$i]?>"></a></div>
							    	<?php
							    }
							}
							if ($count_img >= 0) {
								?>
								<div class="postMain__images__link"><a href="<?=the_permalink($post->ID)?>"><p>Ещё</p><span><?=$count_img?></span><span>фото</span></a></div>
								<?php
							}
							?>
						</div>
					</div>
					<div class="postFooter">
						<div class="postFooter__descr">
							<div class="postFooter__tags">
								теги:  <?=$posttags?>
							</div>
							<div class="postFooter__text"><?=$content?></div>
						</div>
						<div class="postFooter__link">
							<a href="<?=the_permalink()?>">Читать полностью</a>
						</div>
					</div>
				</li>
			<?php
		}
		?>
		</ul>
				</div>
		<?php
		print '<div class="clear"></div>';
	} else { print '<span class="noinfo">У пользователя пока нет материалов, которые ему понравились.</span>'; }
}
/* END Сказать спасибо */

/* Комментарии, оставленные автором */
function comment_author_list() {
	global $curauth;
	$args = array(
		'author__in' => $curauth,
		'status' => 'approve'
	);
	$comments = get_comments($args);
	if($comments){
		foreach ($comments as $comment) {
			$title = get_the_title($comment->comment_post_ID);
			?>
				<div class="comment_blog">
					<div class="comment_blog__content">
						<div class="comment_blog_content-text">
							<?=$comment->comment_content?>
						</div>
					</div>
					<div class="comment_blog__info">
						<div class="comment_blog_content-post">
							К посту: <a href="<?=get_permalink($comment->comment_post_ID)?>"><?=$title?></a>
						</div>
						<div class="comment_blog__info-date">
							<?=$comment->comment_date?>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			<?php
		}
	} else {
		print '<div class="nocomment">Пользователь не оставлял комментарии.</div>';
	}
}

function comment_author_count_list() {
	global $curauth;
	$args = array(
		'author__in' => $curauth,
		'count' => true,
		'status' => 'approve'
	);
	$comments = get_comments($args);
	$comments ? print $comments : print '0';
}
/* END  Комментарии, оставленные автором */

//Вопросы без ответов (таб - 3 в блоке "Вопрос-ответ")
function test_view_forum() {
	$posts = get_posts( array(
			'post_type'           => bbp_get_topic_post_type(),
			'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
			'numberposts' => 4,
			'offset' => 0,
			'order' => 'DESC',
			'exclude' => excludeHiddenTopic(),
			'meta_query' => array(
				array(
					'key' => '_bbp_reply_count',
					'value' => '0'
				)
			)
		) );
	if($posts){
		?>
		<ul class="questionsFront__list">
			<?php page_view_all_forums($posts); ?>
		</ul>
		<?php
	}
}

//Функция добавляет верстку элементов для страницы задавания вопроса через шоткоды
function for_bbp_ask(){
    ?>
    <script>jQuery(document).ready(function($){
        //$('#bbp_topic_subscription').parent().attr('style','background: #83bc37 !important; margin: 0px 0px 0px 0px !important; padding: 0px 0px 0px 0px !important;');
        $('#bbp_topic_subscription').attr('style', 'margin: 5px 0px 0px 5px !important;padding-top: 10px; float: none !important;');
        //$('#bbp_topic_subscription').siblings('label').attr('style','background: #0171a1 !important;color: #fff !important;padding-left: 10px !important;padding-top: 10px !important;padding-bottom: 10px !important;');
        //$('#bbp_topic_subscription').siblings('label').attr('style','background: #0171a1 !important;color: #fff !important;padding-left: 10px !important;padding-top: 10px !important;padding-bottom: 10px !important; width: 90% !important;');
        $('#bbp_topic_subscription').prop( "checked", true);
        $('#bbp_topic_subscription').parent().parent().find('select:first').val(9392);
        //$("#bbp_topic_subscription").parent().prepend('<p>Ntcn</p>');
        console.log('hello!');
    });
    </script>
    <?php
}

add_shortcode('for_bbp_ask', 'for_bbp_ask');

//Создание таблицы, для хранения количества просмотренных пользователем страниц
function count_view_posts_create(){
	global $wpdb;
	$count_view_posts = $wpdb->prefix.count_view_posts;
	
	$sql = 
	"
		CREATE TABLE IF NOT EXISTS ".$count_view_posts." (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`post_id` int(11) NOT NULL,
			PRIMARY KEY (`id`)
		)	ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	$wpdb->query($sql);
}
add_action('init', 'count_view_posts_create');

//Количество уникальных просморов страниц типа: post and topic
function count_view_posts() {
	global $wpdb, $post;
	$count_view_posts = $wpdb->prefix.count_view_posts;

	$user_id = get_current_user_id();
	$post_id = $post->ID;

	$sql = 
	"
		SELECT `user_id`, `post_id` FROM ".$count_view_posts." WHERE `user_id`='".$user_id."' and `post_id`='".$post_id."'  ;
	";
	$all_view = $wpdb->get_results($sql);
	$count_view = count($all_view);

	if(get_post_type() == 'post' || get_post_type() == 'topic') {
		if(is_single() && $post_id && $user_id ){
			if(!$all_view){
				$sql = 
				"
					INSERT INTO ".$count_view_posts."  (`id`, `user_id`, `post_id`) VALUES (NULL, ".$user_id.", ".$post_id.");
				";
				$wpdb->query($sql);
			}
		}
	}
}
add_action('wp_head', 'count_view_posts');

//Вывод количества уникальных просмотров пользователя, count_view_posts();
function count_view_posts_honor($user_id) {
	global $wpdb, $wp_query;
	$count_view_posts = $wpdb->prefix.count_view_posts;

	//$user_id = $wp_query->get_queried_object()->data->ID;

	$sql = 
	"
		SELECT `post_id` FROM ".$count_view_posts." WHERE `user_id`='".$user_id."'  ;
	";
	$result = $wpdb->get_results($sql);
	$count_view = count($result);

	return $count_view;
}

//Создание таблицы для хранения наград пользователей
function user_honor_install() {
	global $wpdb;
	$user_honors = $wpdb->prefix.user_honors;
	
	$sql = 
	"
		CREATE TABLE IF NOT EXISTS ".$user_honors." (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`count_posts` int(11) NULL,
			`count_comments` int(11) NULL,
			`count_reply` int(11) NULL,
			`count_unique_view` int(11) NULL,
			`count_subscriber` int(11) NULL,
			PRIMARY KEY (`id`)
		)	ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	$wpdb->query($sql);
}
add_action('init', 'user_honor_install');

//Обновление данных, таблицы наград пользователей
function user_honors_update() {
	global $wpdb;
	$user_honors = $wpdb->prefix.user_honors;
	$posts = $wpdb->prefix.posts;

	$user_id = get_current_user_id();

	$sql = 
	"
		SELECT * FROM ".$user_honors." WHERE `user_id`='".$user_id."'  ;
	";
	$result = $wpdb->get_results($sql);

	$count_posts = count_user_posts($user_id);
	$count_comments = get_comments('user_id='.$user_id.'&count=true');
	$count_reply = count_user_reply_posts($user_id);
	$count_view_post = count_view_posts_honor($user_id);

	if(!$result && $user_id) {
		$sql = 
		"
			INSERT INTO ".$user_honors."  (`id`, `user_id`, `count_posts`, `count_comments`, `count_reply`, `count_unique_view`, `count_subscriber`) VALUES (NULL, ".$user_id.", ".$count_posts.", ".$count_comments.", ".$count_reply.", ".$count_view_post.", NULL);
		";
		$wpdb->query($sql);
	}
	if($result && $user_id) {
		foreach ($result as $k) {
			$count_posts = count_user_posts($user_id);
			$count_comments = get_comments('user_id='.$user_id.'&count=true');
			$count_reply = count_user_reply_posts($user_id);
			$count_view_post =  count_view_posts_honor($user_id);
			
			if(
				$k->count_posts == $count_posts && 
				$k->count_comments == $count_comments &&
				$k->count_reply == $count_reply &&
				$k->count_unique_view == $count_view_post
			) { 
				return false; 
			} else {
				$sql = 
				"
				UPDATE ".$user_honors." SET
					`count_posts` = '".$count_posts."',
					`count_comments` = '".$count_comments."',
					`count_reply` = '".$count_reply."',
					`count_unique_view` = '".$count_view_post."',
					`count_subscriber` = NULL
					WHERE `user_id`='".$user_id."'
				";
				$wpdb->query($sql);
			}
		} 
	}
}
add_action('init', 'user_honors_update');



function get_award_of_user($type_of_award, $user_id = null){
	if ($user_id === null){
		if(!$_GET['page_namber']) {
			$user_id = $wp_query->get_queried_object()->data->ID;
		}
		else {
			$user_id = $_GET['page_namber'];
		}
	}
	$awards = array();
	switch ($type_of_award) {
		case 'authorship':
			$award = get_award_for_authorship($user_id);
			if ($award !== null) $awards[] = $award;
			break;
		case 'comment':
			$award = get_award_for_comment($user_id);
			if ($award !== null) $awards[] = $award;
			break;
		case 'knowledge':
			$award = get_award_for_knowledge($user_id);
			if ($award !== null) $awards[] = $award;
			break;
		case 'activity':
			$award = get_award_for_activity($user_id);
			if ($award !== null) $awards[] = $award;
			break;
		default:
			$award = get_award_for_authorship($user_id);
			if ($award !== null) $awards[] = $award;
			$award = get_award_for_comment($user_id);
			if ($award !== null) $awards[] = $award;
			$award = get_award_for_knowledge($user_id);
			if ($award !== null) $awards[] = $award;
			$award = get_award_for_activity($user_id);
			if ($award !== null) $awards[] = $award;
			break;
	}
	return $awards;
}

function get_award_for_authorship($user_id){
	global $wpdb, $wp_query;
	$user_honors = $wpdb->prefix.user_honors;

	$sql = 
	"
		SELECT * FROM ".$user_honors." WHERE `user_id`='".$user_id."'  ;
	";
	$result = $wpdb->get_results($sql);

	if($result) {
		foreach ($result as $k) {
			$count_posts = $k->count_posts;
		}
	

		//Количество постов
		if($count_posts && $count_posts >= 3) {
			$award = array();
				if($count_posts >= '3' && $count_posts <= '9') {
					$award['degree'] = 'Автор<br> V степени';
					$award['images_honor'] = 'av_5st.png';
					$award['description'] = 'За количество добавленных записей пользователя';
					$award['counter'] = $count_posts;
				}
				if($count_posts >= '10' && $count_posts <= '24') {
					$award['degree'] = 'Автор<br> IV степени';
					$award['images_honor'] = 'av_4st.png';
					$award['description'] = 'За количество добавленных записей пользователя';
					$award['counter'] = $count_posts;
				}
				if($count_posts >= '25' && $count_posts <= '49') {
					$award['degree'] = 'Автор<br> III степени';
					$award['images_honor'] = 'av_3st.png';
					$award['description'] = 'За количество добавленных записей пользователя';
					$award['counter'] = $count_posts;
				}
				if($count_posts >= '50' && $count_posts <= '99') {
					$award['degree'] = 'Автор<br> II степени';
					$award['images_honor'] = 'av_2st.png';
					$award['description'] = 'За количество добавленных записей пользователя';
					$award['counter'] = $count_posts;
				}
				if($count_posts >= '100') {
					$award['degree'] = 'Автор<br> I степени'; 
					$award['images_honor'] = 'av_1st.png';
					$award['description'] = 'За количество добавленных записей пользователя';
					$award['counter'] = $count_posts;
				}
			return $award;
		}
	}
}

/**
this function
return array or null
*/
function get_award_for_comment($user_id){
	global $wpdb, $wp_query;
	$user_honors = $wpdb->prefix.user_honors;
	$sql = 
	"
		SELECT * FROM ".$user_honors." WHERE `user_id`='".$user_id."'  ;
	";
	$result = $wpdb->get_results($sql);

	if($result){
		foreach ($result as $k){
			$count_comments = $k->count_comments;
		}
		//Количество комметариев
		if($count_comments && $count_comments >= 5) {
			$award = array();
				if($count_comments >= '5' && $count_comments <= '24') {
					$award['degree'] = 'Комментатор<br> V степени';
					$award['images_honor'] = 'com_5st.png';
					$award['description'] = 'За количество комментариев';
					$award['counter'] = $count_comments;

				}
				if($count_comments >= '25' && $count_comments <= '49') {
					$award['degree'] = 'Комментатор<br> IV степени';
					$award['images_honor'] = 'com_4st.png';
					$award['description'] = 'За количество комментариев';
					$award['counter'] = $count_comments;

				}
				if($count_comments >= '50' && $count_comments <= '99') {
					$award['degree'] = 'Комментатор<br> III степени';
					$award['images_honor'] = 'com_3st.png';
					$award['description'] = 'За количество комментариев';
					$award['counter'] = $count_comments;

				}
				if($count_comments >= '100' && $count_comments <= '199') {
					$award['degree'] = 'Комментатор<br> II степени';
					$award['images_honor'] = 'com_2st.png';
					$award['description'] = 'За количество комментариев';
					$award['counter'] = $count_comments;
				}
				if($count_comments >= '200') {
					$award['degree'] = 'Комментатор<br> I степени'; 
					$award['images_honor'] = 'com_1st.png';
					$award['description'] = 'За количество комментариев';
					$award['counter'] = $count_comments;
				}
			return $award;
		}
	}
}


function get_award_for_knowledge($user_id){
	global $wpdb, $wp_query;
	$user_honors = $wpdb->prefix.user_honors;
	$sql = 
	"
		SELECT * FROM ".$user_honors." WHERE `user_id`='".$user_id."'  ;
	";
	$result = $wpdb->get_results($sql);

	if($result){
		foreach ($result as $k) {
			$count_reply = $k->count_reply;
		}
		if($count_reply) {
			$award = array();
				if($count_reply >= '1' && $count_reply <= '4') {
					$award['degree'] = 'Знаток<br> V степени';
					$award['images_honor'] = 'zn_5st.png';
					$award['description'] = 'За количество ответов';
					$award['counter'] = $count_reply;
				}
				if($count_reply >= '5' && $count_reply <= '9') {
					$award['degree'] = 'Знаток<br> IV степени';
					$award['images_honor'] = 'zn_4st.png';
					$award['description'] = 'За количество ответов';
					$award['counter'] = $count_reply;
				}
				if($count_reply >= '10' && $count_reply <= '24') {
					$award['degree'] = 'Знаток<br> III степени';
					$award['images_honor'] = 'zn_3st.png';
					$award['description'] = 'За количество ответов';
					$award['counter'] = $count_reply;
				}
				if($count_reply >= '25' && $count_reply <= '49') {
					$award['degree'] = 'Знаток<br> II степени';
					$award['images_honor'] = 'zn_2st.png';
					$award['description'] = 'За количество ответов';
					$award['counter'] = $count_reply;
				}
				if($count_reply >= '50') {
					$award['degree'] = 'Знаток<br> I степени'; 
					$award['images_honor'] = 'zn_1st.png';
					$award['description'] = 'За количество ответов';
					$award['counter'] = $count_reply;
				}
			return $award;
		}
	}
}


function get_award_for_activity($user_id){
	global $wpdb, $wp_query;
	$user_honors = $wpdb->prefix.user_honors;
	$sql = 
	"
		SELECT * FROM ".$user_honors." WHERE `user_id`='".$user_id."'  ;
	";
	$result = $wpdb->get_results($sql);

	if($result){
		foreach ($result as $k) {
			$count_view_post = $k->count_unique_view;
		}
			if($count_view_post && $count_view_post >= 500) {
				$award = array();
				if($count_view_post >= '500' && $count_view_post <= '1999') {
					$award['degree'] = 'Активность<br> V степени';
					$award['images_honor'] = 'act_5st.png';
					$award['description'] = 'За активность на сайте';
					$award['counter'] = $count_view_post;
				}

				if($count_view_post >= '2000' && $count_view_post <= '4999') {
					$award['degree'] = 'Активность<br> IV степени';
					$award['images_honor'] = 'act_4st.png';
					$award['description'] = 'За активность на сайте';
					$award['counter'] = $count_view_post;
				}

				if($count_view_post >= '5000' && $count_view_post <= '9999') {
					$award['degree'] = 'Активность<br> III степени';
					$award['images_honor'] = 'act_3st.png';
					$award['description'] = 'За активность на сайте';
					$award['counter'] = $count_view_post;
				}

				if($count_view_post >= '10000' && $count_view_post <= '49999') {
					$award['degree'] = 'Активность<br> II степени';
					$award['images_honor'] = 'act_2st.png';
					$award['description'] = 'За активность на сайте';
					$award['counter'] = $count_view_post;
				}

				if($count_view_post >= '50000') {
					$award['degree'] = 'Активность<br> I степени'; 
					$award['images_honor'] = 'act_1st.png';
					$award['description'] = 'За активность на сайте';
					$award['counter'] = $count_view_post;
				}
			return $award;
		}
	}
}


function user_honors_view() {
	global $wpdb, $wp_query;
	$user_honors = $wpdb->prefix.user_honors;

	if(!$_GET['page_namber']) {
		$user_id = $wp_query->get_queried_object()->data->ID;
	} else {
		$user_id = $_GET['page_namber'];
	}
	
	$sql = 
	"
		SELECT * FROM ".$user_honors." WHERE `user_id`='".$user_id."'  ;
	";
	$result = $wpdb->get_results($sql);
		$list_value = get_award_of_user('all', $user_id);

		//var_dump($list_value);
		
		if($list_value){
			foreach ($list_value as $honor) {
				$count++;
				if($count == 4 && !is_page('honor')) break;
				?>
					<div class="honors_one">
						<a href="/spisok-nagrad">
							<img src="/wp-content/themes/1brus_mag/img/honors/<?php print $honor['images_honor']; ?>" data-pagespeed-url-hash="1940506104" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
							<div class="honor_tooltip"><?php print $honor['description']; ?></div>
						</a>
						<span><?php print $honor['degree']; ?></span>
						<?php
							if(is_page('honor')){
								?>
									<div class="honors_count_value">Сейчас: <?php print $honor['counter']; ?></div>
								<?php
							}
						?>
					</div>
				<?php
			}
		}
	
		$count_list_value = count($list_value);

		if($count_list_value && !is_page('honor')) {
		
			?>
			<form id="honors_form" method="get" target="_blank" action="/honor">

				<input type="submit" class="all_honors" value="Все награды">
				<input type="hidden" name="page_namber" value="<?php print $user_id; ?>">
				<!--<div class="honor_popup"></div>-->
			</form>
			<?php

		}

		//unset($list_value);
	 elseif(!$count_list_value) { print '<span style="font:14px Verdana;max-width:none;">У пользователя нет наград.</span>'; 
	}
}


//Вывод всех наград пользователя на отдельной странице
function view_all_honors() {
	global $wpdb;

	$user_id = $_GET['page_namber'];

	user_honors_view($user_id);
}

//Вопросы с последними ответами
function forum_last_reply() {
	$posts = get_posts( array(
			'numberposts' => 4,
			'offset' => 0,
			'post_type'           => bbp_get_topic_post_type(),
			'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
			'meta_key'            => '_bbp_last_active_time',
			'orderby'             => 'meta_value',
			'ignore_sticky_posts' => true,
			'no_found_rows'       => true,
			'order'               => 'DESC',
			'exclude' => excludeHiddenTopic(),
			'meta_query' => array(
				array(
					'key' => '_bbp_reply_count',
					'value' => '0',
					'compare' => '>'
				)
			)
	) );
	if($posts){
		?>
		<ul class="questionsFront__list">
			<?php page_view_all_forums($posts); ?>
		</ul>
		<?php
	}
}

//Вопросы с наибольшим количеством комментариев (самые обсуждаемые)
function forum_count_reply() {
	$posts = get_posts( array(
			'post_type'           => bbp_get_topic_post_type(),
			'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
			'posts_per_page' => 4,
			'meta_key'            => '_bbp_reply_count',
			'orderby'             => 'meta_value_num',
			'ignore_sticky_posts' => true,
			'no_found_rows'       => true,
			'order'               => 'DESC',
			'exclude' => excludeHiddenTopic(),
			'meta_query' => array(
				array(
					'key' => '_bbp_reply_count',
					'value' => '0',
					'compare' => '>'
				)
			)
	) );
	if($posts){
		?>
		<ul class="questionsFront__list">
			<?php page_view_all_forums($posts); ?>
		</ul>
		<?php
	}
}

//Вопросы к торым требуется ответ
function forum_required_reply() {
	global $wpdb;
	/*$posts = get_posts( array(
			'post_type'           => bbp_get_topic_post_type(),
			'post_status'         => bbp_get_public_status_id(),
			'posts_per_page' => -1,
			'orderby'             => 'date',
			'order'               => 'DESC',
	) );*/
	$posts = $wpdb->get_results("SELECT post.* FROM $wpdb->posts post 
		LEFT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id 
		LEFT JOIN $wpdb->posts post2 ON post2.ID = meta.meta_value
		WHERE post.post_author = post2.post_author AND 
			post.post_type = 'topic' AND 
			post2.post_type = 'reply' AND
			(post2.post_status = 'publish' OR post2.post_status = 'closed') AND 
			(post.post_status = 'publish' OR post.post_status = 'closed') AND 
			meta.meta_key = '_bbp_last_reply_id'
			GROUP BY post.ID ORDER BY post2.post_date DESC LIMIT 0, 4");
	if($posts){
		?>
		<ul class="questionsFront__list">
			<?php
				foreach ($posts as $post) {
					$meta = get_post_meta($post->ID);

					$topic_id    = $post->ID;
					$author_link = '';

					//Все метаданные поста
					$meta = get_post_meta($topic_id);

					//Автор вопроса
					$quest_author = get_userdata($post->post_author);
					$quest_author = $quest_author->display_name;

					//Просмотры
					$wiews = get_post_meta($topic_id, 'post_views', true);
					if(!$wiews){
						$wiews = 0;
					}

					//Ответы
					$reply_count = get_post_meta($topic_id, '_bbp_reply_count', true);

					//Вывод первой записи ответа
					//Получение контента и записывание его в буфер
					ob_start();
					bbp_topic_content($topic_id);
					$content = ob_get_clean();
					//Преобразуем контент для вывода
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]>', $content);
					preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
					$content = str_replace($found[0], '', $content);
					$content = str_replace('&nbsp;', '', $content);
					$allowed_html = array(
						//'br' => array(),
						//'em' => array(),
						//'strong' => array(),
						//'p' => array()
					);
					$content = wp_kses($content, $allowed_html);
					//Обрезаем строку до необходимой длины
					if (iconv_strlen($content,'UTF-8') > 135) {
						$content = mb_substr($content, 0, 135).'...';
						$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово, ибо оно в 99% случаев неполное
						$content = $content.'...';
					}

					//Получаем айди последнего ответа к вопросу
					$last_reply_id = get_post_meta($topic_id, '_bbp_last_reply_id', true);

					//Получение автора последнего ответа и записывание его в буфер
					ob_start();
					bbp_author_link( array( 'post_id' => $last_reply_id, 'size' => 1 ) );
					$reply_user = ob_get_clean();
					$reply_user = apply_filters('the_content', $reply_user);
					$reply_user = str_replace(']]>', ']]>', $reply_user);
					$reply_user = str_replace($found[0], '', $reply_user);
					$allowed_html = array(
						'br' => array(),
						'em' => array(),
						'strong' => array(),
						'p' => array()
					);
					$reply_user = wp_kses($reply_user, $allowed_html);

					$author_reply_id = bbp_get_reply_author_id($last_reply_id);
					$ava = get_avatar($author_reply_id, 35);

					$author_id = bbp_get_topic_author_id($topic_id);
					$ava_author = get_avatar($author_id, 35);

					$topic_time = rdate('d M Y - H:i', strtotime($post->post_date));

					$id_bbp_last_reply_id = get_post_meta($topic_id, '_bbp_last_reply_id', true);
					$post_reply = get_post($id_bbp_last_reply_id);
					if(get_post_meta($topic_id, '_bbp_last_reply_id', true) != 0){
						if (iconv_strlen($post_reply->post_content,'UTF-8') > 200) {
							$content_reply = mb_substr($post_reply->post_content, 0, 200).'...';
							$content_reply = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content_reply); //убираем последнее слово, ибо оно в 99% случаев неполное
							$content_reply = $content_reply.'...';
							$content_reply = strip_tags($content_reply, '<a><img><iframe>');
						} else {
							$content_reply = $post_reply->post_content;
						}
					}

					if($reply_user) {
						$bb_topic_last_time = time_as_vk(get_post($id_bbp_last_reply_id)->post_date);
					} else {
						$bb_topic_last_time = time_as_vk(get_post($topic_id)->post_date);
					}
					?>

					<li class="questionsFront__item">

						<div class="questionsFront__item__left">
								<div class="questionsFront__item__views"><b><?=$reply_count?></b> ответов</div>
								<div class="questionsFront__item__answers"><b><?=$wiews?></b> просмотров</div>
						</div>

						<div class="questionsFront__item__right">
							<?php if($reply_user) { ?>
								<a href="<?php bbp_topic_permalink( $topic_id ); ?>#post-<?=$last_reply_id?>">
								Последний<br> комментарий от
								<div class="questionsFront__item__expert">
									<div class="questionsFront__item__expert__avatar"><?=$ava?></div>
									<div class="questionsFront__item__expert__name"><?=$reply_user?></div>
								</div>
								<div class="last_time"><?php print $bb_topic_last_time; ?></div>
								</a>
								<div class="last_reply_comment"><?=$content_reply?></div>
							<?php } else { ?>
								<a href="<?php bbp_topic_permalink( $topic_id ); ?>#post-<?=$last_reply_id?>">
								Автор<br> вопроса
								<div class="questionsFront__item__expert">
									<div class="questionsFront__item__expert__avatar"><?=$ava_author?></div>
									<div class="questionsFront__item__expert__name"><?=$quest_author?></div>
								</div>
								<div class="last_time"><?php print $bb_topic_last_time; ?></div>
								</a>
							<?php } ?>
						</div>

						<div class="questionsFront__item__center">
							<a class="bbp-forum-title questionsFront__item__title" href="<?php bbp_topic_permalink( $topic_id ); ?>"><?php bbp_topic_title( $topic_id ); ?></a>
							<div class="questionsFront__item__descr"><?=$content?></div>
							<div class="questionsFront__item__bottom">
								<div class="questionsFront__item__tags">
									<a href="" class="questionsFront__item__tag"></a>
								</div>
								<div class="questionsFront__item__time">
									Вопрос добавлен: <?=$topic_time?><?//=$topic_time?>
								</div>
								<div class="questionsFront__item__answers"><b><?=$wiews?></b> просмотров</div>
							</div>
						</div>
						<div class="clear"></div>
					</li>
					<?php 
					// Reset the $post global
					wp_reset_postdata();
				}
			?>
		</ul>
		<?php
	}
}

//Последние вопросы с форума
function forum_last_new_reply() {
	$posts = get_posts( array(
		'numberposts' => 4,
		'offset' => 0,
		'post_type'      => bbp_get_topic_post_type(),
		'post_status'    => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
		'ignore_sticky_posts' => true,
		'no_found_rows' => true,
		'order' => 'DESC',
		'orderby' => 'date',
		'exclude' => excludeHiddenTopic()
	) );
	if($posts){
		?>
		<ul class="questionsFront__list">
			<?php page_view_all_forums($posts); ?>
		</ul>
		<?php
	}
}

//Ответы в профиле, оставленные на форуме
function reply_page_author() {
	global $author_id_au_page;
	$author = $author_id_au_page;

	$posts = get_posts( array(
		'post_type'           => bbp_get_topic_post_type(),
		'posts_per_page' => -1,
		'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
		'order'               => 'DESC',
		'author' => $author,
	) );
	if ( !$posts || !$author) {
		return;
	}
	print '<ul class="questionsPage__list">';
	foreach ($posts as $post) {
		$reply_id   = bbp_get_reply_id( $post->ID );
		$topic_id   = $post->ID;
		//$topic_id   = bbp_get_reply_id( $post->post_parent );
		//if(current_user_can('manage_options')){prr($post); }
		$reply_link = '<a class="bbp-reply-topic-title" href="' . esc_url( bbp_get_reply_url( $reply_id ) ) . '" title="' . esc_attr( bbp_get_reply_excerpt( $reply_id, 50 ) ) . '">' . bbp_get_reply_topic_title( $reply_id ) . '</a>';

		$author_link = '';

		$author_link = bbp_get_reply_author_link( array( 'post_id' => $topic_id, 'type' => 'both', 'size' => 14 ) );

		$meta = get_post_meta($reply_id);

		$meta_topic = get_post_meta($topic_id);

		//Получаем айди последнего ответа к вопросу
		$last_reply_id = $meta_topic['_bbp_last_reply_id'][0];

		$content = $post->post_content;
		//Преобразуем контент для вывода
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);
		preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
		$content = str_replace($found[0], '', $content);
		$allowed_html = array(
			'br' => array(),
			'em' => array(),
			//'strong' => array(),
			//'p' => array()
		);
		$content = wp_kses($content, $allowed_html);
		//Обрезаем строку до необходимой длины
		if (iconv_strlen($content,'UTF-8') > 185) {
			$content = mb_substr($content, 0, 185).'...';
			$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово, ибо оно в 99% случаев неполное
		}
		$content = $content.'...';

		//Получаем время последнего ответа к вопросу
		$last_id = $meta_topic['_bbp_last_active_id'][0];
		$last_time = bbp_get_time_since(get_the_time( 'U', $last_id ));

		//Получаем аватар автора
		$author = bbp_get_reply_author_link( array( 'post_id' => $last_reply_id, 'size' => 35 ) );

		//Получение автора последнего ответа и записывание его в буфер
		ob_start();
		bbp_reply_author_link( array( 'post_id' => $last_reply_id, 'size' => 1 ) );
		$reply_user = ob_get_clean();
		$reply_user = apply_filters('the_content', $reply_user);
		$reply_user = str_replace(']]>', ']]>', $reply_user);
		$reply_user = str_replace($found[0], '', $reply_user);
		$allowed_html = array(
			'br' => array(),
			'em' => array(),
			'strong' => array(),
			'p' => array()
		);
		$reply_user = wp_kses($reply_user, $allowed_html);

		$topic_time = bbp_get_time_since(get_the_time( 'U', $topic_id ));

		$ava_author = get_avatar($post->post_author, 35);

		$post_author_name = get_usermeta($post->post_author, 'display_name');
		?>
			<li class="questionsPage__item">
				<span class="questionsPage__item__times">
						<div>Вопрос оставлен <?php print $topic_time; ?></div>
				</span>

				<div class="questionsPage__item__right">
					<?php if($reply_user) { ?>
					<a href="<?php bbp_topic_permalink( $topic_id ); ?>#post-<?=$last_reply_id?>">
					Последний<br> комментарий от
					<div class="questionsPage__item__expert">
						<div class="questionsPage__item__expert__avatar" style="font-size:0;"><?=$author?></div>
						<div class="questionsPage__item__expert__name"><?=$reply_user?></div>
					</div>
					<?php print $last_time; ?>
					</a>
					<?php } else { ?>
						<a href="<?php bbp_topic_permalink( $topic_id ); ?>#post-<?=$topic_id?>">
						Автор<br> вопроса</a>
						<div class="questionsPage__item__expert">
							<div class="questionsPage__item__expert__avatar" style="font-size:0;"><?=$ava_author?></div>
							<div class="questionsPage__item__expert__name"><?=$post_author_name?></div>
						</div>
						<?php print $last_time; ?>
					<?php } ?>
				</div>

				<div class="questionsPage__item__center">
					<a class="questionsPage__item__title" href="<?php bbp_topic_permalink( $topic_id ); ?>#post-<?=$reply_id?>"><?=$content?></a>
					<div class="questionsPage__item__descr">К записи: 
						<span><a href="<?php bbp_topic_permalink( $topic_id ); ?>"><?php bbp_topic_title( $topic_id ); ?></a></span>
					</div>
				</div>
				<div class="clear"></div>
			</li>
		<?php
	}
	print '</ul>';
	wp_reset_postdata();
}

function questions_page_author_contain() {
	print '<ul class="questionsPage__list">';
	questions_page_author();
	print '</ul>';
	wp_reset_postdata();
}

//Вопросы в профиле, оставленные на форуме
function questions_page_author() {
global $author_id_au_page;
$author = $author_id_au_page;

$author = $author_id_au_page ?: $_POST['author'];
$author_us = $author;
$offset = $_POST['offset'] ?: '0';
$posts = get_posts( array(
	'post_type'           => 'reply',
	'posts_per_page' => 10,
	'post_status'         => array( 'publish', 'closed' ),
	'order'               => 'DESC',
	'author' => $author,
	'offset' => $offset
) );

	if (!$posts) {
		print '<input type="hidden" class="nonePost">';
		return;
	}
	foreach ($posts as $post) {
		$reply_id   = bbp_get_reply_id( $post->ID );
		$topic_id   = $post->ID;
		$parent = $post->post_parent;
		$reply_link = '<a class="bbp-reply-topic-title" href="' . esc_url( bbp_get_reply_url( $reply_id ) ) . '" title="' . esc_attr( bbp_get_reply_excerpt( $reply_id, 50 ) ) . '">' . bbp_get_reply_topic_title( $reply_id ) . '</a>';

		$author_link = '';

		$author_link = bbp_get_reply_author_link( array( 'post_id' => $topic_id, 'type' => 'both', 'size' => 14 ) );

		$meta = get_post_meta($reply_id);

		$meta_topic = get_post_meta($meta['_bbp_topic_id'][0]);

		//Получаем айди последнего ответа к вопросу
		$last_reply_id = get_post_meta($parent);
		$last_reply_id = $last_reply_id['_bbp_last_reply_id'][0];

		//Вывод первой записи ответа
		$content = $post->post_content;
		//Преобразуем контент для вывода
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);
		preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
		$content = str_replace($found[0], '', $content);
		$allowed_html = array(
			'br' => array(),
			'em' => array(),
			//'strong' => array(),
			//'p' => array()
		);
		$content = wp_kses($content, $allowed_html);
		//Обрезаем строку до необходимой длины
		if (iconv_strlen($content,'UTF-8') > 185) {
			$content = mb_substr($content, 0, 185).'...';
			$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово, ибо оно в 99% случаев неполное
		}
		$content = $content.'...';

		//Получаем время последнего ответа к вопросу
		//$last_id = $meta['_bbp_last_active_id'][0];
		$last_time = bbp_get_time_since(get_the_time( 'U', $last_reply_id ));

		//Получаем аватар автора
		$author = bbp_get_reply_author_link( array( 'post_id' => $last_reply_id, 'size' => 35 ) );

		//Получение автора последнего ответа и записывание его в буфер
		ob_start();
		bbp_reply_author_link( array( 'post_id' => $last_reply_id, 'size' => 1 ) );
		$reply_user = ob_get_clean();
		$reply_user = apply_filters('the_content', $reply_user);
		$reply_user = str_replace(']]>', ']]>', $reply_user);
		$reply_user = str_replace($found[0], '', $reply_user);
		$allowed_html = array(
			'br' => array(),
			'em' => array(),
			'strong' => array(),
			'p' => array()
		);
		$reply_user = wp_kses($reply_user, $allowed_html);

		$bb_topic_last_time = time_as_vk(get_post($topic_id)->post_date);

		ob_start();
		bbp_topic_post_date();
		$topic_time = ob_get_clean();
		if (iconv_strlen($topic_time,'UTF-8') > 10) {
			$topic_time = mb_substr($topic_time, 0, 10);
		} ?>
			<li class="questionsPage__item">
				<span class="questionsPage__item__times">
						<div>Ответ оставлен <?php print $bb_topic_last_time; ?></div>
				</span>

				<div class="questionsPage__item__right">
					<?php if($reply_user) { ?>
					<a href="<?php bbp_topic_permalink( $parent ); ?>#post-<?=$last_reply_id?>">
					Последний<br> комментарий от
					<div class="questionsPage__item__expert">
						<div class="questionsPage__item__expert__avatar" style="font-size:0;"><?=$author?></div>
						<div class="questionsPage__item__expert__name"><?=$reply_user?></div>
					</div>
					<?php print $last_time; ?>
					</a>
					<?php } ?>
				</div>

				<div class="questionsPage__item__center">
					<a class="questionsPage__item__title" href="<?php bbp_topic_permalink( $parent ); ?>#post-<?=$reply_id?>"><?=$content?></a>
					<div class="questionsPage__item__descr">
						<span><a href="<?php bbp_topic_permalink( $parent ); ?>"><?php bbp_topic_title( $parent ); ?></a></span>
					</div>
				</div>
				<div class="clear"></div>
			</li>
		<?php
	}
	print '<input type="hidden" id="entryPosts" data-entry="0">';
	print '<input type="hidden" id="author_reply_us" data-author="'.$author_us.'">';
	print '<input type="hidden" id="author_reply" data-offset="'.($offset+10).'">';
}
add_action('wp_ajax_questions_page_author', 'questions_page_author');
add_action('wp_ajax_nopriv_questions_page_author', 'questions_page_author');

//view для вывода записей на главной
function front_news_list($posts) {

	foreach ($posts as $post) {
		//Получаем имя автора поста
		$author = $post->post_author;
		$author_name = get_userdata($author)->display_name;
		$author_link = get_userdata($author)->user_nicename;

		//Получаем все изображения поста
		$post_img = catch_that_image_all( $post->post_content );
			$count_img = count($post_img [1]);
			if ($count_img > 4) {
				$count_img -= 4;
			} else {
				$count_img = -1;
			}

		//Получаем категории поста
		$posttags = get_the_category($post->ID);
		//Инициализируем пустой массив
		$posttags_x = array();
		if ($posttags) {
		  foreach($posttags as $tag) {
		  	//Записываем значени в массив
		  	array_push($posttags_x, '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>');
		  }
		}
		//Объединяем элементы массива в строку, через разделитель - "запятую"
		$posttags = implode(", ", $posttags_x);

		//Получаем количество комментариев
		//$count_comments = get_comments_number( $post->ID );
		//pr($count_comments);
		//$count_comments = count(get_comments($post->ID, array('comment_approved'=>1)));
		//pr($count_comments);
		//$count_comments = $posts->comment_count;
		$cc = count(get_comments(array('comment_approved'=>1,'post_id'=>$post->ID)));

		//Получаем аватар
		$ava = get_avatar(get_userdata($author), 30);

		//Получаем контент поста
		$content = $post->post_content;

		//Преобразуем контент для вывода
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);

		preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
		$content = str_replace($found[0], '', $content);

		$allowed_html = array(
			//'br' => array(),
			//'em' => array(),
			//'p' => array()
		);

		//Аватар автора
		$avatar = get_avatar( $author, 30);

		$content = wp_kses($content, $allowed_html);
		if (iconv_strlen($content,'UTF-8') > 200) {
			$content = mb_substr($content, 0, 200).'...';
			$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово, ибо оно в 99% случаев неполное
		}
		$content = $content.'...';
		?>
		<li class="vis" style="margin-bottom:20px;list-style: none;">
			<div class="postMain">
				<div class="postMain__main">
					<a href="<?=the_permalink($post->ID)?>"><img src="<?=$post_img [1] [0]?>"></a>
				<div class="postMain__author">
					<div class="postMain__author__img">
						<?=$avatar?>
						<a href="/author/<?=$author_link?>"><?=$author_name?></a>
					</div>
				</div>
				
				<div class="postMain__title">
					<div class="postMain__title__text">
						<div class="postMain__date"><?=rdate('d M Y', strtotime($post->post_date))?>г.</div>
						<a href="<?=the_permalink($post->ID)?>"><?=$post->post_title?></a>
					</div>
					<div class="postMain__additional">
						<div class="postMain__likes"><?=post_likes_count_view($post->ID)?></div>
						<div class="postMain__comment"><?=$cc?></div>
					</div>
				</div>
				</div>
				<div class="postMain__images">
				<?php
				for ($i = 1; $i <= 3; $i++) {
					if ($post_img [1] [$i] != ''){
				    	?>
				    		<div class="postMain__image"><a href="<?=the_permalink($post->ID)?>"><img src="<?=$post_img [1] [$i]?>"></a></div>
				    	<?php
				    }
				}
				if ($count_img >= 0) {
					?>
					<div class="postMain__images__link"><a href="<?=the_permalink($post->ID)?>"><p>Ещё</p><span><?=$count_img?></span><span>фото</span></a></div>
					<?php
				}
				?>
				</div>
			</div>
			<div class="postFooter">
				<div class="postFooter__descr">
					<div class="postFooter__tags">
						теги:  <?=$posttags?>
					</div>
					<div class="postFooter__text"><?=$content?></div>
				</div>
				<div class="postFooter__link">
					<a href="<?=the_permalink($post->ID)?>">Читать полностью</a>
				</div>
			</div>
		</li>
	<?php
	//print '<input class="more__posts" type="hidden" data-offset="'.($offset+5).'">';
	}
	wp_reset_postdata();
}

//view для вывода записей на странице автора
function author_news_list($posts) {

	foreach ($posts as $post) {
		//Получаем имя автора поста
		$author = $post->post_author;
		$author_name = get_userdata($author)->display_name;
		$author_link = get_userdata($author)->user_nicename;

		//Получаем все изображения поста
		$post_img = catch_that_image_all( $post->post_content );
			$count_img = count($post_img [1]);
			if ($count_img > 4) {
				$count_img -= 4;
			} else {
				$count_img = -1;
			}

		/*$childrens = get_children( array( 
			'post_parent' => $post->ID,
			'post_type'   => 'attachment', 
			'numberposts' => -1
		) );

		if( $childrens ){
			$post_img = array();
			foreach( $childrens as $children ){
				array_push($post_img, $children->guid);
			}
		}*/

		//Получаем категории поста
		$posttags = get_the_category($post->ID);
		//Инициализируем пустой массив
		$posttags_x = array();
		if ($posttags) {
		  foreach($posttags as $tag) {
		  	//Записываем значени в массив
		  	array_push($posttags_x, '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>');
		  }
		}
		//Объединяем элементы массива в строку, через разделитель - "запятую"
		$posttags = implode(", ", $posttags_x);

		//Получаем количество комментариев
		$count_comments = get_comments_number( $post->ID );
		//$count_comments = $posts->comment_count;

		//Получаем аватар
		$ava = get_avatar(get_userdata($author), 30);

		//Получаем контент поста
		$content = $post->post_content;

		//Преобразуем контент для вывода
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);

		preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
		$content = str_replace($found[0], '', $content);
		$content = str_replace('&nbsp;', '', $content);

		$allowed_html = array(
			//'br' => array(),
			//'em' => array(),
			//'strong' => array(),
			//'p' => array()
		);

		//Аватар автора
		$avatar = get_avatar( $author, 30);

		$content = wp_kses($content, $allowed_html);
		if (iconv_strlen($content,'UTF-8') > 200) {
			$content = mb_substr($content, 0, 200).'...';
			$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово, ибо оно в 99% случаев неполное
		}
		$content = $content.'...';
		?>
		<li class="vis" style="margin-bottom:20px;list-style: none;position:relative;">
			<?php 
				if(is_user_logged_in()) {
					if(get_current_user_role() != 'expert') { 
						if(get_post_meta($post->ID, 'frontend', true) == 1 && get_current_user_role() != 'Administrator'){
			?>
				<div class="buttons_record">
					<form id="buttons_record__form" method="get" target="_blank" action="/update-record">
						<input type="submit" value="Редактировать">
						<input type="hidden" name="page_value" value="<?=$post->ID?>">
					</form>
					<form id="buttons_record__form_del" method="get" target="_blank" action="/delete-record">
						<input type="submit" value="X">
						<input type="hidden" name="page_value" value="<?=$post->ID?>">
					</form>
				</div>
			<?php }}} ?>
			<?php 
				if(get_current_user_role() == 'Administrator') {
			?>
				<div class="buttons_record">
					<form id="buttons_record__form" method="get" target="_blank" action="/update-record">
						<input type="submit" value="Редактировать">
						<input type="hidden" name="page_value" value="<?=$post->ID?>">
					</form>
					<form id="buttons_record__form_del" method="get" target="_blank" action="/delete-record">
						<input type="submit" value="X">
						<input type="hidden" name="page_value" value="<?=$post->ID?>">
					</form>
				</div>
			<?php } ?>
			<div class="postMain">
				<div class="postMain__main">
					<a href="<?=the_permalink($post->ID)?>"><img src="<?=$post_img [1] [0]?>"></a>
				<div class="postMain__author">
					<div class="postMain__author__img">
						<?=$avatar?>
						<a href="/author/<?=$author_link?>"><?=$author_name?></a>
					</div>
				</div>
				<?php if($post->post_status == 'pending'){ ?>
					<div class="postPend">
						<div class="postPend__text">Статья проходит модерацию.</div>
					</div>
				<?php } ?>
				<div class="postMain__title">
					<div class="postMain__title__text">
						<div class="postMain__date"><?=rdate('d M Y', strtotime($post->post_date))?>г.</div>
						<a href="<?=the_permalink($post->ID)?>"><?=$post->post_title?></a>
					</div>
					<div class="postMain__additional">
						<div class="postMain__likes"><?=post_likes_count_view($post->ID)?></div>
						<div class="postMain__comment"><?=$count_comments?></div>
					</div>
				</div>
				</div>
				<div class="postMain__images">
				<?php
				for ($i = 1; $i <= 3; $i++) {
					if ($post_img [1] [$i] != ''){
				    	?>
				    		<div class="postMain__image"><a href="<?=the_permalink($post->ID)?>"><img src="<?=$post_img [1] [$i]?>"></a></div>
				    	<?php
				    }
				}
				if ($count_img >= 0) {
					?>
					<div class="postMain__images__link"><a href="<?=the_permalink($post->ID)?>"><p>Ещё</p><span><?=$count_img?></span><span>фото</span></a></div>
					<?php
				}
				?>
				</div>
			</div>
			<div class="postFooter">
				<div class="postFooter__descr">
					<div class="postFooter__tags">
						теги:  <?=$posttags?>
					</div>
					<div class="postFooter__text"><?=$content?></div>
				</div>
				<div class="postFooter__link">
					<a href="<?=the_permalink($post->ID)?>">Читать полностью</a>
				</div>
			</div>
		</li>
	<?php
	}
}

/*function testtest(){
	global $post;
	$posts = get_posts(array(
		'posts_per_page' => -1, //numberposts
	    'post_type' =>  'post',
	    'orderby' => 'post_date',
	    'post_status' => 'publish',
	    'order' => 'DESC'
	) );
	foreach ($posts as $post) {
		update_post_meta($post->ID, 'interest', '0');
	}
}
add_action('wp_head', 'testtest');*/

//Новые статьи на главной
function front_new_posts($offset) {
	
	$offset = $_POST['offset'];

	$posts_int = get_posts(array(
		'posts_per_page' => -1, //numberposts
	    'post_type' =>  'post',
	    'orderby' => 'post_date',
	    'meta_key' => 'interest',
	    'meta_value' => '1',
	    'post_status' => 'publish',
	    'order' => 'DESC'
	) );

	if(!$offset){
		$offset = $posts_int ? '0' : '3'; //Если есть интересные записи, то пропускаем 0 записей
	}

	$posts = get_posts(array(
		'posts_per_page' => 5, //numberposts
	    'post_type' =>  'post',
	    'offset' => $offset,
	    'meta_query' => array(
	    	array(
	    		'key'  => 'interest'
	    	)
	    ),
	    'orderby' => 'meta_value_num post_date',
	    'post_status' => 'publish',
	    'order' => 'DESC'
	) );

	$count = count($posts);

	if(!$posts) {
		print '<div style="font:0px Verdana;" class="noposts">Нет записей с пометкой "Новое"</div>';
	}

	front_news_list($posts);
	print '<input class="more__posts" type="hidden" data-offset="'.($offset+5).'">';
}

//Самые обсуждаемые статьи, по кол-ву комментариев
function front_popular_posts($offset) {
	global $wpdb;
	$posts_db = $wpdb->prefix.posts;

	$offset = $_POST['offset'];
	if(!$offset) { $offset = 0; }

	$sql = 
	"
		SELECT * FROM ".$posts_db." WHERE `comment_count` > '0' ORDER BY `comment_count` DESC LIMIT ".$offset.", 5 ;
	";
	$posts = $wpdb->get_results($sql);

	/*$posts = get_posts(array(
		'numberposts' => 5,
	    'post_type'  =>  'post',
	    'offset'          => $offset,
	    'orderby'=>'comment_count',
	    'post_status'     => 'publish',
	    'order'=>'DESC'
	) );*/

	$count = count($posts);

	if(!$posts) {
		print '<div style="font:0px Verdana;" class="noposts">Нет записей с пометкой "Обсуждаемые"</div>';
	}

	front_news_list($posts);
	print '<input class="more__posts" type="hidden" data-offset="'.($offset+5).'">';
}

//Вкладка личный опыт, вывод статей на главной
function front_private_posts($offset) {
	
	$offset = $_POST['offset'];

	if(!$offset) { $offset = 0; }

	$posts = get_posts(array(
		'numberposts' => 5,
	    'post_type'  =>  'post',
	    'offset'          => $offset,
	    'meta_key' => 'private',
	    'meta_value' => '1',
	    'post_status'     => 'publish',
	    'order'=>'DESC'
	) );

	$count = count($posts);

	if(!$posts) {
		print '<div style="font:0px Verdana;" class="noposts">Нет записей с пометкой "Личный опыт"</div>';
	}

	front_news_list($posts);
	print '<input class="more__posts" type="hidden" data-offset="'.($offset+5).'">';
}

//Вкладка профессионалы, вывод статей на главной
function front_prof_posts($offset) {
	
	$offset = $_POST['offset'];

	if(!$offset) { $offset = 0; }

	$posts = get_posts(array(
		'numberposts' => 5,
	    'post_type'  =>  'post',
	    'offset'          => $offset,
	    'meta_key' => 'prof',
	    'meta_value' => '1',
	    'post_status'     => 'publish',
	    'order'=>'DESC'
	) );

	$count = count($posts);

	if(!$posts) {
		print '<div style="font:0px Verdana;" class="noposts">Нет записей с пометкой "Профессионалы"</div>';
	}

	front_news_list($posts);
	print '<input class="more__posts" type="hidden" data-offset="'.($offset+5).'">';
}

add_action('wp_ajax_front_new_posts', 'front_new_posts');
add_action('wp_ajax_nopriv_front_new_posts', 'front_new_posts');

add_action('wp_ajax_front_popular_posts', 'front_popular_posts');
add_action('wp_ajax_nopriv_front_popular_posts', 'front_popular_posts');

add_action('wp_ajax_front_private_posts', 'front_private_posts');
add_action('wp_ajax_nopriv_front_private_posts', 'front_private_posts');

add_action('wp_ajax_front_prof_posts', 'front_prof_posts');
add_action('wp_ajax_nopriv_front_prof_posts', 'front_prof_posts');

// Добавил Литкевич КВ 26.08.2016
add_shortcode('forum_last_questions', 'forum_last_questions'); 
add_shortcode('forum_last_answers', 'forum_last_answers'); 

function forum_last_answers()
{
    forum_last_reply(30);
}

function forum_last_questions()
{
    forum_last_new_reply(30);
}

//Функция добавления нового поста через фронтэнд
function adding_author_post() {

	$current_user_role = get_current_user_role();//Хз для чего получаю это значение, вдруг кому-нибудь пригодится

	if(!is_user_logged_in()){//Если юзер не залогинен, то отправляем это сделать
		print '<div class="nonauth">Для того, чтобы добавлять статьи, <a class="auth_button">войдите</a> в учетную запись!</div>';
		return false;
	}

	//Определяем статус поста, для админов и редакторов "Опубликованный", для остальных "на утверждении"
	$post_status = (current_user_can('administrator') || current_user_can('editor')) ? 'publish' : 'pending';

	$user = get_current_user_id();//Айди текущего залогиненого юзера

	$redir = '/author/'.get_userdata($user)->user_nicename;

	$cats_all = get_categories($post_id, array('parent'=>0));
?>
	<form id="addPost" action="<?=$_SERVER['REQUEST_URI']?>" method="post">
			<input type="text" name="post_title" value="<?=$postTitle?>" placeholder="Название статьи"><br><br>
			<?php

			$settings = array(
				'media_buttons' => 1,
				'textarea_name' => 'postText',
				'textarea_rows' => 50,
				'editor_height' => 600,
				'quicktags' => false,
				'dfw' => true,
				'tinymce' => array(
					'toolbar1'=> 'bold,italic,strikethrough,bullist,numlist,blockquote,hr,alignleft,aligncenter,alignright,link,unlink,image,media,price',
					'toolbar2'=> ''
				)
			);
			$postContent = '';
			if(isset($_POST['postText']) && !empty($_POST['postText'])) {
				$postContent = $_POST['postText'];
			} else {
				$postContent = $content;
			}
			print '<div class="add_post_editor">';
			wp_editor($postContent, 'addPostEditor', $settings);
			print '</div>';
			print '<br>';
			?>
			<select class="select" multiple name="cat[]">
				<option disabled>Выберите категории статьи ("можно выбрать несколько")</option>
				<?php
				foreach ($cats_all as $key) { //Первый уровень вложенности категорий
					$link = $key->slug;
					if($key->parent == 0){
						?><option value="<?=$key->term_id?>"><div class="testclass"><?=$key->name?></div></option><?php
						$depth2 = get_categories($post_id, array('parent'=>$key->term_id));
						foreach ($depth2 as $key2) {//Второй уровень вложенности категорий
							if($key2->parent == $key->term_id) {
								?><option style="padding-left:20px;" value="<?=$key2->term_id?>"><div class="testclass"><?=$key2->name?></div></option><?php
								$depth3 = get_categories($post_id, array('parent'=>$key2->term_id));
								foreach ($depth3 as $key3) {//Третий уровень вложенности категорий
									if($key3->parent == $key2->term_id) {
										?><option style="padding-left:40px;" value="<?=$key3->term_id?>"><div class="testclass"><?=$key3->name?></div></option><?php
										$depth4 = get_categories($post_id, array('parent'=>$key3->term_id));
										foreach ($depth4 as $key4) {//Четвертый уровень вложенности категорий
											if($key4->parent == $key3->term_id) {
												?><option style="padding-left:60px;" value="<?=$key4->term_id?>"><div class="testclass"><?=$key4->name?></div></option><?php
												$depth5 = get_categories($post_id, array('parent'=>$key4->term_id));
												foreach ($depth5 as $key5) {//Пятый уровень вложенности категорий
													if($key5->parent == $key4->term_id) {
														?><option style="padding-left:80px;" value="<?=$key5->term_id?>"><div class="testclass"><?=$key5->name?></div></option><?php
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				?>
			</select>
			<br><br>
			<input type="hidden" name="edit_post_id" value="<?php if(isset($post_id)) print $post_id; else print '0'; ?>">
			<input type="submit" value="Добавить статью">
		</form>

<?php
	//Добавляем пост
	$post_data = array(
	  'post_title'    => wp_strip_all_tags( $_POST['post_title'] ),
	  'post_content'  => $_POST['postText'],
	  'post_status'   => $post_status,
	  'post_author'   => $user,
	  'post_type' => 'post',
	  'post_category' => $_POST['cat']
	);
	$post_id = wp_insert_post( $post_data );

	update_post_meta($post_id, 'private', '0');
	update_post_meta($post_id, 'frontend', '1');

	if($post_id){
		wp_redirect($redir);
	}
	/*?>
		<style>
			#category_id {
				width: 100%;
				font: 13px/13px cuprumItalic!important;
				padding: 4px 5px;
				border: 1px solid #cccccc;
				border-radius: 3px;
				margin-right: 15px;
				color: #282828;
				outline: none;
			}
		</style>
	<?php*/
}
add_shortcode( 'addPost', 'adding_author_post' );

//Функция редактирования поста через фронтэнд
function updates_author_post() {

	if(!is_user_logged_in()){//Если юзер не залогинен, то отправляем это сделать
		print '<div class="nonauth">Для того, чтобы редактировать статьи, <a class="auth_button">войдите</a> в учетную запись!</div>';
		return false;
	}

	$user = get_current_user_id();//Айди текущего залогиненого юзера

	$post_id = $_GET['page_value']; //Получаем id поста
	$post = get_post($post_id); //Получаем пост по id

	$author = $post->post_author;

	if(get_current_user_role() != 'Administrator'){
		if(get_post_meta($post_id, 'frontend', true) != 1){
			print '<div class="nofront">Вы не можете редактированить эту статью!</div>';
			return false;
		}
	}

	if(get_current_user_role() == 'Expert') {
		print '<div class="nofront">У вас нет прав, на редактирование статьи!</div>';
		return false;
	}

	//Админы могут всё, остальные только свои посты редактировать
	if(get_current_user_role() != 'Administrator'){
		if($post->post_author != $user){ print '<div style="font:14px Verdana;padding:20px 0;">Вы не можете редактировать эту статью!</div>'; return false; }
	}

	//Определяем статус поста, для админов и редакторов "Опубликованный", для остальных "на утверждении"
	$post_status = (current_user_can('administrator') || current_user_can('editor')) ? 'publish' : 'pending';

	$redir = '/author/'.get_userdata($user)->user_nicename;

	//Получаем категории поста
	$cats_post = get_the_category($post_id);
	$cats_post_item = array();
	foreach ($cats_post as $cat) {
		array_push($cats_post_item, $cat->term_id);//Пишем категории в массив
	}
	$cats_all = get_categories($post_id, array('parent'=>0));//Все категории
	?>
	<form id="addPost" action="<?=$_SERVER['REQUEST_URI']?>" method="post">
			<input type="text" name="post_title" value="<?=$post->post_title?>" placeholder="Название статьи"><br><br>
			<?php
			$settings = array(
				'media_buttons' => 1,
				'textarea_name' => 'postText',
				'textarea_rows' => 50,
				'editor_height' => 600,
				'quicktags' => false,
				'dfw' => true,
				'tinymce' => array(
					'toolbar1'=> 'bold,italic,strikethrough,bullist,numlist,blockquote,hr,alignleft,aligncenter,alignright,link,unlink,image,media,price',
					'toolbar2'=> ''
				)
			);
			$postContent = '';
			if(isset($_POST['postText']) && !empty($_POST['postText'])) {
				$postContent = $_POST['postText'];
			} else {
				$postContent = $post->post_content;
			}
			print '<div class="add_post_editor">';
			wp_editor($postContent, 'addPostEditor', $settings);
			print '</div>';
			print '<br>';
			?>
			<select class="select" multiple name="cat[]">
				<option disabled>Выберите категории статьи ("можно выбрать несколько")</option>
				<?php
				foreach ($cats_all as $key) { //Первый уровень вложенности категорий
					$link = $key->slug;
					if($key->parent == 0){
						?><option <?php if(in_array($key->term_id, $cats_post_item)) {print 'selected';} ?> value="<?=$key->term_id?>"><div class="testclass"><?=$key->name?></div></option><?php
						$depth2 = get_categories($post_id, array('parent'=>$key->term_id));
						foreach ($depth2 as $key2) {//Второй уровень вложенности категорий
							if($key2->parent == $key->term_id) {
								?><option <?php if(in_array($key2->term_id, $cats_post_item)) {print 'selected';} ?> style="padding-left:20px;" value="<?=$key2->term_id?>"><div class="testclass"><?=$key2->name?></div></option><?php
								$depth3 = get_categories($post_id, array('parent'=>$key2->term_id));
								foreach ($depth3 as $key3) {//Третий уровень вложенности категорий
									if($key3->parent == $key2->term_id) {
										?><option <?php if(in_array($key3->term_id, $cats_post_item)) {print 'selected';} ?> style="padding-left:40px;" value="<?=$key3->term_id?>"><div class="testclass"><?=$key3->name?></div></option><?php
										$depth4 = get_categories($post_id, array('parent'=>$key3->term_id));
										foreach ($depth4 as $key4) {//Четвертый уровень вложенности категорий
											if($key4->parent == $key3->term_id) {
												?><option <?php if(in_array($key4->term_id, $cats_post_item)) {print 'selected';} ?> style="padding-left:60px;" value="<?=$key4->term_id?>"><div class="testclass"><?=$key4->name?></div></option><?php
												$depth5 = get_categories($post_id, array('parent'=>$key4->term_id));
												foreach ($depth5 as $key5) {//Пятый уровень вложенности категорий
													if($key5->parent == $key4->term_id) {
														?><option <?php if(in_array($key5->term_id, $cats_post_item)) {print 'selected';} ?> style="padding-left:80px;" value="<?=$key5->term_id?>"><div class="testclass"><?=$key5->name?></div></option><?php
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				?>
			</select>
			<br><br>
			<input type="hidden" name="edit_post_id" value="<?php if(isset($post_id)) print $post_id; else print '0'; ?>">
			<input type="submit" value="Сохранить">
		</form>

<?php
	//Обновляем пост
	//После редактирования поста пользователем, пост приобретает статус "Черновик".
	$post_data = array(
	  'ID' => $_POST['edit_post_id'],
	  'post_title'    => wp_strip_all_tags( $_POST['post_title'] ),
	  'post_content'  => $_POST['postText'],
	  'post_status'   => $post_status,
	  'post_author'   => $author,
	  'post_category' => $_POST['cat']
	);
	$post_id = wp_insert_post( $post_data );

	if($post_id) {
		wp_redirect($redir);
	}
}
add_shortcode( 'updatePost', 'updates_author_post' );

//Функция удаления поста через фронтэнд
function delete_author_post() {
	$post_id = $_GET['page_value'];

	$user = get_current_user_id();
	$au_link = get_userdata($user)->user_nicename;

	$post = get_post($post_id);

	if(!current_user_can('administrator') && !current_user_can('editor')){
		if($post->post_author != $user){ print '<div style="font:14px Verdana;padding:20px 0;">Вы не можете удалить эту статью!</div>'; return false; }
	}
	?>
		<div style="font:14px Verdana;padding:10px 0;"><?=$post->post_title?></div>
		<form id="addPost" action="<?=$_SERVER['REQUEST_URI']?>" method="post">
			<input type="hidden" name="edit_post_id" value="<?php if(isset($post_id)) print $post_id; else print '0'; ?>">
			<input type="submit" value="Удалить статью">
		</form>
	<?php
	if($_POST['edit_post_id']){
		wp_trash_post($_POST['edit_post_id']);
		wp_redirect(home_url().'/'.$au_link, 301);
	}
}
add_shortcode( 'deletePost', 'delete_author_post' );

function page_view_all_forums($posts) {
	if($posts){
		$count = 0;
		foreach ($posts as $post) {
			$meta = get_post_meta($post->ID);

			$topic_id    = $post->ID;
			$author_link = '';

			//Все метаданные поста
			$meta = get_post_meta($topic_id);

			//Автор вопроса
			$quest_author = get_userdata($post->post_author);
			$quest_author = $quest_author->display_name;

			//Просмотры
			$wiews = $meta['post_views'][0];
			if(!$wiews){
				$wiews = 0;
			}

			//Ответы
			$reply_count = $meta['_bbp_reply_count'][0];

			//Вывод первой записи ответа
			//Получение контента и записывание его в буфер
			ob_start();
			bbp_topic_content($topic_id);
			$content = ob_get_clean();
			//Преобразуем контент для вывода
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]>', $content);
			preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
			$content = str_replace($found[0], '', $content);
			$content = str_replace('&nbsp;', '', $content);
			$allowed_html = array(
				//'br' => array(),
				//'em' => array(),
				//'strong' => array(),
				//'p' => array()
			);
			$content = wp_kses($content, $allowed_html);
			//Обрезаем строку до необходимой длины
			if (iconv_strlen($content,'UTF-8') > 135) {
				$content = mb_substr($content, 0, 135).'...';
				$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово, ибо оно в 99% случаев неполное
				$content = $content.'...';
			}

			//Получаем айди последнего ответа к вопросу
			$last_reply_id = $meta['_bbp_last_reply_id'][0];

			//Получение автора последнего ответа и записывание его в буфер
			ob_start();
			bbp_author_link( array( 'post_id' => $last_reply_id, 'size' => 1 ) );
			$reply_user = ob_get_clean();
			$reply_user = apply_filters('the_content', $reply_user);
			$reply_user = str_replace(']]>', ']]>', $reply_user);
			$reply_user = str_replace($found[0], '', $reply_user);
			$allowed_html = array(
				'br' => array(),
				'em' => array(),
				'strong' => array(),
				'p' => array()
			);
			$reply_user = wp_kses($reply_user, $allowed_html);

			$author_reply_id = bbp_get_reply_author_id($last_reply_id);
			$ava = get_avatar($author_reply_id, 35);

			$author_id = bbp_get_topic_author_id($topic_id);
			$ava_author = get_avatar($author_id, 35);

			$topic_time = rdate('d M Y - H:i', strtotime($post->post_date));

			$id_bbp_last_reply_id = $meta['_bbp_last_reply_id'][0];
			$post_reply = get_post($id_bbp_last_reply_id);
			//$post_reply = apply_filters('the_content', $post_reply->post_content);
			if($meta['_bbp_last_reply_id'][0] != 0){
				if (iconv_strlen($post_reply->post_content,'UTF-8') > 200) {
					$content_reply = mb_substr($post_reply->post_content, 0, 200).'...';
					$content_reply = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content_reply); //убираем последнее слово, ибо оно в 99% случаев неполное
					$content_reply = $content_reply.'...';
					$content_reply = strip_tags($content_reply, '<a><img><iframe>');
				} else {
					$content_reply = $post_reply->post_content;
				}
			}

			if($reply_user) {
				$bb_topic_last_time = time_as_vk(get_post($id_bbp_last_reply_id)->post_date);
			} else {
				$bb_topic_last_time = time_as_vk(get_post($topic_id)->post_date);
			}
			?>

			<li class="questionsFront__item">

				<div class="questionsFront__item__left">
						<div class="questionsFront__item__views"><b><?=$reply_count?></b> ответов</div>
						<div class="questionsFront__item__answers"><b><?=$wiews?></b> просмотров</div>
				</div>

				<div class="questionsFront__item__right">
					<?php if($reply_user) { ?>
						<a href="<?php bbp_topic_permalink( $topic_id ); ?>#post-<?=$last_reply_id?>">
						Последний<br> комментарий от
						<div class="questionsFront__item__expert">
							<div class="questionsFront__item__expert__avatar"><?=$ava?></div>
							<div class="questionsFront__item__expert__name"><?=$reply_user?></div>
						</div>
						<div class="last_time"><?php print $bb_topic_last_time; ?></div>
						</a>
						<div class="last_reply_comment"><?=$content_reply?></div>
					<?php } else { ?>
						<a href="<?php bbp_topic_permalink( $topic_id ); ?>#post-<?=$last_reply_id?>">
						Автор<br> вопроса
						<div class="questionsFront__item__expert">
							<div class="questionsFront__item__expert__avatar"><?=$ava_author?></div>
							<div class="questionsFront__item__expert__name"><?=$quest_author?></div>
						</div>
						<div class="last_time"><?php print $bb_topic_last_time; ?></div>
						</a>
					<?php } ?>
				</div>

				<div class="questionsFront__item__center">
					<a class="bbp-forum-title questionsFront__item__title" href="<?php bbp_topic_permalink( $topic_id ); ?>"><?php bbp_topic_title( $topic_id ); ?></a>
					<div class="questionsFront__item__descr"><?=$content?></div>
					<div class="questionsFront__item__bottom">
						<div class="questionsFront__item__tags">
							<a href="" class="questionsFront__item__tag"></a>
						</div>
						<div class="questionsFront__item__time">
							Вопрос добавлен: <?=$topic_time?><?//=$topic_time?>
						</div>
						<div class="questionsFront__item__answers"><b><?=$wiews?></b> просмотров</div>
					</div>
				</div>
				<div class="clear"></div>
			</li>
			<?php 
			// Reset the $post global
			wp_reset_postdata();
		}
	}
}

//Последние вопросы с форума
function forum_last_new_reply_str($offset) {

	$offset = $_POST['offset'];

	$offset = $offset ?: '0';

	$posts = get_posts( array(
		'numberposts' => 5,
		'offset' => $offset,
		'post_type'      => bbp_get_topic_post_type(),
		'post_status'    => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
		'ignore_sticky_posts' => true,
		'no_found_rows'       => true,
		'order'          => 'DESC',
		'exclude' => excludeHiddenTopic(),
		'orderby' => 'date'
	) );

	if($posts) {
		page_view_all_forums($posts);
	} else { return false; }
	print '<div class="load_more">Загрузить еще</div>';
	print '<input type="hidden" class="forum_offset_new" data-offset="'.($offset+5).'">';
}

function forum_last_reply_str($offset) {
	$offset = $_POST['offset'];

	$offset = $offset ?: '0';

	$posts = get_posts( array(
		'numberposts' => 5,
		'post_type'           => bbp_get_topic_post_type(),
		'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
		'offset' => $offset,
		'meta_key'            => '_bbp_last_active_time',
		'orderby'             => 'meta_value',
		'ignore_sticky_posts' => true,
		'no_found_rows'       => true,
		'order'               => 'DESC',
		'exclude' => excludeHiddenTopic(),
		'meta_query' => array(
			array(
				'key' => '_bbp_reply_count',
				'value' => '0',
				'compare' => '>'
			)
		)
	) );

	if($posts) {
		page_view_all_forums($posts);
	} else { return false; }
	print '<div class="load_more">Загрузить еще</div>';
	print '<input type="hidden" class="forum_offset_new" data-offset="'.($offset+5).'">';
}

function test_view_forum_str($offset) {
	$offset = $_POST['offset'];

	$offset = $offset ?: '0';

	$posts = get_posts( array(
		'numberposts' => 5,
		'offset' => $offset,
		'post_type'           => bbp_get_topic_post_type(),
		'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
		'order'               => 'DESC',
		'exclude' => excludeHiddenTopic(),
		'meta_query' => array(
			array(
				'key' => '_bbp_reply_count',
				'value' => '0'
			)
		)
	) );

	if($posts) {
		page_view_all_forums($posts);
	} else { return false; }
	print '<div class="load_more">Загрузить еще</div>';
	print '<input type="hidden" class="forum_offset_new" data-offset="'.($offset+5).'">';
}

function forum_count_reply_str($offset) {
	$offset = $_POST['offset'];

	$offset = $offset ?: '0';

	$posts = get_posts( array(
		'numberposts' => 5,
		'offset' => $offset,
		'post_type'           => bbp_get_topic_post_type(),
		'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
		'meta_key'            => '_bbp_reply_count',
		'orderby'             => 'meta_value_num',
		'ignore_sticky_posts' => true,
		'no_found_rows'       => true,
		'order'               => 'DESC',
		'exclude' => excludeHiddenTopic(),
		'meta_query' => array(
			array(
				'key' => '_bbp_reply_count',
				'value' => '0',
				'compare' => '>'
			)
		)
	) );

	if($posts) {
		page_view_all_forums($posts);
	} else { return false; }
	print '<div class="load_more">Загрузить еще</div>';
	print '<input type="hidden" class="forum_offset_new" data-offset="'.($offset+5).'">';
}

function forum_required_reply_str($offset) {
	global $wpdb;
	$offset = $_POST['offset'];

	$offset = $offset ?: '0';

	/*$posts = get_posts( array(
		'posts_per_page' => 5,
		'offset' => $offset,
		'post_type'           => bbp_get_topic_post_type(),
		'post_status'         => bbp_get_public_status_id(),
		'orderby'             => 'date',
		'order'               => 'DESC'
	) );*/
	$posts = $wpdb->get_results("SELECT post.* FROM $wpdb->posts post 
		LEFT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id 
		LEFT JOIN $wpdb->posts post2 ON post2.ID = meta.meta_value
		WHERE post.post_author = post2.post_author AND 
			post.post_type = 'topic' AND 
			post2.post_type = 'reply' AND
			(post2.post_status = 'publish' OR post2.post_status = 'closed') AND 
			(post.post_status = 'publish' OR post.post_status = 'closed') AND 
			meta.meta_key = '_bbp_last_reply_id'
			GROUP BY post.ID ORDER BY post2.post_date DESC LIMIT $offset, 5");

	if($posts) {
		forum_required_reply_view_str($posts);
	} else { return false; }
	print '<div class="load_more">Загрузить еще</div>';
	print '<input type="hidden" class="forum_offset_new" data-offset="'.($offset+5).'">';
}

add_action('wp_ajax_forum_last_new_reply_str', 'forum_last_new_reply_str');
add_action('wp_ajax_nopriv_forum_last_new_reply_str', 'forum_last_new_reply_str');

add_action('wp_ajax_forum_last_reply_str', 'forum_last_reply_str');
add_action('wp_ajax_nopriv_forum_last_reply_str', 'forum_last_reply_str');

add_action('wp_ajax_test_view_forum_str', 'test_view_forum_str');
add_action('wp_ajax_nopriv_test_view_forum_str', 'test_view_forum_str');

add_action('wp_ajax_forum_count_reply_str', 'forum_count_reply_str');
add_action('wp_ajax_nopriv_forum_count_reply_str', 'forum_count_reply_str');

add_action('wp_ajax_forum_required_reply_str', 'forum_required_reply_str');

function forum_required_reply_view_str($posts) {
	foreach ($posts as $post) {
		//$count_con++;
		//if($count_con == 5){break;}
		$meta = get_post_meta($post->ID);

		$topic_id    = $post->ID;
		$author_link = '';

		//Все метаданные поста
		$meta = get_post_meta($topic_id);

		//Автор вопроса
		$quest_author = get_userdata($post->post_author);
		$quest_author = $quest_author->display_name;

		//Просмотры
		$wiews = get_post_meta($topic_id, 'post_views', true);
		if(!$wiews){
			$wiews = 0;
		}

		//Ответы
		$reply_count = get_post_meta($topic_id, '_bbp_reply_count', true);

		//Вывод первой записи ответа
		//Получение контента и записывание его в буфер
		ob_start();
		bbp_topic_content($topic_id);
		$content = ob_get_clean();
		//Преобразуем контент для вывода
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);
		preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
		$content = str_replace($found[0], '', $content);
		$content = str_replace('&nbsp;', '', $content);
		$allowed_html = array(
			//'br' => array(),
			//'em' => array(),
			//'strong' => array(),
			//'p' => array()
		);
		$content = wp_kses($content, $allowed_html);
		//Обрезаем строку до необходимой длины
		if (iconv_strlen($content,'UTF-8') > 135) {
			$content = mb_substr($content, 0, 135).'...';
			$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово, ибо оно в 99% случаев неполное
			$content = $content.'...';
		}

		//Получаем айди последнего ответа к вопросу
		$last_reply_id = get_post_meta($topic_id, '_bbp_last_reply_id', true);

		//Получение автора последнего ответа и записывание его в буфер
		ob_start();
		bbp_author_link( array( 'post_id' => $last_reply_id, 'size' => 1 ) );
		$reply_user = ob_get_clean();
		$reply_user = apply_filters('the_content', $reply_user);
		$reply_user = str_replace(']]>', ']]>', $reply_user);
		$reply_user = str_replace($found[0], '', $reply_user);
		$allowed_html = array(
			'br' => array(),
			'em' => array(),
			'strong' => array(),
			'p' => array()
		);
		$reply_user = wp_kses($reply_user, $allowed_html);

		$author_reply_id = bbp_get_reply_author_id($last_reply_id);
		$ava = get_avatar($author_reply_id, 35);

		$author_id = $post->post_author;
		$ava_author = get_avatar($author_id, 35);

		$topic_time = rdate('d M Y - H:i', strtotime($post->post_date));

		$id_bbp_last_reply_id = get_post_meta($topic_id, '_bbp_last_reply_id', true);
		$post_reply = get_post($id_bbp_last_reply_id);
		
		if(get_post_meta($topic_id, '_bbp_last_reply_id', true) != 0){
			if (iconv_strlen($post_reply->post_content,'UTF-8') > 200) {
				$content_reply = mb_substr($post_reply->post_content, 0, 200).'...';
				$content_reply = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content_reply); //убираем последнее слово, ибо оно в 99% случаев неполное
				$content_reply = $content_reply.'...';
				$content_reply = strip_tags($content_reply, '<a><img><iframe>');
			} else {
				$content_reply = $post_reply->post_content;
			}
		}

		if($reply_user) {
			$bb_topic_last_time = time_as_vk(get_post($id_bbp_last_reply_id)->post_date);
		} else {
			$bb_topic_last_time = time_as_vk(get_post($topic_id)->post_date);
		}
		?>

		<li class="questionsFront__item">

			<div class="questionsFront__item__left">
					<div class="questionsFront__item__views"><b><?=$reply_count?></b> ответов</div>
					<div class="questionsFront__item__answers"><b><?=$wiews?></b> просмотров</div>
			</div>

			<div class="questionsFront__item__right">
				<?php if($reply_user) { ?>
					<a href="<?php bbp_topic_permalink( $topic_id ); ?>#post-<?=$last_reply_id?>">
					Последний<br> комментарий от
					<div class="questionsFront__item__expert">
						<div class="questionsFront__item__expert__avatar"><?=$ava?></div>
						<div class="questionsFront__item__expert__name"><?=$reply_user?></div>
					</div>
					<div class="last_time"><?php print $bb_topic_last_time; ?></div>
					</a>
					<div class="last_reply_comment"><?=$content_reply?></div>
				<?php } else { ?>
					<a href="<?php bbp_topic_permalink( $topic_id ); ?>#post-<?=$last_reply_id?>">
					Автор<br> вопроса
					<div class="questionsFront__item__expert">
						<div class="questionsFront__item__expert__avatar"><?=$ava_author?></div>
						<div class="questionsFront__item__expert__name"><?=$quest_author?></div>
					</div>
					<div class="last_time"><?php print $bb_topic_last_time; ?></div>
					</a>
				<?php } ?>
			</div>

			<div class="questionsFront__item__center">
				<a class="bbp-forum-title questionsFront__item__title" href="<?php bbp_topic_permalink( $topic_id ); ?>"><?php bbp_topic_title( $topic_id ); ?></a>
				<div class="questionsFront__item__descr"><?=$content?></div>
				<div class="questionsFront__item__bottom">
					<div class="questionsFront__item__tags">
						<a href="" class="questionsFront__item__tag"></a>
					</div>
					<div class="questionsFront__item__time">
						Вопрос добавлен: <?=$topic_time?><?//=$topic_time?>
					</div>
					<div class="questionsFront__item__answers"><b><?=$wiews?></b> просмотров</div>
				</div>
			</div>
			<div class="clear"></div>
		</li>
		<?php 
		// Reset the $post global
		wp_reset_postdata();
	}
}


function statistic_model(){

	global $post, $wpdb;
	$topics = $wpdb->get_var("select count(*) from $wpdb->posts WHERE `post_type`='topic' AND `post_status`='publish' OR `post_type`='topic' AND `post_status`='closed'");

	$replies = $wpdb->get_var("select count(*) from $wpdb->posts WHERE `post_type`='reply' AND `post_status`='publish'");
/*
	$comments_count = wp_count_comments();
*/



	$messages = (int) $topics+ (int)$replies;
	$users = $wpdb->get_var("select count(*) from $wpdb->users");

	$usernames = $wpdb->get_results("SELECT user_nicename, user_url, user_email, display_name FROM $wpdb->users ORDER BY ID DESC LIMIT 1");
	$last_user = $usernames[0];
	?>
	

	<ul>
		<li style="display: inline;">Всего сообщений: <b style="font-weight: bold;"><?=$messages?></b> </li>
		<li style="padding-left: 15px; display: inline;">Всего тем: <b style="font-weight: bold;"><?=$replies?></b></li>
		<li style="padding-left: 15px;display: inline;">Зарегистрировано пользователей: <b style="font-weight: bold;"><?=$users?></b></li>
		<li style="padding-left: 15px;display: inline;">Новый пользователь: <a style="font-weight: bold; color: #0171a1; text-decoration: underline;" href="/<?=$last_user->user_nicename?>"><?=$last_user->display_name?></b> </li>
	</ul>
	<?php
}

function statistic() {
	global $post, $wpdb;
	$count_posts = $wpdb->get_var("SELECT count(`ID`) FROM $wpdb->posts WHERE `post_type` = 'post' AND `post_status` = 'publish'");
	$stat_forum = bbp_get_statistics();//reply_count
	$topic = $wpdb->get_var("select count(*) from $wpdb->posts WHERE `post_type`='topic' AND `post_status`='publish' OR `post_type`='topic' AND `post_status`='closed'");
	$reply = $wpdb->get_var("select count(*) from $wpdb->posts WHERE `post_type`='reply' AND `post_status`='publish'");
	$comments_count = wp_count_comments();

	function get_reg_users() {
	  global $wpdb;
	  return $wpdb->get_var("select count(*) from $wpdb->users");
	}

	$user=get_reg_users();

	foreach ($stat_forum as $stat) {
		array_push($stat_forum_arr, $stat);
	}
	?>
	<div class="statistic_block">
		<div class="statistic_block__item">
			<img class="statistic_block__item-img" style="float: left;" src="<?=get_template_directory_uri()?>/img/fp1.png">
			<div class="statistic_block__item-number"><?php print $topic ?></div>
			<div>Вопросов</div>
		</div><div class="statistic_block__item">
			<img class="statistic_block__item-img" style="float: left;" src="<?=get_template_directory_uri()?>/img/fp2.png">
			<div class="statistic_block__item-number"><?php print $reply ?></div>
			<div>Ответов</div>
		</div><div class="statistic_block__item">
			<img class="statistic_block__item-img" style="float: left;" src="<?=get_template_directory_uri()?>/img/fp3.png">
			<div class="statistic_block__item-number"><?php print $count_posts; ?></div>
			<div>Постов в блогах</div>
		</div><div class="statistic_block__item">
			<img class="statistic_block__item-img" style="float: left;" src="<?=get_template_directory_uri()?>/img/fp4.png">
			<div class="statistic_block__item-number"><?php print $user; ?></div>
			<div>Пользователей</div>
		</div><div class="statistic_block__item">
			<img class="statistic_block__item-img" style="float: left;" src="<?=get_template_directory_uri()?>/img/fp5.png">
			<div class="statistic_block__item-number"><?php print $comments_count->approved; ?></div>
			<div>Комментариев в блогах</div>
		</div>
	</div>
	<?php
}

function create_fotogallery_author() {
	global $wpdb;
	$fotogallery = $wpdb->prefix.fotogallery;
	
	$sql = 
	"
		CREATE TABLE IF NOT EXISTS ".$fotogallery." (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`image_id` int(11) NULL,
			`date` datetime NOT NULL,
			PRIMARY KEY (`id`)
		)	ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	$wpdb->query($sql);

}
add_action('init', 'create_fotogallery_author');

function view_fotogallery_author($user_id) {
	global $wpdb;
	$fotogallery = $wpdb->prefix.fotogallery;
	
	$sql = 
	"
		SELECT `image_id` FROM ".$fotogallery." WHERE `user_id`='".$user_id."' ORDER BY `date` DESC;
	";
	$record = $wpdb->get_results($sql);

	$count = count($record);
	
	$count_number = '';
	foreach ($record as $foto) {
		$count_number++;
		if(/*$count > 4 && */$count_number == 4){
			break;
		}
		print wp_get_attachment_image($foto->image_id, array(90,90));
	}
	if($count/* > 4*/) {
		?>
		<form id="user_gallery" name="user_gallery" method="get" action="/user-gallery" target="_blank">
			<!--<div class="all_foto">
				<a href="/user-gallery" target="_blank">Все фото ('.$count.')</a>
			</div>-->
			<input type="submit" value="Все фото (<?=$count?>)">
			<input type="hidden" name="page_number" value="<?=$user_id?>">
		</form>
		<?php
	}
}

function view_page_fotogallery_author() {
	global $wpdb;
	$fotogallery = $wpdb->prefix.fotogallery;

	$user_id = $_GET['page_number'];
	
	$sql = 
	"
		SELECT `image_id` FROM ".$fotogallery." WHERE `user_id`='".$user_id."' ORDER BY `date` DESC;
	";
	$record = $wpdb->get_results($sql);

	print '<h1>Фотогалерея пользователя: '.get_user_meta($user_id, 'nickname', true).'</h1>';
	print '<div class="clear"></div>';

	foreach ($record as $foto) {
		print '<div class="images_gallery_item">';
		$pic = wp_get_attachment_image_src($foto->image_id, 'full');
		$src = $pic[0];
		if(get_current_user_id()==$user_id) {
			print '<form id="delete_foto" name="delete_foto" method="post"><input type="submit" value="X"><input type="hidden" name="deleteFoto" value="'.$user_id.'"><input type="hidden" name="deleteFotoId" value="'.$foto->image_id.'"></form>';
		}
		print '<a href="'.$src.'">';
		print wp_get_attachment_image($foto->image_id, array(180, 180));
		print '</a></div>';
	}
}

function foto_delete() {
	global $wpdb;
	$fotogallery = $wpdb->prefix.fotogallery;

	$user_id = $_POST['user_id'];
	$image_id = $_POST['image_id'];
	
	$wpdb->query(
	"
		DELETE FROM ".$fotogallery." WHERE `user_id`='".$user_id."' AND `image_id`='".$image_id."';
	");
	print 'Удалено!';
}
add_action('wp_ajax_foto_delete', 'foto_delete');

//Фотоголосование, шорткод: fotopolls_view
function create_fotopolls_table(){
	global $wpdb;
	$table = $wpdb->prefix.'fotopolls';

	$sql = 
	"
		CREATE TABLE IF NOT EXISTS ".$table." (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` text NOT NULL,
			`result` int(11),
			PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	$wpdb->query($sql);
}
add_action('init', 'create_fotopolls_table');

function fotopolls_view_container(){
	ob_start();
	bloginfo('stylesheet_directory');
	$directory = ob_get_clean();
	$choise = $choise ?: 0;
	?>
	<div class="fotopolls_view_container">
		<h1 class="fotopolls_view_header">какой фасад лучше?</h1>
		<form id="fotopolls__form" name="fotopolls__form" type="post">
			<div class="fotopolls_img"><img src="<?php print $directory; ?>/img/polls/image-1.jpg"><div class="fotopolls_view_button">Фиброцементный фасад</div></div>
			<div class="fotopolls_img"><img src="<?php print $directory; ?>/img/polls/image-2.jpg"><div class="fotopolls_view_button">Виниловый фасад</div></div>
			<div class="fotopolls_res">
			<?php fotopolls_view(); ?>
			</div>
			<?php /*<input type="submit" class="fotopolls_view__submit" value="Выбрать">*/?>
			<input type="hidden" id="fotopolls__choise" name="fotopolls__choise" value="<?php print $choise; ?>">
		</form>
	</div>
	<style>
		.fotopolls_view_container {
		    margin: 20px 0;
		    max-width: 670px;
		}
		.fotopolls_view_header {
		    font-family: Tahoma;
		    font-size: 16px;
		    font-weight: bold;
		    text-transform: uppercase;
		    text-align: center;
		    border: 1px solid rgba(0, 0, 0, 0.17);
		    padding: 17px;
		    margin-bottom: 14px;
		}
		.fotopolls_img {
		    display: inline-block;
		    width: 49.582%;
		    vertical-align: top;
		}
		.fotopolls_img img,
		.fotopolls_res {
		    width: 100%;
		}
		.fotopolls_view_button {
		    background-image: linear-gradient(to top, #dedede 19% , #fff 74%);
		    text-align: center;
		    border-radius: 5px;
		    padding: 12px;
		    margin: 19px 24px;
		    border: 1px solid #dedede;
		    box-shadow: 0px 3px 3px 1px rgba(0, 0, 0, 0.21);
		    font-size: 14px;
		    font-family: Tahoma;
		    font-weight: bold;
		    text-transform: uppercase;
		    color: #5c5c5c;
		    cursor: pointer;
		}
		.fotopolls_view_button.active {
		    padding: 12px;
		    box-shadow: inset 1px 5px 12px -4px rgba(0, 0, 0, 0.5);
		    border-radius: 4px;
		    background-image: linear-gradient(to top, #fff 19% , #dedede 74%);
		    position: relative;
    		bottom: -1px;
		}
		.persent_container__right {
		    float: right;
		}
		.fotopolls_view__submit {
		    background-color: #006c9e;
		    border: 0px;
		    color: #fff;
		    padding: 0px 10px;
		    cursor: pointer;
		    width: 215px;
		    height: 43px;
		    font: 18px Verdana;
		    text-transform: uppercase;
		}
		.fotopolls_view__submit:hover {
		    opacity: 0.8;
		}
	</style>
	<?php
}
add_shortcode('fotopolls_view', 'fotopolls_view_container');

function fotopolls_view(){
	global $wpdb;
	$table = $wpdb->prefix.'fotopolls';

	$user = get_current_user_id();

	if(!$user){
		fotopolls_view_results($table);
		print 'Авторизируйтесь на сайте, чтобы голосовать.';
		return false;
	}

	$sql = 
	"
		SELECT * FROM ".$table." WHERE `user_id`=".$user."
	";
	$result = $wpdb->get_results($sql);
	
	if(!$result){
		$sql = 
		"
			INSERT INTO ".$table." (`id`, `user_id`, `result`) VALUES (NULL, ".$user.", ".$_POST['choise'].");
		";
		$wpdb->query($sql);
		fotopolls_view_results($table);
	} else {
		$sql = 
		"
			UPDATE ".$table." SET `result`=".$_POST['choise']." WHERE `user_id`=".$user.";
		";
		$wpdb->query($sql);
		fotopolls_view_results($table);
		//print '<div style="font-size:0;">Вы уже голосовали.</div>';
	}
}

add_action('wp_ajax_fotopolls_view', 'fotopolls_view');
add_action('wp_ajax_nopriv_fotopolls_view', 'fotopolls_view');

function fotopolls_view_results($table){
	global $wpdb;
	$sql = 
	"
		SELECT `result` FROM ".$table."
	";
	$results = $wpdb->get_results($sql);
	$i = 0; $arr1 = array(); $arr2 = array();
	foreach ($results as $result) {
		$numeric = $result->result;
		if($numeric){
			$i++;
			//print 'Голос: '.$i.', фото: '.$numeric.'.<br>';
			if($numeric == 1){
				array_push($arr1, $numeric);
			}
			if($numeric == 2){
				array_push($arr2, $numeric);
			}
		}
	}
	$one = count($arr1);
	$two = count($arr2);
	$persent_1 = round($one/($one + $two)*100);
	$persent_2 = round(100 - $persent_1);
	print '<div class="persent_container"><span class="persent_container__left">'.$persent_1.'%</span><span class="persent_container__right">'.$persent_2.'%</span></div>';
	print '<div style="background-image: linear-gradient(to left, #83bc37 '.$persent_2.'% , #0171a1 0%);height:10px;margin-bottom:20px;"></div>';
}

//Дата последнего посещения сайта пользователем
function visiting_users(){
	if(is_user_logged_in()){
		$user = get_current_user_id();
		update_user_meta($user, 'last_visit', current_time('mysql'));
	}
}
add_action('init', 'visiting_users');
function visiting_users_view($user){
	$time = get_user_meta($user, 'last_visit', true);
	$difference = ((int)current_time('timestamp') - strtotime($time));
	if($time && $difference <= 60) {
		print 'Онлайн';
	}
	if($time && $difference > 60) {
		print 'Заходил '.time_as_vk($time);
		//echo rdate('d.m.Y', strtotime($time)); //d-m-Y H:i:s
	}
	if(!$time) {
		print 'Заходил очень давно.';
	}
}

function checkbot(){//Функция обновляет время последнего посещения ботам, которых писец как много
	/*$users = get_users();
	foreach ($users as $user => $value) {
		$id = $value->data->ID;
		$visit = get_user_meta($id, 'last_visit', true);
		if(!$visit){
			update_user_meta($id, 'last_visit', current_time('mysql'));
		}
	}*/
}
add_action('init', 'checkbot');

//Высчитывание количесва ответов на форуме, если пользователь не автор вопроса.
function count_user_reply_posts($user_id) {
	global $wpdb;
	$posts = $wpdb->prefix.posts;

	$sql = 
	"
		SELECT count(*) FROM ".$posts." WHERE `post_type`='reply' AND `post_author` = ".$user_id." AND `post_parent` NOT IN (SELECT `ID` FROM ".$posts." WHERE `post_type`='topic' AND `post_author` = ".$user_id.");
	";
	return $wpdb->get_var($sql);
}

//Создаем таблицу, для хранения все последних действий пользователей
function activity_feed_install(){
	global $wpdb;
	$feed = $wpdb->prefix.feed;
	$feedUsers = $wpdb->prefix.feedUsers;
	
	//Лента
	$wpdb->query("
		CREATE TABLE IF NOT EXISTS ".$feed." (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`post_id` int(11) NOT NULL,
			`date` datetime NOT NULL,
			`action` text NOT NULL,
			`post_name` text,
			`post_title` text NOT NULL,
			`type` text,
			`user_parent` int(11),
			PRIMARY KEY (`id`)
		)	ENGINE=InnoDB DEFAULT CHARSET=utf8;
	");

	//Права пользователя
	$wpdb->query("
		CREATE TABLE IF NOT EXISTS ".$feedUsers." (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`permission` text NULL,
			PRIMARY KEY (`id`)
		)	ENGINE=InnoDB DEFAULT CHARSET=utf8;
	");
}
add_action('init', 'activity_feed_install');

//Готовим к записи все действия пользователей
function activity_feed_processing() {
	global $wpdb;

	$prefix = $wpdb->prefix;
	$table_feed = $prefix.feed;
	$table_likes = $prefix.post_likes;
	$table_posts = $prefix.posts;
	$table_comments = $prefix.comments;
	$table_favorites = $prefix.post_favorites;
	$table_like_dislike_counters = $prefix.like_dislike_counters;

	//Получаем записи, которые добавили в "избранное"
	$posts_favorites = 
	"
		SELECT favorites.user_id, post.post_title, post.post_name, favorites.post_id, favorites.date, post.post_author
		FROM $table_posts post
		RIGHT JOIN $table_favorites favorites ON post.ID = favorites.post_id
		WHERE
			favorites.post_id NOT IN (SELECT `post_id` FROM $table_feed WHERE action='favorites' AND user_id=favorites.user_id AND post_id=favorites.post_id)
	";
	$posts_favorites = $wpdb->get_results($posts_favorites);
	foreach ($posts_favorites as $item) {
		$item->user = $item->user_id;
		$item->title = $item->post_title;
		$item->link = $item->post_name;
		$item->ID = $item->post_id;
		$item->action = 'favorites';
		$item->user_parent = $item->post_author;
    	unset($item->user_id,$item->post_title,$item->post_name,$item->post_id,$item->post_author);
	}
	sql_query_feed($posts_favorites);
}
add_action('wp_head', 'activity_feed_processing');

//Пишем данные в базу feed
function sql_query_feed($items){
	global $wpdb;

	$prefix = $wpdb->prefix;
	$table_feed = $prefix.feed;

	$sql = '';
	$sql .= 
	"
		INSERT INTO $table_feed
		(`id`, `user_id`, `post_id`, `date`, `action`, `post_name`, `post_title`, `type`, `user_parent`)
		VALUES ";
		foreach ($items as $item) {
			$sql .= "(NULL, ".$item->user.", ".$item->ID.", '".$item->date."', '".$item->action."', '".$item->link."', '".$item->title."', '".$item->post_type."', '".$item->user_parent."'),";
		}
		if ($sql{strlen($sql)-1} == ',') {
			$sql = substr($sql,0,-1);
		}
		$sql .= ";";
	$wpdb->query($sql);
}

//Удаляем лишние данные из базы feed
function sql_query_delete_feed(){
	global $wpdb;

	$table_feed = $wpdb->prefix.feed;
	$table_likes = $wpdb->prefix.post_likes;
	$table_favorites = $wpdb->prefix.post_favorites;
	$table_like_dislike_counters = $wpdb->prefix.like_dislike_counters;

	$posts = 
	"
		SELECT DISTINCT post.post_status, feed.ID, favorites.post_id AS favorites_id, feed.action, thanks.post_id AS thanks_id, like_comment.post_id AS like_comment_id, meta.meta_key AS meta 
		FROM
			$table_feed feed
		LEFT JOIN 
			$wpdb->posts post ON feed.post_id = post.ID
		LEFT JOIN
			$wpdb->postmeta meta ON feed.post_id = meta.post_id AND meta.meta_key LIKE 'bbpl_%'
		LEFT JOIN
			$table_like_dislike_counters like_comment ON feed.post_id = like_comment.post_id AND feed.action = like_comment.ul_key 
		LEFT JOIN
			$table_likes thanks ON feed.post_id = thanks.post_id 
		LEFT JOIN
			$table_favorites favorites ON feed.post_id = favorites.post_id 
	";
	$posts = $wpdb->get_results($posts);
	foreach ($posts as $post) {
		//Добавленное в избранное
		if($post->action == 'favorites') {
			if(!$post->favorites_id) {
				delete_row($post->ID);
			}
		}
	}
}
add_action('wp_head', 'sql_query_delete_feed');

function delete_row($id) {
	global $wpdb;
	$table = $wpdb->prefix.feed;
	$sql = $wpdb->query(
	"
		DELETE FROM
			$table
		WHERE
			ID = $id
	");
}

//Получаем данные для ленты новостей
function news_lenta() {
	global $wpdb, $wp_query;
	$table_feed = $wpdb->prefix."feed";
	$prefix = $wpdb->prefix;
	$table_feed = $prefix.feed;
	$table_likes = $prefix.post_likes;
	$table_posts = $prefix.posts;
	$table_comments = $prefix.comments;
	$table_favorites = $prefix.post_favorites;
	$table_like_dislike_counters = $prefix.like_dislike_counters;
	$fotogalleryTable = $prefix.'fotogallery';
	$feedUsers = $prefix.feedUsers;

	$user = get_current_user_id();

	//Пользовательские настройки
	$permission = $wpdb->get_row("SELECT `permission` FROM $feedUsers WHERE `user_id` = '$user'");
	$permission = unserialize($permission->permission);

	extract($_POST);

	$offset = $offset ? $offset : 0;
	$excl = excludeHiddenReply().','.excludeHiddenTopic();
	$excludeHiddenTopic = 'AND post.ID NOT IN ('.excludeHiddenTopic().')';
	$excludeHiddenAll = 'AND post.ID NOT IN ('.$excl.')';

	$curTime = mktime();//Текущее время в UNIX формате
	$postTime = date('Y-m-d H:i:s', $curTime - 86400 * get_option('postTime'));//Дата отчета ленты в формате UNIX
	$curTimeS = current_time('mysql');//Текущее время в mysql формате

	$datePosts = 'AND post.post_date BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\'';

	$union = '';

	$sortTopic = get_option('sortTopic');

	if($sortTopic == 'topic') {

		$postRe = "";
		$pDate = 'post.post_date';
		$where = "";

	} else if($sortTopic == 'reply') {

		$postRe = "LEFT JOIN $wpdb->posts postRe ON postRe.post_parent = post.ID";
		$pDate = 'MAX(postRe.post_date)';
		$where = "AND (postRe.post_status = 'publish' OR postRe.post_status = 'closed') GROUP BY post.ID ";

	} else {

		$postRe = '';
		$pDate = 'post.post_date';
		$where = "";

	}


	/**
	 * Дата вывода статей в ленту, по последнему комментарию
	 */
	$cmdate = "MAX(cm.comment_date)"; // post.post_date
	$cmjoin = "LEFT JOIN $wpdb->comments cm ON post.ID = cm.comment_post_ID";
	$cmgroup = "GROUP BY post.ID ";


	if(is_user_logged_in()){
		if(get_option('logNewQueston') && $permission['gNewReply'] == 'on'){//Новые вопросы
			$union .= "UNION SELECT DISTINCT post.post_author AS user_id, post.ID AS post_id, $pDate AS date, post.post_type AS action, CONCAT('topic/', post.ID) AS link, post.post_content AS post_title, post.post_type AS type, post.post_author AS user_parent FROM $wpdb->posts post $postRe WHERE post.post_type = 'topic' AND (post.post_status = 'publish' OR post.post_status = 'closed') $excludeHiddenTopic $datePosts $where";
		}
	} else {
		if(get_option('newQueston')){
			$union .= "UNION SELECT DISTINCT post.post_author AS user_id, post.ID AS post_id, $pDate AS date, post.post_type AS action, CONCAT('topic/', post.ID) AS link, post.post_content AS post_title, post.post_type AS type, post.post_author AS user_parent FROM $wpdb->posts post $postRe WHERE post.post_type = 'topic' AND (post.post_status = 'publish' OR post.post_status = 'closed') $excludeHiddenTopic $datePosts $where";
		}
	}

	$dateLike_dislike = 'AND like_dislike.time BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\'';
	if(is_user_logged_in()){
		if(get_option('logLikeComments') && $permission['gLikeCom'] == 'on'){//Нравится\не нравится комментарий
			if($permission['MyLikeCom'] == 'on') {
				$MyLikeCom = "AND comments.user_id NOT IN ($user)";
			}
			if($permission['yourLikeCom'] == 'on') {
				$yourLikeCom = "AND like_dislike.user_id NOT IN ($user)";
			}
			$union .= "UNION SELECT like_dislike.user_id AS user, comments.comment_ID AS post_id, like_dislike.time AS date, like_dislike.ul_key AS action, CONCAT(meta.meta_value, '/', post.post_name, '#comment-', comments.comment_ID) AS link, comments.comment_content AS title, like_dislike.ul_key AS type, comments.user_id AS user_parent FROM $table_like_dislike_counters like_dislike LEFT JOIN $table_comments comments ON comments.comment_ID = like_dislike.post_id LEFT JOIN  $table_posts post ON like_dislike.post_parent_id = post.ID LEFT JOIN $wpdb->usermeta meta ON post.post_author = meta.user_id WHERE like_dislike.post_id NOT IN (SELECT `post_id` FROM $table_feed WHERE action=like_dislike.ul_key AND user_id=like_dislike.user_id AND post_id=like_dislike.post_id) AND meta.meta_key = 'nickname' AND comments.comment_approved  = '1' $MyLikeCom $yourLikeCom $dateLike_dislike";
		}
	} else {
		if(get_option('likeComments')){
			$union .= "UNION SELECT like_dislike.user_id AS user, comments.comment_ID AS post_id, like_dislike.time AS date, like_dislike.ul_key AS action, CONCAT(meta.meta_value, '/', post.post_name, '#comment-', comments.comment_ID) AS link, comments.comment_content AS title, like_dislike.ul_key AS type, comments.user_id AS user_parent FROM $table_like_dislike_counters like_dislike LEFT JOIN $table_comments comments ON comments.comment_ID = like_dislike.post_id LEFT JOIN  $table_posts post ON like_dislike.post_parent_id = post.ID LEFT JOIN $wpdb->usermeta meta ON post.post_author = meta.user_id WHERE like_dislike.post_id NOT IN (SELECT `post_id` FROM $table_feed WHERE action=like_dislike.ul_key AND user_id=like_dislike.user_id AND post_id=like_dislike.post_id) AND meta.meta_key = 'nickname' AND comments.comment_approved  = '1' $dateLike_dislike";
		}
	}

	$dateMeta = 'AND meta.meta_value BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\'';
	if(is_user_logged_in()){
		if(get_option('logLikeForum') && $permission['gLikeReply'] == 'on'){//Нравится\не нравится ответ или вопрос
			if($permission['MyLikeReply'] == 'on') {
				$MyLikeReply = "AND post.post_author NOT IN ($user)";
			}
			if($permission['YourLikeReply'] == 'on') {
				$YourLikeReply = "AND SUBSTRING_INDEX(meta.meta_key, '_', -1) NOT IN ($user)";
			}
			$union .= "UNION SELECT SUBSTRING_INDEX(meta.meta_key, '_', -1) AS user, post.ID AS post_id, meta.meta_value AS date, SUBSTRING_INDEX(meta.meta_key, '_', 2) AS action, post.ID AS link, post.post_content AS title, post.post_type AS type, post.post_author AS user_parent FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id WHERE (meta.meta_key LIKE 'bbpl_like%' OR meta.meta_key LIKE 'bbpl_dislike%') AND (post.post_status = 'publish' OR post.post_status = 'closed') AND (post.post_type = 'topic' OR post.post_type = 'reply') AND post.ID NOT IN (SELECT `post_id` FROM v7ub9zxxg_feed WHERE (action LIKE 'bbpl_like%' OR action LIKE 'bbpl_dislike%') AND user_id=CAST(SUBSTRING_INDEX(meta.meta_key, '_', -1) AS UNSIGNED INTEGER) AND post_id=post.ID) $MyLikeReply $YourLikeReply $excludeHiddenAll $dateMeta";
		}
	} else {
		if(get_option('likeForum')){
			$union .= "UNION SELECT SUBSTRING_INDEX(meta.meta_key, '_', -1) AS user, post.ID AS post_id, meta.meta_value AS date, SUBSTRING_INDEX(meta.meta_key, '_', 2) AS action, post.ID AS link, post.post_content AS title, post.post_type AS type, post.post_author AS user_parent FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id WHERE (meta.meta_key LIKE 'bbpl_like%' OR meta.meta_key LIKE 'bbpl_dislike%') AND (post.post_status = 'publish' OR post.post_status = 'closed') AND (post.post_type = 'topic' OR post.post_type = 'reply') AND post.ID NOT IN (SELECT `post_id` FROM v7ub9zxxg_feed WHERE (action LIKE 'bbpl_like%' OR action LIKE 'bbpl_dislike%') AND user_id=CAST(SUBSTRING_INDEX(meta.meta_key, '_', -1) AS UNSIGNED INTEGER) AND post_id=post.ID) $excludeHiddenAll $dateMeta";
		}
	}

	if(is_user_logged_in()){
		if(get_option('logPostProf') && $permission['gPostProf'] == 'on'){//Запись профессионалы
			$profPost = "UNION SELECT post.post_author AS user, post.ID AS post_id, $cmdate AS date, meta.meta_key AS action,  CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS post_parent FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id $cmjoin WHERE meta.meta_key = 'prof' AND meta.meta_value = '1' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname' $datePosts $cmgroup";
		}
	} else {
		if(get_option('postProf')){
			$profPost = "UNION SELECT post.post_author AS user, post.ID AS post_id, $cmdate AS date, meta.meta_key AS action,  CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS post_parent FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id $cmjoin WHERE meta.meta_key = 'prof' AND meta.meta_value = '1' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname' $datePosts $cmgroup";
		}
	}
		if($profPost){
			$tempID = $wpdb->get_col("SELECT post.ID AS post_id FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id WHERE meta.meta_key = 'prof' AND meta.meta_value = '1' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname'");
			if($tempID){
				$tmpArr = implode(',',$tempID);
			}
		}

	if(is_user_logged_in()){
		if($profPost){
			$tmpStr =  'AND post.ID NOT IN ('.$tmpArr.')';
		}
		if(get_option('logPostPrivate') && $permission['gPostPriv'] == 'on'){//Запись личная
			$privatePost = "UNION SELECT post.post_author AS user, post.ID AS post_id, $cmdate AS date, meta.meta_key AS action,  CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS post_parent FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id $cmjoin WHERE meta.meta_key = 'private' AND meta.meta_value = '1' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname' $tmpStr $datePosts $cmgroup";
		}
	} else {
		if($profPost){
			$tmpStr =  'AND post.ID NOT IN ('.$tmpArr.')';
		}
		if(get_option('postPrivate')){
			$privatePost = "UNION SELECT post.post_author AS user, post.ID AS post_id, $cmdate AS date, meta.meta_key AS action,  CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS post_parent FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id $cmjoin WHERE meta.meta_key = 'private' AND meta.meta_value = '1' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname' $tmpStr $datePosts $cmgroup";
		}
	}
		if($privatePost){
			$tempID = $wpdb->get_col("SELECT post.ID AS post_id FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id WHERE meta.meta_key = 'private' AND meta.meta_value = '1' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname'");
			if($tempID){
				$tmpArr = implode(',',$tempID);
			}
		}

	if(is_user_logged_in()){
		if($privatePost){
			$tmpStr =  'AND post.ID NOT IN ('.$tmpArr.')';
		}
		if(get_option('logPostInterest') && $permission['gPostInter'] == 'on'){//Запись интересная
			$interestPost = "UNION SELECT post.post_author AS user, post.ID AS post_id, $cmdate AS date, meta.meta_key AS action,  CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS post_parent FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id $cmjoin WHERE meta.meta_key = 'interest' AND meta.meta_value = '1' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname' $tmpStr $datePosts $cmgroup";

		}
	} else {
		if($privatePost){
			$tmpStr =  'AND post.ID NOT IN ('.$tmpArr.')';
		}
		if(get_option('postInterest')){
			$interestPost = "UNION SELECT post.post_author AS user, post.ID AS post_id, $cmdate AS date, meta.meta_key AS action,  CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS post_parent FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id $cmjoin WHERE meta.meta_key = 'interest' AND meta.meta_value = '1' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname' $tmpStr $datePosts $cmgroup";
		}
	}

		if($interestPost){
			$tempID = $wpdb->get_col("SELECT post.ID AS post_id FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id WHERE meta.meta_key = 'interest' AND meta.meta_value = '1' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname'");
			if($tempID){
				$tmpArr = implode(',',$tempID);
			}
		}

	if(is_user_logged_in()){
		if($permission['MyBlogpost'] == 'on') {
			$MyBlogpost = "AND post.post_author NOT IN ($user)";
		}
		if($interestPost){
			$tmpStr =  'AND post.ID NOT IN ('.$tmpArr.')';
		}
		if(get_option('logNewPosts') && $permission['gPostAll'] == 'on'){//Записи все
			$allPost = "UNION SELECT post.post_author AS user, post.ID AS post_id, $cmdate AS date, post.post_type AS action,  CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS post_parent 
			FROM $wpdb->posts post 
			RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id 
			LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id 
			$cmjoin
			WHERE (meta.meta_key = 'interest' OR meta.meta_key = 'prof' OR meta.meta_key = 'private') AND meta.meta_value = '0' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname' $MyBlogpost $tmpStr $datePosts $cmgroup";
		}
	} else {
		if($interestPost){
			$tmpStr =  'AND post.ID NOT IN ('.$tmpArr.')';
		}
		if(get_option('newPosts')){
			$allPost = "UNION SELECT post.post_author AS user, post.ID AS post_id, $cmdate AS date, post.post_type AS action,  CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS post_parent 
			FROM $wpdb->posts post 
			RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id 
			LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id 
			$cmjoin
			WHERE (meta.meta_key = 'interest' OR meta.meta_key = 'prof' OR meta.meta_key = 'private') AND meta.meta_value = '0' AND post.post_status = 'publish' AND umeta.meta_key = 'nickname' $MyBlogpost $tmpStr $datePosts $cmgroup";
		}
	}

	$dateMeta1 = 'AND meta1.meta_value BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\'';
	if(is_user_logged_in()){//Статус
		$union .= "UNION SELECT meta1.user_id AS user, NULL AS post_id, meta1.meta_value AS date, meta1.meta_key AS action, meta3.meta_value AS link, meta2.meta_value AS title, meta1.meta_key AS type, NULL AS user_parent FROM $wpdb->usermeta meta1 LEFT JOIN $wpdb->usermeta meta2 ON meta1.user_id = meta2.user_id LEFT JOIN $wpdb->usermeta meta3 ON meta1.user_id = meta3.user_id WHERE meta1.meta_key='statusTime' AND meta2.meta_key='status' AND meta3.meta_key = 'nickname' $dateMeta1";
	} else {
		if(get_option('newStatus')){
			$union .= "UNION SELECT meta1.user_id AS user, NULL AS post_id, meta1.meta_value AS date, meta1.meta_key AS action, meta3.meta_value AS link, meta2.meta_value AS title, meta1.meta_key AS type, NULL AS user_parent FROM $wpdb->usermeta meta1 LEFT JOIN $wpdb->usermeta meta2 ON meta1.user_id = meta2.user_id LEFT JOIN $wpdb->usermeta meta3 ON meta1.user_id = meta3.user_id WHERE meta1.meta_key='statusTime' AND meta2.meta_key='status' AND meta3.meta_key = 'nickname' $dateMeta1";
		}
	}

	if($permission['MyThanks'] == 'on') {
		$MyThanks = "AND likes.user_id NOT IN ($user)";
	}
	if($permission['AddYourMyPost'] == 'on') {
		$AddYourMyPost = "AND post.post_author NOT IN ($user)";
	}
	$dateLikes = 'AND likes.date BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\'';
	if(is_user_logged_in()){//Сказал спасибо
		$union .= "UNION SELECT likes.user_id AS user, likes.post_id AS post_id, likes.date AS date, 'thanks' AS action, CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS user_parent FROM $table_posts post RIGHT JOIN $table_likes likes ON post.ID = likes.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id WHERE umeta.meta_key = 'nickname' $dateLikes $MyThanks $AddYourMyPost";
	} else {
		if(get_option('newLikePost')){
			$union .= "UNION SELECT likes.user_id AS user, likes.post_id AS post_id, likes.date AS date, 'thanks' AS action, CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS user_parent FROM $table_posts post RIGHT JOIN $table_likes likes ON post.ID = likes.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id WHERE umeta.meta_key = 'nickname' $dateLikes";
		}
	}

	$dateFotogallery = 'AND `date` BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\'';
	if(is_user_logged_in()){
		if(get_option('logNewPhoto') && $permission['gFoto'] == 'on'){//Добавил фотографии
			$union .= "UNION SELECT `user_id` AS user, NULL AS post_id, CONCAT(SUBSTRING_INDEX(`date`, ':', 2), '%'), 'fotogallery' AS action, CONCAT('user-gallery/?page_number=', `user_id`) AS link, NULL AS title, 'foto' AS type, `user_id` AS user_parent FROM $fotogalleryTable WHERE `date` <> '0000-00-00 00:00:00' $dateFotogallery";
		}
	} else {
		if(get_option('newPhoto')){
			$union .= "UNION SELECT `user_id` AS user, NULL AS post_id, CONCAT(SUBSTRING_INDEX(`date`, ':', 2), '%'), 'fotogallery' AS action, CONCAT('user-gallery/?page_number=', `user_id`) AS link, NULL AS title, 'foto' AS type, `user_id` AS user_parent FROM $fotogalleryTable WHERE `date` <> '0000-00-00 00:00:00' $dateFotogallery";
		}
	}

	if(is_user_logged_in()){
		if(get_option('logNewPhoto') && $permission['gFoto'] == 'on'){//Обновил аватарку
			$union .= "UNION SELECT meta1.user_id AS user, NULL AS post_id, meta1.meta_value AS date, 'avatar' AS action, meta2.meta_value AS link, NULL AS title, 'fotoAva' AS type, meta1.user_id AS user_parent FROM $wpdb->usermeta meta1 LEFT JOIN $wpdb->usermeta meta2 ON meta1.user_id = meta2.user_id WHERE meta1.meta_key = 'userAvatar' AND meta2.meta_key = 'nickname' $dateMeta1";
		}
	} else {
		if(get_option('newPhoto')){
			$union .= "UNION SELECT meta1.user_id AS user, NULL AS post_id, meta1.meta_value AS date, 'avatar' AS action, meta2.meta_value AS link, NULL AS title, 'fotoAva' AS type, meta1.user_id AS user_parent FROM $wpdb->usermeta meta1 LEFT JOIN $wpdb->usermeta meta2 ON meta1.user_id = meta2.user_id WHERE meta1.meta_key = 'userAvatar' AND meta2.meta_key = 'nickname' $dateMeta1";
		}
	}

	$dateUsers = 'WHERE users.user_registered BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\'';
	if(!is_user_logged_in()) {//Новые зарегистрированные пользователи
		if(get_option('newRegister')){
			$union .= "UNION SELECT users.ID AS user, NULL AS post_id, users.user_registered AS date, 'userReg' AS action, users.user_nicename AS link, NULL AS title, 'userReg' AS type, users.ID AS user_parent FROM $wpdb->users users $dateUsers";
		}
	}

	$datePostzz = 'AND posts.post_date BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\'';
	if(is_user_logged_in()){
		$dateLike_dislikeCom = 'AND like_dislike.time BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\'';
		if($permission['MyLikeCom'] == 'on'){//Нравится/не нравится мой комментарий
			$union .= "UNION SELECT like_dislike.user_id AS user, comments.comment_ID AS ID, like_dislike.time AS date, CONCAT('MyLikeCom_', like_dislike.ul_key) AS action, CONCAT(post.post_name, '#comment-', comments.comment_ID) AS link, comments.comment_content AS title, like_dislike.ul_key AS type, comments.user_id AS user_parent FROM $wpdb->comments comments LEFT JOIN $table_like_dislike_counters like_dislike ON comments.comment_ID = like_dislike.post_id LEFT JOIN  $table_posts post ON comments.comment_post_ID = post.ID WHERE comments.user_id = $user $dateLike_dislikeCom";
		}

		if($permission['yourLikeCom'] == 'on'){//Мне нравится/не нравится чей то комментарий
			$union .= "UNION SELECT like_dislike.user_id AS user, comments.comment_ID AS post_id, like_dislike.time AS date, CONCAT(like_dislike.ul_key, '_yourLikeCom') AS action, CONCAT(meta.meta_value, '/', post.post_name, '#comment-', comments.comment_ID) AS link, comments.comment_content AS title, like_dislike.ul_key AS type, comments.user_id AS user_parent FROM $table_like_dislike_counters like_dislike LEFT JOIN $table_comments comments ON comments.comment_ID = like_dislike.post_id LEFT JOIN  $table_posts post ON like_dislike.post_parent_id = post.ID LEFT JOIN $wpdb->usermeta meta ON post.post_author = meta.user_id WHERE like_dislike.post_id NOT IN (SELECT `post_id` FROM $table_feed WHERE action = like_dislike.ul_key AND user_id = like_dislike.user_id AND post_id=like_dislike.post_id) AND meta.meta_key = 'nickname' AND like_dislike.user_id = $user AND comments.comment_approved  = '1' $dateLike_dislikeCom";
		}

		if($permission['MyLikeReply'] == 'on'){//Нравится/не нравится мой ответ на форуме
			$union .= "UNION SELECT SUBSTRING_INDEX(meta.meta_key, '_', -1) AS user, post.ID AS post_id, meta.meta_value AS date, CONCAT(SUBSTRING_INDEX(meta.meta_key, '_', 2), '_MyLikeReply') AS action, post.ID AS link, post.post_content AS title, post.post_type AS type, post.post_author AS user_parent FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id WHERE (meta.meta_key LIKE 'bbpl_like%' OR meta.meta_key LIKE 'bbpl_dislike%') AND (post.post_status = 'publish' OR post.post_status = 'closed') AND (post.post_type = 'topic' OR post.post_type = 'reply') AND post.ID NOT IN (SELECT `post_id` FROM v7ub9zxxg_feed WHERE (action LIKE 'bbpl_like%' OR action LIKE 'bbpl_dislike%') AND user_id=CAST(SUBSTRING_INDEX(meta.meta_key, '_', -1) AS UNSIGNED INTEGER) AND post_id=post.ID) AND post.post_author = $user $dateMeta";
		}

		if($permission['YourLikeReply'] == 'on'){//Мне Нравится/не нравится ответ на форуме
			$union .= "UNION SELECT SUBSTRING_INDEX(meta.meta_key, '_', -1) AS user, post.ID AS post_id, meta.meta_value AS date, CONCAT(SUBSTRING_INDEX(meta.meta_key, '_', 2), '_YourLikeReply') AS action, post.ID AS link, post.post_content AS title, post.post_type AS type, post.post_author AS user_parent FROM $wpdb->posts post RIGHT JOIN $wpdb->postmeta meta ON post.ID = meta.post_id WHERE (meta.meta_key LIKE 'bbpl_like%' OR meta.meta_key LIKE 'bbpl_dislike%') AND (post.post_status = 'publish' OR post.post_status = 'closed') AND (post.post_type = 'topic' OR post.post_type = 'reply') AND post.ID NOT IN (SELECT `post_id` FROM v7ub9zxxg_feed WHERE (action LIKE 'bbpl_like%' OR action LIKE 'bbpl_dislike%') AND user_id=CAST(SUBSTRING_INDEX(meta.meta_key, '_', -1) AS UNSIGNED INTEGER) AND post_id=post.ID) AND (meta.meta_key = CONCAT('bbpl_like_', $user) OR meta.meta_key = CONCAT('bbpl_dislike_', $user)) $dateMeta";
		}

		$dateFeed = 'AND feed.date BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\'';
		if($permission['AddMyFavorite'] == 'on'){//Добавление в избранное мной
			$union .= "UNION SELECT feed.user_id, feed.post_id, feed.date, 'AddMyFavorite' AS action, feed.post_name, feed.post_title, feed.type, feed.user_parent FROM $table_feed feed WHERE feed.user_id = $user $dateFeed";
		}

		if($permission['YourFavoriteMyPost'] == 'on'){//Кто то добавил мою статью в избранное
			$union .= "UNION SELECT feed.user_id, feed.post_id, feed.date, 'YourFavoriteMyPost' AS action, feed.post_name, feed.post_title, feed.type, feed.user_parent FROM $table_feed feed WHERE feed.user_parent = $user $dateFeed";
		}

		if($permission['MyThanks'] == 'on'){//Добавление в "спасибо" мной
			$union .= "UNION SELECT likes.user_id AS user, likes.post_id AS post_id, likes.date AS date, 'MyThanks' AS action, CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS user_parent FROM $table_posts post RIGHT JOIN $table_likes likes ON post.ID = likes.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id WHERE umeta.meta_key = 'nickname' AND likes.user_id = $user $dateLikes";
		}

		if($permission['MyThanks'] == 'on' || $permission['AddYourMyPost'] == 'on') {
			$AddMyPostThanks = "AND post.post_author <> likes.user_id";
		}
		if($permission['AddYourMyPost'] == 'on'){//Кто то добавил мою статью в "спасибо"
			$union .= "UNION SELECT likes.user_id AS user, likes.post_id AS post_id, likes.date AS date, 'AddYourMyPost' AS action, CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS user_parent FROM $table_posts post RIGHT JOIN $table_likes likes ON post.ID = likes.post_id LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id WHERE umeta.meta_key = 'nickname' AND post.post_author = $user $AddMyPostThanks $dateLikes";
		}

		if($permission['MyBlogpost'] == 'on'){//Новые записи мои
			$union .= "UNION SELECT post.post_author AS user, post.ID AS post_id, post.post_date AS date, 'MyBlogpost' AS action, CONCAT(umeta.meta_value, '/', post.post_name) AS link, post.post_title AS title, post.post_type AS type, post.post_author AS post_parent FROM $wpdb->posts post LEFT JOIN $wpdb->usermeta umeta ON post.post_author = umeta.user_id WHERE umeta.meta_key = 'nickname' AND post.post_status = 'publish' AND post.post_type = 'post' AND post.post_author = $user $datePosts";
		}
	}
	if($permission['AddMyFavorite'] == 'on') {
		$AddMyFavorite = "feed.user_id NOT IN ($user) AND";
	}
	if($permission['YourFavoriteMyPost'] == 'on') {
		$YourFavoriteMyPost = "AND feed.user_parent NOT IN ($user)";
	}
	//$ddd = "AND feed.id = 0"; //Если понадобится отключить вывод добавленного в избранное
	$dateFeedz = 'WHERE '.$AddMyFavorite.' feed.date BETWEEN \''.$postTime.'\' AND \''.$curTimeS.'\' '.$YourFavoriteMyPost.' '.$ddd.'';
	$posts = "
		SELECT feed.user_id, feed.post_id, feed.date, feed.action, feed.post_name, feed.post_title, feed.type, feed.user_parent FROM $table_feed feed $dateFeedz $profPost $privatePost $interestPost $allPost $union ORDER BY date DESC LIMIT 10 OFFSET $offset
	";
	$posts = $wpdb->get_results($posts);

	if(!$posts) { print '<input type="hidden" class="nonePost">'; return; }

	news_lenta_view($posts);
	print '<input type="hidden" name="feed_pages" data-offset="'.($offset+10).'">';
	print '<div class="clear"></div>';
}
//Вывод ленты новостей
function news_lenta_view($posts){
	global $wpdb;
	$fotogalleryTable = $wpdb->prefix.'fotogallery';
	foreach ($posts as $post) {
		$userdata = get_userdata($post->user_id);
		$author = $userdata->user_nicename;
		$author_name = $userdata->display_name;
		$avatar = get_avatar($post->user_id, 63);
		$avatar_url = get_avatar_data($post->user_id);
		$link = home_url().'/'.$post->post_name;
		if($post->type == 'reply'){
			$parent = $wpdb->get_var("SELECT `post_parent` FROM $wpdb->posts WHERE `ID` = $post->post_id");
			if($post->action == 'bbpl_like' || $post->action == 'bbpl_dislike'){
				$link = home_url().'/topic/'.$parent;
				$numberPage = numberPage($post->post_id);
				if($numberPage){
					$link .= $numberPage;
				}
				$link .= '#post-'.$post->post_id;
			}
		}
		if($post->action == 'bbpl_like_MyLikeReply' || $post->action == 'bbpl_dislike_MyLikeReply') {
			$parent = $wpdb->get_var("SELECT `post_parent` FROM $wpdb->posts WHERE `ID` = $post->post_id");
			$link = home_url().'/topic/'.$parent;
			$numberPage = numberPage($post->post_id);
			if($numberPage){
				$link .= $numberPage;
			}
			$link .= '#post-'.$post->post_id;
		}
		if($post->action == 'bbpl_like_YourLikeReply' || $post->action == 'bbpl_dislike_YourLikeReply') {
			$parent = $wpdb->get_var("SELECT `post_parent` FROM $wpdb->posts WHERE `ID` = $post->post_id");
			$link = home_url().'/topic/'.$parent;
			$numberPage = numberPage($post->post_id);
			if($numberPage){
				$link .= $numberPage;
			}
			$link .= '#post-'.$post->post_id;
		}
		if($post->type == 'topic'){
			if($post->action == 'bbpl_like' || $post->action == 'bbpl_dislike'){
				$link = home_url().'/topic/'.$post->post_id;
			}
		}
		if($post->action == 'favorites'){
			$aut = get_userdata($post->user_parent)->user_nicename;
			$link = home_url().'/'.$aut.'/'.$post->post_name;
		}
		if($post->action == 'AddMyFavorite'){
			$aut = get_userdata($post->user_parent)->user_nicename;
			$link = home_url().'/'.$aut.'/'.$post->post_name;
		}
		if($post->action == 'YourFavoriteMyPost'){
			$aut = get_user_meta($post->user_parent, 'nickname', true);
			$link = home_url().'/'.$aut.'/'.$post->post_name;
		}
		if($post->action == 'topic'){
			//Все вложения поста
			$thisPostImg = $wpdb->get_results("SELECT `ID`, `post_name`, `post_mime_type`, `post_title` FROM $wpdb->posts WHERE `post_parent` = $post->post_id AND `post_type` = 'attachment'");
			
			//Кол-во вложений
			$countThisPostImg = count($thisPostImg);

			$picture = array();
			if(count($countThisPostImg) >= 1){
				$count = 0;
				foreach ($thisPostImg as $image) {
					$count++;
					if($count > 7){continue;}

					//Картинки
					$image = wp_get_attachment_image($image->ID);
					array_push($picture, $image);
				}
			}

			$postCont = get_post($post->post_id)->post_content;
			$postCont = wp_filter_nohtml_kses(apply_filters('the_content', $postCont));
			if (iconv_strlen($postCont,'UTF-8') > 200) {
				$postCont = mb_substr($postCont, 0, 200);
				$postCont = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $postCont); //убираем последнее слово, ибо оно в 99% случаев неполное
				$postCont = $postCont.'...';
			}

			$viewImg = '';
			$viewImg .= '<div style="margin-top:14px;">';
			$count = 0;
			foreach ($picture as $key) {
				$count++;
				if($count == '1'){
					$viewImg .= '<div class="fotogallery_one"><a href="'.$link.'">'.$key.'</a></div>';
				} else {
					$viewImg .= '<div class="fotogallery_second"><a href="'.$link.'">'.$key.'</a></div>';
				}
			}
			$viewImg .= '</div>';

			$ttl = $wpdb->get_var("SELECT `post_title` FROM $wpdb->posts post WHERE `ID` = $post->post_id");
			$ttl = ' в <span class="reply_theme"><a href="'.$link.'">тему &laquo;'.$ttl.'&raquo;</a>:</span>';
			$ttl .= '<span class="feed__cont"> '.$postCont.'</span>';
			$ttl .= $viewImg;

		}

		//Записи и все картинки к ним
		if($post->action == 'post' || $post->action == 'interest' || $post->action == 'prof' || $post->action == 'private' || $post->action == 'MyBlogpost'){
			$action = 'опубликовал запись';
			if($post->action == 'MyBlogpost') {
				$action = 'Вы опубликовали запись';
			}
			$pic = 'r8';
			$thisPost = get_post($post->post_id);
			$attachments = catch_that_image_all($thisPost->post_content);
			$attachments = $attachments['1'];
			$countAtt = count($attachments);

			$picture = array();
			if(count($countAtt) >= 1){
				$count = 0;
				foreach ($attachments as $key => $value) {
					$count++;
					if($count > 7){continue;}
					$str = '<img src="'.$value.'">';
					array_push($picture, $str);
				}
			}
			$viewImg = '';
			$viewImg .= '<div style="margin-top:14px;">';
			$count = 0;
			foreach ($picture as $key) {
				$count++;
				if($count == '1'){
					$viewImg .= '<div class="fotogallery_one"><a href="'.$link.'">'.$key.'</a></div>';
				} else {
					$viewImg .= '<div class="fotogallery_second"><a href="'.$link.'">'.$key.'</a></div>';
				}
			}
			$viewImg .= '</div>';
			$postCont = get_post($post->post_id)->post_content;
			$postCont = wp_filter_nohtml_kses(apply_filters('the_content', $postCont));
			if (iconv_strlen($postCont,'UTF-8') > 200) {
				$postCont = mb_substr($postCont, 0, 200);
				$postCont = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $postCont); //убираем последнее слово, ибо оно в 99% случаев неполное
				$postCont = $postCont.'...';
			}
			$ttl = $wpdb->get_var("SELECT `post_title` FROM $wpdb->posts post WHERE `ID` = $post->post_id");
			$ttl = ' <span class="reply_theme"> <a href="'.$link.'">&laquo;'.$ttl.'&raquo;</a></span>';
			$ttl .= '<span class="feed__cont"> '.$postCont.'</span>';
			$ttl .= $viewImg;
		}

		$content = $post->post_title;
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);
		preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
		$content = str_replace($found[0], '', $content);
		$allowed_html = array(
			'br' => array(),
			'em' => array(),
		);
		$content = wp_kses($content, $allowed_html);
		if (iconv_strlen($content,'UTF-8') > 150) {
			$content = mb_substr($content, 0, 150);
			$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово
			$content = $content.'...';
		}
		if($post->action == 'thanks'){
			$ttl = ' <span class="reply_theme"><a href="'.$link.'">&laquo;'.$content.'&raquo;</a></span>';
		}
		if($post->action == 'MyThanks'){
			$ttl = ' <span class="reply_theme"><a href="'.$link.'">&laquo;'.$content.'&raquo;</a></span>';
		}
		if($post->action == 'AddYourMyPost'){
			$ttl = ' <span class="reply_theme"><a href="'.$link.'">&laquo;'.$content.'&raquo;</a></span>';
		}
		if($post->action == 'AddMyFavorite'){
			$ttl = ' <span class="reply_theme"><a href="'.$link.'">&laquo;'.$content.'&raquo;</a></span>';
		}
		if($post->action == 'YourFavoriteMyPost'){
			$ttl = ' <span class="reply_theme"><a href="'.$link.'">&laquo;'.$content.'&raquo;</a></span>';
		}
		if($post->action == 'favorites'){
			$ttl = ' <span class="reply_theme"><a href="'.$link.'">&laquo;'.$content.'&raquo;</a></span>';
		}
		if($post->action == 'avatar'){
			$avat = $wpdb->prefix.user_avatar;
			$avatarka = "SELECT `meta_value` FROM $wpdb->usermeta WHERE `user_id` = $post->user_id AND `meta_key` = '$avat'";
				$avatarka = $wpdb->get_var($avatarka);
				$avatarka = wp_get_attachment_image_src($avatarka, 'full');
				$avatarka = '<img style="max-width:370px;max-height:250px;display:block;margin-top:20px;" src="'.$avatarka[0].'">';
		}
		if($post->action == 'comment'){
			$ttl = $wpdb->get_var("SELECT post.post_title FROM $wpdb->comments com LEFT JOIN $wpdb->posts post ON post.ID = com.comment_post_ID WHERE `comment_ID` = $post->post_id");
			$ttl = ' <span class="reply_theme"> <a href="'.$link.'">&laquo;'.$ttl.'&raquo;</a></span>';
		}
		if($post->action == 'comment_MyBlogCom'){
			$ttl = $wpdb->get_var("SELECT post.post_title FROM $wpdb->comments com LEFT JOIN $wpdb->posts post ON post.ID = com.comment_post_ID WHERE `comment_ID` = $post->post_id");
			$ttl = ' <span class="reply_theme"> <a href="'.$link.'">&laquo;'.$ttl.'&raquo;</a></span>';
		}
		if($post->action == 'comment_MyComYourBlog'){
			$ttl = $wpdb->get_var("SELECT post.post_title FROM $wpdb->comments com LEFT JOIN $wpdb->posts post ON post.ID = com.comment_post_ID WHERE `comment_ID` = $post->post_id");
			$ttl = ' <span class="reply_theme"> <a href="'.$link.'">&laquo;'.$ttl.'&raquo;</a></span>';
		}
		if($post->action == 'fotogallery'){
			$ttl = $wpdb->get_col("SELECT `image_id` FROM $fotogalleryTable WHERE `date` LIKE '$post->date'");
			$count_pictures = count($ttl);
			$picture = array();
			if(count($ttl) > 1){
				$count = 0;
				foreach ($ttl as $key => $value) {
					$count++;
					if($count > 7){continue;}
					$image = wp_get_attachment_image_src($value, 'full');
					$str = '<img src="'.$image[0].'">';
					array_push($picture, $str);
				}
			} else {
					$ttl = implode(',', $ttl);
					$ttl = wp_get_attachment_image_src($ttl, 'full');
					$str = '<img src="'.$ttl[0].'">';
					array_push($picture, $str);
			}
			$viewFoto = '';
			if($post->action == 'fotogallery') {
				$viewFoto .= '<div style="margin-top:14px;">';
				$count = 0;
				foreach ($picture as $key) {
					$count++;
					if($count == '1'){
						$viewFoto .= '<div class="fotogallery_one">'.$key.'</div>';
					} else {
						$viewFoto .= '<div class="fotogallery_second">'.$key.'</div>';
					}
				}
				$viewFoto .= '</div>';
			}
		}
		if($post->type == 'topic'){ $type = 'вопрос'; }
		if($post->type == 'reply'){ $type = 'ответ'; }
		switch ($post->action) {
			case 'thanks':
				$action = 'сказал спасибо за статью';
				$pic = 'r9-3';
				break;
			case 'MyThanks':
				$action = 'Вы сказали спасибо за статью';
				$pic = 'r9-3';
				break;
			case 'AddYourMyPost':
				$action = 'сказал спасибо за вашу статью';
				$pic = 'r9-3';
				break;
			case 'favorites':
				$action = 'добавил в избранное статью';
				$pic = 'r9-4';
				break;
			case 'AddMyFavorite':
				$action = 'Вы добавили в избранное статью';
				$pic = 'r9-4';
				break;
			case 'YourFavoriteMyPost':
				$action = 'добавил в избранное вашу статью';
				$pic = 'r9-4';
				break;
			case 'c_like':
				$action = 'нравится комментарий';
				$pic = 'r6';
				break;
			case 'c_dislike':
				$action = 'не нравится комментарий';
				$pic = 'r5';
				break;
			case 'bbpl_like':
				$action = 'нравится '.$type;
				$pic = 'r3';
				break;
			case 'bbpl_dislike':
				$action = 'не нравится '.$type;
				$pic = 'r4';
				break;
			case 'topic':
				$action = 'задал вопрос';
				$pic = 'r1';
				break;
			case 'comment':
				$action = 'добавил новый комментарий к статье';
				$pic = 'r9';
				break;
			case 'comment_MyBlogCom':
				$action = 'добавил новый комментарий к вашей статье';
				$pic = 'r9';
				break;
			case 'comment_MyComYourBlog':
				$action = 'Вы добавили новый комментарий к статье';
				$pic = 'r9';
				break;
			case 'statusTime':
				$action = 'изменил статус:';
				$pic = 'r9-1';
				break;
			case 'fotogallery':
				$action = 'добавил <span class="count_pic">'.$count_pictures.'</span> новых фотографий:';
				$pic = 'r7';
				break;
			case 'avatar':
				$action = 'обновил аватар';
				$pic = 'r7';
				$link = $author;
				break;
			case 'MyLikeCom_c_like':
				$action = 'понравился ваш комментарий';
				$pic = 'r6';
				break;
			case 'MyLikeCom_c_dislike':
				$action = 'не понравился ваш комментарий';
				$pic = 'r5';
				break;
			case 'c_like_yourLikeCom':
				$action = 'Вам понравился комментарий';
				$pic = 'r6';
				break;
			case 'c_dislike_yourLikeCom':
				$action = 'вам не понравился комментарий';
				$pic = 'r5';
				break;
			case 'bbpl_like_MyLikeReply':
				$action = 'нравится ваш ответ на форуме';
				$pic = 'r3';
				break;
			case 'bbpl_dislike_MyLikeReply':
				$action = 'не нравится ваш ответ на форуме';
				$pic = 'r4';
				break;
			case 'bbpl_like_YourLikeReply':
				$action = 'вам нравится ответ на форуме';
				$pic = 'r3';
				break;
			case 'bbpl_dislike_YourLikeReply':
				$action = 'вам не нравится ответ на форуме';
				$pic = 'r4';
				break;
			case 'userReg':
				$action = '';
				$pic = 'r1';
				break;
			
			default:
				break;
		}
		if($post->action == 'bbpl_like' || $post->action == 'bbpl_dislike'){
			$userParent = get_user_meta($post->user_parent, 'nickname', true);
			$userName = get_userdata($post->user_parent)->display_name;
			$replyUs = ' пользователя <a class="feed__linkAuthor" href="'.home_url().'/'.$userParent.'">'.$userName.'</a>';
		}
		if($post->action == 'c_like' || $post->action == 'c_dislike'){
			$userParent = get_user_meta($post->user_parent, 'nickname', true);
			$userName = get_userdata($post->user_parent)->display_name;
			$replyUs = ' пользователя <a class="feed__linkAuthor" href="'.home_url().'/'.$userParent.'">'.$userName.'</a>';
		}
		if($post->action == 'c_like_yourLikeCom' || $post->action == 'c_dislike_yourLikeCom') {
			$userParent = get_user_meta($post->user_parent, 'nickname', true);
			$userName = get_userdata($post->user_parent)->display_name;
			$replyUs = ' пользователя <a class="feed__linkAuthor" href="'.home_url().'/'.$userParent.'">'.$userName.'</a>';
		}
		$date = rdate('H:i | d.m.Y', strtotime($post->date));

		$user_comment_avatar = get_avatar(get_current_user_id(), 63);
		?>
		<li class="questionsPage__item feed-page">
			<div class="feed">
				<div class="feed__content">
					<div class="feed__content-name">
						<a href="<?=home_url()?>/<?=$author?>"><?=$avatar?><br><?=$author_name?></a>
					</div>
					<div class="feed__content-action">
						<img src="<?=get_template_directory_uri()?>/img/actions/<?=$pic?>.png">
					</div>
					<div class="feed__content-text">
						<div class="feed__date"><?=$date?></div>
						<div class="feed__text">
						<?php 
						if($post->action != 'comment_MyComYourBlog' &&
							$post->action != 'userReg' &&
							$post->action != 'AddMyFavorite' &&
							$post->action != 'MyThanks' &&
							$post->action != 'MyBlogpost' &&
							$post->action != 'c_like_yourLikeCom'){ 
						?>Пользователь <a class="feed__linkAuthor" href="<?=home_url()?>/<?=$author?>"><?=$author_name?></a> <?php } 
						if($post->action == 'userReg') {
							print 'Зарегистрировался новый пользователь: <a class="feed__linkAuthor" href="'.home_url().'/'.$author.'">'.$author_name.'</a>';} 
						?><span class="feed__comment-title"><?=$action?></span>
						<?php
							if(//$post->action == 'reply' ||
								$post->action == 'bbpl_like' ||
								$post->action == 'bbpl_dislike' ||
								$post->action == 'c_like' ||
								$post->action == 'c_dislike' ||
								$post->action == 'c_like_yourLikeCom' ||
								$post->action == 'c_dislike_yourLikeCom') {
								print $replyUs;
							}
							if(//$post->action == 'reply' ||
								$post->action == 'topic' ||
								$post->action == 'thanks' ||
								$post->action == 'MyThanks' ||
								$post->action == 'AddYourMyPost' ||
								$post->action == 'favorites' ||
								$post->action == 'AddMyFavorite' ||
								$post->action == 'YourFavoriteMyPost' ||
								$post->action == 'private' ||
								$post->action == 'interest' ||
								$post->action == 'prof' ||
								$post->action == 'post' ||
								$post->action == 'MyBlogpost' ||
								$post->action == 'comment' ||
								$post->action == 'comment_MyBlogCom' ||
								$post->action == 'comment_MyComYourBlog') {
									print $ttl;
							} ?>
							<span class="feed__cont"> 
							<?php 
							if($post->action != 'thanks' &&
								$post->action != 'MyThanks' &&
								$post->action != 'AddYourMyPost' &&
								$post->action != 'favorites' &&
								$post->action != 'MyBlogpost' &&
								$post->action != 'private' &&
								$post->action != 'interest' &&
								$post->action != 'prof' &&
								$post->action != 'post' &&
								$post->action != 'AddMyFavorite' &&
								$post->action != 'topic' &&
								$post->action != 'YourFavoriteMyPost') {
									print $content;
							}
							if($post->action == 'avatar') {
								print $avatarka;
							}
							if($post->action == 'fotogallery') {
								print $viewFoto;
							}
							?>
							<input type="hidden" id="hidEntry" data-entry="0"><div class="clear"></div></span></div>
					</div>
					<div class="feed__content-link">
						<a href="<?=$link?>" target="_blank">Посмотреть</a>
					</div>	
				</div>
			</div>
		</li>
		<?php
		if($post->action == 'topic') {
			$opt = get_option('countReply');
			if(!$opt){ $opt = 3; }
			$posts = get_posts(array('numberposts'=>$opt, 'post_parent'=>$post->post_id, 'post_type'=>'reply', 'post_status'=>array('publish', 'closed'), 'order'=>'DESC'));
			$posts = array_reverse($posts);
			$posts_last_count = count($posts);
			$posts_count = get_posts(array('numberposts'=>-1, 'post_parent'=>$post->post_id, 'post_type'=>'reply', 'post_status'=>array('publish', 'closed'), 'order'=>'DESC'));
			$posts_count = count($posts_count);
			//if($posts) {
				print '<div class="all_comments">';
				if($posts) {
				print '<div class="feed__comment-title_com">Последние ответы:';
				print '<div class="feed__comment-view_com">Всего '.chti($posts_count,'ответ','ответа','ответов');
				if($posts_count > get_option('countReply')) {
					print '. Отображены последние '.chti($posts_last_count,'ответ','ответа','ответов').'.</div></div>';
				} else {
					print '</div></div>';
				}
				foreach ($posts as $post_reply) {
					$author_data = get_userdata($post_reply->post_author);
					$author_name = $author_data->display_name;
					$author_link = $author_data->user_nicename;
					$avatar = get_avatar($author_data->ID);
					$post_reply->post_content = strip_tags($post_reply->post_content);
					if(iconv_strlen($post_reply->post_content,'UTF-8') > 200){
						$post_reply->post_content = mb_substr($post_reply->post_content, 0, 200);
						$post_reply->post_content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $post_reply->post_content).'...'; //убираем последнее слово
					}
					?>
					<div class="comment_item depth_1">
						<span class="feed__comment_item-name">
							<a href="/<?=$author_link?>">
								<?=$avatar?><br>
								<?=$author_name?>
							</a>
						</span>
						<div class="feed__comment_item-content">
							<?=$post_reply->post_content?>
						<div class="clear butComFeedBottom">
						<span class="feed__comment_item-date">
							<?=time_as_vk($post_reply->post_date)?>
						</span>
						<button id="butCom_<?=$post_reply->ID?>" class="butComFeedTopic" data-id="<?=$post_reply->ID?>">Ответить</button><div class="clear"></div>
						</div></div>
					</div>
					<?php 
				} ?>
				<div class="clear"></div>
				<div class="feed__comment-line"></div>
				<?php } ?>
				<div class="feedCommentsConteinerTopic">
					<div class="avatarCurComment"><?=$user_comment_avatar?></div>
					<form name="feedComment" method="post">
						<div class="feed__comment-title_com feed__comment-title_com_textar">Написать ответ:</div>
						<textarea name="textCommentTopic" class="textCommentTopic" placeholder="Введите ответ"></textarea>
						<input type="hidden" name="feedIDTopic" value="<?=$post->post_id?>">
						<input type="hidden" name="feedParentTopic" value="0">
						<label class="feedCommentLabelTopic" for="feedCommentTopic<?=$post->post_id?>"></label>
						<input type="submit" class="feedCommentTopic" id="feedCommentTopic<?=$post->post_id?>" value="Ответить" />
					</form>
				</div>
				<?php 
				print '</div>';
			//}
		}
		if($post->action == 'private' ||
			$post->action == 'interest' ||
			$post->action == 'prof' ||
			$post->action == 'post' ||
			$post->action == 'MyBlogpost') {
			//if(get_current_user_id()==55 || get_current_user_id()==126){
				print '<div class="all_comments">';
				$comments = get_comments(array('post_id' => $post->post_id, 'number' => 3, 'order' => 'DESC'));
				if($comments) {
					print '<div class="feed__comment-title_com">Комментарии к записи:</div>';
				}
				foreach ($comments as $comment => $value) {
					$avatar = get_avatar($value->user_id, 30);
					$user_link = get_home_url().'/'.get_user_meta($value->user_id, 'nickname', true);
					$date = time_as_vk($value->comment_date);
					?>
					<div class="comment_item depth_1">
						<span class="feed__comment_item-name">
							<a href="<?=$user_link?>">
								<?=$avatar?><br>
								<?=$value->comment_author?>
							</a>
						</span>
						<div class="feed__comment_item-content">
							<?=$value->comment_content?>
						<div class="clear butComFeedBottom">
						<span class="feed__comment_item-date">
							<?=$date?>
						</span>
						<button id="butCom_<?=$value->comment_ID?>" class="butComFeed" data-id="<?=$value->comment_ID?>">Ответить</button><div class="clear"></div>
						</div></div>
					</div>
					<?php 
				}
				if($comments) { ?>
				<div class="clear"></div>
				<div class="feed__comment-line"></div>
				<?php } ?>
				<div class="feedCommentsConteiner">
					<div class="avatarCurComment"><?=$user_comment_avatar?></div>
					<form name="feedComment" method="post">
						<div class="feed__comment-title_com feed__comment-title_com_textar">Написать комментарий:</div>
						<textarea name="textComment" class="textComment" placeholder="Введите комментарий"></textarea>
						<input type="hidden" name="feedID" value="<?=$post->post_id?>">
						<input type="hidden" name="feedParent" value="0">
						<label class="feedCommentLabel" for="feedComment_<?=$post->post_id?>"></label>
						<input type="submit" class="feedComment" id="feedComment_<?=$post->post_id?>" value="Комментировать" />
					</form>
				</div>
				<?php
				print '</div>';
			//}//Проверка id пользователя
		}
	}
}

//Добавление комментария к записям в ленте активности
function feedPostComment() {
	$comment_content = nl2br($_POST['textComment']);
	$parent = $_POST['feedParent'];
	$post_id = $_POST['feedID'];

	//Пользователь, оставивший комментарий
	$user = get_current_user_id();
	$user_info = get_userdata($user);
	$name = $user_info->display_name;
	$email = $user_info->user_email;
	$author_url = $user_info->author_url;
	$user_link = get_home_url().'/'.get_user_meta($user, 'nickname', true);
	$avatar = get_avatar($user, 30);

	//Текущее время
	$date = current_time('mysql');

	if($comment_content) {
		$commentdata = array(
			'comment_author' => $name,
			'comment_author_email' => $email,
			'comment_author_url' => $author_url,
			'comment_content' => $comment_content,
			'comment_post_ID' => $post_id,
			//'comment_type' => 'review',
			'user_id' => $user,
			'comment_parent' => $parent,
			//'comment_author_IP' => '',
			//'comment_agent' => '',
			'comment_date' => $date,
			'comment_approved' => 1,
		);
		//Добавляем комментарий в базу
		$comment_id = wp_insert_comment($commentdata);

		$date = '1 секунду назад';

		//Если комментарий успешно добавлен, то выводим его в ленту, учитывая вложенность
		if($comment_id){
			$comment = get_comment($comment_id); ?>
			<div class="comment_item depth_1">
				<span class="feed__comment_item-name">
					<a href="<?=$user_link?>">
						<?=$avatar?><br>
						<?=$name?>
					</a>
				</span>
				<div class="feed__comment_item-content">
					<?=$comment_content?>
				<div class="clear butComFeedBottom">
				<span class="feed__comment_item-date">
					<?=$date?>
				</span>
				</div></div>
			</div>
			<?php
		}
	}
}
add_action('wp_ajax_feedPostComment', 'feedPostComment');

//Добавление ответа к вопросу в ленте активности
function feedPostCommentTopic() {
	$post_content = nl2br($_POST['textCommentTopic']);
	$parent = $_POST['feedParentTopic'];
	$post_id = $_POST['feedIDTopic'];

	//Пользователь, оставивший ответ
	$user = get_current_user_id();
	$user_info = get_userdata($user);
	$name = $user_info->display_name;
	$email = $user_info->user_email;
	$author_url = $user_info->author_url;
	$user_link = get_home_url().'/'.get_user_meta($user, 'nickname', true);
	$avatar = get_avatar($user, 30);

	//Текущее время
	$date = current_time('mysql');

	if($post_content) {
		$post_data = array(
		  'post_title'    => '',
		  'post_content'  => $post_content,
		  'post_status'   => 'publish',
		  'post_author'   => $user,
		  'post_type' => 'reply',
		  'post_parent' => $post_id
		);
		$post_id = wp_insert_post( $post_data );

		if($parent != 0) {
			update_post_meta($post_id, '_bbp_reply_to', $parent);
		}

		$date = '1 секунду назад';

		//Если ответ успешно добавлен, то выводим его в ленту
		if($post_id){
			//$post = get_post($post_id); ?>
			<div class="comment_item">
				<span class="feed__comment_item-name">
					<a href="<?=$user_link?>">
						<?=$avatar?><br>
						<?=$name?>
					</a>
				</span>
				<div class="feed__comment_item-content">
					<?=$post_content?>
				<div class="clear butComFeedBottom">
				<span class="feed__comment_item-date">
					<?=$date?>
				</span>
				</div></div>
			</div>
			<?php
		}
	}
}
add_action('wp_ajax_feedPostCommentTopic', 'feedPostCommentTopic');

//Получаем уровень вложенности комментария в блогах
function true_get_comment_depth( $comment_id ) {
	$depth = 0;
	while( $comment_id > 0  ) {
		$comment = get_comment( $comment_id );
		$comment_id = $comment->comment_parent;
		$depth++;
	}
	return $depth;
}

//Font Awesome!!!
function wpb_load_fa() {
	wp_enqueue_style( 'wpb-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' );
}

add_action( 'wp_enqueue_scripts', 'wpb_load_fa' );


add_action('wp_ajax_news_lenta', 'news_lenta');
add_action('wp_ajax_nopriv_news_lenta', 'news_lenta');

//Сохрание настроек ленты пользователей в базу данных
function feedOptionsUser(){
	global $wpdb;
	$feedUsers = $wpdb->prefix.feedUsers;

	if(current_user_can('administrator')) {
	    if($_POST['profile_id_upd']) {
	        unset( $current_user ); // удалим, чтобы wp_set_current_user() переустановила все заново
	        wp_set_current_user( $_POST['profile_id_upd'] ); // переустанавливаем
	    }
	}

	$user = get_current_user_id()
;
	$permission['MyReply'] = $_POST['MyReply']; //Ответы в темах с моим участием
	$permission['MyLikeCom'] = $_POST['MyLikeCom']; //Нравится/не нравится мой комментарий
	$permission['yourLikeCom'] = $_POST['yourLikeCom']; //Мне нравится/не нравится чей то комментарий
	$permission['MyLikeReply'] = $_POST['MyLikeReply']; //Нравится/не нравится мой ответ на форуме
	$permission['YourLikeReply'] = $_POST['YourLikeReply']; //Мне Нравится/не нравится ответ на форуме
	//$permission['MyBlogCom'] = $_POST['MyBlogCom']; //Комментарии к моим блогам
	//$permission['MyComYourBlog'] = $_POST['MyComYourBlog']; //Мои комментарии к блогам
	$permission['AddMyFavorite'] = $_POST['AddMyFavorite']; //Добавление в избранное мной
	$permission['YourFavoriteMyPost'] = $_POST['YourFavoriteMyPost']; //Кто то добавил мою статью в избранное
	$permission['MyThanks'] = $_POST['MyThanks']; //Добавление в "спасибо" мной
	$permission['AddYourMyPost'] = $_POST['AddYourMyPost']; //Кто то добавил мою статью в "спасибо"
	$permission['MyHonor'] = $_POST['MyHonor']; //Назначение медалей мне (настройка заблокирована)
	$permission['YourHonor'] = $_POST['YourHonor']; //Назначение медалей кому то (настройка заблокирована)
	$permission['MyBlogpost'] = $_POST['MyBlogpost']; //Новые записи мои

	//Email оповещения
	$permission['MyReplySub'] = $_POST['MyReplySub']; //Ответы в темах с моим участием
	$permission['MyLikeComSub'] = $_POST['MyLikeComSub']; //Нравится/не нравится мой комментарий
	$permission['MyLikeReplySub'] = $_POST['MyLikeReplySub']; //Нравится/не нравится мой ответ на форуме
	$permission['MyBlogComSub'] = $_POST['MyBlogComSub']; //Комментарии к моим блогам
	$permission['YourFavoriteMyPostSub'] = $_POST['YourFavoriteMyPostSub']; //Кто то добавил мою статью в избранное
	$permission['AddYourMyPostSub'] = $_POST['AddYourMyPostSub']; //Кто то добавил мою статью в "спасибо"
	$permission['MyHonorSub'] = $_POST['MyHonorSub']; //Назначение медалей мне (настройка заблокирована)
	$permission['YourHonorSub'] = $_POST['YourHonorSub']; //Назначение медалей кому то (настройка заблокирована)
	$permission['MyCommentMyActionSub'] = $_POST['MyCommentMyActionSub']; //Комментарии к блогам с моим участием
	$permission['heRepliesInYourTopicsSub'] = $_POST['heRepliesInYourTopicsSub']; //Ответы в ваших темах

	//Глобальные настройки ленты активности для пользователя
	$permission['gNewReply'] = $_POST['gNewReply']; //Новые вопросы
	$permission['gLikeCom'] = $_POST['gLikeCom']; //Нравится/не нравится комментарий
	$permission['gLikeReply'] = $_POST['gLikeReply']; //Нравится/не нравится ответ на форуме
	$permission['gFoto'] = $_POST['gFoto']; //Новые фото
	$permission['gPostProf'] = $_POST['gPostProf']; //Новые записи профессиональные
	$permission['gPostPriv'] = $_POST['gPostPriv']; //Новые записи личные
	$permission['gPostInter'] = $_POST['gPostInter']; //Новые записи интересные
	$permission['gPostAll'] = $_POST['gPostAll']; //Новые записи все
	$permission['gNewCom'] = $_POST['gNewCom']; //Новые комментарии в блогах

	$permission = serialize($permission);

	if($_POST['feedOptionsSubmit']){
		$tbUser = $wpdb->get_row("SELECT `id` FROM $feedUsers WHERE `user_id` = $user");
		if($tbUser){
			$sql = $wpdb->query("UPDATE $feedUsers SET `user_id` = $user, `permission` = '$permission' WHERE `user_id` = $user");
		} else {
			$sql = $wpdb->query("INSERT INTO $feedUsers (`id`, `user_id`, `permission`) VALUES (NULL, $user, '$permission')");
		}
	}
}
add_action('init', 'feedOptionsUser');

//scroll to top
function scrtop(){
	?>
	<div id="scrtop">
		<img src="<?php print get_stylesheet_directory_uri(); ?>/img/actions/scrtop.png">
		<div>Наверх</div>
	</div>
	<?php
}

//Получение номера страницы пагинации ответа (reply) на вопрос (topic) для bbpress.
function numberPage($post_id) {
	global $wpdb;

	//Получаем все ответы на вопрос post(reply), post2(topic), post3(all reply for topic)
	$posts = "SELECT post3.ID AS ID FROM $wpdb->posts post LEFT JOIN $wpdb->posts post2 ON post.post_parent = post2.ID LEFT JOIN $wpdb->posts post3 ON post2.ID = post3.post_parent WHERE post.ID = $post_id AND (post3.post_status = 'publish' OR post3.post_status = 'closed') ORDER BY post3.post_date ASC";
	$posts = $wpdb->get_col($posts);

	//Подсчитываем количество ответов + сам вопрос
	$count = count($posts);

	//Номер ответа по счету
	$number = '';
	foreach ($posts as $post => $value) {
		if($value == $post_id){
			$number = $post;
		}
	}
	$number++;//Прибавляем сам вопрос

	if($number && $number > 14){
		$numberPage = ceil($number / 14);
		$numberPage = '/page/'.$numberPage.'/';
		return $numberPage;
	}
}

function time_as_vk($date) {//Время в формате mysql(Y-m-d H:i:s)
	$dateUnix = strtotime($date); //Время объекта в UNIX
	$currently = (int)current_time('timestamp'); //Текущее время в UNIX

	$date = mb_substr($date, 0, mb_strpos($date, ' ', 0, 'UTF-8')); //Дата объекта
	$date_now = date('Y-m-d', $currently); //Дата сегодня
	$date_yesterday = date('Y-m-d', ($currently - 86400));//Дата вчера

	if($date_now == $date){//Если сегодня
		$hour = floor(($currently - $dateUnix)/3600);//Часов назад
		$min = floor((($currently - $dateUnix)%3600)/60);//Минут назад
		$hour2 = date('G', $dateUnix);
		$min2 = date('i', $dateUnix);
		$sec = date('s', $dateUnix);
		if($hour == 0 && $min == 0) { return chti($sec,'секунду','секунды','секунд').' назад'; }
		if($hour == 0) { return chti($min,'минуту','минуты','минут').' назад'; }
		if($hour == 1) { return 'час назад'; }
		if($hour == 2) { return 'два часа назад'; }
		if($hour == 3) { return 'три часа назад'; }
		if($hour > 3) { return 'сегодня в '.chti($hour2,'час','часа','часов').' '.chti($min2,'минуту','минуты','минуту'); }
	}
	if($date_yesterday == $date) {//Если вчера
		$hour = chti(date('G', $dateUnix),'час','часа','часов');
		$min = chti(intval(date('i', $dateUnix)),'минуту','минуты','минут');
		if($min != 0) {
			return 'Вчера в '.$hour.' '.$min;
		} else {
			return 'Вчера в '.$hour;
		}
	}
	if($date_yesterday > $date) {
		return date('d.m.Y - G:i', $dateUnix);
	}
}

//Универсальная функция склонения слова в зависимости от подставленного к нему числа (например: 1 минута, 10 минут, 2 минуты) вызов функции: chti($min,'минуту','минуты','минут');
function chti($string, $ch1, $ch2, $ch3){
	$ff=Array('0','1','2','3','4','5','6','7','8','9');
	if(substr($string,-2, 1)==1 AND strlen($string)>1) $ry=array("0 $ch3","1 $ch3","2 $ch3","3 $ch3" ,"4 $ch3","5 $ch3","6 $ch3","7 $ch3","8 $ch3","9 $ch3");
	else $ry=array("0 $ch3","1 $ch1","2 $ch2","3 $ch2","4 $ch2","5 $ch3","6 $ch3","7 $ch3","8 $ch3","9 $ch3");
	$string1=substr($string,0,-1).str_replace($ff, $ry, substr($string,-1,1));
	return $string1;
}

function chti2($string, $ch1, $ch2, $ch3){
	$ff=Array('0','1','2','3','4','5','6','7','8','9');
	if(substr($string,-2, 1)==1 AND strlen($string)>1) $ry=array("$ch3","$ch3","$ch3","$ch3" ,"$ch3","$ch3","$ch3","$ch3","$ch3","$ch3");
	else $ry=array("$ch3","$ch1","$ch2","$ch2","$ch2","$ch3","$ch3","$ch3","$ch3","$ch3");
	$string1=str_replace($ff, $ry, substr($string,-1,1));
	return $string1;
}

//Список вопросов(topic) на форуме в скрытом разделе (Раздел на модерацию)
//Добавить в массив get_posts: 'exclude' => excludeHiddenTopic(),
function excludeHiddenTopic() {
	global $wpdb;

	$parPost = $wpdb->get_col("SELECT `ID` FROM $wpdb->posts WHERE `post_type` = 'topic' AND (`post_parent` = '11489' OR `post_parent` = '71673')");
	$list_id = array();
	foreach ($parPost as $post) {
		array_push($list_id, $post);
	}
	return implode(',', $list_id);

}

//Список ответов(reply) на форуме в скрытом разделе (Раздел на модерацию)
function excludeHiddenReply() {
	global $wpdb;

	$topics = excludeHiddenTopic();

	$parPost = $wpdb->get_col("SELECT `ID` FROM $wpdb->posts WHERE `post_type` = 'reply' AND `post_parent` IN ($topics)");
	$list_id = array();
	foreach ($parPost as $post) {
		array_push($list_id, $post);
	}
	return implode(',', $list_id);
}

//Установка настроек ленты по дефолту новым зарегистрировавшимся пользователям
add_action('user_register', 'newUserPermissionForFeed');
function newUserPermissionForFeed($user_id) {
	global $wpdb;
	$feedUsers = $wpdb->prefix.feedUsers;

	$serializeMass = 'a:31:{s:7:"MyReply";N;s:9:"MyLikeCom";s:2:"on";s:11:"yourLikeCom";s:2:"on";s:11:"MyLikeReply";s:2:"on";s:13:"YourLikeReply";s:2:"on";s:9:"MyBlogCom";s:2:"on";s:13:"MyComYourBlog";s:2:"on";s:13:"AddMyFavorite";s:2:"on";s:18:"YourFavoriteMyPost";s:2:"on";s:8:"MyThanks";s:2:"on";s:13:"AddYourMyPost";s:2:"on";s:7:"MyHonor";N;s:9:"YourHonor";N;s:10:"MyBlogpost";s:2:"on";s:10:"MyReplySub";s:2:"on";s:12:"MyLikeComSub";s:2:"on";s:14:"MyLikeReplySub";s:2:"on";s:12:"MyBlogComSub";s:2:"on";s:21:"YourFavoriteMyPostSub";s:2:"on";s:16:"AddYourMyPostSub";s:2:"on";s:10:"MyHonorSub";N;s:12:"YourHonorSub";N;s:9:"gNewReply";s:2:"on";s:8:"gLikeCom";s:2:"on";s:10:"gLikeReply";s:2:"on";s:5:"gFoto";s:2:"on";s:9:"gPostProf";s:2:"on";s:9:"gPostPriv";s:2:"on";s:10:"gPostInter";s:2:"on";s:8:"gPostAll";s:2:"on";s:7:"gNewCom";s:2:"on";}';

	$wpdb->query("INSERT INTO $feedUsers (`id`, `user_id`, `permission`) VALUES (NULL, $user_id, '$serializeMass')");
}

//Вывод последних 6 записей из форума->новости
function forum_last_news() {
	global $wpdb;
	//if(get_current_user_id()==55) {
	$posts = get_posts(
		array(
			'numberposts' => '6',
			'post_type' => 'topic',
			'post_parent' => '71673',
			'order' => 'DESC'
		)
	);
	//$posts = array_reverse($posts);

	if($posts) {

		print '<div id="forum_news">';
		print '
			<div class="forum_news-header">
				<span class="forum_news-header_title">Новости</span>
				<span class="forum_news-header_link"><a href="'.home_url().'/forum/71673?view=all">Все новости</a></span>
			</div><ul>';

		$count = 0;

		$count_posts = count($posts);

		foreach ($posts as $post) {

			preg_match('/<img(.*)src(.*)=(.*)"(.*)"/U', $post->post_content, $regexResult);
			$firstImgScr = array_pop($regexResult);
			$image = '<img src="'.$firstImgScr.'" />';

			if(!$firstImgScr) {
				$image = $wpdb->get_var("SELECT `ID` FROM $wpdb->posts WHERE `post_parent` = $post->ID AND `post_type` = 'attachment' ORDER BY `post_date` DESC LIMIT 1");
				$image = wp_get_attachment_image($image);
			}

			$date = rDate('d M Yг.', strtotime($post->post_date)); //'d.m.Y H:i'
			$title = $post->post_title;
			$link = $post->guid;
			?>
			<li class="forum_news-block" <?php if($count_posts < 4) { print 'style="border-bottom:none;"'; } ?>>
				<div class="forum_news-block_one">
					<div class="forum_news-date"><?=$date?></div>
					<div class="forum_news-image"><a href="<?=$link?>"><?=$image?></a></div>
					<div class="forum_news-title"><a href="<?=$link?>"><?=$title?></a></div>
					<div class="forum_news-link"><a href="<?=$link?>">Подробнее</a></div>
				</div>
			</li>
			<?php
		}
		print '</ul></div>';
	}
	//}//Только я вижу
}
add_shortcode('forum_last_news', 'forum_last_news');

//Вывод последних 6 записей из форума->новости в sidebar
function forum_last_news_sidebar() {
	global $wpdb;
	//if(get_current_user_id()==55) {
	$posts = get_posts(
		array(
			'numberposts' => '6',
			'post_type' => 'topic',
			'post_parent' => '71673',
			'order' => 'DESC'
		)
	);
	//$posts = array_reverse($posts);

	if($posts) {

		print '<div id="forum_news_sidebar">';
		print '
			<div class="forum_news-header">
				<span class="forum_news-header_title">Новости</span>
				<span class="forum_news-header_link"><a href="'.home_url().'/forum/71673?view=all">Все</a></span>
			</div><ul>';

		$count = 0;

		$count_posts = count($posts);

		foreach ($posts as $post) {

			preg_match('/<img(.*)src(.*)=(.*)"(.*)"/U', $post->post_content, $regexResult);
			$firstImgScr = array_pop($regexResult);
			$image = '<img src="'.$firstImgScr.'" />';

			if(!$firstImgScr) {
				$image = $wpdb->get_var("SELECT `ID` FROM $wpdb->posts WHERE `post_parent` = $post->ID AND `post_type` = 'attachment' ORDER BY `post_date` DESC LIMIT 1");
				$image = wp_get_attachment_image($image);
			}

			$date = rDate('d M Yг.', strtotime($post->post_date));
			$title = $post->post_title;
			$link = $post->guid;
			?>
			<li class="forum_news-block forum_news_sidebar-block" <?php if($count_posts < 2) { print 'style="border-bottom:none;"'; } ?>>
				<!-- <div class="forum_news-block_one"> -->
					<div class="forum_news-date"><?=$date?></div>
					<div class="forum_news-image"><a href="<?=$link?>"><?=$image?></a></div>
					<div class="forum_news-title"><a href="<?=$link?>"><?=$title?></a></div>
					<div class="forum_news-link"><a href="<?=$link?>">Подробнее</a></div>
				<!-- </div> -->
			</li>
			<?php
		}
		print '</ul></div>';
	}
	//}//Только я вижу
}
add_shortcode('forum_last_news_sidebar', 'forum_last_news_sidebar');

//Разлогинивание
function siteExit() {
	wp_logout();
}
add_action('wp_ajax_siteExit', 'siteExit');

/**
 * Вывод формы синхронизации
 */
function ulogin_synchronisation_panel_new() {
	if(!is_user_logged_in())
		return;
	$str_info['h3'] = __("Синхронизация аккаунтов");
	$str_info['str1'] = __("Синхронизация аккаунтов");
	$str_info['str2'] = __("Привязанные аккаунты");
	$str_info['about1'] = __("Привяжите ваши аккаунты соц. сетей к личному кабинету для быстрой авторизации через любой из них");
	$str_info['about2'] = __("Вы можете удалить привязку к аккаунту, кликнув по значку");

	?>
	<script type="text/javascript">
		jQuery(document).ready(function () {
			var uloginNetwork = jQuery('#ulogin_synchronisation').find('.ulogin_network');
			uloginNetwork.click(function () {
				var network = jQuery(this).data('uloginNetwork');
				uloginDeleteAccount(network);
			});
		});

		function uloginDeleteAccount(network) {
			jQuery.ajax({
				url: '/?ulogin=deleteaccount',
				type: 'POST',
				dataType: 'json',
				cache: false,
				data: {
					network: network
				},
				error: function (data, textStatus, errorThrown) {
					console.log('error');
					alert('Не удалось выполнить запрос');
				},
				success: function (data) {
					console.log('success');
					switch (data.answerType) {
						case 'error':
							alert(data.title + "\n" + data.msg);
							break;
						case 'ok':
							var accounts = jQuery('#ulogin_accounts'),
								nw = accounts.find('[data-ulogin-network=' + network + ']');
							if (nw.length > 0) nw.hide();
							break;
						default:
							break;
					}
				}
			});
		}
	</script>
	<?php echo get_ulogin_panel(2, false, true); ?>
<?php
}

//Добавление пунктов меню к активному wp_nav_menu
add_filter( 'wp_nav_menu_items', 'new_item_menu', 10, 2 );
function new_item_menu ( $items, $args ) {
    $items .= '<li class="menu-item menu-item-type-custom menu-item-object-custom new-menu-item"><a href="/">Отзывы</a></li>';
    $items .= '<li class="menu-item menu-item-type-custom menu-item-object-custom new-menu-item"><a href="/calc">Калькуляторы</a></li>';
    return $items;
}

//Порядок сортировки тем(topic) в ленте активности
function sort_topic_for_feed() {
	if($_POST['sortTopic']) {
		update_option('sortTopic', $_POST['sortTopic']);
	}
}
add_action('init', 'sort_topic_for_feed');

//Кол-во ответов к теме в ленте
function count_reply_for_feed() {
	if($_POST['countReply']) {
		update_option('countReply', $_POST['countReply']);
	}
}
add_action('init', 'count_reply_for_feed');

/* Спасибо на форуме */
function bbp_get_thank_reply_link($id) {
	global $wpdb;
	$users = $wpdb->get_col("SELECT SUBSTRING_INDEX(`meta_key`, '-', -1) FROM $wpdb->postmeta WHERE `post_id` = $id AND `meta_key` LIKE 'likes_forum-%' ORDER BY `meta_id` DESC");
	$cur_user = get_post_meta($id, 'likes_forum-'.get_current_user_id());
	$user_click = $cur_user ? 1 : 0;
	$cur_name = get_userdata(get_current_user_id())->display_name;
	$text = '<div class="thanks_forum">';
	if($users) {
		$num = 0;
		$c_users = count($users);
		$count_users = $c_users - 3;
		$text .= '<span class="thanks_forum_users"><span class="user-name-thanks-forum">';
		foreach ($users as $user) {

			$name = get_userdata($user)->display_name;

			$num++;

			if($c_users > 3) {

				$text .= $name.', ';

				$plural_users = chti2($count_users,'пользователь','пользователя','пользователей');
				$plural_say = chti2($count_users,'сказал','сказали','сказали');
				
				if($num == 4) {
					$text = mb_substr(trim($text), 0, -1);
					$text .= '</span><span class="user-doprow-thanks-forum"> и еще <span class="user-count-thanks-forum">'.$count_users.'</span> '.$plural_users.' <span>&laquo;'.$plural_say.'&raquo;</span> спасибо</span>';
					break;
				}

			}
			if($c_users <= 3) {

				$text .= $name.', ';

				$plural = ($c_users == 1) ? 'сказал' : 'сказали' ;

				if($num == $c_users) {
					$text = mb_substr(trim($text), 0, -1);
					$text .= '</span><span class="user-doprow-thanks-forum"> <span>&laquo;'.$plural.'&raquo;</span> спасибо</span>';
					break;
				}
			}
		}
		$text .= '</span>';
	} else {
		$text .= '<span class="thanks_forum_users none"><span class="user-name-thanks-forum"></span></span>';
	}
	if(!is_user_logged_in()) { $noauth = 'noauththank'; } else { $noauth = ''; }
	$text .= '<div class="thanks_forum_button '.$noauth.'" data-id="'.$id.'" data-user="'.get_current_user_id().'" data-name="'.$cur_name.'" data-click="'.$user_click.'">Сказать спасибо</div>';
	$text .= '</div>';
	return $text;
}

function like_forum_cl() {
	global $wpdb;
	/* 
	 *@id - id поста,
	 *@user - id пользователя,
	 *@name - display name пользователя,
	 *@click - говорил спасибо или нет
	*/ 
	extract($_POST);

	$meta = get_post_meta($id, 'likes_forum-'.get_current_user_id());

	if($meta) { // Если говорил спасибо, то удаляем из базы
		delete_post_meta($id, 'likes_forum-'.$user);
		$users = $wpdb->get_col("SELECT SUBSTRING_INDEX(`meta_key`, '-', -1) FROM $wpdb->postmeta WHERE `post_id` = $id AND `meta_key` LIKE 'likes_forum-%' ORDER BY `meta_id` DESC");
	} else {
		$upd_base = add_post_meta($id, 'likes_forum-'.$user, current_time('mysql'));
		$users = $wpdb->get_col("SELECT SUBSTRING_INDEX(`meta_key`, '-', -1) FROM $wpdb->postmeta WHERE `post_id` = $id AND `meta_key` LIKE 'likes_forum-%' ORDER BY `meta_id` DESC");
		//$users = array_unshift($users, $name);
	}

	if($users) {
		$num = 0;
		$c_users = count($users);
		$count_users = $c_users - 3;
		$user_click = $meta ? 1 : 0;
		$cur_name = get_userdata(get_current_user_id())->display_name;
		$text = '<span class="thanks_forum_users"><span class="user-name-thanks-forum">';
		foreach ($users as $user) {

			$name = get_userdata($user)->display_name;

			$num++;

			if($c_users > 3) {

				$text .= $name.', ';

				$plural_users = chti2($count_users,'пользователь','пользователя','пользователей');
				$plural_say = chti2($count_users,'сказал','сказали','сказали');

				if($num == 4) {
					$text = mb_substr(trim($text), 0, -1);
					$text .= '</span><span class="user-doprow-thanks-forum"> и еще <span class="user-count-thanks-forum">'.$count_users.'</span> '.$plural_users.' <span>&laquo;'.$plural_say.'&raquo;</span> спасибо</span>';
					break;
				}

			}
			if($c_users <= 3) {

				$text .= $name.', ';

				$plural = ($c_users == 1) ? 'сказал' : 'сказали' ;

				if($num == $c_users) {
					$text = mb_substr(trim($text), 0, -1);
					$text .= '</span><span class="user-doprow-thanks-forum"> <span>&laquo;'.$plural.'&raquo;</span> спасибо</span>';
					break;
				}
			}
		}
		$text .= '</span>';
	} else {
		$text .= '<span class="thanks_forum_users none"><span class="user-name-thanks-forum"></span></span>';
	}

	if(!is_user_logged_in()) { $noauth = 'noauththank'; } else { $noauth = ''; }
	$text .= '<div class="thanks_forum_button '.$noauth.'" data-id="'.$id.'" data-user="'.get_current_user_id().'" data-name="'.$cur_name.'" data-click="'.$user_click.'">Сказать спасибо</div>';

	print $text;
}
add_action('wp_ajax_like_forum_cl', 'like_forum_cl');
/* END Спасибо на форуме */

function qqq_quote() {
    $id = bbp_get_reply_id();

    $is_reply = true;
    if ($id == 0) {
        $is_reply = false;
        $id = bbp_get_topic_id();
    }

    if (d4p_bbt_o('quote_method', 'tools') == 'html') {
        $url = ''; $ath = '';

        if ($is_reply) {
            $url = bbp_get_reply_url($id);
            $ath = bbp_get_reply_author_display_name($id);
        } else {
            $url = get_permalink($id);
            $ath = bbp_get_topic_author_display_name($id);
        }
        if(is_user_logged_in()) {
        	return '<a href="#'.$id.'" bbp-url="'.$url.'" bbp-author="'.$ath.'" class="d4p-bbt-quote-link nodelete"><i class="fa fa-quote-left" aria-hidden="true"></i></a>';
        }
    } else {
    	if(is_user_logged_in()) {
        	return '<a href="#'.$id.'" class="d4p-bbt-quote-link nodelete"><i class="fa fa-quote-left" aria-hidden="true"></i></a>';
    	}
    }
}

/**
* function for forms on the page
* @link http://obustroeno.com/start/
* Регистрация нового пользователя и создание статьи от его имени.
* Статья попадает на модерацию.
* @var ltext - контент статьи.
* @var lemail - email пользователя.
* @var lheader - заголовок статьи.
* @var login - логин пользователя.
* @var id_img - id загруженного изображения.
*/
function landing_start() {
	//if(!is_user_logged_in()) {
		if($_POST['ltext']) { //if($_POST['lemail'] && $_POST['ltext']) {
			
			extract($_POST);

			$lheader = mb_substr(strip_tags($ltext), 0, 80);

			if(!is_user_logged_in()) {
				if (!preg_match("|^[-0-9a-z_\.]+@[-0-9a-z_^\.]+\.[a-z]{2,6}$|i", $lemail)) { return 'Неверный формат e-mail'; }
				$login = explode('@', $lemail);
				$login = $login[0];
			}

			// Регистрируем нового пользователя
			if(!is_user_logged_in()) {
				$pass = wp_generate_password(12, false);
				$user = wp_create_user($login, $pass, $lemail);
				$user_id = get_user_by('ID',$user);
		        $user_id->add_role( 'subscriber' );
		        $user_id->add_role( 'participant' );
	    	} else {
	    		$user = get_current_user_id();
	    		$pass = get_userdata($user)->user_password;
	    	}

	        // login пользователя
	        $user_name = get_userdata($user)->user_nicename;

	        if(!is_user_logged_in()) {
	        	//Текст статьи
				$txt = '<br>Здравствуйте, благодарим за регистрацию на сайте <a style="color:#0171a1;" href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.current_time('mysql').'&utm_campaign=register_start&utm_content='.$lemail.'" target="_blank" rel="noopener noreferrer">Обустроено</a>!
				<br><br>Вы создали аккаунт:<br><br><div style="background:#ececec;width:100%">';

				$txt .= 'login: '.$login.'<br>';
				$txt .= 'password: '.$pass.'</div>';
				$txt .= '<br><p style="font-weight:bold;">Ваши записи вы можете посмотреть в вашем профиле по ссылке: <a href="http://obustroeno.com/'.$user_name.'/?wif_login=&wif_pass=&utm_source=email&utm_medium=email&utm_term='.current_time('mysql').'&utm_campaign=register_start&utm_content='.$lemail.'" style="color:#0171a1;" class="daria-goto-anchor" target="_blank" rel="noopener noreferrer">http://obustroeno.com/'.$user_name.'</a>. Чтобы добавить новую запись, нажмите на красную кнопку "добавить статью <a href="http://obustroeno.com/'.$user_name.'/?wif_login=&wif_pass=&utm_source=email&utm_medium=email&utm_term='.current_time('mysql').'&utm_campaign=register_start&utm_content='.$lemail.'" style="color:#0171a1;" class="daria-goto-anchor" target="_blank" rel="noopener noreferrer">в вашем профиле</a> на сайте."
	         		</p>';
			
				// Уведомление на почту с логином и паролем
				if(function_exists('ob_send_mail')){
					ob_send_mail($lemail, $txt, 'Регистрация на сайте Обустроено');
				} else {
					wp_mail($lemail, 'Регистрация на сайте Обустроено', $txt);
				}
			}

			if(isset($user)) {
				//Добавляем пост
				$post_data = array(
				  'post_author'   => $user,
				  'post_title'    => wp_strip_all_tags( $lheader ),
				  'post_content'  => $ltext,
				  'post_status'   => 'pending',
				  'post_type' => 'post',
				);
				$post_id = wp_insert_post( $post_data );

				if($post_id){

					//Заливаем картинки в медиатеку
					if (isset($_FILES)) {
						require_once(ABSPATH . "wp-admin" . '/includes/image.php');
					    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
					    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

					    $mass_pic = array();

						foreach ($_FILES as $itemID => $item) {
							if ($item['error']) { continue; }
							if (($item['type'] != "image/jpg") && ($item['type'] != "image/jpeg") && ($item['type'] != "image/png") && ($item['type'] != "image/gif")) { continue; }
							$id_img = media_handle_upload($itemID, $post_id); // загружаем файл в медиатеку, первый аргумент это индекс файла, второй это id поста
							if (is_wp_error($id_img)) { continue; } // если добавление в медиабиблиотеку вернуло ошибку
							if(isset($id_img)) {
								array_push($mass_pic, $id_img);
							}
						}
					}// $_FILES

					// Формируем текст записи с картинками
					$tmp = '';
					foreach ($mass_pic as $pic_id) {
						$pic = wp_get_attachment_image($pic_id, 'large');
						$tmp .= '<p>'.$pic.'</p><p>&nbsp;</p>';
					}
					$tmp .= '<p>'.$ltext.'</p>';
					$ltext = $tmp;

					// Перезаписываем статью с текстом с картинками
					$post_data = array(
					  'ID' => $post_id,
					  'post_author'   => $user,
					  'post_title'    => wp_strip_all_tags( $lheader ),
					  'post_content'  => $ltext,
					  'post_status'   => 'pending',
					  'post_type' => 'post',
					);
					$post_id = wp_insert_post( $post_data );

					// Добавляем мета данные, обязательно!!! Без них не будет работать вывод поста
					update_post_meta($post_id, 'start_page_'.$user, current_time('mysql'));
					update_post_meta($post_id, 'private', '0');
					update_post_meta($post_id, 'frontend', '1');

					// Авторизируем пользователя
					if(!is_user_logged_in()) {
						$arg = array(
							'user_login'    => $user_name,
							'user_password' => $pass,
							'remember'      => true,
						);
						$auuser = wp_signon($arg);
					}

					// Отправляем на главную
					wp_redirect(home_url().'/'.get_userdata($user)->user_nicename);
				}
			}

		}// $_POST['lemail'] && $_POST['ltext']
	//}// is_user_logged_in()
}
//Функция проверки заполнения полей формы страницы http://obustroeno.com/start/
//Возвращает цифровое значение
function landing_start_check() {
	//if(!is_user_logged_in()) {
		if(!is_user_logged_in()) {
			$lemail = $_POST['lemail'];
			if(!$lemail) { print '1'; return; }
			if (!preg_match("|^[-0-9a-z_\.]+@[-0-9a-z_^\.]+\.[a-z]{2,6}$|i", $lemail)) { print '2'; return; }
		}
		$ltext = $_POST['ltext'];
		if(!$ltext) { print '3'; return; }
		global $wpdb;
		$rez = $wpdb->get_var("SELECT `ID` FROM $wpdb->users WHERE `user_email` = $lemail");
		if($rez) { print '5'; return; }
		print '4';
	//} else {
		//print '5';
	//}
}
add_action('wp_ajax_landing_start_check', 'landing_start_check');
add_action('wp_ajax_nopriv_landing_start_check', 'landing_start_check');

/**
 * ЕКоличество посто пользоваеля, которые опубликованы или на модерации.
 * @return integer
 */
function user__cposts() {
	global $wpdb;
	$id = get_current_user_id();
	$rezz = $wpdb->get_var("SELECT count(`ID`) FROM $wpdb->posts WHERE `post_author` = $id AND (`post_status` = 'pending' OR `post_status` = 'publish') AND `post_type` = 'post'");
	return $rezz;
}

function blget_reg_users() {
  global $wpdb;
  return $wpdb->get_var("select count(*) from $wpdb->users");
}

// Страница с блогерами
function view_blogers() {
	global $wpdb;

	$val = $_POST['value'];
	$set = $_POST['set'];
	$set = isset($set) ? $set : 0;

	if(isset($val)) {
		$set = $_POST['set'];
		$set = isset($set) ? $set : 0;
		$val2 = "WHERE `display_name` LIKE '%$val%'";
		$val = "AND `display_name` LIKE '%$val%'";
	}
	
	$count_us = $wpdb->get_var("SELECT count(`id`) FROM $wpdb->users $val2");

	$users = $wpdb->get_results("SELECT userr.ID AS id, userr.user_nicename AS login, userr.display_name AS name, meta.meta_value AS rating FROM $wpdb->users userr LEFT JOIN $wpdb->usermeta meta ON meta.user_id = userr.ID WHERE meta.meta_key = 'akk_rating' $val ORDER BY CAST(meta.meta_value AS SIGNED) DESC LIMIT $set, 13");

	if($users) {
		$text = '';
		foreach ($users as $user) {
			$ava = get_avatar($user->id, '128');
			ob_start();
			visiting_users_view($user->id);
			$visit = ob_get_clean();
			$countPosts = count_user_posts($user->id, 'post', false);
			ob_start();
			bluser_honors_view($user->id);
			$honor = ob_get_clean();
			$count_p = $wpdb->get_col("SELECT count(*)  FROM $wpdb->posts WHERE `post_status` = ('publish' OR 'closed') AND `post_type` = 'topic' AND `post_author` = $user->id
				UNION
				SELECT count(*) FROM $wpdb->posts WHERE `post_status` = ('publish' OR 'closed') AND `post_type` = 'reply' AND `post_author` = $user->id");
			if(!$count_p[0]) { $count_p[0] = 0; }
			if(!$count_p[1]) { $count_p[1] = 0; }
			
			$text .= '	<li>
							<div class="img">
								<a href="/'.$user->login.'">'.$ava.'</a>
								<a class="blhidden" href="/'.$user->login.'">'.$user->name.'</a>
								<span class="blvisit blhidden">'.$visit.'</span>
							</div>
							<div class="name">
								<span class="blvisit">'.$visit.'</span>
								<a href="/'.$user->login.'">'.$user->name.'</a>
							</div>
							<div class="stat">
								<span class="txt"><span class="zap">Записи:</span><span>'.$countPosts.'</span></span>
								<span class="txt"><span class="vop">Вопросы:</span><span>'.$count_p[0].'</span></span>
								<span class="txt"><span class="otv">Ответы:</span><span>'.$count_p[1].'</span></span>
								<div class="images">'.$honor.'</div>
								<div class="rating blreithidden">
									Текущий рейтинг:<br><span>'.$user->rating.'</span>
								</div>
							</div>
							<div class="rating">
								Текущий рейтинг:<br><span>'.$user->rating.'</span>
							</div>
						</li>';
			$text .= '<input type="hidden" id="blconcat" data-set="'.($set + 13).'" data-ckeck="0">';
		}
		//if(isset($count_us)) {
			//ob_start();
			//blpagenavi($count_us, 13, 5, $set);
			//$text .= ob_get_clean();
		//}	
	} else {
		$text = '<li class="blnouser">Не найдено!</li>';
	}
	print $text;
}
add_action('wp_ajax_view_blogers', 'view_blogers');
add_action('wp_ajax_nopriv_view_blogers', 'view_blogers');

/**
 * Пагинатор
 * 
 * @param item - количество элементов, которые нужно разбить на страницы
 * @param countView - количество элементов на одной странице
 * @param vPage - Количество иконок страниц пагинации
 * @param set - Номер открытой страницы
 * 
 * @var page - Количество страниц
 * @var lastPage - есть или нет последняя неполная страница
 * 
 * @return String
 */
function blpagenavi($items = 1, $countView = 10, $vPage = 5, $set = 1) {
	if($items < $countView) {
		$page = 1;
		return;
	} else {
		$page = floor($items / $countView);
		$lastPage = $items % $countView;
		if($lastPage) { $page += 1; }
	}
	if($page < $vPage) {
		$vPage = $page;
	} else {
		$vPage = $vPage + $set - 1;
	}
	?>
		<div class="blpaginate">
	        <?php if($page > 1 && $set > 1 ) { ?>
	            <span class="blbegin">В начало</span>
	        <?php } ?>
	         
	        <?php for($i = $set; $i<=$vPage; $i++) { ?>
	            <span <?=($i == $set) ? 'class="blcurpage"' : '';?>> <?=$i;?> </span>
	            <?php if($i == $page) {break;} ?>
	        <?php } ?>

	        <?php if($set < $page) { ?>
	            <span class="blnext">Дальше</span>
	        <?php } ?>
	        <input type="hidden" id="blconcat" data-set="<?=$set?>" data-status="0">
	    </div>
	<?php
}

function bluser_honors_view($user_id) {
	$list_value = get_award_of_user('all', $user_id);
		
	if($list_value){
		foreach ($list_value as $honor) {
			?>
				<div>
					<a href="/spisok-nagrad">
						<img src="/wp-content/themes/1brus_mag/img/honors/<?php print $honor['images_honor']; ?>">
					</a>
				</div>
			<?php
		}
	}
}

/**
 * Рейтинг на сайте = количество дней + количество статей + количество ответов на форуме
 * 15.12.2016 - количество дней убрали из рассчета
 */
function upd_user_meta() {
	if(is_user_logged_in()) {
		$user_id = get_current_user_id();
		global $wpdb;
		//$data = registration_data(get_userdata($user_id)->user_registered, current_time('mysql'));
		$cpost = count_user_posts($user_id, 'post', false);
		$count_p = $wpdb->get_var("SELECT count(*)  FROM $wpdb->posts WHERE `post_status` = ('publish' OR 'closed') AND `post_type` = 'reply' AND `post_author` = $user_id");
		update_user_meta($user_id, 'akk_rating', (/*$data + */$cpost + $count_p));
	}
}
add_action('wp_head', 'upd_user_meta');

//Добавляет в поля со стоимостью, сложностью, часами.
function post_field_block($args) {
	ob_start();
	?>
	<div class="post_field_block">
		<?php if($args['money']) { ?>
			<div class="post_field_block-item post_field_block-price">
				<img class="post_field_block-left" src="<?=get_template_directory_uri()?>/img/post_block_1.png">
				<span><div class="post_field_block-price-title">Стоимость</div><div class="post_field_block-price-mon"><?=$args['money']?> &#8381;</div></span>
			</div>
		<?php } ?>
		<?php if($args['ball']) { ?>
			<div class="post_field_block-item post_field_block-ball">
				<div class="post_field_block-number post_field_block-left"><?=$args['ball']?></div>
				<span><?=chti2($args['ball'],'Балл','Балла','Баллов')?> сложности</span>
			</div>
		<?php } ?>
		<?php if($args['time']) { ?>
			<div class="post_field_block-item post_field_block-time">
				<div class="post_field_block-number number_2 post_field_block-left"><?=$args['time']?></div>
				<span><?=chti2($args['time'],'Час','Часа','Часов')?></span>
			</div>
		<?php } ?>
	</div>
	<div class="clear"></div>
	<?php
	$block = ob_get_clean();
	
	return $block;
}
add_shortcode('CPFB', 'post_field_block');


/**
 * Добавляем кнопку в mce редактор
 */
function asdfetchimg_addbutton() {
	add_filter("mce_external_plugins", "asadd_fetchimg_tinymce_plugin");
	add_filter('mce_buttons', 'asregister_fetchimg_button');
}
add_action('init', 'asdfetchimg_addbutton');
// В этом функции указываем ссылку на JavaScript-файл кнопки
function asadd_fetchimg_tinymce_plugin($plugin_array) {
  $plugin_array['price'] = get_bloginfo('template_url').'/js/mcenewbutton.js?3';
  return $plugin_array;
}
// Регистрируем кнопку в редакторе
function asregister_fetchimg_button($buttons) {
  array_push($buttons, "price");
  return $buttons;
}

add_filter('wp_die_handler', 'get_my_custom_die_handler');
function get_my_custom_die_handler() {
    return 'my_custom_die_handler';
}
function my_custom_die_handler($message,$title='',$args=array()) {
    $errorTemplate = get_theme_root().'/'.get_template().'/commenterror.php';
    if(!is_admin() && file_exists($errorTemplate)) {
    $defaults = array( 'response' => 500 );
    $r = wp_parse_args($args, $defaults);
    $have_gettext = function_exists('__');
    if ( function_exists( 'is_wp_error' ) && is_wp_error( $message ) ) {
        if ( empty( $title ) ) {
            $error_data = $message->get_error_data();
            if ( is_array( $error_data ) && isset( $error_data['title'] ) )
                $title = $error_data['title'];
        }
        $errors = $message->get_error_messages();
        switch ( count( $errors ) ) :
        case 0 :
            $message = '';
            break;
        case 1 :
            $message = "<p>{$errors[0]}</p>";
            break;
        default :
            $message = "<ul>\n\t\t<li>" . join( "</li>\n\t\t<li>", $errors ) . "</li>\n\t</ul>";
            break;
        endswitch;
    } elseif ( is_string( $message ) ) {
        $message = "<p>$message</p>";
    }
    if ( isset( $r['back_link'] ) && $r['back_link'] ) {
        $back_text = $have_gettext? __('« Back') : '« Back';
        $message .= "\n<p><a href='javascript:history.back()'>$back_text</a></p>";
    }
    if ( empty($title) )
        $title = $have_gettext ? __('WordPress › Error') : 'WordPress › Error';
    require_once($errorTemplate);
    die();
 } else {
    _default_wp_die_handler($message, $title, $args);
 }
}
?>