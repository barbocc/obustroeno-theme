<?php get_header(); ?>


<?php 
	global $wp_query, $wpdb; global $post_author_reply_count;$tssuat_settings = unserialize(get_option('tssuat_settings'));
	$curauth = $wp_query->get_queried_object()->data->ID;
	$userRole = $wp_query->get_queried_object()->roles;

	function noindexAuthor() {
		global $wp_query;
		$userRole = $wp_query->get_queried_object()->roles;
		if(!in_array('expert', $userRole)) {
			print '<meta name="robots" content="noindex"/>';
			print '<meta name="robots" content="nofollow"/>';
		}
	}

	$cur_author = $curauth;
	$cur_author = get_userdata($cur_author);

	$author_nickname = $wpdb->get_var("SELECT `display_name` FROM $wpdb->users WHERE `ID` = $curauth");

	//Никнейм автора
	//$nickname = $cur_author->user_nicename;
	$nickname = $author_nickname;

	//Ид автора
	$user_id = $cur_author->ID;

	/*$user_vk = get_usermeta($curauth, 'profile_vk', true);
	$user_ok = get_usermeta($curauth, 'profile_ok', true);
	$user_fb = get_usermeta($curauth, 'profile_fb', true);
	$user_tw = get_usermeta($curauth, 'profile_tw', true);
	$user_yout = get_usermeta($curauth, 'profile_yout', true);
	$user_in = get_usermeta($curauth, 'profile_in', true);*/

	global $author_id_au_page;
	$author_id_au_page = $user_id;

	//Дата рождения
	$user_date = get_usermeta($curauth, 'date', true);
	if($user_date){
		$user_date = rdate('d M Y', strtotime($user_date));
	}

	//Город проживания
	$user_sity = get_usermeta($curauth, 'sity', true);

	//Получаем контакты
	$user_contacts = get_usermeta($curauth, 'contacts', true);

	//Статус
	$full_user_status = get_user_meta($curauth, 'status', true);
	if(mb_strlen($full_user_status) > 60){
		$user_status = mb_substr($full_user_status, 0, 42);
		$user_status .= '...';
	} else {
		$user_status = $full_user_status;
	}

	//Об авторе
	$description = get_usermeta($curauth, 'description');

	//Имя, фамилия автора
	$author_name = get_usermeta($curauth, 'first_name', true);
	if(current_user_can('administrator') || get_current_user_id() == $user_id){
		if($author_name) {
			$author_name .= ' '.get_usermeta($curauth, 'last_name', true);
		}
	}

//if(current_user_can('manage_options')){prr($user_date);}

	//Аватар автора
	if(get_avatar($cur_author->ID, 235) != null) $meta_values = get_avatar($cur_author->ID, 235); else $meta_values = '<img src="/wp-content/plugins/firms_by_tss/img/img_not_found_mini.png" width="235" height="235" />';
	$avatar = explode('"', $meta_values);
	$avatar = trim($avatar[1]);
	//$avatar = get_avatar($user_id, 35);
	//$avatar = get_usermeta($curauth, 'simple_local_avatar', true);
	//$avatar = $avatar['full'];
	$ava = get_avatar( $author, 30);

	$post = $cur_author->data;

	//Дата регистрации
	$registr_date = $cur_author->user_registered;
	$reg_day = rdate('d M Y', strtotime($registr_date));
	//var_dump($registr_date);
	$registr_date_2 = rdate('Y-m-d h:i:s', strtotime($_SERVER['REQUEST_TIME']).'г.');
	$rez = registration_data($registr_date, $registr_date_2);

	//Вывод количества записей пользователя
	$userID = array($cur_author->ID);
	$countPosts = count_user_posts($userID, 'post', true);
	if($countPosts == get_current_user_id()){
		$countPosts = count_user_posts($userID, 'post', false);
	}
	
	//Количество вопросов на форуме
	$author_count_query = get_posts( array( 'post_type'           => bbp_get_topic_post_type(), 'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ), 'posts_per_page' => -1, 'author' => $user_id ) );
	$post_author_count = count($author_count_query);
	//Количество ответов на форуме
	$reply_count_query = get_posts( array( 'post_type'=> bbp_get_reply_post_type(),
			'posts_per_page' => -1, 'post_status'=> array( bbp_get_public_status_id(), bbp_get_closed_status_id() ), 'author' => $user_id ) );
	$post_author_reply_count = count($reply_count_query);

//$rat = $rez + $countPosts + $post_author_reply_count;
$rat = get_user_meta($user_id, 'akk_rating', true);

function social_buttons() {
	global $wp_query;

	$curauth = $wp_query->get_queried_object()->data->ID;

	$user_vk = get_usermeta($curauth, 'profile_vk', true);
	$user_ok = get_usermeta($curauth, 'profile_ok', true);
	$user_fb = get_usermeta($curauth, 'profile_fb', true);
	$user_tw = get_usermeta($curauth, 'profile_tw', true);
	$user_yout = get_usermeta($curauth, 'profile_yout', true);
	$user_in = get_usermeta($curauth, 'profile_in', true);

	print '<div class="social">';
	if($user_vk) {
		print '<a href="'.$user_vk.'" class="vk"></a>';
	} 
	if($user_ok) {
		print '<a href="'.$user_ok.'" class="ok"></a>';
	} 
	if($user_fb) {
		print '<a href="'.$user_fb.'" class="fb"></a>';
	} 
	if($user_tw) {
		print '<a href="'.$user_tw.'" class="tw"></a>';
	} 
	if($user_yout) {
		print '<a href="'.$user_yout.'" class="yout"></a>';
	} 
	if($user_in) {
		print '<a href="'.$user_in.'" class="in"></a>';
	}
	print '</div>';
}
?>
<div id="full_content">
<div id="main_content">
	<div id="main" class="author_page">
		<div id="article">
		<?//=get_related_author_posts()?>
<?php $url_theme = '/wp-content/themes/1brus_mag'; ?>
<div id="personal_page"><!-- personal_page -->
	<div class="personal_top"><!-- personal_top -->
		<div class="personal_left"><!-- personal_left -->
			<img class="photo" src="<?php print $avatar; ?>">
			<div class="info">
				<div class="count_subsribe_user">Количество подписчиков <span>0</span></div>
				<div class="count_subsribe">Количество подписок <span>0</span></div>
				<div class="rating">Рейтинг: <span><?=$rat?>
				<?php 
				if ( !get_user_meta( $cur_author->ID, 'generated', true ) && 
				( strtotime( $cur_author->user_registered ) >= strtotime( '2016-11-17' )))
				{ 
				?>
				<span style="color: #9d2c24">*</span>
				<?php } ?>
				</span></div>
				<div class="count_publication">Количество публикаций: <span><?=$countPosts?></span></div>
				<div class="time_site">Время на сайте (дни) <span><?=$rez?></span></div>
			</div>
		</div><!-- end personal_left -->
		<div class="personal_right"><!-- personal_right -->

			<div class="personal_right_top">
				<?php if($nickname) { ?>
					<div class="name"><?=$nickname?></div>
				<?php } ?>
				<?php if($user_status){ ?>
				<div class="status"><?=$user_status?><span class="tooltip"><?=$full_user_status?></span></div>
				<?php } else { ?>
					<div class="personal_empty"></div>
				<?php } ?>
				<!-- <input type="text" class="status" disabled="disabled" value="Я - дизайнер, Мне...можно)"> -->
				<div class="contacts">
					<?php if($author_name) { ?>
						<div>Имя: <span class="contacts_name_uppercase"><?=$author_name?></span></div>
					<?php } ?>
					<?php if($user_date && current_user_can('administrator') || $user_date && get_current_user_id() == $user_id){ ?>
					<div class="dr">День рождения: <span><?=$user_date?> г.</span></div>
					<?php } ?>
					<div class="register">Дата регистрации: <span><?=$reg_day?> г.</span></div>
					<?php if($user_sity){ ?>
					<div class="sity">Город: <span><?=$user_sity?></span></div>
					<?php } ?>
					<?php if($user_contacts && current_user_can('administrator') || $user_contacts && get_current_user_id() == $user_id){ ?>
					<div class="namber_phone">Контакты <span><?=$user_contacts?></span></div>
					<?php } ?>
				</div>
			</div>
			
			<div class="personal_info"><div class="edit">
				<?php if(get_current_user_id() == $user_id || current_user_can('administrator')){ ?>
					<form id="profile_redact" action="/profile_page" method="get">
						<input type="submit" value="Редактировать профиль">
						<input type="hidden" name="profile_id_upd" value="<?=$author_id_au_page?>">
					</form>
				<?php } ?>
				<div class="status_active">
					<?php visiting_users_view($user_id); ?>
				</div></div>
				<div class="honors <?php if(is_user_logged_in()){print 'log';} ?>">
					<?php user_honors_view(); ?>
				</div>
				<div class="photo">
					<?php view_fotogallery_author($user_id); ?>
				</div>
			</div>
			<div class="clear"></div>
			<div class="full_personal_description">
				<div class="personal_about">
					<span>О себе:</span>
					<?php
						$role = get_userdata($curauth)->roles;
						$role_act = get_userdata(get_current_user_id())->roles;

						if(!in_array('expert', $role) || !is_user_logged_in()) {
							social_buttons();
						} else {
							if(in_array('expert', $role) == in_array('expert', $role_act) || current_user_can('administrator')) {
								social_buttons();
							}
						}
					?>
				</div>
				<div class="personal_description"><?php /*the_author_description();*/ echo $description; ?></div>
				<?php if(mb_strlen($description) > 256) { ?>
				<div class="description_turn"><a>Читать польностью</a></div>
				<?php } ?>
			</div>

		</div><!-- end personal_right -->
	</div><!-- end personal_top -->
	<div class="clear"></div>

	<div class="personal_tags">
		<div class="personal_wrap">
			<?php if(get_current_user_id() == $curauth) { ?>
				<a href="/new-record"><div style="background:#e25655;" class="application_button app_send app_send_posts">Написать статью</div></a>
			<?php } else { ?>
				<a href="/vopros"><div class="application_button app_send">отправить сообщения</div></a>
				<a href="/vopros"><div class="application_button app_send_2"></div></a>
			<?php } ?>
		</div>
		<div class="personal_menu">
			<ul>
				<li class="active">посты (<?=$countPosts?>)</li>
				<li class="">понравилось (<?=count_likes_posts_menu()?>)</li>
				<li class="">вопросы (<?=$post_author_count?>)</li>
				<li class="">ответы (<?=$post_author_reply_count?>)</li>
				<li class="">комментарии (<?=comment_author_count_list()?>)</li>
				<li class="">идеи (<?=count_favorites_posts_menu()?>)</li>
			</ul>
		</div><div class="clear"></div>
	</div><!-- end personal_top -->
	<div class="clear"></div>
	<div class="info_blocks">
		<div class="postPage visible">
			<ul>
				<?=get_author_posts($user_id)?>
			</ul>
			<?php //if(!$countPosts) { print 'Пользователь не писал статьи.'; } ?>
		</div>
		<div class="likePage">
			<?=post_likes_author()?>
		</div>
		<div class="voprPage">
			<?=reply_page_author()?>
			<?php if(!$post_author_count){ print 'Пользователь не задавал вопросов.'; } ?>
		</div>
		<div class="questionsPage">
			<?=questions_page_author_contain()?>
			<?php if(!$post_author_reply_count){ print 'У пользователя нет ответов.'; } ?>
		</div><!--quetsionsPage -->
		<div class="commPage">
			<?=comment_author_list()?>
		</div>
		<div class="ideaPage"><!--Идеи-->
			<?=post_favorites_author()?>
		</div>
	</div><!-- info block -->
</div><!--end personal_page -->

			<div class="clear"></div>
		</div>
	</div>

	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>
