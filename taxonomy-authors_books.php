<?php get_header(); ?>

<div id="full_content">
<div id="main_content">
	<div id="main">
		<div id="breadcrumbs">
			<div class="breadcrumbs_block">
			<?php if (function_exists('dimox_breadcrumbs')) {dimox_breadcrumbs();} ?>
			</div>
		</div>
	
		<div id="article" class="question_page">
			<div class="content_text question_page">
				<?php echo do_shortcode( '[author_book_list]' ); ?>
				
			</div>
		</div>
	</div>

	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>
