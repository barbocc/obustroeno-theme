<?php get_header(); $tssuat_settings = unserialize(get_option('tssuat_settings'));?>

<div id="full_content">
<div id="main_content">
	<div id="main">
		<div id="breadcrumbs">
			<div class="breadcrumbs_block">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			</div>
		</div>

		<div id="article" class="question_page post_page">
			<div class="content_text book_page">
				<div class="title firm-title"><div class="text">Книги</div></div>
				<?php echo do_shortcode( '[book_page]' ); ?>
			</div>
			<?php if($tssuat_settings['books']){
				comments_template();
			} ?>
		</div>

	</div>

	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>
