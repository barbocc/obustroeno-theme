<?php get_header(); ?>

<div id="full_content">
<div id="main_content">
	<div id="main">
		<div id="breadcrumbs">
			<div class="breadcrumbs_block">
			<?php if (function_exists('dimox_breadcrumbs')) {dimox_breadcrumbs();} ?>
			</div>
		</div>
	
		<div id="article" class="question_page post_page">
			<div class="content_text question_page">
				<!--<div class="title firm-title"><div class="midline"></div><div class="text">Ответы эксперта</div><div class="midline"></div></div>-->
				<?php /*
				<div class="front_firms_header">
					<a href="/question" class="add_firm">
						Добавить вопрос
					</a>
					<div class="firm_count">
						В каталоге: <span class="count"><?php
							$cat_title = 'products';
							$cat_posts = get_posts( array(
								'numberposts'     => -1, // тоже самое что posts_per_page
								'orderby'         => 'post_date',
								'order'           => 'DESC',
								'post_type'       => 'question',
								'post_status'     => 'publish'
							));
							echo count($cat_posts);
							?></span> вопросов
					</div>
				</div>
				*/ ?>
				<?php echo do_shortcode( '[question_page]' ); ?>
				
			</div>
		</div>
		
		
		<?php comments_template(); ?>
			<!--<form class="add_comment" action="/questions" method="post" enctype="multipart/form-data" id="commentform">
				
				<div class="labels">
					<div>Ваше имя<span class="red_star">*</span>:</div>
					<div><span>Ваш e-mail:</span></div>
					<div><span>Ваш вопрос:</span></div>
				</div>
				<div class="inputs">
					<input type="text" class="margine" name="username" placeholder="Ваше имя" required>
					<input  type="text" class="margine" name="usermail" placeholder="Ваш e-mail" required>
					<textarea name="expert_question"></textarea>
					<div class="together">			
						<div class="file_upload">
							<label for="question_file"><div>Прикрепить файл</div></label>
							<input type="file" id="question_file" name="question_file"/>
						</div>
						<div  class="submit_button">
							<input  type="submit"  value="Опубликовать">
						</div>
				</div>
				<?php wp_nonce_field('question_file'); ?>
				<input type="hidden" name="check" value="0">
				</div>-->
		
		
	</div>

	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>
