<?php get_header(); $tssuat_settings = unserialize(get_option('tssuat_settings')); ?>
<?php $category = get_queried_object();?>
<div id="full_content">
<div id="main_content" class="post_page tags tags_cat">
	<div id="main">
	<div id="breadcrumbs">
			<div class="breadcrumbs_block">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			</div>
		</div>
		<div id="article">
			<div class="content_text">
			<h1><?=single_tag_title()?></h1>


			<?php //wp_list_categories(); ?>
			<?php
			$child = get_categories(array('child_of' => $category->term_id));
			$id_parent = $category->term_id;
			print '<div class="categories"><ul>';
			foreach ($child as $key) {
				$id_cat_parent = $key->parent;
				$child_new = get_categories(array('child_of' => $key->term_id));
				$link = $key->slug;
				if( $id_cat_parent === $id_parent ) {
					print '<li><a href="/tags/'.$link.'">'.$key->name.'</a>';
					if( $child_new ) {
						print '<ul class="children">';
						foreach ($child_new as $keychild) {
							$link_child = $keychild->slug;
							if( $key->term_id === $keychild->parent ){
								print '<li><a href="/tags/'.$link_child.'">'.$keychild->name.'</a></li>';
							}
						}
						print '</ul>';
					}
					print '</li>';
				}
			}
			print '</ul></div>';
			?>

                <div class="frontPosts__list">
 <?php //echo tag_description(); ?>
 <?php if (have_posts()) : while (have_posts()) : the_post(); global $post;  ?>
 	<div class="frontPosts__list">
	<ul>
	<?php

	//foreach ($posts as $k => $post) {

		$GLOBALS['adds_posts'][] = $post->ID;

		//Получаем все изображения поста
		$post_img = catch_that_image_all( $post->post_content );
		$count_img = count($post_img [1]);
		if ($count_img > 4) {
			$count_img -= 4;
		} else {
			$count_img = -1;
		}

		//Получаем имя автора поста
		$author = $post->post_author;
		$author_name = get_userdata($author)->display_name;
		$author_link = get_userdata($author)->user_nicename;

		//Получаем заголовок поста
		$title_amount = !$k ? 120 : 80;
		$post_title = title_preview( $post->post_title, $title_amount );

		/*$post_meta = '
			<span class="post_views">'.post_count_views($post->ID).'</span>
			
			<span class="post_comm">'.$post->comment_count.'</span>';*/

		//Получаем ссылку на пост
		$post_link = get_permalink( $post->ID );

		//Получаем контент поста
		$content = $post->post_content;

		//Преобразуем контент для вывода
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);

		preg_match('/\<div id\=\"toc_container\" class\=\"no_bullets\">(.+)\<\/div\>/isU', $content, $found);
		$content = str_replace($found[0], '', $content);

		$allowed_html = array(
			'br' => array(),
			'em' => array(),
			'strong' => array(),
			'p' => array()
		);

		$content = wp_kses($content, $allowed_html);
		if (iconv_strlen($content,'UTF-8') > 200) {
			$content = mb_substr($content, 0, 200).'...';
			$content = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ', $content); //убираем последнее слово, ибо оно в 99% случаев неполное
		}

		//Получаем категории поста
		$posttags = get_the_category();
		//Инициализируем пустой массив
		$posttags_x = array();
		if ($posttags) {
		  foreach($posttags as $tag) {
		  	//Записываем значени в массив
		  	array_push($posttags_x, '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>');
		  }
		}
		//Объединяем элементы массива в строку, через разделитель - "запятую"
		$posttags = implode(", ", $posttags_x);

		//Получаем количество комментариев
		$count_comments = $post->comment_count;

		//Получаем аватар
		$ava = get_avatar($post->post_author, 30);

		//Выводим на экран форму
		?><li style="margin-bottom:20px;list-style: none;">
				<div class="postMain">
											<div class="postMain__main">
												<a href="<?=the_permalink()?>"><img src="<?=$post_img [1] [0]?>"></a>
											<div class="postMain__author">
												<div class="postMain__author__img">
													<?=$ava?>
													<a href="/author/<?=$author_link?>"><?=$author_name?></a>
												</div>
											</div>
											
											<div class="postMain__title">
												<div class="postMain__title__text">
													<div class="postMain__date"><?=rdate('d M Y', strtotime($post->post_date))?>г.</div>
													<a href="<?=the_permalink()?>"><?=the_title()?></a>
												</div>
												<div class="postMain__additional">
													<div class="postMain__likes"><?=post_likes_count_view($post->ID)?></div>
													<div class="postMain__comment"><?=$count_comments?></div>
												</div>
											</div>
											</div>
											<div class="postMain__images">
											<?php
											for ($i = 1; $i <= 3; $i++) {
												if ($post_img [1] [$i] != ''){
											    	?>
											    		<div class="postMain__image"><a href="<?=the_permalink()?>"><img src="<?=$post_img [1] [$i]?>"></a></div>
											    	<?php
											    }
											}
											if ($count_img >= 0) {
												?>
												<div class="postMain__images__link"><a href="<?=the_permalink()?>"><p>Ещё</p><span><?=$count_img?></span><span>фото</span></a></div>
												<?php
											}
											?>
											</div>
											<!-- <a class="my_links" href="<?//=the_permalink()?>"></a> -->
										</div>
				<div class="postFooter">
					<div class="postFooter__descr">
						<div class="postFooter__tags">
							теги:  <?=$posttags?>
						</div>
						<div class="postFooter__text"><?=$content?></div>
					</div>
					<div class="postFooter__link">
						<a href="<?=$post_link?>">Читать полностью</a>
					</div>
				</div>
			</li>
			<?php //} ?>
		</ul>
	</div>
		<?php endwhile; else: ?>
						<p>Нет новостей! :(</p>
					<?php endif; ?>
				<?php wp_pagenavi(); ?>
				</div><!-- end frontPosts__list -->
				
			</div>
		</div>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>