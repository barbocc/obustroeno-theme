<div id="comments">

<?php if ( post_password_required() ) : ?>
	<div class="nopassword"><?php _e( 'This post is protected. Enter the password to view any comments.', 'obustroeno' ) ?></div>
	</div><!-- .comments -->
<?php return; endif; ?>

<?php if ( have_comments() ) : ?>
	<div id="comments-list" class="comments">
		<div id="comments-nav-above" class="comments-navigation">
			<?php paginate_comments_links(); ?>
		</div>
		<?php wp_list_comments('callback=comment_theme'); ?>
		<div id="comments-nav-below" class="comments-navigation">
			<?php paginate_comments_links(); ?>
		</div>
	</div><!-- #comments-list .comments -->
<?php endif; // check for comment navigation ?>
		
<?php if ( comments_open() ) : // If comments are open, but there are no comments ?>
	<h3 id="reply-title" class="comment-reply-title">Пожалуйста, оставьте комментарий</h3>
	<?php $usr = get_current_user_id(); print '<div class="com_ava">'.get_avatar($usr, 62).'</div>'; ?>
	<?php row_editor_comments(); ?>
	<div class="clear"></div>
	<?php else : // or, if we don't have comments:
			/* If there are no comments and comments are closed,
			 * let's leave a little note, shall we?
			 * But only on posts! We don't want the note on pages.
			 */
			if ( ! comments_open() && ! is_page() ) :
			?>
			<?php /*<p class="nocomments">Комментирование закрыто.</p>*/ ?>
			<?php endif; // end ! comments_open() && ! is_page() ?>

<?php endif // REFERENCE: if ( have_comments() ) ?>

</div><!-- #comments -->