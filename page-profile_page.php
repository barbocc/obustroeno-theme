<?php get_header(); 
?>
<div id="full_content">
<?php if (is_front_page() || is_page('firms') || is_page('foto')){
	print '<div id="top_fon"></div>';
}
// заглобалим переменную с объектом данных текущего пользователя
global $current_user;
?>
<div id="main_content">
	<div id="main">
<?php if (!is_user_logged_in()) { // если юзер не залогинен, форму показывать не будем ?>
<?php if($_GET['profile_id_upd'] != $current_user->ID) {
	print '<p>У вас нет доступа к этой странице.</p>';
	} ?>
<?php } elseif($_GET['profile_id_upd'] != $current_user->ID && !current_user_can('administrator')) {
	print '<p>У вас нет доступа к этой странице.</p>';
} else { // если залогинен, показываем страницу профиля 
$cuser_id = $current_user->ID; // запомним ID
if($_GET['profile_id_upd']) {
	unset( $current_user ); // удалим, чтобы wp_set_current_user() переустановила все заново
	wp_set_current_user( $_GET['profile_id_upd'] ); // переустанавливаем
}
?>
<!--<p class="user_prifile_title">Страница профиля пользователя: --><?php //echo $current_user->user_login; ?><!--</p>-->
<div class="author_editor__menu">
	<ul>
		<li class="active">Основное</li><li class="">Социальные сети</li><li>Фото</li><li style="border-left:1px solid #0171a1;">Настройка уведомлений</li><?php if(current_user_can('administrator') || current_user_can('expert')){?><li style="border-left:1px solid #0171a1;" class="">Оплата</li><?php } ?><li style="border-left:1px solid #0171a1;" id="siteExit">Выход</li>
	</ul>
</div>
<div class="author_editor__form visible">
<form name="profileform" id="profileform" method="post" class="userform" action="">

	<!--<input type="email" name="user_email" id="user_email" placeholder="Email" value="--><?php //echo $current_user->user_email; ?><!--" required>--><!-- ну тут всякие обычные поля -->
	<div class="author_editor__form__left">
		<div class="form_left_text">
			<p>Псевдоним</p>
			<p>Имя</p>
			<p>Фамилия</p>
			<p>День рождение</p>
			<p>Город</p>
			<p>Контакты</p>
			<p>E-mail</p>
			<p>Письма</p>
			<p>Изменить пароль</p>
		</div>
		<div class="form_left_enter">
			<input type="text" name="display_name" id="display_name" placeholder="Имя" value="<?php echo $current_user->display_name; ?>">
			<input type="text" name="first_name" id="first_name" placeholder="Имя" value="<?php echo $current_user->first_name; ?>">
			<input type="text" name="last_name" id="last_name" placeholder="Фамилия" value="<?php echo $current_user->last_name; ?>">
			<!--<input type="text" name="user_url" id="user_url" placeholder="Сайт" value="--><?php //echo $current_user->user_url; ?><!--">-->
			<input type="date" name="date" id="date" value="<?php echo get_usermeta($current_user->ID, 'date', true); ?>">
			<input type="text" name="sity" id="sity" placeholder="Город" value="<?php echo get_usermeta($current_user->ID, 'sity', true); ?>">
			<input type="text" name="contacts" id="contacts" placeholder="Контакты" value="<?php echo get_usermeta($current_user->ID, 'contacts', true); ?>">
			<?php if(get_current_user_id()==$current_user->ID) { ?>
				<input type="text" placeholder="none" value="<?php echo $current_user->user_email; ?>" disabled="disabled">
			<?php } ?>
			<input type="checkbox" name="stop_send_letters" id="stop_send_letters" value="1" <?php checked(get_usermeta($current_user->ID, 'stop_send_letters', true), true);?>>
			<input type="password" name="current_pass" id="current_pass" placeholder="Текущий пароль"><!-- если захотят поменять пароль, надо будет заполнить все 3 поля -->
			<input type="password" name="pass1" id="pass1" placeholder="Новый пароль">
			<input type="password" name="pass2" id="pass2" placeholder="Повторите новый пароль">
		</div>
	</div>
	<div class="author_editor__form__right">
	    <span class="about">Об авторе</span>
	    <?php //echo get_avatar($current_user->ID,25); //функция достает аватар, первый параметр - ID пользователя, второй - размер ?>
	    <?php //if(current_user_can('manage_options')){pr(get_usermeta($current_user->ID));} ?>
	    <!--<input type="file" name="avatar" id="avatar" placeholder="Добавить фотографию"> <span></span>-->

	    <label class="file_upload">
	    	<div class="editor_picture"><?php echo get_avatar($current_user->ID,25); ?></div>
	        <span class="button">Добавить фотографию</span>
	        <!--<mark>Файл не выбран</mark>-->
	        <input type="file" name="avatar" id="avatar">
	    </label>

	    <textarea name="description" placeholder="Обо мне"><?php echo $current_user->description; ?></textarea>
		<input type="text" name="status" id="status" placeholder="Статус" value="<?php echo get_usermeta($current_user->ID, 'status', true); ?>">
	</div>
	<div class="clear"></div>
	<input type="submit" value="Сохранить"> <!-- субмит -->
	<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"> <!-- куда отправим юзера если все прошло ок, в нашем случае это не понадобиться, а вообще может если форма сквозная -->
	<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('edt_pr_nonce'); ?>"> <!-- поле со строкой безопасности, будем проверять её в обработчике чтобы убедиться, что форма отправлена откуда надо -->
	<input type="hidden" name="action" value="edit_profile"> <!-- обязательное поле, по нему запустится нужная функция -->
	<input type="hidden" name="profile_id_upd" value="<?=$_GET['profile_id_upd']?>">
	<div class="response"></div> <!-- ну сюда будем пихать ответ от сервера -->
</form>
<?php 
if(current_user_can('manage_options')){
	//print do_shortcode('[avatar_upload]');
}
?>
</div>



<div class="author_editor__form">
<form name="profileform" id="profileform2" method="post" class="userform" action="">
	<?php //echo get_ulogin_user_accounts_panel_front(); ?>
	<div class="form_block">
		<img src="/wp-content/themes/1brus_mag/img/red_vk_ico.png">
		<input type="text" name="profile_vk" id="profile_vk" placeholder="https://vk.com/id профиля" value="<?php echo $current_user->profile_vk; ?>">
		<span><a>Добавить</a></span>
	</div>
	<div class="form_block">
		<img src="/wp-content/themes/1brus_mag/img/red_ok_ico.png">
		<input type="text" name="profile_ok" id="profile_ok" placeholder="https://ok.ru/profile/id профиля" value="<?php echo $current_user->profile_ok; ?>">
		<span><a>Добавить</a></span>
	</div>
	<div class="form_block">
		<img src="/wp-content/themes/1brus_mag/img/red_fb_ico.png">
		<input type="text" name="profile_fb" id="profile_fb" placeholder="https://www.facebook.com/id профиля" value="<?php echo $current_user->profile_fb; ?>">
		<span><a>Добавить</a></span>
	</div>
	<div class="form_block">
		<img src="/wp-content/themes/1brus_mag/img/red_tw_ico.png">
		<input type="text" name="profile_tw" id="profile_tw" placeholder="https://twitter.com/id профиля" value="<?php echo $current_user->profile_tw; ?>">
		<span><a>Добавить</a></span>
	</div>
	<div class="form_block">
		<img src="/wp-content/themes/1brus_mag/img/red_yout_ico.png">
		<input type="text" name="profile_yout" id="profile_yout" placeholder="https://www.youtube.com/channel/id канала" value="<?php echo $current_user->profile_yout; ?>">
		<span><a>Добавить</a></span>
	</div>
	<div class="form_block">
		<img src="/wp-content/themes/1brus_mag/img/red_in_ico.png">
		<input type="text" name="profile_in" id="profile_in" placeholder="https://www.instagram.com/id профиля" value="<?php echo $current_user->profile_in; ?>">
		<span><a>Добавить</a></span>
	</div>

	<input type="submit" value="Сохранить"> <!-- субмит -->
	<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"> <!-- куда отправим юзера если все прошло ок, в нашем случае это не понадобиться, а вообще может если форма сквозная -->
	<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('edt_pr_nonce'); ?>"> <!-- поле со строкой безопасности, будем проверять её в обработчике чтобы убедиться, что форма отправлена откуда надо -->
	<input type="hidden" name="action" value="edit_profile"> <!-- обязательное поле, по нему запустится нужная функция -->
	<input type="hidden" name="profile_id_upd" value="<?=$_GET['profile_id_upd']?>">
	<div class="response"></div> <!-- ну сюда будем пихать ответ от сервера -->
</form>
</div>

<div class="author_editor__form">
	<h1 style="font-size:18px;font-weight:bold;text-transform:uppercase;margin:10px 0;">Добавить фото в галерею</h1>
	<form name="profileform" id="profileform3" method="post" class="userform" action="">
		<label class="file_upload_fotogallery">
	        <span class="button"></span>
	        <!--<mark>Файл не выбран</mark>-->
	        <input type="file" name="fotogallery" id="fotogallery">
	    </label>
	    <label class="file_upload_fotogallery">
	        <span class="button"></span>
	        <!--<mark>Файл не выбран</mark>-->
	        <input type="file" name="fotogallery2" id="fotogallery2">
	    </label>
	    <label class="file_upload_fotogallery">
	        <span class="button"></span>
	        <!--<mark>Файл не выбран</mark>-->
	        <input type="file" name="fotogallery3" id="fotogallery3">
	    </label>
	    <label class="file_upload_fotogallery">
	        <span class="button"></span>
	        <!--<mark>Файл не выбран</mark>-->
	        <input type="file" name="fotogallery4" id="fotogallery4">
	    </label>

		<div class="clear"></div>
		<input type="submit" value="Сохранить"> <!-- субмит -->
		<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"> <!-- куда отправим юзера если все прошло ок, в нашем случае это не понадобиться, а вообще может если форма сквозная -->
		<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('edt_pr_nonce'); ?>"> <!-- поле со строкой безопасности, будем проверять её в обработчике чтобы убедиться, что форма отправлена откуда надо -->
		<input type="hidden" name="action" value="edit_profile"> <!-- обязательное поле, по нему запустится нужная функция -->
		<input type="hidden" name="profile_id_upd" value="<?=$_GET['profile_id_upd']?>">
		<div class="response"></div> <!-- ну сюда будем пихать ответ от сервера -->
	</form>
</div>

<div class="author_editor__form">
<div class="feedOptions">
<?php
global $wpdb;
$table = $wpdb->prefix.feedUsers;
$user = get_current_user_id();
$var = $wpdb->get_var("SELECT `permission` FROM $table WHERE `user_id` = $user;");
$perm = unserialize($var);
?>
<form method="post" name="feedOptionsForm">
	<table>
		<tr>
			<td class="feedOptionsForm-headers">Настройка ленты</td>
			<td class="feedOptionsForm-headers">Email оповещение</td>
		</tr>
		<tr>
			<td class="feedOptionsForm-headers"><input type="checkbox" id="allCheckOne" name="allCheckOne" <?= $perm['allCheckOne'] == 'on' ? 'checked="checked"':''; ?>><label for="allCheckOne">Отметить всё</label></td>
			<td class="feedOptionsForm-headers"><input type="checkbox" id="allCheckSecond" name="allCheckSecond" <?= $perm['allCheckSecond'] == 'on' ? 'checked="checked"':''; ?>><label for="allCheckSecond">Отметить всё</label></td>
		</tr>
		<tr>
            <td><input type="checkbox" id="heRepliesInYourTopics" name="heRepliesInYourTopics" <?= $perm['heRepliesInYourTopics'] == 'on' ? '':''; ?>><label for="" class="nobefore">Ответы в ваших темах</label></td>
        	<td><input type="checkbox" class="feedOptionsFormSecond" id="heRepliesInYourTopicsSub" name="heRepliesInYourTopicsSub" <?= $perm['heRepliesInYourTopicsSub'] == 'on' ? 'checked="checked"':''; ?>><label for="heRepliesInYourTopicsSub">&nbsp;</label></td>
        </tr>
		<tr>
            <td><input type="checkbox" id="MyReply" name="MyReply" <?= $perm['MyReply'] == 'on' ? '':''; ?>><label for="" class="nobefore">Ответы в темах с вашим участием</label></td>
        	<td><input type="checkbox" class="feedOptionsFormSecond" id="MyReplySub" name="MyReplySub" <?= $perm['MyReplySub'] == 'on' ? 'checked="checked"':''; ?>><label for="MyReplySub">&nbsp;</label></td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="MyLikeCom" name="MyLikeCom" <?= $perm['MyLikeCom'] == 'on' ? 'checked="checked"':''; ?>><label for="MyLikeCom">Нравится/не нравится мой комментарий</label></td>
            <td><input type="checkbox" class="feedOptionsFormSecond" id="MyLikeComSub" name="MyLikeComSub" <?= $perm['MyLikeComSub'] == 'on' ? 'checked="checked"':''; ?>><label for="MyLikeComSub">&nbsp;</label></td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="yourLikeCom" name="yourLikeCom" <?= $perm['yourLikeCom'] == 'on' ? 'checked="checked"':''; ?>><label for="yourLikeCom">Мне нравится/не нравится чей то комментарий</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="MyLikeReply" name="MyLikeReply" <?= $perm['MyLikeReply'] == 'on' ? 'checked="checked"':''; ?>><label for="MyLikeReply">Нравится/не нравится мой ответ на форуме</label></td>
            <td><input type="checkbox" class="feedOptionsFormSecond" id="MyLikeReplySub" name="MyLikeReplySub" <?= $perm['MyLikeReplySub'] == 'on' ? 'checked="checked"':''; ?>><label for="MyLikeReplySub">&nbsp;</label></td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="YourLikeReply" name="YourLikeReply" <?= $perm['YourLikeReply'] == 'on' ? 'checked="checked"':''; ?>><label for="YourLikeReply">Мне Нравится/не нравится ответ на форуме</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" id="MyBlogCom" name="MyBlogCom" disabled="disabled" <?= $perm['MyBlogCom'] == 'on' ? 'checked="checked"':'checked="checked"'; ?>><label class="MyBlogLabel" for="MyBlogCom">Комментарии к моим блогам</label></td>
            <td><input type="checkbox" class="feedOptionsFormSecond" id="MyBlogComSub" name="MyBlogComSub" <?= $perm['MyBlogComSub'] == 'on' ? 'checked="checked"':''; ?>><label for="MyBlogComSub">&nbsp;</label></td>
        </tr>
        <tr>
            <td><input type="checkbox" id="MyComYourBlog" name="MyComYourBlog" disabled="disabled" <?= $perm['MyComYourBlog'] == 'on' ? 'checked="checked"':'checked="checked"'; ?>><label class="MyBlogLabel" for="MyComYourBlog">Мои комментарии к блогам</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" id="MyCommentMyAction" name="MyCommentMyAction" <?= $perm['MyCommentMyAction'] == 'on' ? '':''; ?>><label for="" class="nobefore">Комментарии к блогам с моим участием</label></td>
            <td><input type="checkbox" class="feedOptionsFormSecond" id="MyCommentMyActionSub" name="MyCommentMyActionSub" <?= $perm['MyCommentMyActionSub'] == 'on' ? 'checked="checked"':''; ?>><label for="MyCommentMyActionSub">&nbsp;</label></td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="AddMyFavorite" name="AddMyFavorite" <?= $perm['AddMyFavorite'] == 'on' ? 'checked="checked"':''; ?>><label for="AddMyFavorite">Добавление в избранное мной</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="YourFavoriteMyPost" name="YourFavoriteMyPost" <?= $perm['YourFavoriteMyPost'] == 'on' ? 'checked="checked"':''; ?>><label for="YourFavoriteMyPost">Кто то добавил/удалил мою статью в избранное</label></td>
            <td><input type="checkbox" class="feedOptionsFormSecond" id="YourFavoriteMyPostSub" name="YourFavoriteMyPostSub" <?= $perm['YourFavoriteMyPostSub'] == 'on' ? 'checked="checked"':''; ?>><label for="YourFavoriteMyPostSub">&nbsp;</label></td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="MyThanks" name="MyThanks" <?= $perm['MyThanks'] == 'on' ? 'checked="checked"':''; ?>><label for="MyThanks">Добавление в "спасибо" мной</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="AddYourMyPost" name="AddYourMyPost" <?= $perm['AddYourMyPost'] == 'on' ? 'checked="checked"':''; ?>><label for="AddYourMyPost">Кто то сказал мне "спасибо"</label></td>
            <td><input type="checkbox" class="feedOptionsFormSecond" id="AddYourMyPostSub" name="AddYourMyPostSub" <?= $perm['AddYourMyPostSub'] == 'on' ? 'checked="checked"':''; ?>><label for="AddYourMyPostSub">&nbsp;</label></td>
        </tr>
        <!--<tr>
            <td><input disabled="disabled" type="checkbox" id="MyHonor" name="MyHonor" <?= $perm['MyHonor'] == 'on' ? 'checked="checked"':''; ?>><label for="MyHonor" class="nobefore">Назначение медалей мне (настройка заблокирована)</label></td>
            <td><input disabled="disabled" type="checkbox" id="MyHonorSub" name="MyHonorSub" <?= $perm['MyHonorSub'] == 'on' ? 'checked="checked"':''; ?>><label for="MyHonorSub" class="nobefore">&nbsp;</label></td>
        </tr>
        <tr>
            <td><input disabled="disabled" type="checkbox" id="YourHonor" name="YourHonor" <?= $perm['YourHonor'] == 'on' ? 'checked="checked"':''; ?>><label for="YourHonor" class="nobefore">Назначение медалей кому то (настройка заблокирована)</label></td>
            <td><input disabled="disabled" type="checkbox" id="YourHonorSub" name="YourHonorSub" <?= $perm['YourHonorSub'] == 'on' ? 'checked="checked"':''; ?>><label for="YourHonorSub" class="nobefore">&nbsp;</label></td>
        </tr>-->
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="MyBlogpost" name="MyBlogpost" <?= $perm['MyBlogpost'] == 'on' ? 'checked="checked"':''; ?>><label for="MyBlogpost">Новые записи мои</label></td>
            <td>&nbsp;</td>
        </tr>
        <?php 
        /* 
         * Глобальные настройки ленты у пользователя 
         */ 
        ?>
        <tr>
        	<td colspan="2" style="border:none !important;"><h2 style="text-align:center;font:17px Verdana;padding:10px;">Глобальные настройки ленты активности</h2></td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="gNewReply" name="gNewReply" <?= $perm['gNewReply'] == 'on' ? 'checked="checked"':''; ?>><label for="gNewReply">Новые вопросы</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="gLikeCom" name="gLikeCom" <?= $perm['gLikeCom'] == 'on' ? 'checked="checked"':''; ?>><label for="gLikeCom">Нравится/не нравится комментарий</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="gLikeReply" name="gLikeReply" <?= $perm['gLikeReply'] == 'on' ? 'checked="checked"':''; ?>><label for="gLikeReply">Нравится/не нравится ответ на форуме</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="gFoto" name="gFoto" <?= $perm['gFoto'] == 'on' ? 'checked="checked"':''; ?>><label for="gFoto">Новые фото</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="gPostProf" name="gPostProf" <?= $perm['gPostProf'] == 'on' ? 'checked="checked"':''; ?>><label for="gPostProf">Новые записи профессиональные</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="gPostPriv" name="gPostPriv" <?= $perm['gPostPriv'] == 'on' ? 'checked="checked"':''; ?>><label for="gPostPriv">Новые записи личные</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="gPostInter" name="gPostInter" <?= $perm['gPostInter'] == 'on' ? 'checked="checked"':''; ?>><label for="gPostInter">Новые записи интересные</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="gPostAll" name="gPostAll" <?= $perm['gPostAll'] == 'on' ? 'checked="checked"':''; ?>><label for="gPostAll">Новые записи все</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><input type="checkbox" class="feedOptionsFormFirst" id="gNewCom" name="gNewCom" <?= $perm['gNewCom'] == 'on' ? 'checked="checked"':''; ?>><label for="gNewCom">Новые комментарии в блогах</label></td>
            <td>&nbsp;</td>
        </tr>
	</table>
	<input type="submit" name="feedOptionsSubmit" value="Сохранить">
	<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"> <!-- куда отправим юзера если все прошло ок, в нашем случае это не понадобиться, а вообще может если форма сквозная -->
	<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('edt_pr_nonce'); ?>"> <!-- поле со строкой безопасности, будем проверять её в обработчике чтобы убедиться, что форма отправлена откуда надо -->
	<input type="hidden" name="action" value="edit_profile"> <!-- обязательное поле, по нему запустится нужная функция -->
	<input type="hidden" name="profile_id_upd" value="<?=$_GET['profile_id_upd']?>">
	<div class="response"></div> <!-- ну сюда будем пихать ответ от сервера -->
</form>
</div>
</div>
	<div class="clear"></div>

<?php if(current_user_can('administrator') || current_user_can('expert')){?>
<div class="author_editor__form">
<div class="forum_statistic">
   <?php
   
$data = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA1IAAAE7CAYAAADXQrC8AAANvUlEQVR4nO3dLXoqWRuGUebDGBgBA0BjsZFxuDhcVFQMKiYmBoPBIDCImAgEAoGoCexPpM9PkirgSYo06W+JZbr7UPu8u0XdV8GuTlVVBQAAgNN1/u0FAAAA/DRCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQAAABCQgoAACAkpAAAAEJCCgAAICSkAAAAQkIKAAAgJKQA/g/st5uyWi7K09NjeXx8LE9Ps7JYPZfd/v9rDf8F5ghwGYQUQJPdsoyHwzIajXLDYbm6nZV9w2cMB8MyXW7fXG+3mpbh4P1/OyzD62nZVlWpql15GI/K8P1nDUflbv5S+3fYrJ7KeNgrnU6nQbeMbqblZXfqXC5hDZ/Yp+GwXN8vv3//D8zs0J8dTz+uNZ5ja+sHoI6QAmiwXUwO3LSeoHdTNtt56Tf8+5v55rTr9SZlU1WlqrZl0qv/rN549m79+zKbDE9ea/fDn29yCWt4a3fqPvUm/wTpN+7/kZk16d3MvzzH9tYPQB0hBdDgyzei/UnZ7uZl0EJIvQbAttz26z+r/+bGuyrzySBa6/Xj84lzuYQ1vLWejk68xqgsg6+/tbL/R2bW5O9ZfnaO7a0fgDpCCqDByU86Gg3LYvP9IbVb3sVrfb+WZpewhrdm4/6J1+iVx5dv3v/d4ZkdC6mvzLG99QNQR0gBNFiETwI+6pfZy3eH1LbcDdJ19srD8/7EuVzCGt6uZxJEShJrrez/9vDMmrx+te9rc2xv/QDUEVIADTbzuw+HPwwHzT/27w/fHhIxGI7L8rufSG2eSu/IDXJ/MCyDfveTN8yXsIa/7JdlWHeNbrf2uoPbxffu/5EnUr1+zUEkw8HrwRhfnGN76wegjpACCOyXtw03ooOyqPv9zTf/Rurg72J612X+vPt9vc3y4XVt3euyPnkGl7CGv7w81sbGcPJQbocf/3n36uFLJ9HF+39kZpPFrvFa55jj59YPQB0hBRBovrlteKLyzSG1nl413nzfrz+ub7eclE5nHJzOdglr+Gtm85uGQFmV+7qvxX3xJLp4/4/M7NBXDc8xx8+tH4A6QgogcOhGdH4BITWfNBy80L0uz7V/p31Zr9bBU5pLWMMfy9v63wFNltuyqv13gzL/wtfV4v0/MrNDIXWOOX5u/QDUEVIAgTZDarJ490Lec4bU73dRfdUlrOGXfXkY1f0Wql/m+6ohpLrlftX8dbrW9//IzG7m28ZrnWOOQgqgPUIKINBmSF1P5+V5vS6r1aqs189lMb3+ckitH5q+DtYt0/VnTsV77xLW8Mum3NS+6LZf5ruq7Bv2avz08n37f2Rm46fnUu13Zbd7a3+mOQopgPYIKYBAmyF1siCkdo2HCXRKp9Mrt7P1F2dwCWv4M9v+gSc2Tet4/+Lgs+7/kZk1uZlvzjJHIQXQHiEFELj0kGo8DvzN5w3L7cO8bD51StslrOHVbtXwstpfX31rmv3gruy+a/+PzOxQSJ1jjkIKoD1CCiBw8SFVNR/A8FG3XN/Nwqi4hDW8enkaHw26Ud01P3vU+mf2/8jMDobUGeYopADaI6QAAj8hpKpqW6ZXzS9e/fj547I6+SS7S1jDq0XDYQx/1tG01s8f9f3dIdX2HIUUQHuEFEDgZ4TUq/XTXRnWHsZQ4+Svu13CGl7XcVf3nqhOp0x+n4TXdKrfxxMTz7b/R2ZWr1um67cnC7Y1RyEF0B4hBRC49PdI1XlZzcrk6vhXxMazU06zu4Q1VKWq1uWqW/8ZvcmsPK9XZb1el4fr+qdWo/vV9+z/kZmNH9dlt92WzWbzx7b5RL6vzlFIAbRHSAEEfmJI/bJ/WZRxw1OcTqdTulePJ8zgEtZQlWo7qz+x70S98ex79v/IzA69R+occxRSAO0RUgCBnxxSr9blqikwfl/nkEtYw4GXF5/qxOt8ef+PzOz9/wOZfI5CCqA9Qgog8DNCalcWT/PGUHh5bHjxb2dQ5kcPfLiENVRldT/6Wkh1hmURHm7xqf0/MrPjIdXuHIUUQHuEFEDgZ4TUttx0OmVwu6j9OzQ/zRmccDN9CWuoytM4OMmuVq88PDf/Fqm1/T8ys8lid+Sa7c5RSAG0R0gBBH5KSP367wbjaXl+92Ti4ar+JLt2v9p33jVMwuPET5n/Wfb/yMx6w6tyfX39xtXVqNw8rM4yRyEF0B4hBRD4aSH1qltG40m5n96X8aj+FLtOp1M6/duzhFTra9gtmmf6z2l1+/2+7PdVqapNY3T1J/VPeVrd/yMza9JrfBfW1+YopADaI6QAAj8ipD757qrhSUeC//tr2D8/lO6JM62qqsxv6oOjO5qW/bn3/8jMmvyeZctzFFIA7RFSAIH/bkid9tukS1jDdn7T8Od75fHl4++eFpOGJze9m/Jy7v0/MrPzhFTzHIUUQHuEFEDg0I3orO5GdDtvfN9RElKbqiqHfhv0PmLSdyxNTv690L+/hsWk6YW09TGwaQyvhj1rc/+PzKxJ76+QanOOn1s/AHWEFECg+Ua04Tjt3bKMGm5475a70z578Ov3LrtyP6w/XKA/efsOp+3zvNyOR6V37IZ9cF1m62Mnx/3t31/DvDGkRmW5T/asX542Z97/f2Y2HTUcCtHgaro+yxw/t34A6ggpgP+0fdk8r8ti9lQeHx7KdDotD49PZbZYlpdtfvz3z13Df4E5AlwSIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQElIAAAAhIQUAABASUgAAACEhBQAAEBJSAAAAISEFAAAQ+h9OaCdW3kXmdwAAAABJRU5ErkJggg==';
$data = str_replace('data:image/png;base64,', '', $data);
$data = str_replace(' ', '+', $data);
$data = base64_decode($data); // Decode image using base64_decode
$file = uniqid() . '.png'; //Now you can put this image data to your desired file using file_put_contents function like below:
$success = file_put_contents($file, $data);

//echo '<img src="'.uniqid() . '.png" />';
    //var_dumP($current_user->role);
   
   wif_4forum_full_statistic_tab($current_user->roles[0]=='administrator');
   ?>
   </div>
   <div class="clear"></div>
</div>
<?php }} ?>

<div class="author_editor__form">
	<div style="font:14px Tahoma;padding:10px 0;">Осуществляем выход...</div>
</div>

</div>	
</div>
<?php get_footer(); ?>