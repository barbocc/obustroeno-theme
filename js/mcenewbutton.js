(function() {
	tinymce.create('tinymce.plugins.price', {
	 	init : function(ed, url) {
			ed.addButton('price', {
				icon: 'perec',
				title : 'Cтоимость, время, сложность',
				image : url+'/mseico/bar_chart.png',
				onclick: function() {
					ed.windowManager.open( {
						title: 'Задайте параметры полей',
						body: [
							{
								type: 'textbox', // тип textbox = текстовое поле
								name: 'tx1', // ID, будет использоваться ниже
								label: 'Стоимость', // лейбл
								value: '0' // значение по умолчанию
							},
							{
								type: 'textbox',
								name: 'tx2',
								label: 'Баллы',
								value: '0'
							},
							{
								type: 'textbox',
								name: 'tx3',
								label: 'Часы',
								value: '0'
							}
						],
						onsubmit: function( e ) { // это будет происходить после заполнения полей и нажатии кнопки отправки
							ed.insertContent( '[CPFB money="'+e.data.tx1+'" ball="'+e.data.tx2+'" time="'+e.data.tx3+'"]');
						}
					});
				}
			});
		},
		createControl : function(n, cm) {
			return null;
		},
	});
	tinymce.PluginManager.add('price', tinymce.plugins.price);
})();