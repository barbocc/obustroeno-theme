jQuery(document).ready(function($) {

	// Добавление превью картинки
	jQuery('.lform_block').on('change', 'input', function(e){
		var th = jQuery(this).closest('div').find('label');
		var files = $(this).get(0).files;
	    if (files && files.length) {
	      jQuery.each(files, function(i, file) {
	        if (/^image\/\w+/.test(file.type)) {
	          var src = URL.createObjectURL(file);
	          th.html('<img src="'+src+'">');
	        } else {
	          window.alert('Пожалуйста, выберите изображение');
	        }
	      })
	    }
	})

	// Проверка валидности e-mail
	$("#lemail").keyup(function(){
    	var email = jQuery("#lemail").val();
	    if(email != 0) {
	    	if(isValidEmailAddress(email)) {
	    		jQuery("#lemail").css('outline', 'none');
	    	} else {
	    		jQuery("#lemail").css('outline', '1px solid tomato');
	    	}
	    } else {
	    	jQuery("#lemail").css('outline', 'none');
	    }
    })
    function isValidEmailAddress(emailAddress) {
    	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    	return pattern.test(emailAddress);
    }
	jQuery('#ltext').keyup(function() {
		jQuery(this).css('outline', 'none');
	});

	// Регистрация пользователя и создание записи
	jQuery('#lbtn').click(function(e){
		th = jQuery(this);
		e.preventDefault();
		var lemail = jQuery('#lemail').val();
		var ltext = jQuery('#ltext').val();
		jQuery.ajax({
			type: 'POST',
			url: '/wp-admin/admin-ajax.php',
			data: 'action=landing_start_check&lemail='+lemail+'&ltext='+ltext,
			success: function(data, html) {
				if(data == '1') {
					jQuery('#lemail').css('outline', '1px solid red');
				} else if(data == '2') {
					jQuery('#lemail').css('outline', '1px solid red');
				} else if(data == '3') {
					jQuery('#ltext').css('outline', '1px solid red');
				} else if(data == 4) {
					jQuery('#respond').show().val('Подождите, идет добавление записи в дневник...');
					jQuery('#lform').submit();
				} else if(data == 5) {
					jQuery('#respond').show().val('Вы уже зарегистрированы!');
				}else {
					jQuery('#respond').show().val('Непридвиденная ошибка, попробуйте еще раз!');
				}
			}
		})
	})
});