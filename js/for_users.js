var ajaxgo = false; // глобальная переменная, чтобы проверять обрабатывается ли в данный момент другой запрос
jQuery(document).ready(function(){ // после загрузки DOM
	var userform = jQuery('.userform'); // пишем в переменную все формы с классом userform
	function req_go(data, form, options) { // ф-я срабатывающая перед отправкой
	    if (ajaxgo) { // если какой либо запрос уже был отправлен
		form.find('.response').html('<p class="error">Необходимо дождаться ответа от предыдущего запроса.</p>'); // в див для ответов напишем ошибку
		return false; // и ничего не будет делать
	    }
            form.find('input[type="submit"]').attr('disabled', 'disabled').val('Подождите..'); // выключаем кнопку и пишем чтоб подождали
	    form.find('.response').html(''); // опусташаем див с ответом
	    ajaxgo = true; // записываем в переменную что аякс запрос ушел
	    console.log(data);
	    console.log('req_go');
	}
	function req_come(data, statusText, xhr, form)  { // ф-я срабатывающая после того как пришел ответ от сервера, внутри data будет json объект с ответом
		var response = '<p class="success">Регистрация выполнена!</p>'; // пишем ответ в <p> с классом success
		form.find('input[type="submit"]').val('Готово'); // в кнопку напишем Готово
		console.log(data);
		console.log('req_come');
		if (data.success) { // если все хорошо и ошибок нет
			var response = '<p class="success">'+data.data.message+'</p>'; // пишем ответ в <p> с классом success
			form.find('input[type="submit"]').val('Готово'); // в кнопку напишем Готово
			var form = jQuery('#autorize');
			var tmp = form.find('input[type="submit"]').val();
			jQuery('input[type="submit"]').click(function(){
				if(tmp == 'Готово'){
					jQuery('#autorize').hide();
					jQuery('#fon_autorize').hide();
					console.log('hide');
				}
			});
		} else {  // если есть ошибка
		    var response = '<p class="error">'+data.data.message+'</p>'; // пишем ответ в <p> с классом error
		    form.find('input[type="submit"]').prop('disabled', false).val('Отправить'); // снова включим кнопку
		}
		form.find('.response').html(response); // выводим ответ
		if (data.data.redirect) window.location.href = data.data.redirect; // если передан redirect, делаем перенаправление
		ajaxgo = false; // аякс запрос выполнен можно выполнять следующий
	}

	var args = { // аргументы чтобы прикрепить аякс отправку к форме
		dataType:  'json',
		beforeSubmit: req_go, // ф-я которая сработает перед отправкой
		success: req_come, // ф-я которая сработает после того как придет ответ от сервера
                error: function(data) { // для дебага
			console.log(arguments);
		},
		error: function(jqXHR, textStatus, errorThrown){
			console.log('Ошибка: ' + textStatus + ' | ' + errorThrown);
		},
		url: ajax_var.url  // куда отправляем, задается в wp_localize_script  
	}; 
	userform.ajaxForm(args); // крепим аякс к формам

	jQuery('.logout').click(function(e){ // ловим клик по ссылке "выйти"
		e.preventDefault(); // выключаем стандартное поведение
		if (ajaxgo) return false; // если в данный момент обрабатывается другой запрос то ничего не делаем
		var lnk = jQuery(this); // запишем ссылку в переменную
		jQuery.ajax({ // инициализируем аякс
		    type: 'POST', // шлем постом
		    url: ajax_var.url, // куда шлем
		    dataType: 'json', // ответ ждем в json
		    data: 'action=logout_me&nonce='+jQuery(this).data('nonce'), // что отправляем
	        beforeSend: function(data) { // перед отправкой
	        	lnk.text('Подождите...'); // напишем чтоб подождали
	        	ajaxgo = true; // аякс отпраляется
	        },
	        success: function(data){ // после того как ответ пришел
	        	if (data.success) { // если ошибок нет
	        		lnk.text('Выходим...'); // пишем что выходим
	        		window.location.reload(true); // и обновляемся
	        	} else { // если ошибки есть
	        		alert(data.data.message); // просто покажим их алертом
	        	}
	        },
	        error: function (xhr, ajaxOptions, thrownError) { // для дебага
	            console.log(arguments);
	        },
	        complete: function(data) { // при любом исходе
	            ajaxgo = false; // аякс больше не выполняется
	        }       
		});
	});
});

//Всплывающий блок вывода полного текста статуса в профиле автора
jQuery(document).ready(function(){
	var tooltip = jQuery('.tooltip');
	jQuery('#personal_page .status').mousemove(function(){
		tooltip.show();
	}).mouseout(function () {
        tooltip.hide();
    });
    tooltip.mousemove(function(){
		return false;
	}).mouseout(function () {
        return false;
    });
});

//Всплывающая подсказка награды пользователя
jQuery(document).ready(function(){
	var block = jQuery('.honors .honors_one');
	var tooltip = jQuery('.honor_tooltip');
	block.mousemove(function(){
		jQuery(this).find(tooltip).show();
	}).mouseout(function () {
        jQuery(this).find(tooltip).hide();
    });
    tooltip.mousemove(function(){
		return false;
	}).mouseout(function () {
        return false;
    });
});

jQuery(document).ready(function(){
	 jQuery('.author_editor__menu ul li').click(function(){
		if (!jQuery(this).hasClass('active')) {
			jQuery('.author_editor__menu ul li').removeClass('active');
			jQuery(this).addClass('active');
			var index = jQuery(this).index();
			jQuery('.author_editor__form').removeClass('visible');
			jQuery('.author_editor__form').eq(index).addClass('visible');
		}
	}); 
});


//Вкладка добавления социальных сетей на странице редактирования профилля
jQuery(document).ready(function(){
	jQuery('.form_block input').each(function() {
		var thiss = jQuery(this);
		var value = jQuery(this).val();
		if(value != ''){
			thiss.css('display', 'block');
			thiss.closest('.form_block').find('span').addClass('nax').hide();
		}
		jQuery('.form_block span a').click(function(e){
			var qwe = jQuery(e.target);
			qwe.closest('.form_block').find('input').css('display', 'block');
			qwe.closest('.form_block').find('span').addClass('nax').hide();
		});
	});
});