jQuery(document).ready(function() {
	jQuery('.single_posts blockquote p').before('<p>Обратите внимание!</p>').addClass('qheader').end().appendTo('</blockquote>');

	jQuery(document).on('click', '.delete_favorites_profile', function(){
		var th = jQuery(this);
		var user_id = th.data('user_id');
		var post_id = th.data('post_id');
		if(confirm("Уверены, что хотите удалить из избранного?")) {
			jQuery.ajax({
				type: "POST",
				url: "/wp-admin/admin-ajax.php",
				data: "action=post_favorites_user&user_id="+user_id+"&post_id="+post_id,
				beforeSend: function(){
					th.css('font-size', '12').css('right', '13').val('Удаление...');
				},
				success: function(data){
					th.closest('.ideas_block').remove();
				}
			})
		}
	})

	jQuery(document).on('mousemove', '.ideas_block', function(){
		jQuery(this).closest('.ideas_block').find('.delete_favorites_profile').show();
	})

	jQuery(document).on('mouseleave', '.ideas_block', function(){
		jQuery(this).closest('.ideas_block').find('.delete_favorites_profile').hide();
	})

	jQuery(document).on('mousemove', '.bottom_post_buttons img', function(){
		var left_os = jQuery('.bottom_post_buttons').offset().left;
		var left = jQuery(this).offset().left;
		jQuery(this).closest('a').find('.tooltip_favorite').css('left', left - left_os).show();
	})

	jQuery(document).on('mouseleave', '.bottom_post_buttons img', function(){
		jQuery(this).closest('a').find('.tooltip_favorite').hide();
	})

	jQuery('.tocMyLinkNoLog').click(function(e){
		e.preventDefault();
		jQuery('#autorize').show();
		jQuery('#fon_autorize').show();
	})

	// jQuery('.aec_anon_text').each(function() {
	// 	jQuery(this).text('Редактировать');
	// })

	jQuery('#search input[type="text"]').focus(function(){
		jQuery('.search_placeholder').hide();
	})
	jQuery('#search input[type="text"]').blur(function(){
		jQuery('.search_placeholder').show();
	})

	function resWindow() {
		var win = jQuery(window).width();
		if(win <= 600) {
			jQuery('#header-logo').find('img').css('width', '100px').attr('src', 'http://obustroeno.com/wp-content/themes/1brus_mag/img/min_logo.svg');
		} else {
			jQuery('#header-logo').find('img').css('width', '200px').attr('src', 'http://obustroeno.com/wp-content/themes/1brus_mag/img/logo.svg');
		}
	}

	resWindow();

	jQuery(window).resize(function(){
		resWindow();
	})

	//Выход с аккаунта
	jQuery('#siteExit').click(function(){
		jQuery.ajax({
		    type: 'POST',
		    url: "/wp-admin/admin-ajax.php",
		    data: 'action=siteExit',
	        success: function(){
	        	var url = window.location.host;
				window.location.replace('http://'+url);
	        }
		});
	})

	//Ответ на комментарий пользователя
	jQuery('.page_lenta').on('click', '.butComFeed', function(){
		var comID = jQuery(this).data('id');
		var elem = jQuery(this).closest('.all_comments').find('input[name="feedParent"]');
		var elemCom = jQuery(this).closest('.all_comments').find('.textComment');
		var name = jQuery(this).closest('.comment_item').find('.feed__comment_item-name img').attr('alt');
		if(elem.val() > 0 && elem.val() == comID) {
			elem.val('0');
			elemCom.attr('placeholder', 'Введите комментарий');
		} else {
			elem.val(comID);
			elemCom.attr('placeholder', 'Ответ пользователю '+name);
		}
	})

	//Ответ на комментарий пользователя
	jQuery('.page_lenta').on('click', '.butComFeedTopic', function(){
		var comID = jQuery(this).data('id');
		var elem = jQuery(this).closest('.all_comments').find('input[name="feedParentTopic"]');
		var elemCom = jQuery(this).closest('.all_comments').find('.textCommentTopic');
		var name = jQuery(this).closest('.comment_item').find('.feed__comment_item-name img').attr('alt');
		if(elem.val() > 0 && elem.val() == comID) {
			elem.val('0');
			elemCom.attr('placeholder', 'Введите комментарий');
		} else {
			elem.val(comID);
			elemCom.attr('placeholder', 'Ответ пользователю '+name);
		}
	})
	
	/*
	 * Отметить всё, в настройках уведомлений
	 */
	var allCheckOneTMP = 0;
	var allCheckSecondTMP = 0;
	jQuery('#allCheckOne').closest('tbody').find('.feedOptionsFormFirst').each(function(){
		if(jQuery(this).is(":checked")) {
			allCheckOneTMP++;
		}
		if(allCheckOneTMP == 18) {
			jQuery('#allCheckOne').prop("checked","checked");
		}
	});
	jQuery('#allCheckSecond').closest('tbody').find('.feedOptionsFormSecond').each(function(){
		if(jQuery(this).is(":checked")) {
			allCheckSecondTMP++;
		}
		if(allCheckSecondTMP == 8) {
			jQuery('#allCheckSecond').prop("checked","checked");
		}
	});

	jQuery('#allCheckOne').click(function(){
		var th = jQuery(this).closest('tbody');
		var first = th.find('.feedOptionsFormFirst');
		if(jQuery(this).is(":checked")){
			first.prop("checked","checked");
		} else {
			first.removeAttr("checked");
		}
	})
	jQuery('#allCheckSecond').click(function(){
		var th = jQuery(this).closest('tbody');
		var first = th.find('.feedOptionsFormSecond');
		if(jQuery(this).is(":checked")){
			first.prop("checked","checked");
		} else {
			first.removeAttr("checked");
		}
	})

	jQuery('.feedOptionsFormFirst').click(function(){
		var allCheckOneTMP = 0;
		if(!jQuery(this).is(":checked")) {
			jQuery('#allCheckOne').removeAttr("checked");
		} else {
			jQuery('.feedOptionsFormFirst').each(function(){
				if(jQuery(this).is(":checked")) {
					allCheckOneTMP++;
				}
			})
			if(allCheckOneTMP == 18) {
				jQuery('#allCheckOne').prop("checked","checked");
			}
		}
	})
	jQuery('.feedOptionsFormSecond').click(function(){
		var allCheckSecondTMP = 0;
		if(!jQuery(this).is(":checked")) {
			jQuery('#allCheckSecond').removeAttr("checked");
		} else {
			jQuery('.feedOptionsFormSecond').each(function(){
				if(jQuery(this).is(":checked")) {
					allCheckSecondTMP++;
				}
			})
			if(allCheckSecondTMP == 8) {
				jQuery('#allCheckSecond').prop("checked","checked");
			}
		}
	})
	/*
	 * END - Отметить всё, в настройках уведомлений
	 */

	function body_height(){
		var body_height = jQuery('body').height();
		var window_height = jQuery(window).height();
		var footer_height = jQuery('footer').height();
		var header_height = jQuery('header').height();
		if(body_height < window_height){
			var cont = window_height - footer_height - header_height;
			jQuery('#full_content').css('min-height', cont);
		}
		return;
	}
	body_height();

	jQuery('#scrtop').click(function(){
		jQuery(window).scrollTop(0);
	})

	jQuery(window).scroll(function(){
		if(jQuery(window).width() < 480){jQuery('#scrtop').hide();return false;}
		var wind = jQuery(window).height() / 2;
		if(wind < jQuery(window).scrollTop()){
			jQuery('#scrtop').show(600);
		} else {
			jQuery('#scrtop').hide(600);
		}
	})

	jQuery('.close_map').click(function(){
		jQuery('#first_map').slideToggle(300);
	})

	$('.menu > .menu-item').hover(function (e) {
		var height = jQuery('.sub-menu', this).height();
		jQuery('.menu > li > .sub-menu').css('column-count', '3');
		var width = jQuery(window).width();
		width_x2 = width/2;
		if(width > 1024) {
			clearTimeout($.data(this,'timer'));
			$('> .sub-menu',this).stop(true,true).show();
			var pos = $(this).offset().left;
			if(width_x2 < pos){
				jQuery('.menu > li > .sub-menu').css('left', 'auto').css('right', '0');
			} else {
				jQuery('.menu > li > .sub-menu').css('left', '0').css('right', 'auto');
			}
		}
	}, function () {
		var width = jQuery(window).width();
		if(width > 1024){
			$.data(this,'timer', setTimeout($.proxy(function() {
				$('> .sub-menu',this).stop(true,true).hide();
			}, this)));
		}
	});

	$('.frontPosts__tabs ul li').click(function(){
		if (!$(this).hasClass('active')) {
			$('.frontPosts__tabs ul li').removeClass('active');
			$(this).addClass('active');
			var index = $(this).index();
			$('.frontPosts__list').removeClass('visible');
			$('.frontPosts__list').eq(index).addClass('visible');
		}
	});

	$('.front_forum_menu__tabs ul li').click(function(){
		if (!$(this).hasClass('active')) {
			$('.front_forum_menu__tabs ul li').removeClass('active');
			$(this).addClass('active');
			var index = $(this).index();
			$('.questionsFront__tab').removeClass('visible');
			$('.questionsFront__tab').eq(index).addClass('visible');
		}
	});

	$('.personal_menu ul li').click(function(){
		if (!$(this).hasClass('active')) {
			$('.personal_menu ul li').removeClass('active').css('text-decoration', 'underline');
			$(this).addClass('active').css('text-decoration', 'none');
			var index = $(this).index();
			$('.info_blocks > div').removeClass('visible');
			$('.info_blocks > div').eq(index).addClass('visible');
		}
	});

	$('.description_turn a').click(function(){
		var tmp = $('.personal_description');
		if(tmp.hasClass('full')){
			tmp.removeClass('full');
		} else {
			tmp.addClass('full');
		}
	});

	$('.autorize_menu .autorize_menu__button').click(function(){
		if (!$(this).hasClass('active')) {
			$('.autorize_menu .autorize_menu__button').removeClass('active');
			$(this).addClass('active');
			var index = $(this).index();
			$('.autorize_menu__block').removeClass('visible');
			$('.autorize_menu__block').eq(index).addClass('visible');
		}
	});

	jQuery('.noauththank').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		jQuery('#autorize').show();
		jQuery('#fon_autorize').show();
	})

	jQuery('#header_auth a').click(function(){
		jQuery('#autorize').show();
		jQuery('#fon_autorize').show();
	})
	
	jQuery('.reg_submit_bbp').click(function(){
		jQuery('#autorize').show();
		jQuery('#fon_autorize').show();
	})

	jQuery('.auth_button').click(function(){
		jQuery('#autorize').show();
		jQuery('#fon_autorize').show();
	})

	jQuery('.comment-reply-login').click(function(){
		jQuery('#autorize').show();
		jQuery('#fon_autorize').show();
	})

	jQuery('#fon_autorize').click(function(){
		jQuery('#autorize').hide();
		jQuery('#fon_autorize').hide();
	})

	jQuery('.min_menu_act').click(function(){
		if(!jQuery(this).hasClass('active')){
			jQuery(this).addClass('active');
			jQuery('#min_menu').slideDown(300);
			jQuery('#min_search').css('background', '#0070a0');
		} else {
			jQuery(this).removeClass('active');
			jQuery('#min_menu').slideUp(300);
			jQuery('#min_search').css('background', '#f1f1f1');
		}
		
	})
	
	jQuery('.frontPosts__tabs__expertFilter').click(function(){
		jQuery(this).toggleClass('active');
	})

	jQuery('#main_articles > ul.menu > li').click(function() {
		if (jQuery(this).find('ul').is(":visible") == false) {
			jQuery('#main_articles > ul.menu ul').stop(true, true);
			jQuery('#main_articles > ul.menu ul').slideUp('0');
			jQuery(this).find('ul').slideToggle('slow');
		}
	})

	jQuery('#sidebar_category > ul.menu > li').click(function() {
		if (jQuery(this).find('ul').is(":visible") == false) {
			jQuery('#sidebar_category > ul.menu ul').slideUp('slow');
			jQuery(this).find('ul').slideToggle('slow');
		}
	})

	$('.rating_review label').click(function(){
		var index = $(this).index('label');
		$('.rating_review label').removeClass('active_rating');
		var i = 0;
		while(i <= index) {
			$('.rating_review label').eq(i).addClass('active_rating');
			i++;
		}
	})

	$('.rating_review label').mouseover(function(){
		var index = $(this).index('label');
		$('.rating_review label').removeClass('active_rating_hover');
		var i = 0;
		while(i <= index) {
			$('.rating_review label').eq(i).addClass('active_rating_hover');
			i++;
		}

		 $('.rating_review').removeClass('need');
	})

	$('#non_front_sections #sections > ul > li').mouseover(function(){
		var height = $(this).find('.section_img').height()+$(this).find('.section_link').height();
		$(this).find('.section_block').height(height);
	})
	$('#non_front_sections #sections > ul > li').mouseleave(function(){
		$(this).find('.section_block').height(254);
	})

	$('.rating_review label').mouseleave(function(){
		$('.rating_review label').removeClass('active_rating_hover');
	})

	jQuery('.all_honors a').click(function(){
		jQuery('.honor_popup').toggle();
	})

	var location = '';
	var index_one = window.location.href.indexOf('\?');
	var index_two = window.location.href.indexOf('#');
	location = (index_one < index_two && index_one != -1) ? window.location.href.slice(0,index_one) :
		(index_one == -1 && index_two != -1) ? window.location.href.slice(0,index_two) :
		(index_one == index_two) ? window.location.href :
		(index_one > index_two) ? window.location.href.slice(0,index_two) : window.location.href;

	jQuery('#commentform input[type="submit"]').click(function(){
		var user = jQuery(this).attr('class');//Получаем id юзверя
		if(user == 0 || user == '') {
			jQuery('#autorize').show();//Показать форму регистрации
			jQuery('#fon_autorize').show();
	        var frameText = jQuery('#comment_ifr').contents().find('#tinymce p').text();//Получаем текст из поля комментария
	        document.cookie = "userText="+frameText+"";//Записываем текст в куки
	        document.cookie = "userLocation="+location+"";
	        //jQuery('#comment_ifr').contents().find('#tinymce p').text('');
			return false;
		}
	})

	function get_cookie ( cookie_name )
	{
	  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
	 
	  if ( results )
	    return ( unescape ( results[2] ) );
	  else
	    return null;
	}
	var userLocation = get_cookie('userLocation');
	var userText = get_cookie('userText');
	function get_text_iframe(){
		jQuery('#comment_ifr').contents().find('#tinymce p').text(userText);
	}
	if(location == userLocation){
		setTimeout(get_text_iframe, 1500);
		if(jQuery('#comment_ifr').contents().find('#tinymce p').text() == '') {
			setTimeout(get_text_iframe, 4000);
		}
	}

	jQuery('#commentform').on('submit', function(){
		document.cookie = "userText=";
	})

	//Добавление в избранное
	jQuery('.addFavorite input').click(function(){
		var th = jQuery(this);
		var form = jQuery('#post_favorites');
		var user = jQuery('#post_favorites input[name="user_id"]').val();
		var post = jQuery('#post_favorites input[name="post_id"]').val();
		var status = false;
		jQuery.ajax({ // инициализируем аякс
		    type: 'POST', // шлем постом
		    url: ajax_var.url, // куда шлем
		    data: 'action=post_favorites_user&user_id='+user+'&post_id='+post, // что отправляем
	        beforeSend: function(data) { // перед отправкой
	        	jQuery('.nofavorite').css('display', 'none');
	        },
	        success: function(data){ // после того как ответ пришел
	        	jQuery('.bottom_post_buttons ul li').each(function(){
	        		if(jQuery(this).hasClass('new_favorite')){
	        			status = true;
	        		}
	        	})
	        	if(status == false){
	        		jQuery('.bottom_post_buttons ul').prepend(data);
	        		th.val('Удалить из избранного');
	        	} else {
	        		jQuery('.bottom_post_buttons ul li.new_favorite').remove();
	        		th.val('Добавить в избранное');
	        		if(!jQuery('.bottom_post_buttons .nofavorite').is(':visible')){
	        			jQuery('.nofavorite').css('display', 'block');
	        		}
	        	}
	        }     
		});
	});

	//Кнопка "Сказать спасибо"
	jQuery('#like_post input').click(function(){
		var form = jQuery('#like_post');
		var user = jQuery('#like_post input[name="user_id"]').val();
		var post = jQuery('#like_post input[name="post_id"]').val();
		jQuery.ajax({
		    type: 'POST',
		    url: ajax_var.url,
		    data: 'action=post_likes_user&user_id='+user+'&post_id='+post,
	        beforeSend: function(data) {
	        	console.log('=(^.^)=');
	        },
	        success: function(data){
	        	jQuery('.like_post_count').html(data);
		        if(jQuery('.like_post_count span').hasClass('nlo')){
					jQuery('#autorize').show().find('.autorize_social-text').text('Авторизуйтесь чтобы поблагодарить');
					jQuery('#fon_autorize').show();
				}
	        }  
		});
	});

	//Автоподгрузка постов на главной
	$(window).scroll(function(){
		var index = $('.frontPosts__tabs ul li.active').index();
		if(index || index === 0){
			var tmp = jQuery('.frontPosts__list').eq(index).find('ul div').hasClass('noposts');
			if(tmp){ return false; }
			var offset = eval(jQuery('.frontPosts__list').eq(index).find('ul .more__posts').attr('data-offset'));
			if(offset){
				if( jQuery(document).height() - jQuery(window).height() - 390 <= jQuery(document).scrollTop() ) {
					jQuery('.frontPosts__list ul').eq(index).addClass('x_loading');
					if(index == 0) {
						var action = 'front_new_posts';
					} if(index == 1) {
						var action = 'front_popular_posts';
					} if(index == 2) {
						var action = 'front_private_posts';
					} if(index == 3) {
						var action = 'front_prof_posts';
					}
					jQuery.ajax({
						url: "/wp-admin/admin-ajax.php",
						type: "POST",
						async: false,
						data: "action="+action+"&offset="+offset+"&index="+index,
					success: function(html, data){
						jQuery('.frontPosts__list').eq(index).find('.more__posts').remove();
						jQuery('.frontPosts__list').eq(index).find('ul').removeClass('x_loading');
						jQuery('.frontPosts__list').eq(index).find('ul').append(html);
					}			
				});	
				}
			}
		}
	});

	//Автоподгрузка постов на странице автора
	jQuery(window).scroll(function(){
		if(!jQuery('#main').hasClass('author_page') || !jQuery('.postPage').hasClass('visible')) { return; }
		var th = jQuery('.author_page').find('.postPage.visible');
		if(th.find('div').hasClass('noposts_author')) { return false; }
		
		var hei = jQuery(document).height() - jQuery(window).height() - 200;
		var top = jQuery(window).scrollTop();
		
		if(top >= hei && jQuery('#authorEntry').data('entry') != '1'){
			console.log(jQuery('#authorEntry').data('entry'));
			var author = th.find('.more__posts_author').data('author');
			var offset = eval(jQuery('.info_blocks .visible .more_author_posts').attr('data-offset'));
			jQuery('#authorEntry').data('entry', '1');
			jQuery.ajax({
					url: "/wp-admin/admin-ajax.php",
					type: "POST",
					data: "action=get_author_posts&offset="+offset+"&author="+author,
				beforeSend: function() {
					jQuery('.postPage ul').addClass('x_loading');
				},
				success: function(html, data){
					th.find('#authorEntry').remove();
					th.find('.more_author_posts').remove();
					th.find('.more__posts_author').remove();
					th.find('ul').append(html);
				},
				complete: function() {
					th.find('ul').removeClass('x_loading');
				}
			})
		}
	})

	jQuery('.postPage').on('mouseover', 'ul li', function(){
			jQuery(this).find('.buttons_record').show();
	});

	jQuery('.postPage').on('mouseleave', 'ul li', function(){
			jQuery(this).find('.buttons_record').hide();
	});

	//Страница вопрос-ответ, табы, подгузка постов
	jQuery('.questionsPage__tab').on('click', '.load_more', function(){
		var index = $('.page_forum_menu__tabs ul li.active').index();
		var offset = eval(jQuery('.questionsPage__tab').eq(index).find('.forum_offset_new').attr('data-offset'));
		if(index == 0) {
			var action = 'forum_last_new_reply_str';
		} if(index == 1) {
			var action = 'forum_last_reply_str';
		} if(index == 2) {
			var action = 'test_view_forum_str';
		} if(index == 3) {
			var action = 'forum_count_reply_str';
		} if(index == 4) {
			var action = 'forum_required_reply_str';
		}
		jQuery.ajax({
				url: "/wp-admin/admin-ajax.php",
				type: "POST",
				data: "action="+action+"&offset="+offset+"&index="+index,
			beforeSend: function(xhr){
				jQuery('.questionsPage__tab').eq(index).find('ul').addClass('x_loading');
			},
			success: function(html, data){
				console.log(offset);
				jQuery('.questionsPage__tab').eq(index).find('.load_more').remove();
				jQuery('.questionsPage__tab').eq(index).find('.forum_offset_new').remove();
				jQuery('.questionsPage__tab').eq(index).find('ul').append(html);
			},
			error: function(xhr, ajaxOptions, thrownError){
				alert('Error 0x00000090');
				console.log(arguments);
			},
			complete: function(data) {
	        	jQuery('.questionsPage__tab').eq(index).find('ul').removeClass('x_loading');
	        }
		});
	});

	//Всплывающая подсказка "последниц комментарий" вопрос-ответ
	jQuery('.questionsFront__list').on('mouseover', '.questionsFront__item__right', function(){
		jQuery(this).find('.last_reply_comment').show();
	})
	jQuery('.questionsFront__list').on('mouseleave', '.questionsFront__item__right', function(){
		jQuery(this).find('.last_reply_comment').hide();
	})

	//Фотоголосование
	jQuery('.fotopolls_view_container').on('click', '.fotopolls_img', function(){
		var index = jQuery(this).index()+1;
		jQuery('#fotopolls__choise').val(index);
		jQuery('.fotopolls_img div').each(function(){
			jQuery(this).removeClass('active');
		});
		jQuery(this).find('div').addClass('active');
		var choise = jQuery('#fotopolls__choise').val();
		jQuery.ajax({
				url: "/wp-admin/admin-ajax.php",
				type: "POST",
				data: "action=fotopolls_view&choise="+choise,
			success: function(html, data){
				jQuery('.fotopolls_res div').remove();
				jQuery('.fotopolls_res').html(html);
			}
		});
		return false;
	})

	//Лента новостей, подгрузка записей по кнопке (кнопка сейчас не активна, т.к. стоит автоподгрузка постов)
	jQuery('.page_lenta').on('click', '.feed_button', function(){
		var offset = jQuery('input[name="feed_pages"]').data('offset');
		var value = jQuery('.r_group_feed input[name="group1"]:checked').val();
		jQuery.ajax({
				url: "/wp-admin/admin-ajax.php",
				type: "POST",
				data: "action=news_lenta&offset="+offset+"&value="+value,
			success: function(html, data){
				jQuery('.feed_button').remove();
				jQuery('input[name="feed_pages"]').remove();
				jQuery('.questionsPage__list').append(html);
			}
		});
		return false;
	})

	jQuery('#respond').on('mouseover', '#comment_ifr', function(){
		var mythis = jQuery(this);
		var height = mythis.contents().find('body').height()+23;
		mythis.attr('style', 'height:'+height+'px !important');
		jQuery('#comments .mce-edit-area').attr('style', 'height:'+height+'px !important');
	})

	//Автоподгрузка ленты активности
	var handlerUploadPostFeed = function(){
		var offset = jQuery('input[name="feed_pages"]').data('offset');
		jQuery.ajax({
				url: "/wp-admin/admin-ajax.php",
				type: "POST",
				data: "action=news_lenta&offset="+offset,
			beforeSend: function() {
				jQuery('.page_lenta ul.questionsPage__list').addClass('x_loading');
			},
			success: function(html, data){
				jQuery('.feed_button').remove();
				jQuery('#hidEntry').remove();
				jQuery('input[name="feed_pages"]').remove();
				jQuery('.page_lenta .questionsPage__list').append(html);
				jQuery('.page_lenta ul.questionsPage__list').removeClass('x_loading');
			}
		});
		return false;
	}

	jQuery(window).scroll(function(){
		if(jQuery('.page_lenta ul.questionsPage__list input').hasClass('nonePost')){return false;}
		var hei = jQuery(document).height() - jQuery(window).height() - 200;
		var top = jQuery(window).scrollTop();
		if(top >= hei && jQuery('#hidEntry').data('entry') != '1'){
			jQuery('#hidEntry').data('entry', '1');
			handlerUploadPostFeed();
		}
	})

	//Автоподгрузка ответов на странице автора
	$(window).scroll(function(){
			var th = jQuery('.questionsPage.visible ul');
			if(jQuery('.author_page .questionsPage.visible .questionsPage__list input').hasClass('nonePost')){th.removeClass('x_loading'); return false;}
			var offset = th.find('#author_reply').data('offset');
			var author = th.find('#author_reply_us').data('author');
			if(jQuery(window).scrollTop() >= jQuery(document).height() - jQuery(window).height() - 200 && th.find('#entryPosts').data('entry') != '1') {
				th.find('#entryPosts').data('entry', '1');
				jQuery.ajax({
						url: "/wp-admin/admin-ajax.php",
						type: "POST",
						data: "action=questions_page_author&offset="+offset+"&author="+author,
					beforeSend: function() {
						th.addClass('x_loading');
					},
					success: function(html, data) {
						th.find('#entryPosts').remove();
						th.find('#author_reply').remove();
						th.find('#author_reply_us').remove();
						th.append(html);
					},
					complite: function() {
						th.removeClass('x_loading');
					}
				});	
			}
	});

	//Добавление комментария к статье в ленте активности
	jQuery('.page_lenta').on('click', '.feedComment', function(e){
		e.preventDefault();
		var feedID = jQuery(this).parent('form').find('input[name="feedID"]').val();//ID статьи
		var textComment = jQuery(this).parent('form').find('.textComment').val();//Текст комментария
		var this_element = jQuery(this);//Кнопка добавления комментария
		var feedParent = jQuery(this).parent('form').find('input[name="feedParent"]').val();
		jQuery.ajax({
				url: "/wp-admin/admin-ajax.php",
				type: "POST",
				data: "action=feedPostComment&feedID="+feedID+"&textComment="+textComment+"&feedParent="+feedParent,
			success: function(html, data){
				this_element.closest('.feedCommentsConteiner').before(html);
				this_element.parent('form').find('.textComment').val('');
			}
		})
	})

	//Добавление ответа к вопросу в ленте активности
	jQuery('.page_lenta').on('click', '.feedCommentTopic', function(e){
		e.preventDefault();
		var feedIDTopic = jQuery(this).parent('form').find('input[name="feedIDTopic"]').val();//ID статьи
		var textCommentTopic = jQuery(this).parent('form').find('.textCommentTopic').val();//Текст комментария
		var this_element = jQuery(this);//Кнопка добавления комментария
		var feedParentTopic = jQuery(this).parent('form').find('input[name="feedParentTopic"]').val();
		jQuery.ajax({
				url: "/wp-admin/admin-ajax.php",
				type: "POST",
				data: "action=feedPostCommentTopic&feedIDTopic="+feedIDTopic+"&textCommentTopic="+textCommentTopic+"&feedParentTopic="+feedParentTopic,
			success: function(html, data){
				this_element.closest('.feedCommentsConteinerTopic').before(html);
				this_element.parent('form').find('.textCommentTopic').val('');
			}
		})
	})

	//Удаление фото в профиле
	jQuery('.images_gallery_item').on('click', 'input[type="submit"]', function(e){
		e.preventDefault();
		var th = jQuery(this).parent('#delete_foto');
		var user_id = th.find('input[name="deleteFoto"]').val();
		var image_id = th.find('input[name="deleteFotoId"]').val();
		if(confirm("Уверены, что хотите удалить фотографию?")){
			jQuery.ajax({
					url: "/wp-admin/admin-ajax.php",
					type: "POST",
					data: "action=foto_delete&user_id="+user_id+"&image_id="+image_id,
				success: function(html, data){
					th.parent('.images_gallery_item').remove();
				}
			})
		}
	})

	//Обработка кнопки "спасибо" на форуме
	jQuery(document).on('click', '.thanks_forum_button', function(){
		var th = jQuery(this);
		var user = th.data('user'); // ID текущего пользователя
		var id = th.data('id'); // ID поста
		var click = th.data('click'); //Говорил спасибо или нет 1 - да / 0 - нет
		var name = th.data('name'); // display_name текущего пользователя
		var row = th.closest('.thanks_forum');
		jQuery.ajax({
				url: "/wp-admin/admin-ajax.php",
				type: "POST",
				data: "action=like_forum_cl&user="+user+"&id="+id+"&click="+click+"&name="+name,
			success: function(html, data){
				if(click == 0) { // Добавляем "спасибо"
					row.text(' ').prepend(html).removeClass('none');
					th.data('click', 1);
				} else {
					row.text(' ').prepend(html);
					th.data('click', 0);
				}
			}
		})
	})


	// search
	jQuery(document).on('click', '.blsearch_cont input[type="submit"]', function(e) {
		e.preventDefault();
		var value = jQuery('#blsearch').val();
		jQuery('#blconcat').data('set', 1);
		jQuery.ajax({
			url: "/wp-admin/admin-ajax.php",
			type: "POST",
			data: "action=view_blogers&value="+value,
			beforeSend: function(){
				jQuery('.bllist li').remove();
				jQuery('.blpaginate').remove();
				jQuery('.bllist').addClass('x_loading');
			},
			success: function(html, data) {
				jQuery('.bllist').append(html);
				jQuery('.bllist').removeClass('x_loading');
			}
		})
	})

	// concat
	/*jQuery(document).on('click', '.blpaginate span', function() {
		var vset;
		if(jQuery(this).hasClass('blbegin')) {
			vset = 1;
		} else if(jQuery(this).hasClass('blnext')) {
			vset = jQuery('#blconcat').data('set') + 1;
		} else {
			vset = parseInt(jQuery(this).text());
		}
		var value = jQuery('#blsearch').val();
		jQuery.ajax({
			url: "/wp-admin/admin-ajax.php",
			type: "POST",
			data: "action=view_blogers&set="+vset+"&value="+value,
			beforeSend: function() {
				jQuery('.bllist li').remove();
				jQuery('.blpaginate').remove();
				jQuery('.bllist').addClass('x_loading');
			},
			success: function(html, data) {
				jQuery('.bllist').append(html);
				jQuery('#blconcat').data('set', vset);
				jQuery('.bllist').removeClass('x_loading');
			}
		})
	})*/

	jQuery(window).on('scroll', function() {
		var value = jQuery('#blsearch').val();
		var set = jQuery('#blconcat').data('set');
		var bl = jQuery('#blconcat');
		if(jQuery('.bllist li').hasClass('blnouser')) {
			return;
		}
		if(jQuery(window).scrollTop() >= jQuery(document).height() - jQuery(window).height() - 200 && bl.data('ckeck') == 0) {
			jQuery.ajax({
				url: "/wp-admin/admin-ajax.php",
				type: "POST",
				data: "action=view_blogers&value="+value+"&set="+set,
				beforeSend: function() {
					bl.data('ckeck', '1');
					jQuery('.bllist').addClass('x_loading');
				},
				success: function(html, data) {
					jQuery('#blconcat').data('set', set + 13);
					jQuery('.bllist').append(html);
					jQuery('.bllist').removeClass('x_loading');
				},
				complete: function() {
					bl.data('ckeck', '0');
				}
			})
		}
	})

	jQuery('#article .wp-caption').each(function(){
		var art = jQuery('#article').width();
		var wd = jQuery(this).find('img').width();
		if(wd >= art) {
			jQuery(this).css('width', '100%');
			jQuery(this).find('img').css('width', '100%');
		} else {
			jQuery(this).css('display', 'block').css('margin', '0 auto');
		}
	})
	
})