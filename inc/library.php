<div id="lib_full">

<div class="lib_left_block">
	<h2 class="lib_title"><a href="/">Нормативы и ГОСТы</a></h2>
	<ul>
		<li><a href="/">Законодательство</a></li>
		<li><a href="/">Правила</a></li>
		<li><a href="/">Типовые инструкции</a></li>
		<li><a href="/">Другие документы</a></li>
		<li><a href="/">Сварка</a></li>
		<li><a href="/">Устройства</a></li>
		<li><a href="/">Метизы</a></li>
		<li><a href="/">Качество</a></li>
		<li><a href="/">Стандартизация и патенты</a></li>
		<li><a href="/">ОСТы</a></li>
	</ul>
	<h2 class="lib_title"><a href="/">Инструкции по эксплуатации техники</a></h2>
	<ul>
		<li><a href="/">Поиск технической документации</a></li>
	</ul>
	<h2 class="lib_title"><a href="/">Словари терминов</a></h2>
	<ul>
		<li><a href="/">Описания профессий</a></li>
		<li><a href="/">Техпроцессы и операции</a></li>
		<li><a href="/">Работы</a></li>
		<li><a href="/">Предприятия и техника</a></li>
		<li><a href="/">Машины и механизмы</a></li>
		<li><a href="/">Надписи на узлах и машинах</a></li>
	</ul>
	<h2 class="lib_title"><a href="/">Конвертер мер и весов</a></h2>
	<h2 class="lib_title"><a href="/">Программы и утилиты для конструкторов</a></h2>
</div>

<div class="lib_right_block">
	<h2 class="lib_title"><a href="/">Словари терминов</a></a></h2>
	<ul>
		<li><a href="/">Описания профессий</a></li>
		<li><a href="/">Техпроцессы и операции</a></a></li>
		<li><a href="/">Работы</a></a></a></li>
		<li><a href="/">Предприятия и техника</a></li>
		<li><a href="/">Машины и механизмы</a></li>
		<li><a href="/">Надписи на узлах и машинах</a></li>
	</ul>
	<h2 class="lib_title"><a href="/">Конвертер мер и весов</a></h2>
	<h2 class="lib_title"><a href="/">Программы и утилиты для конструкторов</a></h2>
	<h2 class="lib_title"><a href="/">Предложения и обмен</a></h2>
	<h2 class="lib_title"><a href="/">Нормативы и ГОСТы</a></h2>
	<ul>
		<li><a href="/">Законодательство</a></li>
		<li><a href="/">Правила</a></li>
		<li><a href="/">Типовые инструкции</a></li>
		<li><a href="/">Другие документы</a></li>
		<li><a href="/">Сварка</a></li>
		<li><a href="/">Устройства</a></li>
		<li><a href="/">Метизы</a></li>
		<li><a href="/">Качество</a></li>
		<li><a href="/">Стандартизация и патенты</a></li>
		<li><a href="/">ОСТы</a></li>
	</ul>
</div>
<div class="clear"></div>
</div><!-- end lib_full -->
<div class="clear"></div>


<div class="popular">
<div class="title title_single">Самые обсуждаемые блоги</div>
<div class="popular_posts">
<?php
$pc = new WP_Query('orderby=comment_count&posts_per_page=5'); ?>
<?php while ($pc->have_posts()) : $pc->the_post(); ?>
<?php $product_img = catch_that_image(); ?>
<div class="popular_post">
<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
<div class="post_image" style="background: url(<?=$product_img?>) no-repeat;background-size: cover;"></div>
<div class="post_link"><?php the_title(); ?></div>
</a>
</div>
<?php endwhile; ?>
</div>
</div>
<div class="last_answer">
<?php sidebar_answers(0); ?>
</div>
<div class="clear"></div>