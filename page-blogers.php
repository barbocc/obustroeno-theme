<?php get_header(); ?>
<div id="full_content">
	<div id="main_content" class="post_page single_posts blogers">
		<div id="main">
			<div id="breadcrumbs">
				<div class="breadcrumbs_block">
				<?php if (function_exists('dimox_breadcrumbs')) {dimox_breadcrumbs();} ?>
				</div>
			</div>
			<div id="article">
				<h1><?=the_title()?></h1>
				<form class="blsearch_cont">
					<span class="blsearch_cont-user"><?=blget_reg_users()?><span>пользователей</span></span>
					<span class="blsearch_cont-search"><input type='text' name='blsearch' id='blsearch' placeholder='Поиск пользователя' placeholder="Поиск пользователя"></span>
					<span class="blsearch_cont-smb"><input type="submit" value="Найти"></span>
				</form>
				<ul class="bllist">
					<?=view_blogers()?>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<?php get_footer(); ?>