<?php get_header(); ?>

<div id="full_content">
<div id="main_content" class="post_page">
	<div id="main">

		<div id="breadcrumbs">
			<div class="breadcrumbs_block">
			<?php if (function_exists('dimox_breadcrumbs')) {dimox_breadcrumbs();} ?>
			</div>
		</div>

		<div id="article">
			<div class="content_text">
				<div class="title firm-title">
					<!--div class="post_rating">Текущий рейтинг новости: <?=comment_percent($post->ID)?></div-->
					<div class="text">Новости</div>
				</div>

				<h1><?=the_title()?></h1>

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php the_content(__('')); ?>

                <?php endwhile; else: ?>
                    <p>Нет новостей!</p>
                <?php endif; ?>
				<?php wp_pagenavi(); ?>
				<div class="bottom_post_buttons">
					<a class="addFavorite" onclick="Bookmark()">Добавить в избранное</a>
					<a class="printButton" onclick="print()">Версия для печати</a>
				</div>
			</div>
		</div>
		<?php bottom_post_block(); ?>

		<!--?php comments_template(); ?-->
	</div>

	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>