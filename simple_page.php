<?php
 /**
 * Template Name: Simple Pages
 */
 get_header(); 
?>
<div id="full_content">
<div id="main_content">
	<div id="main" class="post_page">
		<div id="breadcrumbs">
			<div class="breadcrumbs_block">
			<?php if (function_exists('dimox_breadcrumbs')) {dimox_breadcrumbs();} ?>
			</div>
		</div>
		
		<div id="article">
			<div class="content_text">
				<div class="title firm-title"><div class="text"><?=the_title()?></div></div>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content(__('')); ?>
				<?php endwhile; else: ?>
					<p>Нет новостей!</p>
				<?php endif; ?>
				<?php wp_pagenavi(); ?>
			</div>
		</div>
	</div>
	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>