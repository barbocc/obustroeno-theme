<?php
$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : ''; // сначала возьмем скрытое поле nonce
if (!wp_verify_nonce($nonce, 'regme_nonce_sfhdios')) wp_send_json_error(array('message' => 'Данные присланные со сторонней страницы ', 'redirect' => false)); // проверим его, и если вернулся фолс - используем wp_send_json_error и умираем

if (is_user_logged_in()) wp_send_json_error(array('message' => 'Вы уже авторизованы!', 'redirect' => false)); // далее проверим залогинен ли уже юзер, если да - то делать ничего не надо

if (!get_option('users_can_register')) wp_send_json_error(array('message' => 'Регистрация пользователей временно недоступна.', 'redirect' => false)); // если регистрацию выключат в админке - то же не будем ничего делать

// теперь возьмем все поля и рассуем по переменным
$user_login = isset($_POST['user_login']) ? $_POST['user_login'] : '';
$user_email = isset($_POST['user_email']) ? $_POST['user_email'] : '';
$pass1 = isset($_POST['pass1']) ? $_POST['pass1'] : '';
$pass2 = isset($_POST['pass2']) ? $_POST['pass2'] : '';

$redirect_to = isset($_POST['redirect_to']) ? $_POST['redirect_to'] : false;

// теперь проверим нужные поля на заполненность и валидность
if (!$user_email) wp_send_json_error(array('message' => 'Email - обязательное поле.', 'redirect' => false));
if (!preg_match("|^[-0-9a-z_\.]+@[-0-9a-z_^\.]+\.[a-z]{2,6}$|i", $user_email)) wp_send_json_error(array('message' => 'Ошибочный формат email', 'redirect' => false));
if (!$user_login) wp_send_json_error(array('message' => 'Логин - обязательное поле.', 'redirect' => false));
if (!$pass1) wp_send_json_error(array('message' => 'Пароль - обязательное поле.', 'redirect' => false));
if (!$pass2) wp_send_json_error(array('message' => 'Повторите пароль', 'redirect' => false));

// теперь проверим все ли ок с паролями
if ($pass1 != $pass2) wp_send_json_error(array('message' => 'Пароли не совпадают', 'redirect' => false));
if (strlen($pass1) < 4) wp_send_json_error(array('message' => 'Слишком короткий пароль', 'redirect' => false));
if (false !== strpos(wp_unslash($pass1), "\\" ) ) wp_send_json_error(array('message' => 'Пароль не может содержать обратные слеши "\\"', 'redirect' => false));

$user_id = wp_create_user($user_login,$pass1,$user_email); // пробуем создать пользователя с переданными данными

// если есть ошибки
if (is_wp_error($user_id) && $user_id->get_error_code() == 'existing_user_email') wp_send_json_error(array('message' => 'Пользователь с таким email уже существует.', 'redirect' => false));
elseif (is_wp_error($user_id) && $user_id->get_error_code() == 'existing_user_login') wp_send_json_error(array('message' => 'Пользователь с таким логином уже существует.', 'redirect' => false));
elseif (is_wp_error($user_id) && $user_id->get_error_code() == 'empty_user_login') wp_send_json_error(array('message' => 'Логин только латиницей.', 'redirect' => false));
elseif (is_wp_error($user_id)) wp_send_json_error(array('message' => $user_id->get_error_code(), 'redirect' => false));

wp_send_json_success(array('message' => 'Все прошло отлично. Вы зарегистрировались.', 'redirect' => false)); // говорим что все прошло ок, если нужен редирект то вместо false поставьте $redirect_to
?>