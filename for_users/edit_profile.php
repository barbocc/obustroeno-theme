<?php $nonce = isset($_POST['nonce']) ? $_POST['nonce'] : ''; // сначала возьмем скрытое поле nonce
if(current_user_can('administrator')) {
    if($_POST['profile_id_upd']) {
        unset( $current_user ); // удалим, чтобы wp_set_current_user() переустановила все заново
        wp_set_current_user( $_POST['profile_id_upd'] ); // переустанавливаем
    }
}
//if(!current_user_can('administrator')) {
    if (!wp_verify_nonce($nonce, 'edt_pr_nonce')) wp_send_json_error(array('message' => 'Данные присланные со сторонней страницы ', 'redirect' => false)); // проверим его, и если вернулся фолс - используем wp_send_json_error и умираем
//}
//


if (!is_user_logged_in()) wp_send_json_error(array('message' => 'Вы не залогинены.', 'redirect' => false)); // если юзера как то разлогинело, то ничего не делаем

// теперь возьмем все поля и рассуем по переменным
//$user_email = isset($_POST['user_email']) ? $_POST['user_email'] : '';
$first_name = isset($_POST['first_name']) ? $_POST['first_name'] : '';
$display_name = isset($_POST['display_name']) ? $_POST['display_name'] : '';
$last_name = isset($_POST['last_name']) ? $_POST['last_name'] : '';

$user_url = isset($_POST['user_url']) ? $_POST['user_url'] : '';
$user_date = isset($_POST['date']) ? $_POST['date'] : '';
$user_sity = isset($_POST['sity']) ? $_POST['sity'] : '';
$user_status = isset($_POST['status']) ? $_POST['status'] : '';
$user_contacts = isset($_POST['contacts']) ? $_POST['contacts'] : '';
$user_stop_send_flag = $_POST['stop_send_letters']==true ? $_POST['stop_send_letters'] : false;
$description = isset($_POST['description']) ? $_POST['description'] : ''; // стандртное мета поле у юзера

$user_vk = isset($_POST['profile_vk']) ? $_POST['profile_vk'] : '';
$user_ok = isset($_POST['profile_ok']) ? $_POST['profile_ok'] : '';
$user_fb = isset($_POST['profile_fb']) ? $_POST['profile_fb'] : '';
$user_tw = isset($_POST['profile_tw']) ? $_POST['profile_tw'] : '';
$user_yout = isset($_POST['profile_yout']) ? $_POST['profile_yout'] : '';
$user_in = isset($_POST['profile_in']) ? $_POST['profile_in'] : '';

$pass1 = isset($_POST['pass1']) ? $_POST['pass1'] : ''; // поля с паролями
$pass2 = isset($_POST['pass2']) ? $_POST['pass2'] : '';
$current_pass = isset($_POST['current_pass']) ? $_POST['current_pass'] : '';

$redirect_to = isset($_POST['redirect_to']) ? $_POST['redirect_to'] : false;

// теперь проверим обязательные поля на заполненность и валидность - у нас это только поле с почтой
/*if (!$user_email) wp_send_json_error(array('message' => 'Email - обязательное поле.', 'redirect' => false));
if (!preg_match("|^[-0-9a-z_\.]+@[-0-9a-z_^\.]+\.[a-z]{2,6}$|i", $user_email)) wp_send_json_error(array('message' => 'Ошибочный формат email', 'redirect' => false));
*/
global $current_user; // получим текущие данные пользователя
get_currentuserinfo(); 

/*if ($current_user->user_email != $user_email && get_user_by('email', $user_email)) wp_send_json_error(array('message' => 'Пользователь с таким email уже существует', 'redirect' => false));*/ // если попытались изменить email то проверим что новый email не занят
/*if($_POST['profile_id']) {
    unset( $current_user ); // удалим, чтобы wp_set_current_user() переустановила все заново
    wp_set_current_user( $_POST['profile_id'] ); // переустанавливаем
}*/

$fields = array(); // подготовим массив для обновления данных пользователя
$fields['ID'] = $current_user->ID;
//$fields['user_email'] = $user_email;
if($first_name){
    $fields['first_name'] = $first_name;
}
if($display_name){
    $fields['display_name'] = $display_name;
}
if($last_name){
    $fields['last_name'] = $last_name;
}
if($user_url){
    $fields['user_url'] = $user_url;
}

if ($pass1 && $pass2 && $current_pass) { // если заполнены все 3 поля с паролями
    if ($pass1 != $pass2) wp_send_json_error(array('message' => 'Пароли не совпадают', 'redirect' => false)); // проверим что новые пароли совпадают
    if (strlen($pass1) < 4) wp_send_json_error(array('message' => 'Слишком короткий пароль', 'redirect' => false)); // проверим длину пароля
    if (false !== strpos(wp_unslash($pass1), "\\" ) ) wp_send_json_error(array('message' => 'Пароль не может содержать обратные слэши "\\"', 'redirect' => false)); // это для безопасности
    if (!wp_check_password($current_pass, $current_user->user_pass, $current_user->ID)) wp_send_json_error(array('message' => 'Текущий пароль не верный.', 'redirect' => false)); // проверим что текущий пароль введен правильно
    $fields['user_pass'] = esc_attr($pass1); // добавим новый пароль в массив данных для изменения
} elseif ($pass1 || $pass2 || $current_pass) { // если были заполнены не все поля с паролями
    wp_send_json_error(array('message' => 'Для изменения пароля заполните все поля с паролями.', 'redirect' => false)); // покажим ошибку
}

if($_FILES){
    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');
    global $wpdb;
    $fotogallery = $wpdb->prefix.fotogallery;
}

// теперь проверим все ли ок с новой аватаркой, если она передана
if (isset($_FILES['avatar'])) { // если в глобальном массиве $_FILES есть элемент с индексом avatar
    if ($_FILES['avatar']['error']) wp_send_json_error(array('message' => "Ошибка загрузки: " . $_FILES['avatar']['error'].". (".$_FILES['avatar']['name'].") ", 'redirect' => false)); // если произошла серверная ошибка при загрузке файла
    $type = $_FILES['avatar']['type']; // теперь возьмем расширение файла
    if (($type != "image/jpg") && ($type != "image/jpeg") && ($type != "image/png") && ($type != "image/gif")) wp_send_json_error(array('message' => "Формат файла может быть только jpg или png. (".$_FILES['avatar']['name'].")", 'redirect' => false)); // если формат плохой
    

    // если скрипт до сих пор не умер то продолжаем
    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attach_id_img = media_handle_upload('avatar', 0); // пробуем залить файл в медиабиблиотеку и получить его id, первый аргумент это индекс файла в глобальном массиве $_FILES, у нас это avatar, второй это id поста к чему файл привязывается, нам это не надо поэтому 0
    
    if (is_wp_error($attach_id_img)) wp_send_json_error(array('message' => "Что-то не так с загрузкой аватарки.", 'redirect' => false)); // если добавление в медиабиблиотеку вернуло ошибку

    if($attach_id_img) {
        $sql = 
        "
            INSERT INTO ".$fotogallery."  (`id`, `user_id`, `image_id`, `date`) VALUES (NULL, ".$current_user->ID.", ".$attach_id_img.", '0000-00-00 00:00:00');
        ";
        $wpdb->query($sql);
        update_user_meta($current_user->ID, 'userAvatar', current_time('mysql'));
    }
    
$avatar = $attach_id_img;
    //$avatar = array(); // подготовим массив с данными для привязки аватарки к юзеру (только для плагина simple local avatars)
    //$avatar['media_id'] = $attach_id_img; // сюда id аватарки
    //$avatar['full'] = wp_get_attachment_url($attach_id_img); // а сюда её url
    
    //update_option('avatar_default', 'wp_user_avatar');
    ///update_user_meta($current_user->ID, 'simple_local_avatar', $avatar ); // привяжем новый аватар
    ///update_user_meta($current_user->ID, 'wp_user_avatar', $avatar );
global $wpdb;
$user_id = $current_user->ID;
      update_user_meta($user_id, $wpdb->get_blog_prefix($blog_id).'user_avatar', $avatar);

}

if($description){
    update_user_meta($current_user->ID, 'description', $description); // обновим мета поле "Обо мне", остальные мета поля обновляются по такому же принципу
}

if($user_vk){
    update_user_meta($current_user->ID, 'profile_vk', $user_vk);
}
if($user_ok){
    update_user_meta($current_user->ID, 'profile_ok', $user_ok);
}
if($user_fb){
    update_user_meta($current_user->ID, 'profile_fb', $user_fb);
}
if($user_tw){
    update_user_meta($current_user->ID, 'profile_tw', $user_tw);
}
if($user_yout){
    update_user_meta($current_user->ID, 'profile_yout', $user_yout);
}
if($user_in){
    update_user_meta($current_user->ID, 'profile_in', $user_in);
}

if($user_date){
    update_user_meta($current_user->ID, 'date', $user_date);
}
if($user_sity){
    update_user_meta($current_user->ID, 'sity', $user_sity);
}
if($user_status){
    update_user_meta($current_user->ID, 'status', $user_status);
    update_user_meta($current_user->ID, 'statusTime', current_time('mysql'));
}
if($user_contacts){
    update_user_meta($current_user->ID, 'contacts', $user_contacts);
}

if($user_stop_send_flag){
    update_user_meta($current_user->ID, 'stop_send_letters', $user_stop_send_flag);
}
else{
    delete_user_meta( $current_user->ID, 'stop_send_letters');
}


$update_user = wp_update_user($fields); // обновляем данные юзера

if (is_wp_error($update_user)) wp_send_json_error(array('message' => 'Системная ошибка: '.$update_user->get_error_code(), 'redirect' => false)); // если что-то пошло не так


if (isset($_FILES['fotogallery'])) {
    if ($_FILES['fotogallery']['error']) wp_send_json_error(array('message' => "Ошибка загрузки: " . $_FILES['fotogallery']['error'].". (".$_FILES['fotogallery']['name'].") ", 'redirect' => false)); // если произошла серверная ошибка при загрузке файла
    $type = $_FILES['fotogallery']['type']; // теперь возьмем расширение файла
    if (($type != "image/jpg") && ($type != "image/jpeg") && ($type != "image/png") && ($type != "image/gif")) wp_send_json_error(array('message' => "Формат файла может быть только jpg, gif или png. (".$_FILES['fotogallery']['name'].")", 'redirect' => false)); // если формат плохой

    $attach_id_img = media_handle_upload('fotogallery', 0); // пробуем залить файл в медиабиблиотеку и получить его id, первый аргумент это индекс файла в глобальном массиве $_FILES, у нас это fotogallery, второй это id поста к чему файл привязывается, нам это не надо поэтому 0

    if($attach_id_img) {
        $sql = 
        "
            INSERT INTO ".$fotogallery."  (`id`, `user_id`, `image_id`, `date`) VALUES (NULL, ".$current_user->ID.", ".$attach_id_img.", '".current_time('mysql')."');
        ";
        $wpdb->query($sql);
    }
    
    if (is_wp_error($attach_id_img)) wp_send_json_error(array('message' => "Что-то не так с загрузкой аватарки.", 'redirect' => false)); // если добавление в медиабиблиотеку вернуло ошибку
}

if (isset($_FILES['fotogallery2'])) {
    if ($_FILES['fotogallery2']['error']) wp_send_json_error(array('message' => "Ошибка загрузки: " . $_FILES['fotogallery2']['error'].". (".$_FILES['fotogallery2']['name'].") ", 'redirect' => false)); // если произошла серверная ошибка при загрузке файла
    $type = $_FILES['fotogallery2']['type']; // теперь возьмем расширение файла
    if (($type != "image/jpg") && ($type != "image/jpeg") && ($type != "image/png") && ($type != "image/gif")) wp_send_json_error(array('message' => "Формат файла может быть только jpg, gif или png. (".$_FILES['fotogallery2']['name'].")", 'redirect' => false)); // если формат плохой

    $attach_id_img = media_handle_upload('fotogallery2', 0); // пробуем залить файл в медиабиблиотеку и получить его id, первый аргумент это индекс файла в глобальном массиве $_FILES, у нас это fotogallery, второй это id поста к чему файл привязывается, нам это не надо поэтому 0

    if($attach_id_img) {
        $sql = 
        "
            INSERT INTO ".$fotogallery."  (`id`, `user_id`, `image_id`, `date`) VALUES (NULL, ".$current_user->ID.", ".$attach_id_img.", '".current_time('mysql')."');
        ";
        $wpdb->query($sql);
    }
    
    if (is_wp_error($attach_id_img)) wp_send_json_error(array('message' => "Что-то не так с загрузкой аватарки.", 'redirect' => false)); // если добавление в медиабиблиотеку вернуло ошибку
}

if (isset($_FILES['fotogallery3'])) {
    if ($_FILES['fotogallery3']['error']) wp_send_json_error(array('message' => "Ошибка загрузки: " . $_FILES['fotogallery3']['error'].". (".$_FILES['fotogallery3']['name'].") ", 'redirect' => false)); // если произошла серверная ошибка при загрузке файла
    $type = $_FILES['fotogallery3']['type']; // теперь возьмем расширение файла
    if (($type != "image/jpg") && ($type != "image/jpeg") && ($type != "image/png") && ($type != "image/gif")) wp_send_json_error(array('message' => "Формат файла может быть только jpg, gif или png. (".$_FILES['fotogallery3']['name'].")", 'redirect' => false)); // если формат плохой

    $attach_id_img = media_handle_upload('fotogallery3', 0); // пробуем залить файл в медиабиблиотеку и получить его id, первый аргумент это индекс файла в глобальном массиве $_FILES, у нас это fotogallery, второй это id поста к чему файл привязывается, нам это не надо поэтому 0

    if($attach_id_img) {
        $sql = 
        "
            INSERT INTO ".$fotogallery."  (`id`, `user_id`, `image_id`, `date`) VALUES (NULL, ".$current_user->ID.", ".$attach_id_img.", '".current_time('mysql')."');
        ";
        $wpdb->query($sql);
    }
    
    if (is_wp_error($attach_id_img)) wp_send_json_error(array('message' => "Что-то не так с загрузкой аватарки.", 'redirect' => false)); // если добавление в медиабиблиотеку вернуло ошибку
}

if (isset($_FILES['fotogallery4'])) {
    if ($_FILES['fotogallery4']['error']) wp_send_json_error(array('message' => "Ошибка загрузки: " . $_FILES['fotogallery4']['error'].". (".$_FILES['fotogallery4']['name'].") ", 'redirect' => false)); // если произошла серверная ошибка при загрузке файла
    $type = $_FILES['fotogallery4']['type']; // теперь возьмем расширение файла
    if (($type != "image/jpg") && ($type != "image/jpeg") && ($type != "image/png") && ($type != "image/gif")) wp_send_json_error(array('message' => "Формат файла может быть только jpg, gif или png. (".$_FILES['fotogallery4']['name'].")", 'redirect' => false)); // если формат плохой

    $attach_id_img = media_handle_upload('fotogallery4', 0); // пробуем залить файл в медиабиблиотеку и получить его id, первый аргумент это индекс файла в глобальном массиве $_FILES, у нас это fotogallery, второй это id поста к чему файл привязывается, нам это не надо поэтому 0

    if($attach_id_img) {
        $sql = 
        "
            INSERT INTO ".$fotogallery."  (`id`, `user_id`, `image_id`, `date`) VALUES (NULL, ".$current_user->ID.", ".$attach_id_img.", '".current_time('mysql')."');
        ";
        $wpdb->query($sql);
    }
    
    if (is_wp_error($attach_id_img)) wp_send_json_error(array('message' => "Что-то не так с загрузкой аватарки.", 'redirect' => false)); // если добавление в медиабиблиотеку вернуло ошибку
}

wp_send_json_success(array('message' => 'Данные успешно изменены. Обновляемся...', 'redirect' => $redirect_to)); // если все ок, напишем об этом и обновимся