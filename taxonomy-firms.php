<?php get_header(); ?>
<?php 
	global $wp_query;
	$term_slug = $wp_query->query['firms'];	
	$term = get_term_by( 'slug', $term_slug, 'firms' );
?>
<div id="full_content">
<div id="main_content">
	<div id="main">
		<div id="article" class="question_page">
			<div class="content_text question_page">
				<!--<div class="title firm-title"><div class="midline"></div><div class="text">Поиск фирмы</div><div class="midline"></div></div>-->
				<?php echo do_shortcode( '[firm_list]' ); ?>
			</div>
		</div>
	</div>

	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>
