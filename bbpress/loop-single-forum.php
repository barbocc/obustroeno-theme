<?php

/**
 * Forums Loop - Single Forum
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<ul id="bbp-forum-<?php bbp_forum_id(); ?>" <?php bbp_forum_class(); ?>>

	<li class="bbp-forum-info">

		<?php if ( bbp_is_user_home() && bbp_is_subscriptions() ) : ?>

			<span class="bbp-row-actions">

				<?php do_action( 'bbp_theme_before_forum_subscription_action' ); ?>

				<?php bbp_forum_subscription_link( array( 'before' => '', 'subscribe' => '+', 'unsubscribe' => '&times;' ) ); ?>

				<?php do_action( 'bbp_theme_after_forum_subscription_action' ); ?>

			</span>

		<?php endif; ?>

		<?php do_action( 'bbp_theme_before_forum_title' ); ?>

		<a class="bbp-forum-title" href="<?php bbp_forum_permalink(); ?>"><?php bbp_forum_title(); ?></a>

		<?php do_action( 'bbp_theme_after_forum_title' ); ?>

		<?php do_action( 'bbp_theme_before_forum_description' ); ?>

		<div class="bbp-forum-content"><?php bbp_forum_content(); ?></div>


		<?php bbp_forum_row_actions(); ?>

	</li>

	<li class="bbp-forum-topic-count">
	    <span class="bbp_bold-span" >Тем:</span><br>
	    <?php bbp_forum_topic_count(); ?></li>

	<li class="bbp-forum-reply-count">
	    <span class="bbp_bold-span">Сообщений:</span><br>
	    <?php bbp_show_lead_topic() ? bbp_forum_reply_count() : bbp_forum_post_count(); ?></li>

	<li class="bbp-forum-freshness">
        <span class="bbp_bold-span">Последнее сообщение:</span><br>
        
        <p class="bbp-topic-meta">

			<?php do_action( 'bbp_theme_before_topic_author' ); ?>

			<span class="bbp-topic-freshness-author"><?php bbp_author_link( array( 'post_id' => bbp_get_forum_last_active_id(), 'size' => 14 ) ); ?></span>

			<?php do_action( 'bbp_theme_after_topic_author' ); ?>

		</p>
		<?php do_action( 'bbp_theme_before_forum_freshness_link' ); ?>

		<?php bbp_forum_freshness_link(); ?>

		<?php do_action( 'bbp_theme_after_forum_freshness_link' ); ?>

	</li>


<?php do_action( 'bbp_theme_before_forum_sub_forums' ); ?>
<?php 
$args = array(
		'before'            => '<ul class="bbp-forums-list">',
		'after'             => '</ul>',
		'link_before'       => '<li class="bbp-forum">',
		'link_after'        => '</li>',
		'count_before'      => ' (',
		'count_after'       => ')',
		'count_sep'         => ', ',
		'separator'         => ' ',
		'forum_id'          => '',
		'show_topic_count'  => true,
		'show_reply_count'  => false,
	);
	?>
<div class="sub_forums">

    <?php
    bbp_list_forums($args);
    ?>

</div>

<?php do_action( 'bbp_theme_after_forum_sub_forums' ); ?>

</ul>
<?php do_action( 'bbp_theme_after_forum_description' ); ?>



<!-- #bbp-forum-<?php bbp_forum_id(); ?> -->
