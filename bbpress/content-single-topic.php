<?php

/**
 * Single Topic Content Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<?php
// Для счетчика количества просмотров тем

if( !function_exists('add_wpbbp_topic_counter') ) :
////////////////////////////////////////////////////////////////////////////////
// init the view counter in bbpress single topic
////////////////////////////////////////////////////////////////////////////////
function add_wpbbp_topic_counter() {
    global $wp_query;
    $bbp = bbpress();
    $active_topic = $bbp->current_topic_id;
    set_wpbbp_post_view( $active_topic );
}

add_action('bbp_template_after_single_topic', 'add_wpbbp_topic_counter');
endif;

?>




<div id="bbpress-forums">

	<?php     
	if(function_exists('bbp_print_rfd_breadcrumb')) bbp_print_rfd_breadcrumb();
    else bbp_breadcrumb(); 
    ?>

	<?php do_action( 'bbp_template_before_single_topic' ); ?>

	<?php if ( post_password_required() ) : ?>

		<?php bbp_get_template_part( 'form', 'protected' ); ?>

	<?php else : ?>

		<?php bbp_topic_tag_list(); ?>

		<?php bbp_single_topic_description(); ?>

		<?php if ( bbp_show_lead_topic() ) : ?>

			<?php bbp_get_template_part( 'content', 'single-topic-lead' ); ?>

		<?php endif; ?>

		<?php if ( bbp_has_replies() ) : ?>

			<?php bbp_get_template_part( 'pagination', 'replies' ); ?>

			<?php bbp_get_template_part( 'loop',       'replies' ); ?>

			<?php bbp_get_template_part( 'pagination', 'replies' ); ?>

		<?php endif; ?>

		<?php bbp_get_template_part( 'form', 'reply' ); ?>

	<?php endif; ?>

	<?php do_action( 'bbp_template_after_single_topic' ); ?>

</div>
