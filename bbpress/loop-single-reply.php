<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

?>
<div id="post-<?php bbp_reply_id(); ?>" class="bbp-reply-header">
	<div class="bbp-meta">
	    
		<?php if ( bbp_is_single_user_replies() ) : ?>
			<span class="bbp-header">
				<?php _e( 'in reply to: ', 'bbpress' ); ?>
				<a class="bbp-topic-permalink" href="<?php bbp_topic_permalink( bbp_get_reply_topic_id() ); ?>"><?php bbp_topic_title( bbp_get_reply_topic_id() ); ?></a>
			</span>

		<?php endif; ?>

	</div><!-- .bbp-meta -->

</div><!-- #post-<?php bbp_reply_id(); ?> -->

<div itemprop="comment" itemscope itemtype="http://schema.org/Comment"  <?php bbp_reply_class(); ?>>

	<div class="bbp-reply-author" itemprop="author" itemscope itemtype="http://schema.org/Person">

		<?php
		do_action( 'bbp_theme_before_reply_author_details' );	?>
		
		<?php 

			//bbp_reply_author_link(array( 'sep' => '<br />', 'show_role' => true ));
		if (function_exists('custom_bbp_get_reply_author_link'))
            $author_info = custom_bbp_get_reply_author_link(array( 'sep' => '<br />', 'show_role' => true ));
		else
            $author_info = bbp_get_reply_author_link(array( 'sep' => '<br />', 'show_role' => true ));

            $author_info = str_replace('class="bbp-author-name" rel="nofollow">', 'class="bbp-author-name" rel="nofollow"><span itemprop="name">', $author_info);
            $author_info = str_replace('<div class="bbp-author-role"', '</span><div class="bbp-author-role"', $author_info);
            echo $author_info;




		?>

		<?php if ( bbp_is_user_keymaster() ) : ?>

			<?php do_action( 'bbp_theme_before_reply_author_admin_details' ); ?>

			<div class="bbp-reply-ip"><?php bbp_author_ip( bbp_get_reply_id() ); ?></div>

			<?php do_action( 'bbp_theme_after_reply_author_admin_details' ); ?>

		<?php endif; ?>

		<?php 

		do_action( 'bbp_theme_after_reply_author_details' ); 

		?>
        
	</div><!-- .bbp-reply-author -->

	<div class="bbp-reply-content">
        <?php
        $reply_id = bbp_get_reply_id();

        global $current_user;

        if ( bbp_is_topic((int)$reply_id) ) {

        	$args4children = array('post_parent'=>$reply_id,'orderby' => 'date','order' => 'DESC','post_type' => 'reply', 'numberposts' => -1, 'post_status'      => 'publish');

        	$subcribe =  bbp_get_topic_subscription_link(array('topic_id' =>$reply_id, 'user_id' => $current_user->ID, 'before'=>''));

        	$children = get_posts($args4children);
			$children = get_posts($args4children);

			echo '<h2 itemprop="headline">'.get_the_title(bbp_get_topic_id()).'</h2>';
			global $wpdb;
			//var_dump($wpdb);
			$args = array(
						'id' => (int)$reply_id,
						'sep' => ' ',
						);

			$links = array(
				'reply' => bbp_get_topic_reply_link( $args ),
				'spam'  => bbp_get_topic_spam_link ( $args ),
				'edit'  => bbp_get_topic_edit_link ( $args ),
				'close' => bbp_get_topic_close_link( $args ),
				'stick' => bbp_get_topic_stick_link( $args ),
				'merge' => bbp_get_topic_merge_link( $args ),
				'trash' => bbp_get_topic_trash_link( $args ),
			);

			$args = array(
						'id' => (int)$reply_id,
						'sep' => ' ',
						'links' => $links
						);

			global $bbpl;
			if ($bbpl){
				$likes_in_topic = $bbpl->get_likes_number($reply_id);

				foreach ($children as $child) {
					$likes_in_topic += $bbpl->get_likes_number($child->ID);
				}

				$icons = '<div class="post_name">
						<span class="views">'.get_wpbbp_post_view($reply_id).'</span>
						<span class="like">'. $likes_in_topic .'</span>
						<span class="comm">'.count($children).'</span>
					</div>';
			}
		}
		else{
			$icons = '';
			$subcribe = '';
			$args = array(
				'id'  => (int)$reply_id,
				'sep' => ' ',
			);

			$links = array(
				'reply' => bbp_get_reply_to_link   ( $args ),
				'spam'  => bbp_get_reply_spam_link (  $args ),
				'edit'  => bbp_get_reply_edit_link ( $args ),
				'move'  => bbp_get_reply_move_link ( $args ),
				'split' => bbp_get_topic_split_link( $args ),
				'trash' => bbp_get_reply_trash_link( $args ),
			);

			$args = array(
						'id' => (int)bbp_get_reply_id(),
						'sep' => ' ',
						'links' => $links
						);
		}

        //bufferization output for data muarkuping
        ob_start();
        	$reply = get_post($reply_id);
        	$full_date = $reply->post_date;
            $date = rdate('d M Y', strtotime($full_date));
    	ob_end_clean();

        ?>

        <span class="bbp-reply-post-date" itemprop="datePublished" content="<?php 
            echo $full_date;
            ?>">
            <?php 


            if ( ! get_post_meta( bbp_get_topic_id(), 'generated', true ) 
            	&& ( strtotime( get_post( $reply_id )->post_date ) >= strtotime( '2016-11-17' )) 
            	&& bbp_is_topic( $reply_id) ){ ?>
				<span class = "star">.</span>
			<?php } ?>
        <?php
    	//var_dump($x);
    	//$reply = get_post($reply_id);
    	//var_dump($reply->post_date);
        echo $date .' в ' . date('H:i',strtotime($full_date)); 
        ?>
        </span>
		<?php 
		do_action( 'bbp_theme_before_reply_content' );
		?>
		
        <div itemprop="text">
		<?php 
		//echo bbp_get_reply_content();
		    //str_replace(array('<div class="bbp-attachments">', '<span class="bbp-admin-links">'), array('</span><div class="bbp-attachments">','</span><span class="bbp-admin-links">'), bbp_get_reply_content());
		    //Микрозаметка текста и картинок
		$content = 	str_replace('<div class="bbp-attachments">','</span><div class="bbp-attachments">', bbp_get_reply_content());
		//$content = 	str_replace('<bloquote', '<bloquote itemprop="citation"');
		if ($content==bbp_get_reply_content()) {
		    $content = 	str_replace('<bloquote', '<bloquote itemprop="citation"', $content);
		    echo $content . $icons . '</span>';
		}
		
		else {
		    $content = 	str_replace('<bloquote', '<bloquote itemprop="citation"', $content);
		    $content = str_replace('<a class="" rel="lightbox"', '<a class="" rel="lightbox"', $content);
		    $content_arr = explode('<div class="bbp-attachments">', $content);
		    $content_arr[1] = str_replace('<img', '<img itemprop="image"', $content_arr[1]);
		    echo $content_arr[0] . $icons .'</span><div class="bbp-attachments">' . $content_arr[1];
		    }
		?>
        </div>
		<?php do_action( 'bbp_theme_after_reply_content' ); ?>
        
        <?php do_action( 'bbp_theme_before_reply_admin_links' ); ?>
		
		<?php 
			$string_of_links = bbp_get_reply_admin_links($args);
			
			$array_of_links = explode('</a> ', $string_of_links);

			$string_of_links = '';

			$array_quote = array();
			array_splice($array_of_links, 2, 0, end($array_of_links));

				//print_r($array_of_links[1]);
			$new_arr = array();

			for($i = 0; $i < count($array_of_links)-1; $i++){
				//$array_of_links["(int)$i"] = $array_of_links[(int)$i] . '</a>';
				//$array_of_links["$i"] = $array_of_links["$i"] . "</a>";
				$new_arr[] =  $array_of_links["$i"] . "</a>";
			}



			ob_start();
		    bbp_like_button();
		    $btn = ob_get_clean(); //Кнопка спасибо
		    $btn_rrr = qqq_quote(); //Кнопка "цитировать"
			
			/* белый писец */
			$new_arr[1] = $subcribe.$new_arr[1];
			$new_arr[2] = $new_arr[2].bbp_get_thank_reply_link($reply_id);
			$new_arr[2] .= '<div style="clear: both; margin-top: 20px; "></div>';
			$temp = $new_arr[1];
			$temp = str_replace('</i></a></span></a>', '</i></a>', $temp);
			$temp .= $btn_rrr . $btn. '</span>';
			$new_arr[1] = $temp;

			$string_of_links = implode(' ', $new_arr);

			echo $string_of_links;

			//bbp_reply_admin_links();
		?>

		<?php do_action( 'bbp_theme_after_reply_admin_links' ); ?>
	</div><!-- .bbp-reply-content -->

</div><!-- .reply -->

<?php
	if(bbp_is_topic((int)$reply_id)){
		$same_topics = bbp_get_same_topics((int)$reply_id);
		if (!empty($same_topics)){
		?>
			<div <?php bbp_reply_class(); ?> style="background: #fff !important;" id="same_id">
				<div class="bbp-reply-author same_topics">
					<span class="bbp-author-name">Похожие темы</span>
				</div>
				<div class="bbp-reply-content" style="border-left: 2px solid #ebebeb;">
					<?php

						foreach ($same_topics as $topic) {
							$user = get_user_by('ID', $topic->post_author);
							?>
							<ul>
								<li class="bbp-author-role" style="display: inline;">
									<a href = "<?=$topic->guid?>">
									<?=$topic->post_title?></a>
								</li><li class="bbp-author-role right_role" style="display: inline; padding-left: 5px;">автор
									<a href = "<?=get_site_url()?>/<?=$user->user_login?>">
									<?=$user->display_name?></a> 
								</li>
								<li style="padding-left: 5px; padding-top: 0px;">
									<?=substr($topic->post_date, 0, -3)?>
								</li>

							</ul>
							<?php
						}
					?>
				</div>
			</div>
		<?php
		}
	}
?>
