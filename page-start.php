<!DOCTYPE html>
<html>
<head>
	<title>Obustroeno.com</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/lending.css?123" media="all">
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/lending.js?123"></script>
	<!--[if lt IE 9]> 
	 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
	<![endif]-->
</head>
<body>
<header></header>
<div id="main_content"><?php landing_start(); $cup = user__cposts(); ?>
	<div class="main_content_top">
		<span class="head_text">Делаете ремонт? <span class="head_text-right">Ведите дневник<br><span>у нас!</span></span><h1>это специализированная платформа для ведения дневников ремонта</h1></span>
		<div class="info_block ib1">
			<div class="info_block-left">комментарии<br>экспертов<br>строителей<br>к ходу ремонта</div>
			<div class="info_block-right">
				<div class="info_block-right_head">кто бесплатно консультирует:</div>
				<div class="info_block-right_body">
					<div class="info_block-user">
						<?php
						$count_users = count(get_users('role=expert'))-4;						
						?>
						<?php
						$user1 = get_user_by('id', 7)->data;
						print get_avatar($user1->ID, 128);
						?>
						<span class="info_block-username"><?php print $user1->display_name; ?></span>
						<div class="info_block-userinfo">32 года, опыт работы прорабом 10 лет. Отлично разбирается во всех сферах ремонта</div>
					</div>
					<div class="info_block-user">
						<?php
						$user2 = get_user_by('id', 14)->data;
						print get_avatar($user2->ID, 128);
						?>
						<span class="info_block-username"><?php print $user2->display_name; ?></span>
						<div class="info_block-userinfo">32 года, опыт работы прорабом 10 лет. Отлично разбирается во всех сферах ремонта</div>
					</div>
					<div class="info_block-user">
						<?php
						$user3 = get_user_by('id', 2734)->data;
						print get_avatar($user3->ID, 128);
						?>
						<span class="info_block-username"><?php print $user3->display_name; ?></span>
						<div class="info_block-userinfo">32 года, опыт работы прорабом 10 лет. Отлично разбирается во всех сферах ремонта</div>
					</div>
					<div class="info_block-user">
						<?php
						$user4 = get_user_by('id', 16)->data;
						print get_avatar($user4->ID, 128);
						?>
						<span class="info_block-username"><?php print $user4->display_name; ?></span>
						<div class="info_block-userinfo">32 года, опыт работы прорабом 10 лет. Отлично разбирается во всех сферах ремонта</div>
					</div>
				</div>
				<div class="info_block-right_bottom">И еще <?php print $count_users ?> <?php print chti2($count_users, 'специалист', 'специалиста', 'специалистов'); ?></div>
			</div>
		</div>
		<div class="info_block ib2">
			<div class="info_block-left">поможет уложиться<br>точно в сроки</div>
			<div class="info_block-right">
				Чтобы раскрыть тему как можно полнее, нужно рассказать о двух вариантах подготовки, ведь стены из гипсокартона могут быть недавно возведенными, 
				а могут быть уже оклеенными обоями. Естественно, что процесс работы будет отличаться, поэтому каждый из вариантов мы будем рассматривать отдельно.
			</div>
		</div>
		<div class="info_block ib3">
			<div class="info_block-left">Память. вам самим<br>будет интересно, как<br>это было и как стало</div>
			<div class="info_block-right"><img src="<?php print get_bloginfo('template_url'); ?>/img/pic_bott.png"></div>
		</div>
		<a class="info_block-link" href="/">Загрузить вашу первую запись</a>
	</div>
	<div class="main_content_center">
		<?php
			function lget_reg_users() {
			  global $wpdb;
			  return $wpdb->get_var("select count(*) from $wpdb->users");
			}
			$c_user = lget_reg_users();
		?>
		<div class="info_block-center_head"><span><?php print $c_user; ?></span> <?php print chti2($c_user, 'пользователь', 'пользователя', 'пользоватей'); ?> Уже ведут дневники</div>
		<div class="info_block-center_body blce">
			<div class="info_block-center_user">
				<?php
				$user1 = get_user_by('id', 19)->data;
				print get_avatar($user1->ID, 128);
				?>
				<span class="info_block-username"><?php print $user1->display_name; ?></span>
				<div class="info_block-center_userinfo">32 года, Хабаровск,<br> строит дом мечты.</div>
			</div>
			<div class="info_block-center_user">
				<?php
				$user2 = get_user_by('id', 3224)->data;
				print get_avatar($user2->ID, 128);
				?>
				<span class="info_block-username"><?php print $user2->display_name; ?></span>
				<div class="info_block-center_userinfo">33 года, Хабаровск,<br> ремонтирует спальню.</div>
			</div>
			<div class="info_block-center_user">
				<?php
				$user3 = get_user_by('id', 3828)->data;
				print get_avatar($user3->ID, 128);
				?>
				<span class="info_block-username"><?php print $user3->display_name; ?></span>
				<div class="info_block-center_userinfo">27 лет, Москва,<br> строит дом.</div>
			</div>
			<div class="info_block-center_user">
				<?php
				$user4 = get_user_by('id', 3)->data;
				print get_avatar($user4->ID, 128);
				?>
				<span class="info_block-username"><?php print $user4->display_name; ?></span>
				<div class="info_block-center_userinfo">32 года, Киев,<br> строит дом мечты.</div>
			</div>
			<div class="info_block-center_user">
				<?php
				$user4 = get_user_by('id', 17)->data;
				print get_avatar($user4->ID, 128);
				?>
				<span class="info_block-username"><?php print $user4->display_name; ?></span>
				<div class="info_block-center_userinfo">22 года, Хабаровск,<br> строит космодром.</div>
			</div>
		</div>
		<a class="info_block-center_link" href="/">Все участники</a>
	</div>
	<div class="main_content_bottom">
		<div class="main_content_bottom-header"><?php if($cup) { print 'добавьте фото'; } else { print 'добавьте первые фото'; } ?><div>прямо сейчас, не откладывайте</div></div>
		<form method="post" id="lform" name="lform" enctype="multipart/form-data">
			<div>
				<div class="lform_block">
					<input type="file" id="limg1" name="limg1">
					<label for="limg1" id="limg1Label"></label>
				</div><div class="lform_block">
					<input type="file" id="limg2" name="limg2">
					<label for="limg2"></label>
				</div><div class="lform_block">
					<input type="file" id="limg3" name="limg3">
					<label for="limg3"></label>
				</div><div class="lform_block">
					<input type="file" id="limg4" name="limg4">
					<label for="limg4"></label>
				</div><div class="lform_block">
					<input type="file" id="limg5" name="limg5">
					<label for="limg5"></label>
				</div>
			</div>
			<div>
				<textarea id="ltext" name="ltext" placeholder="Введите текст с описанием к фото, расскажите историю про ремонт "></textarea><br>
				<?php if(!is_user_logged_in()) { ?>
					<input type="text" id="lemail" name="lemail" placeholder="Ваш e-mail"><br>
					<input type="submit" id="lbtn" name="lbtn" value="Опубликовать вашу первую запись">
				<?php } else { ?>
					<input type="submit" id="lbtn" name="lbtn" value="<?php if($cup) { print 'Опубликовать запись'; } else { print 'Опубликовать вашу первую запись'; } ?>">
				<?php } ?>
			</div>
		</form>
		<input type="text" id="respond" name="respond" value="" style="display:none;" disabled="disabled">
	</div>
</div>
<footer>
	<div class="footer">© COPYRIGHT 2013–2016, obustroeno.com строительный портал</div>
</footer>
</body>
</html>