<?php get_header(); $tssuat_settings = unserialize(get_option('tssuat_settings'));?>

<div id="full_content">
<div id="main_content">
	<div id="main">
		<div id="breadcrumbs">
			<div class="breadcrumbs_block">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			</div>
		</div>

		<div id="article" class="question_page post_page">
			<div class="content_text question_page">
				<div class="title firm-title"><div class="text"><?=the_title()?></div></div>
				<?php echo do_shortcode( '[brands_page]' ); ?>
			</div>
		</div>
		<?php if($tssuat_settings['brands']){
			comments_template();
		} ?>
	</div>

	<?php //get_sidebar(); ?>
	<div class="clear"></div>
</div>
</div>
<?php get_footer(); ?>
