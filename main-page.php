<div id="min_menu">
      <?php wp_nav_menu(array( 'container_class' => 'menu-header')); ?>
      <div class="menu-header">
      <ul>
        <li><a href="/foto">Фотогалерея</a></li>
        <li><a href="/firms">Фирмы</a></li>
      </ul>
      </div>
    </div>

<div class="front_block">
	<div class="front_block-content last_posts">
		<?php do_shortcode( '[last_posts amount="3"]' ); ?>
	</div>
</div>
<?php/*
<div id="access">
      <?php wp_nav_menu(array( 'container_class' => 'menu-header')); ?>
</div> */?>
<?php statistic(); ?>
<div class="questionsFront__title">Вопрос — ответ</div>
<div class="front_forum_container_fr">
	<div class="front_forum_menu__tabs">
		<ul>
			<li class="active">Последние вопросы</li>
			<li>Последние ответы</li>
			<li>Вопросы без ответов</li>
			<li>Самые обсуждаемые</li>
			<?php if(current_user_can('administrator') || current_user_can('expert')) { ?>
				<li>Требуется ответ</li>
			<?php } ?>
		</ul>
	</div>
</div>
<div class="questionsFront">
	<div class="questionsFront__tab visible">
		<?php forum_last_new_reply(); //ПОСЛЕДНИЕ ВОПРОСЫ?>
	</div>
	<div class="questionsFront__tab">
		<?php forum_last_reply(); //ПОСЛЕДНИЕ ОТВЕТЫ?>
	</div>
	<div class="questionsFront__tab">
		<?=test_view_forum(); //ВОПРОСЫ БЕЗ ОТВЕТОВ?>
	</div>
	<div class="questionsFront__tab">
		<?php forum_count_reply(); //САМЫЕ ОБСУЖДАЕМЫЕ?>
	</div>
	<?php if(current_user_can('administrator') || current_user_can('expert')) { ?>
		<div class="questionsFront__tab">
			<?php forum_required_reply(); ?>
		</div>
	<?php } ?>
	<a href="/all-forums" class="question_front_link">Все вопросы</a>
</div>

<div class="frontPosts">
	<div class="frontPosts__tabs">
		<ul>
			<li class="active">Новое</li>
			<li>Обсуждаемое</li>
			<li>Личный опыт</li>
			<li>Профессионалы</li>
			<!-- <li>Вопрос-ответ</li> -->
		</ul>
	</div>
	<div class="frontPosts__list visible">
			<ul class="x_loading">
		<?php front_new_posts(); ?>
			</ul>
	</div>


	<div class="frontPosts__list"><!-- Популярное -->
			<ul class="x_loading">
		<?php front_popular_posts(); ?>
			</ul>
	</div>


	<div class="frontPosts__list"><!--ЛИЧНЫЙ ОПЫТ -->
			<ul class="x_loading">
		<?php front_private_posts(); ?>
			</ul>
	</div>


	<div class="frontPosts__list"><!--Профессионалы -->
			<ul class="x_loading">
		<?php front_prof_posts(); ?>
			</ul>
	</div>
</div>





































