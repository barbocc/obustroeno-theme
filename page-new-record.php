<?php get_header(); ?>
<div id="full_content">
	<div id="main_content" class="post_page single_posts single_honors">
		<div id="main">
			<div id="article">
				<h1><?=the_title()?></h1>
				<?=do_shortcode('[addPost]')?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<?php get_footer(); ?>