<!--noindex-->
<?php
	global $wp;
?>
<?php if($wp->request==='forums'): ?>
<div class="stat_footer">
	<div id="blue_stat_line">
		<div class="footer_block">
			Кто сейчас на форуме?
		</div>
	</div>
	<div id="grey_stat_line">
		<div class="footer_block">
			<?php if (function_exists('users_online')): ?>
				    <p>
					    <div id="useronline-count">
					    <?php users_online(); ?>
					    </div>
				    </p>
	    		<?php endif; ?>
				<?php if (function_exists('get_users_browsing_site')): ?>
				 <!--<div id="useronline-browsing-site">
				<?php echo get_users_browsing_site(); ?>	
				 </div>-->
			<?php endif; ?>
		</div>
	</div>
	<div id="white_stat_line">
		<div class="footer_block">
			Статистика
		</div>
	</div>
	<div id="white_block_blue_boders">
		<div class="footer_block">
			<?php statistic_model(); ?>
		</div>
	</div>
</div>

<?php endif;?>

<footer id="footer">
	<div id="footer_wrapper">
	<div id="footer_block">
	<div class="row row2">
				<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
						<div class="socials_body social_body_footer">
							<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="facebook,twitter,vkontakte,odnoklassniki" data-yashareTheme="counter"></div>
							<!-- <a href="http://youtube.com"><div class="firm_social youtube"></div></a> -->
							<!-- <a href="http://instagram.com"><div class="firm_social instagram"></div></a> -->
						</div>
		</div>
		<div class="row row1">
			<div class="col col1">
				<div class="col_title">КОМПАНИЯ</div>
				<div class="link"><a rel="nofollow" href="/about">Об Обустроено</a></div>
				<div class="link"><a rel="nofollow" href="/contacts">Контакты</a></div>
				<!--div class="link"><a rel="nofollow" href="/">О нас пишут</a></div>
				<div class="link"><a rel="nofollow" href="/">Новости</a></div>
				<div class="link"><a rel="nofollow" href="/">Вакансии</a></div>
				
				<div class="link"><a rel="nofollow" href="/">Правила и Безопасность</a></div>
				<div class="link"><a rel="nofollow" href="/">Авторское право и товарный знак</a></div-->
<br><br><br><br>
			</div>
			<div class="col col2">
				<!--div class="col_title">Сайт</div>
				<div class="link"><a rel="nofollow" href="/">Виджеты</a></div>
				<div class="link"><a rel="nofollow" href="/">Мобильные приложения</a></div>
				<div class="link"><a rel="nofollow" href="/">Экспертам</a></div> 
				
				<div class="link"><a rel="nofollow" href="/">FAQ</a></div-->
				<div class="link"><a rel="nofollow" href="/feedback">Оставить отзыв</a></div>
				<div class="link"><a rel="nofollow" href="/chat">Чат VK</a></div>
				<div class="link"><a rel="nofollow" href="/calc">Строительные калькуляторы</a></div>
				<div class="link"><a rel="nofollow" href="/tables">Строительные таблицы</a></div>
			</div>
		</div>

		
		<div class="row row3">
			<div class="col col1">
			    <!--noindex-->
				© Copyright 2013–2015 Копирование материалов сайта возможно без предварительного согласования в случае установки активной индексируемой ссылки на наш сайт.<br>
				<!--/noindex-->
			</div>
			<div class="col col2">
			</div>
		</div>
	</div></div>


<!--LiveInternet counter--><script type="text/javascript">document.write("<a href='//www.liveinternet.ru/click' "+"target=_blank><img src='//counter.yadro.ru/hit?t45.7;r"+escape(document.referrer)+((typeof(screen)=="undefined")?"":";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+";"+Math.random()+"' alt='' title='LiveInternet' "+"border='0' width='31' height='31'><\/a>")</script><!--/LiveInternet-->


</footer>
    <!--/noindex-->
<?php wp_footer(); ?>
</body>
</html>